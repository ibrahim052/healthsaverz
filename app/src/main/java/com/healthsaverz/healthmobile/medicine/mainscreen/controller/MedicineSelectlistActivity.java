package com.healthsaverz.healthmobile.medicine.mainscreen.controller;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.medicineSearch.modal.MedicineSearchResponse;

import java.util.ArrayList;

public class MedicineSelectlistActivity extends Activity {

    private ListView medicineNameList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_selectlist);
        getActionBar().setTitle("Medicine List");
        medicineNameList = (ListView)findViewById(R.id.medicineNameList);
        ArrayList<String> strings = getIntent().getStringArrayListExtra("medList");
        medicineNameList.setAdapter(new MedicineNameListAdapter(strings, this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_medicine_selectlist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (item.getItemId()){
            case R.id.action_save:
                break;
            case R.id.action_cancel:
                finish();
                break;
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    class MedicineNameListAdapter extends BaseAdapter
    {
        private ArrayList<String> medicineNameList;
        private Context context;
        LayoutInflater layoutInflater;


        public MedicineNameListAdapter(ArrayList<String> medicineNameList, Context context) {
        this.medicineNameList = medicineNameList;
        this.context = context;
        this.layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

        @Override
        public int getCount() {
        return medicineNameList.size();
    }

        @Override
        public Object getItem(int i) {
        return medicineNameList.get(i);
    }

        @Override
        public long getItemId(int i) {
        return i;
    }

        @Override
        public View getView(int i, View productView, ViewGroup viewGroup) {

        if (productView == null){
            productView = layoutInflater.inflate(R.layout.cell_med_name, null);
            ViewHolder holder = new ViewHolder();
            holder.ManfactureName= (TextView)productView.findViewById(R.id.medi_title);
            holder.toggleName = (Switch)findViewById(R.id.medi_switch);
            productView.setTag(holder);
        }
//brand name, strength name, strenght unit, strngth unit name, packaging size name, packaging unit name, packaging name
        ViewHolder holder = (ViewHolder)productView.getTag();
        String medicineName = medicineNameList.get(i);
        holder.ManfactureName.setText(medicineName);
        return productView;
    }

        private class ViewHolder {
            public TextView ManfactureName;
            public TextView toggleName;
            //public TextView tabletQuantity;

        }
    }

    @Override
    public void onBackPressed() {

    }
}
