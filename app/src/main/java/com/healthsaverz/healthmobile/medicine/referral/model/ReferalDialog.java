package com.healthsaverz.healthmobile.medicine.referral.model;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.doctor.modal.Doctor;
import com.healthsaverz.healthmobile.medicine.doctor.view.DoctorParser;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.DialogProgressFragment;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.SmsParser;

/**
 * Created by priyanka kale on 3/17/2015.
 */
public class ReferalDialog extends DialogFragment implements View.OnTouchListener, SmsParser.SmsParserListener, DoctorParser.DoctorDetailParserListener {

    private static final java.lang.String TAG ="ReferalDialog" ;
    private String mobileNo;
    private int screenWidth;

    private SmsParser smsParser;
    private String text;
    private String pinCode;
    private static final String METHOD_ADD_FRIENDDETAIL = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/MappUserDoctorReferenceServices/addMappUserDoctorReference";
    public static boolean flagIsEmailSent = true;
    private DialogProgressFragment progressFragment;

    public static ReferalDialog newInstance(String type, String text) {
        ReferalDialog refillDialog = new ReferalDialog();
        refillDialog.text = text;
        return refillDialog;
    }

    private TextView headText;

    public TextView getHeadText() {
        if (headText == null) {
            headText = (TextView) getView().findViewById(R.id.headText);

        }
        return headText;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private EditText edtMobileText, edtName;

    public EditText getMobile() {
        if (edtMobileText == null) {
            edtMobileText = (EditText) getView().findViewById(R.id.edtMobile);
            if (edtMobileText != null) {
                edtMobileText.setText("");
                edtMobileText.append(text);
            }
        }
        return edtMobileText;
    }

    private String docTorName, docNum, docFullAddress;

    public EditText getName() {
        if (edtName == null) {
            edtName = (EditText) getView().findViewById(R.id.edtName);
            if (edtName != null) {
                edtName.setText("");
                edtName.append(text);
            }
        }
        return edtName;
    }

    private Button setOkButton;

    public Button getOKButton() {
        if (setOkButton == null) {
            setOkButton = (Button) getView().findViewById(R.id.btnOk);
            setOkButton.setOnTouchListener(this);
        }
        return setOkButton;
    }

    public DoctorParser doctorParser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_friend_detail, container, false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return dialog;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int screenHeight;
        if (Build.VERSION.SDK_INT >= 11) {
            Point size = new Point();
            try {
                getActivity().getWindowManager().getDefaultDisplay().getSize(size);
                screenWidth = size.x;
                screenHeight = size.y;
            } catch (NoSuchMethodError e) {
                screenHeight = getActivity().getWindowManager().getDefaultDisplay().getHeight();
                screenWidth = getActivity().getWindowManager().getDefaultDisplay().getWidth();
            }

        } else {
            DisplayMetrics metrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
            screenWidth = metrics.widthPixels;
            screenHeight = metrics.heightPixels;
        }
        RelativeLayout detaillyt = (RelativeLayout) view.findViewById(R.id.detaillyt);
        detaillyt.setMinimumWidth(screenWidth - 150);

        getOKButton();
        getName();
        getMobile();
        getHeadText();
        getOKButton().setOnTouchListener(this);

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(view, null);
            return true;
        } else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
//            final int viewID = view.getId();
            ViewHelper.fadeIn(view, null);

            switch (view.getId()) {

                case R.id.btnOk:

                    /**
                     * to get Doctor Detail service is Called
                     */
                    if ((edtMobileText.getText().toString().length() != 0) && (edtName.getText().toString().length() != 0)) {
                        progressFragment =  new DialogProgressFragment();
                        progressFragment.show(getActivity().getFragmentManager(),TAG);
                        progressFragment.setCancelable(false);
                        
                        doctorParser = new DoctorParser(getActivity());
                        doctorParser.setProductMenuParserListener(this);
                        doctorParser.getDoctorDetailsFromSever();
                    } else {

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Alert");
                        alertDialog.setMessage("Please Fill Details");
                        alertDialog.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Write your code here to execute after dialog
                                        dialog.cancel();
                                    }
                                });
                        // Showing Alert Dialog
                        alertDialog.show();

                    }
                    return true;
            }
//
        }
        return false;
    }

    public SmsParser getSmsParser() {
//        Log.d("SMS Parser  ",""+"getSmsParser");
        if (smsParser != null) {
            smsParser.cancel(true);
            smsParser = null;
        }
        smsParser = new SmsParser(getActivity());
        smsParser.setSmsParserListener(this);
        return smsParser;
    }

    @Override
    public void smsParserDidReceiveData(String message) {
        Log.d("SMS RESPONSE", ">>>>>>>>>>" + message);
        if (message.equals("1701") && flagIsEmailSent) {
            flagIsEmailSent = false;//restrict recursive calling
            /*
                send friend details to server
             */
            AsyncLoaderNew asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.POST_HEADER);
            String emailAddress = "N.A";
            mobileNo = edtMobileText.getText().toString().trim();
            String status = "A";
            int created_By = Integer.parseInt(PreferenceManager.getStringForKey(getActivity(), PreferenceManager.ID, ""));

            String requestXML = "<mappUserDoctorReference>" +
                    "<clientID>" + "healthsaverz" + "</clientID>" +
                    "<sessionID>" + PreferenceManager.getStringForKey(getActivity(), PreferenceManager.SESSION_ID, "") + "</sessionID>" +
                    "<doctorId>" + PreferenceManager.getStringForKey(getActivity(), PreferenceManager.DOCTORID, "") + "</doctorId>" +
                    "<referFrom>" + PreferenceManager.getStringForKey(getActivity(), PreferenceManager.USERNAME, "") + "</referFrom>" +
                    "<referTo>" + edtName.getText().toString().trim() + "</referTo>" +
                    "<emailAddress>" + emailAddress + "</emailAddress>" +
                    "<pincode>" + "" + "</pincode>" +
                    "<status>" + "A" + "</status>" +
                    "<mobileNo>" + mobileNo + "</mobileNo>" +
                    "<created_By>" + created_By + "</created_By>" +
                    "<extra_1>" + "" + "</extra_1>" +
                    "<extra_2>" + "" + "</extra_2>" +
                    "<extra_3>" + "" + "</extra_3>" +
                    "<extra_4>" + "" + "</extra_4>" +
                    "<extra_5>" + "" + "</extra_5>" +
                    "</mappUserDoctorReference>";

            Log.d("SUBMIT requestXML ", "requestXML " + requestXML);
            /*
              Send sms to friend, doctor details
             */
            Log.d("SMS-SUCCESS ", "" + "SEND SMS TO FRIEND ");

//            if (docTorName.length() == 0) {
//                docTorName = "Archana Girish Sabnis";
//            }
//            if(docFullAddress.length() == 0){
//                docFullAddress = " SABNISH HOSPITAL SAURABH BUILDING 1 00 90 FEET D P ROAD BHARAT CO OPERATIVE BANK 90 FEET ROAD MULUND EAST";
//            }
                Log.d("SERVER HIT THREE", "getSmsToReferrals SMS");
                Log.d("SERVER HIT FOUR", "send friend details to server");

                getSmsParser().getSmsToReferrals(edtMobileText.getText().toString(), docTorName, docFullAddress, docNum);

                asyncLoaderNew.requestMessage = requestXML;
                asyncLoaderNew.executeRequest(METHOD_ADD_FRIENDDETAIL);
                dismiss();


        } else {
            Log.d("","else-----------");
        }
    }

    @Override
    public void smsParserDidReceivedConnectionError(AsyncLoaderNew.Status status) {

    }

    @Override
    public void smsParserDidReceivedProcessingError(String message) {

    }

    @Override
    public void doctorDetailParserDidReceivedMenu(Doctor docdetails) {
        if(progressFragment!=null) {
            progressFragment.dismiss();
            progressFragment = null;
        }
       docNum = docdetails.cellNumber;
        if (docNum != null) {
            if (docNum.length() == 10) {
                docNum = "91" + docNum;
            }
        }
        String frndNumber = edtMobileText.getText().toString().trim();
        if (frndNumber.length() == 10) {
            frndNumber = "91" + frndNumber;
        }
        docFullAddress = docdetails.fullAddress;
        docTorName = docdetails.doctorName;
        /*
            Send sms to doctor
         */
        String referTo = edtName.getText().toString().trim();
        String referedBy = PreferenceManager.getStringForKey(getActivity(), PreferenceManager.EMAIL_ADDRESS, "");
        String cellNumber = PreferenceManager.getStringForKey(getActivity(), PreferenceManager.CELL_NUMBER, "");

        Log.d("SERVER HIT TWO", "getSmsToDoctor SMS");
        if(docNum.length()!=0) {
            getSmsParser().getSmsToDoctor(docNum, referedBy, cellNumber, referTo, edtName.getText().toString().trim(), frndNumber);
        }
    }

    @Override
    public void doctorDetailDidReceivedConnectionError(AsyncLoaderNew.Status status) {
        if(progressFragment!=null) {
            progressFragment.dismiss();
            progressFragment = null;
        }
    }

    @Override
    public void doctorDetailDidReceivedProcessingError(String message) {
        if(progressFragment!=null) {
            progressFragment.dismiss();
            progressFragment = null;
        }

    }
}
