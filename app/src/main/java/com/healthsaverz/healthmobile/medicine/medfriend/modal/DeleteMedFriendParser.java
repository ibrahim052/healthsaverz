package com.healthsaverz.healthmobile.medicine.medfriend.modal;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;

/**
 * Created by ABCD on 2/5/2015.
 */
public class DeleteMedFriendParser extends AsyncTask<String,Void,Object> implements AsyncLoaderNew.Listener {
     /*
    Start from loginUser method
    Takes String as input and out is based on VerifyUserParserListener
     */

    private Context context;

    public DeleteMedFriendParser(Context context){

        this.context = context;
    }

    public interface DeleteFriendParserListener{

        void deleteFriendParserDidReceiveData(Object message); //message as 100 for success

    }

    private DeleteFriendParserListener deleteFriendParserListener;

    public void setMedFriendParserListener(DeleteFriendParserListener deleteFriendParserListener) {
        this.deleteFriendParserListener = deleteFriendParserListener;
    }

    private static final String METHOD_DELETE_MED_FRIEND = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/MedFriendServices/deleteMedFriend/";

    //local method
    public void deleteMedFriend(MedFriend medFriend){

        AsyncLoaderNew asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.GET);

        String clientID = PreferenceManager.getStringForKey(context, PreferenceManager.CLIENT_ID, "healthsaverz");
        String sessionID = PreferenceManager.getStringForKey(context, PreferenceManager.SESSION_ID, "-1");


        String requestXML = METHOD_DELETE_MED_FRIEND + sessionID +"/"+ clientID +"/"+ medFriend._medFriendID;

        Log.d("requestXML ", "requestXML " + requestXML);

        //asyncLoaderNew.requestMessage   =   requestXML;
        asyncLoaderNew.setListener(this);
        asyncLoaderNew.executeRequest(requestXML);


    }


    @Override
    protected Object doInBackground(String... params) {
//        if(!isCancelled())
//        {
//            try {
//            /*
//            {"sessionID":"","clientID":"","message":"100","gender":"Male",
//            "deviceUdid":"353743054004708","deviceToken":"DEVICE_TOKEN","osType":"A",
//            "verificationCode":"7031","verificationFlag":"","fileDetailList":null,"state":"",
//            "country":"","id":361,"password":"","userName":"priyank@nimapinfotech.com","status":"I",
//            "firstName":"","lastName":"","address1":"","address2":"","postalCode":"","emailAddress":"priyank@nimapinfotech.com",
//            "subUserId":0,"userGroupId":3,"phoneNumber":"","cellNumber":"+919869357889","created_By":0,"city":""}
//             */
//                JSONObject jsonObject = new JSONObject(params[0]);
//                String k = jsonObject.getString("user_id");
//                MedFriend medFriend;
//                if(jsonObject.getString("message").equals("100") || jsonObject.getString("message").equals("you are activated valid user")){
//                    // indicates success in registering the user. We need to add to persistent storage so that it can be used later on when ever needed.
//                    medFriend = new MedFriend();
//                    medFriend.sessionID = jsonObject.getString("sessionID");
//                    medFriend.clientID = jsonObject.getString("clientID");
//                    medFriend._medFriendID = jsonObject.getInt("med_friend_id");
//                    medFriend._userID = jsonObject.getInt("user_id");
//                    medFriend._friendName = jsonObject.getString("med_friend_name");
//                    medFriend._friendEmail = jsonObject.getString("med_friend_email");
//                    medFriend._friendMobile = jsonObject.getString("med_friend_mobile");
//                    medFriend._missCount = jsonObject.getInt("med_miss_count");
//                    medFriend._skipCount = jsonObject.getInt("med_skip_count");
//                    medFriend.extra_1 = jsonObject.getString("extra_1");
//                    medFriend.extra_2 = jsonObject.getString("extra_2");
//                    medFriend.extra_3 = jsonObject.getString("extra_3");
//                    medFriend.extra_4 = jsonObject.getString("extra_4");
//                    medFriend.extra_5 = jsonObject.getString("extra_5");
//
//                    return medFriend;
//                }
//                else
//                {
//                    return null;
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//        else {
//            cancel(true);
//        }
        return null;
    }

    @Override
    protected void onPostExecute(Object s) {
        super.onPostExecute(s);

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();

        if (this.deleteFriendParserListener != null) {
            this.deleteFriendParserListener.deleteFriendParserDidReceiveData("Process Abrupt");
        }
    }


    //AsyncLoaderNew.Listener method implementation
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {
        if(deleteFriendParserListener != null){
            deleteFriendParserListener.deleteFriendParserDidReceiveData(status);
        }
    }

    @Override
    public void didReceivedData(byte[] data) {
        String response = new String(data);
        Log.d("LoginUserParser", "didReceivedData is " + response);
        if(this.deleteFriendParserListener != null){
            switch (response) {
                case "100":
                    deleteFriendParserListener.deleteFriendParserDidReceiveData(response);
                    break;
                case "101":
                    deleteFriendParserListener.deleteFriendParserDidReceiveData("Error occurred while updating MedFriend");
                    break;
                default:
                    deleteFriendParserListener.deleteFriendParserDidReceiveData("Connection Error");
                    break;
            }
        }
        //execute(responseString);
    }

    @Override
    public void didReceivedProgress(Double progress) {

    }
}
