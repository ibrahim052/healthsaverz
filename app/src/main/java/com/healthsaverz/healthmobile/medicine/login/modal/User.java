package com.healthsaverz.healthmobile.medicine.login.modal;


import android.content.Context;

/**
 * Created by DG on 10/9/2014.
 */
public class User {
    public static final char STATE_ACTIVE = 'A';
    public static final char STATE_INACTIVE = 'I';

    public int id;
//    public String userName ="";
    public String clientID = "healthsaverz";
    public String sessionID = "test";

    public String userName;
   public String password ="dinesh123";
    public String firstName = "";
    public String lastName = "";
    public String gender = "Male";
    public String postalCode="" ;
    public String cellNumber="";
    public String emailAddress ="";
    public String state = "";
    public String country = "";

    public int phoneNumber ;


    public String extra_1 ="";
    public String extra_2 ="";
    public String extra_3 ="";
    public String extra_4 ="";
    public String extra_5 ="";
    public char status = STATE_ACTIVE;
    public int created_By = 0;
    public int userGroupId= 3;
    public String email = "";


    public String deviceToken="this is device token";
    public String deviceUdid ="this is device udid";
    public String osType="A";

//    private double latitude = 0.0f;
//    private double longitude = 0.0f;
//
//    private String verificationCode = "";
//    private String verificationCodeExpiryFlag = "";
//    private String userProfileImage = "NA";
//    private String deviceId = "";
//    private String deviceToken = "";



    public User(Context context, String emailAddress, String firstName, String lastName, String postalCode, String cellNumber) {

        this.userName = (new StringBuilder().append(firstName.toString()).append(lastName).toString());
        this.emailAddress = emailAddress;
        this.firstName = firstName;
        this.lastName = lastName;
        this.postalCode = postalCode;
        this.cellNumber = cellNumber;
    }


    public String toAddUserXml() {

        return "<user>" +
                "<clientID>"+clientID+"</clientID>" +
                "<sessionID>"+sessionID+"</sessionID>" +
                "<userName>"+userName+"</userName>" +
                "<password>"+password+"</password>" +
                "<firstName>"+firstName+"</firstName>" +
                "<lastName>"+lastName+"</lastName>" +
                "<gender>"+gender+"</gender>" +
                "<postalCode>"+postalCode+"</postalCode>" +
                "<userGroupId>"+userGroupId+"</userGroupId>" +
                "<cellNumber>"+cellNumber+"</cellNumber>" +
                "<emailAddress>"+emailAddress+"</emailAddress>" +
                "<deviceToken>"+deviceToken+"</deviceToken>" +
                "<deviceUdid>"+deviceUdid+"</deviceUdid>" +
                "<osType>"+osType+"</osType>" +
                "</user>";
    }
}