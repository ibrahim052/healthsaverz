package com.healthsaverz.healthmobile.medicine.help.controller;

import android.app.Activity;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.bookaservice.controller.BookAServiceActivity;
import com.healthsaverz.healthmobile.medicine.bookaservice.controller.DealsActivity;
import com.healthsaverz.healthmobile.medicine.doctor.controller.SignaturePaintActivity;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
import com.healthsaverz.healthmobile.medicine.help.modal.HelpWalkthroughDialog;
import com.healthsaverz.healthmobile.medicine.register.controller.GoogleLoginActivity;

import org.json.JSONObject;

import java.util.Arrays;

public class HelpActivity extends FragmentActivity implements View.OnTouchListener {

    private static final String TAG = "HelpActivity";
    private LoginButton loginButton;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        TextView walkthrough = (TextView) findViewById(R.id.walkthrough);
        TextView foundABug = (TextView) findViewById(R.id.foundABug);
        TextView featureRequest = (TextView) findViewById(R.id.featureRequest);
        TextView talkToUs = (TextView) findViewById(R.id.talkToUs);
        TextView faqs = (TextView) findViewById(R.id.faqs);
        TextView bookAService = (TextView) findViewById(R.id.bookAService);
        TextView doctorPrescription = (TextView) findViewById(R.id.doctorPrescription);
        TextView alarmRingtone = (TextView) findViewById(R.id.alarmRingtone);
        TextView googleSignIn = (TextView) findViewById(R.id.googleSignIn);
        TextView tabDeals = (TextView) findViewById(R.id.tabDeals);
        walkthrough.setOnTouchListener(this);
        foundABug.setOnTouchListener(this);
        featureRequest.setOnTouchListener(this);
        talkToUs.setOnTouchListener(this);
        faqs.setOnTouchListener(this);
        bookAService.setOnTouchListener(this);
        doctorPrescription.setOnTouchListener(this);
        alarmRingtone.setOnTouchListener(this);
        googleSignIn.setOnTouchListener(this);
        tabDeals.setOnTouchListener(this);
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("user_friends");
        //loginButton.setReadPermissions(Arrays.asList("first_name, last_name, email"));

        // If using in a fragment
        //loginButton.setFragment(this);
        // Other app specific specialization

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                // Application code
                                Log.v("-------FB_LoginActivity------", response.toString());
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "first_name, last_name, email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                Log.v("--------FBLoginActivity", "cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                //Log.e("--------FBLoginActivity", exception.getCause().toString());
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_help, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(view, null);
            return true;
        }
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
            ViewHelper.fadeIn(view, null);
            Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", AppConstants.email_us, null));
            intent.putExtra(Intent.EXTRA_SUBJECT, "APP FEEDBACK -");
            switch (view.getId()) {
//                case R.id.appWork:
//                    return true;
//                case R.id.iconsColors:
//                    return true;
//                case R.id.addingReminder:
//                    return true;
//                case R.id.deacCertWeek:
//                    return true;
                case R.id.walkthrough:
                    //HelpWalkthroughDialog.newInstance(this).show(getSupportFragmentManager(), TAG);
                    startActivity(new Intent(this, WalkthroughActivity.class));
                    return true;
                case R.id.foundABug:
                    startActivity(Intent.createChooser(intent, "Found a Bug!"));
                    return true;
                case R.id.featureRequest:
                    startActivity(Intent.createChooser(intent, "Feature request"));
                    return true;
                case R.id.faqs:
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.f_a_q));
                    startActivity(browserIntent);
                    return true;
                case R.id.talkToUs:
                    String uri = "tel:" + AppConstants.call_us.trim() ;
                    Intent intent1 = new Intent(Intent.ACTION_DIAL);
                    intent1.setData(Uri.parse(uri));
                    startActivity(intent1);
                    return true;
                case R.id.bookAService:
                    startActivity(new Intent(this, BookAServiceActivity.class));
                    return true;
                case R.id.doctorPrescription:
                    startActivity(new Intent(this, SignaturePaintActivity.class));
                    return true;
                case R.id.alarmRingtone:
                    //startActivityForResult(new Intent(android.provider.Settings.ACTION_SOUND_SETTINGS), 0);
                    Intent intentSound = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                    intentSound.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
                    intentSound.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone");
                    intentSound.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, (Uri) null);
                    startActivityForResult(intentSound, 5);
                    return true;
                case R.id.googleSignIn:
                    Intent intent2 = new Intent(this, GoogleLoginActivity.class);
                    startActivity(intent2);
                    return true;
                case R.id.tabDeals:
                    startActivity(new Intent(this, DealsActivity.class));
                    return true;
            }
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == 5)
        {
//            Uri uri = intent.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
//
//            if (uri != null)
//            {
//                this.chosenRingtone = uri.toString();
//            }
//            else
//            {
//                this.chosenRingtone = null;
//            }
        }
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
