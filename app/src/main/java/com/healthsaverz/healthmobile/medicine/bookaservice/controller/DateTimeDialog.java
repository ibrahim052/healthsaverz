package com.healthsaverz.healthmobile.medicine.bookaservice.controller;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TimePicker;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by healthsaverz5 on 3/24/2015.
 * @author Priyanka Kale
 */
public class DateTimeDialog extends DialogFragment implements View.OnTouchListener, View.OnClickListener {


    TimeSetlistener timeSetlistener;

    private int mDay;
    private int mMonth;
    private int mYear;
    Calendar c = Calendar.getInstance();
    private Button setDateBtn, cancelDateBtn;
    private Calendar calendar;

    public static DateTimeDialog newInstance(TimeSetlistener timeSetlistener) {
        DateTimeDialog dateTimeDialog = new DateTimeDialog();
        dateTimeDialog.timeSetlistener = timeSetlistener;
        return dateTimeDialog;
    }

    public DateTimeDialog() {
    }


    private ImageButton imgBtnCalDate;

    private ImageButton getCalDateButton() {
        if (imgBtnCalDate == null) {
            imgBtnCalDate = (ImageButton) getView().findViewById(R.id.imgBtnCalenderDate);
        }
        return imgBtnCalDate;
    }


    private TimePicker timepicker;

    public TimePicker getTimepicker() {
        if (timepicker == null) {
            timepicker = (TimePicker) getView().findViewById(R.id.timepicker);

        }
        return timepicker;
    }

    private EditText edTextCalDate;

    public EditText getSelectedDate() {
        if (edTextCalDate == null) {
            edTextCalDate = (EditText) getView().findViewById(R.id.edTextCalDate);

        }
        return edTextCalDate;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBtnCalenderDate:
                // Launch Date Picker Dialog
                DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                edTextCalDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                Log.d("DATE SELECTED", "------------" + edTextCalDate.getText());
                                mYear = year;
                                mMonth = (monthOfYear + 1);
                                mDay = dayOfMonth;
                                calendar = new GregorianCalendar();
                                calendar.set(mYear, mMonth, mDay);


                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
                break;

            case R.id.cancelButton:
                getDialog().dismiss();
                break;

            default:
                break;
        }

    }

    public interface TimeSetlistener {
        void onTimeChange(Calendar cal, Integer currentHour, Integer currentMinute);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return inflater.inflate(R.layout.dialog_datetime, container, false);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        int screenHeight;
        int screenWidth;
        if (Build.VERSION.SDK_INT >= 11) {
            Point size = new Point();
            try {
                getActivity().getWindowManager().getDefaultDisplay().getSize(size);
                screenWidth = size.x;
                screenHeight = size.y;
            } catch (NoSuchMethodError e) {
                screenHeight = getActivity().getWindowManager().getDefaultDisplay().getHeight();
                screenWidth = getActivity().getWindowManager().getDefaultDisplay().getWidth();
            }

        } else {
            DisplayMetrics metrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
            screenWidth = metrics.widthPixels;
            screenHeight = metrics.heightPixels;
        }
        RelativeLayout dialogLyt = (RelativeLayout) view.findViewById(R.id.dialoglyt);
        dialogLyt.setMinimumWidth(screenWidth - 350);
        dialogLyt.setMinimumHeight(screenHeight - 350);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        getTimepicker();
        getSelectedDate();
        timepicker.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);

        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        // c.add(Calendar.DAY_OF_MONTH, 1); // <--
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);
        String tomorrowDate = new SimpleDateFormat("dd-MM-yyyy").format(c.getTime());
        Log.d("TOMORROW DATE    ", "" + tomorrowDate);
        edTextCalDate.setText(tomorrowDate);
        timepicker.setIs24HourView(false);
        if (c == null) {
            c = Calendar.getInstance();
            timepicker.setCurrentHour(mHour);
            timepicker.setCurrentMinute(mMinute);


        } else {
            c.add(Calendar.HOUR_OF_DAY, 3); // <--
            timepicker.setCurrentHour(c.get(Calendar.HOUR_OF_DAY));
            timepicker.setCurrentMinute(c.get(Calendar.MINUTE));
        }

        getCalDateButton();
        getCalDateButton().setOnClickListener(this);
        getSetDateTime();
        cancelDateBtn = (Button) view.findViewById(R.id.cancelButton);
        cancelDateBtn.setOnClickListener(this);
    }

    private Button setDateTimeBtn;

    private Button getSetDateTime() {
        setDateTimeBtn = (Button) getView().findViewById(R.id.setButton);
        setDateTimeBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    ViewHelper.fadeOut(view, null);
                    return true;
                } else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                    ViewHelper.fadeIn(view, null);

                    Calendar calendar = new GregorianCalendar();
                    calendar.set(Calendar.MILLISECOND, 0);
                    if (timeSetlistener != null) {

                        /*Calendar calendar1 = new GregorianCalendar();
                        calendar1.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                                calendar.get(Calendar.DAY_OF_MONTH),timepicker.getCurrentHour(), timepicker.getCurrentMinute(), 0);
                        calendar1.set(Calendar.MILLISECOND, 0);*/

                        timeSetlistener.onTimeChange(calendar, timepicker.getCurrentHour(), timepicker.getCurrentMinute());
                    }
                    dismiss();
                    return true;


                }
                return false;
            }
        });
        return setDateBtn;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return dialog;
    }
}
