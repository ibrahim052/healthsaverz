package com.healthsaverz.healthmobile.medicine.mainscreen.modal;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by DELL PC on 2/19/2015.
 */
public class UserMedicineNameParser extends AsyncTask<String,Void,ArrayList<String>> implements AsyncLoaderNew.Listener {
     /*
    Start from userMedicineName method
    Takes String as input and out is based on VerifyUserParserListener
     */

    private Context context;

    public UserMedicineNameParser(Context context){

        this.context = context;
    }

    public interface UserMedicineNameParserListener{

        void userMedicineNameParserDidRecieveData(ArrayList<String> message); //message as 100 for success
        void userMedicineNameParserDidReceivedConnectionError(AsyncLoaderNew.Status status);// AsyncLoaderNew.Status
        void userMedicineNameParserDidReceivedProcessingError(String message);//server message for unsuccessful
    }

    private UserMedicineNameParserListener userMedicineNameListener;

    public UserMedicineNameParserListener getUserMedicineNameListener() {
        return userMedicineNameListener;
    }

    public void setUserMedicineNameListener(UserMedicineNameParserListener userMedicineNameListener) {
        this.userMedicineNameListener = userMedicineNameListener;
    }

    private static final String METHOD_GET_MEDICINE_NAMES = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/MedicineInfoServices/getUserMedicineNames/";

    //local method
    public void userMedicineName(){
        AsyncLoaderNew asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.GET);
        String userClientID = PreferenceManager.getStringForKey(context, PreferenceManager.CLIENT_ID, "-1");
        String userID = PreferenceManager.getStringForKey(context, PreferenceManager.ID, "-1");
        String sessionID = PreferenceManager.getStringForKey(context, PreferenceManager.SESSION_ID, "-1");
        if(userClientID.equals("") || userClientID.equals("-1")){
            userClientID = "healthsaverz";
        }
        asyncLoaderNew.setListener(this);
        asyncLoaderNew.executeRequest(METHOD_GET_MEDICINE_NAMES + userClientID + "/" + sessionID + "/UserId/" + userID);

    }


    @Override
    protected ArrayList<String> doInBackground(String... params) {
        ArrayList<String> strings;
        if(!isCancelled())
        {
            try {
                strings = new ArrayList<>();
                JSONArray jsonArray = new JSONArray(params[0]);
                for (int i=0; i<jsonArray.length();i++){
                    strings.add(jsonArray.getString(i));
                }
                //strings = jsonArray.getString();
                return strings;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            cancel(true);
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<String> s) {
        super.onPostExecute(s);
        if(this.userMedicineNameListener != null){
            if(s != null){
                userMedicineNameListener.userMedicineNameParserDidRecieveData(s);
            }
            else
            {
                userMedicineNameListener.userMedicineNameParserDidReceivedProcessingError("Error in fetching Medicines");
            }
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();

        if (this.userMedicineNameListener != null) {
            this.userMedicineNameListener.userMedicineNameParserDidReceivedProcessingError("Verification Abrupted");
        }
    }


    //AsyncLoaderNew.Listener method implementation
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {
        if(userMedicineNameListener != null){
            userMedicineNameListener.userMedicineNameParserDidReceivedConnectionError(status);
        }
    }

    @Override
    public void didReceivedData(byte[] data) {
        String responseString = new String(data);
        Log.d("UserMedicineNameParser", "didReceivedData is " + responseString);
        execute(responseString);
    }

    @Override
    public void didReceivedProgress(Double progress) {

    }
}
