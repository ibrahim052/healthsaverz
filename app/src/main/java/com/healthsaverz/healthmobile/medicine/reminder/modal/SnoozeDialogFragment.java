package com.healthsaverz.healthmobile.medicine.reminder.modal;



import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;

/**
 * A simple {@link android.app.Fragment} subclass.
 *
 */
public class SnoozeDialogFragment extends DialogFragment {

    public static final String TAG = "SnoozeDialogFragment";
    public Integer[] snoozetimes = {10,15,30};

    private Context context;

    public SnoozeDialogFragment() {
        // Required empty public constructor
    }

    public static SnoozeDialogFragment newInstance(SnoozeDialogListener listener){

        SnoozeDialogFragment snoozeDialogFragment = new SnoozeDialogFragment();
        snoozeDialogFragment.setDialogListener(listener);
        return snoozeDialogFragment;
    }

    public interface SnoozeDialogListener{

        void didSelectSnoozeTime(int snoozeTime);
    }

    private SnoozeDialogListener dialogListener = null;

    public SnoozeDialogListener getDialogListener() {
        return dialogListener;
    }

    public void setDialogListener(SnoozeDialogListener dialogListener) {
        this.dialogListener = dialogListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_snooze_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ListView listView = (ListView) view.findViewById(R.id.daysList);
        listView.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return snoozetimes.length;
            }

            @Override
            public Object getItem(int position) {
                return snoozetimes[position];
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                LayoutInflater inflater = LayoutInflater.from(getActivity());

                MyViewHolder mViewHolder;
                if(convertView == null) {
                    convertView = inflater.inflate(R.layout.day_cell, null);
                    mViewHolder = new MyViewHolder();
                    convertView.setTag(mViewHolder);
                    mViewHolder.daysText = (TextView) convertView.findViewById(R.id.daysText);
                    mViewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.checkboxDays);
                } else {
                    mViewHolder = (MyViewHolder) convertView.getTag();
                }

                mViewHolder.daysText.setText("Snooze for "+snoozetimes[position]+" min");
                mViewHolder.checkBox.setVisibility(View.INVISIBLE);

                return convertView;

            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(dialogListener != null){

                    dialogListener.didSelectSnoozeTime(snoozetimes[position].intValue());
                    dismiss();
                }
            }
        });

        Button cancelButton = (Button) view.findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(dialogListener != null){

                    dialogListener.didSelectSnoozeTime(-1);
                    dismiss();
                }
            }
        });

    }

    private class MyViewHolder {
        TextView daysText;
        CheckBox checkBox;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog =  super.onCreateDialog(savedInstanceState);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        return dialog;
    }
}
