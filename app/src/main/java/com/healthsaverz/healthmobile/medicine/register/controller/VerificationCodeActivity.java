package com.healthsaverz.healthmobile.medicine.register.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.DialogProgressFragment;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;
import com.healthsaverz.healthmobile.medicine.global.UseCases;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
import com.healthsaverz.healthmobile.medicine.login.controller.LoginActivity;
import com.healthsaverz.healthmobile.medicine.login.modal.LoginUserParser;
import com.healthsaverz.healthmobile.medicine.mainscreen.controller.MainScreenActivity;
import com.healthsaverz.healthmobile.medicine.register.model.VerifyUserParser;

public class VerificationCodeActivity extends Activity implements VerifyUserParser.VerifyUserParserListener, LoginUserParser.LoginUserParserListener {

    private static final String TAG = "VerificationCodeActivity";
    public EditText verificationCodeEditText1 = null;
    public EditText verificationCodeEditText2 = null;
    public EditText verificationCodeEditText3 = null;
    public EditText verificationCodeEditText4 = null;
    VerifyUserParser verifyUserParser   =   null;
    LoginUserParser loginUserParser = null;
    DialogProgressFragment progressFragment;
    private String total;
    private TextView verifyTextView;
//    public interface VerificationCodeActivityListener {
//
//        void isVerified(boolean code);//
//    }
//    public static VerificationCodeActivity newInstance(VerificationCodeActivityListener listener){
//
//        VerificationCodeActivity verificationDialogFragment = new VerificationCodeActivity();
//        verificationDialogFragment.setVerificationListener(listener);
//        return verificationDialogFragment;
//    }
//    private VerificationCodeActivityListener verificationListener;
//
//    public VerificationCodeActivityListener getVerificationListener() {
//        return verificationListener;
//    }
//
//    public void setVerificationListener(VerificationCodeActivityListener verificationListener) {
//        this.verificationListener = verificationListener;
//    }

    public VerificationCodeActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_code);
        getActionBar().hide();
//        view.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    // leave this blank in order to disable the back press
//                    return true;
//                } else {
//                    return false;
//                }
//            }
//        });
//        String verifiedCodeReceived = PreferenceManager.getStringForKey(DialogVerificationCodeFragment.this.getActivity(),PreferenceManager.VERIFICATION_CODE,"-1");
//        verificationCodeEditText1.setText("");
//        verificationCodeEditText1.append(verifiedCodeReceived);
        verificationCodeEditText1 = (EditText)findViewById(R.id.verificationCode1);
        verificationCodeEditText2 = (EditText)findViewById(R.id.verificationCode2);
        verificationCodeEditText3 = (EditText)findViewById(R.id.verificationCode3);
        verificationCodeEditText4 = (EditText)findViewById(R.id.verificationCode4);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        verificationCodeEditText1.addTextChangedListener(new GenericTextWatcher(verificationCodeEditText1));
        verificationCodeEditText2.addTextChangedListener(new GenericTextWatcher(verificationCodeEditText2));
        verificationCodeEditText3.addTextChangedListener(new GenericTextWatcher(verificationCodeEditText3));
        verificationCodeEditText4.addTextChangedListener(new GenericTextWatcher(verificationCodeEditText4));
        verifyTextView = (TextView) findViewById(R.id.action_verify);
        ViewHelper.enableDisableView(verifyTextView, false);
        TextView cancelTextView = (TextView) findViewById(R.id.action_cancel);
        verifyTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String verifiedCodeReceived = PreferenceManager.getStringForKey(VerificationCodeActivity.this, PreferenceManager.VERIFICATION_CODE, "-1");

                if(verifiedCodeReceived.equals(total)){
                    //verifiedCodeReceived.equals(verificationCodeEditText1.getText().toString().trim())
                    isVerified(true);
                }
                else {
                    isVerified(false);
                }
            }
        });
        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                UseCases.startActivityByClearingStack(VerificationCodeActivity.this, LoginActivity.class);
            }
        });
    }

    private void isVerified(boolean code) {
        if(code){
            Log.d("isVerified ", " user is verified call server");
            if(verifyUserParser != null)
            {
                verifyUserParser.cancel(true);
                verifyUserParser = null;
            }

            verifyUserParser = new VerifyUserParser(this);
            verifyUserParser.setVerifyUserListener(this);
            verifyUserParser.verifyUser(PreferenceManager.getStringForKey(this,PreferenceManager.VERIFICATION_CODE,"-1"));

            if(progressFragment != null)
            {
                progressFragment = null;
            }

            progressFragment =  new DialogProgressFragment();
            progressFragment.show(getFragmentManager(), TAG);
            progressFragment.setCancelable(false);

        }
        else {
            Log.d("isVerified "," user is not verified");

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Verification Error");
            alertDialog.setMessage("Verification Failed. Invalid Code entered.");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            dialog.cancel();
                        }
                    });
            // Showing Alert Dialog
            alertDialog.show();
        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_verification_code, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //VerifyUserParser.VerifyUserParserListener methods
    @Override
    public void verifyUserParserDidUserVerified(String message) {



        if(message.equals("100")){

            if(loginUserParser != null)
            {
                loginUserParser.cancel(true);
                loginUserParser = null;
            }

            loginUserParser = new LoginUserParser(this);
            loginUserParser.setLoginUserListener(this);
            loginUserParser.loginUser(PreferenceManager.getStringForKey(this,PreferenceManager.USERNAME,""),
                    PreferenceManager.getStringForKey(this,PreferenceManager.PASSWORD,""));
        }
        else
        {
            progressFragment.dismiss();
            progressFragment = null;
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Verification Error");
            alertDialog.setMessage(message);
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            dialog.cancel();
                        }
                    });
            // Showing Alert Dialog
            alertDialog.show();
        }
    }

    @Override
    public void verifyUserParserDidReceivedConnectionError(AsyncLoaderNew.Status status) {

        progressFragment.dismiss();
        progressFragment = null;

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Verification Error");
        alertDialog.setMessage(AppConstants.ConnectionError(status));
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }

    @Override
    public void verifyUserParserDidReceivedProcessingError(String message) {

        progressFragment.dismiss();
        progressFragment = null;

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Verification Error");
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }

    @Override
    public void loginUserParserDidUserLoggedIn(String message) {
        progressFragment.dismiss();
        progressFragment = null;
        if(message.equals("100")){
            PreferenceManager.saveStringForKey(this,PreferenceManager.STATUS,"A");
            UseCases.startActivityByClearingStack(this, MainScreenActivity.class);
            Toast.makeText(this, "User Verified", Toast.LENGTH_SHORT).show();
            finish();
        }
        else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Login Error");
            alertDialog.setMessage(message);
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            dialog.cancel();
                        }
                    });
            // Showing Alert Dialog
            alertDialog.show();
        }
    }

    @Override
    public void loginUserParserDidReceivedConnectionError(AsyncLoaderNew.Status status) {
        progressFragment.dismiss();
        progressFragment = null;

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Login Error");
        alertDialog.setMessage(AppConstants.ConnectionError(status));
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }

    @Override
    public void loginUserParserDidReceivedProcessingError(String message) {
        progressFragment.dismiss();
        progressFragment = null;

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Login Error");
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }


    private class GenericTextWatcher implements TextWatcher{

        private View view;
        private GenericTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        public void afterTextChanged(Editable editable) {
            if (editable.length() > 0) {
                switch (view.getId()) {
                    case R.id.verificationCode1:
                        verificationCodeEditText2.requestFocus();
                        break;
                    case R.id.verificationCode2:
                        verificationCodeEditText3.requestFocus();
                        break;
                    case R.id.verificationCode3:
                        verificationCodeEditText4.requestFocus();
                        break;
                }
            }
            total = verificationCodeEditText1.getText().toString()
                    +verificationCodeEditText2.getText().toString()+verificationCodeEditText3.getText().toString()
                    +verificationCodeEditText4.getText().toString();
            if (total.length()>3){
                ViewHelper.enableDisableView(verifyTextView, true);
            }
            else {
                ViewHelper.enableDisableView(verifyTextView, false);
            }

        }
    }
}
