package com.healthsaverz.healthmobile.medicine.medicineSearch.controller;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoader;
import com.healthsaverz.healthmobile.medicine.asyncloader.ParserListener;
import com.healthsaverz.healthmobile.medicine.global.App;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.Parser;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
import com.healthsaverz.healthmobile.medicine.medicineDetail.controller.MedicineDetailActivity;
import com.healthsaverz.healthmobile.medicine.medicineSearch.modal.MedicineAdapter;
import com.healthsaverz.healthmobile.medicine.medicineSearch.modal.MedicineRequest;
import com.healthsaverz.healthmobile.medicine.medicineSearch.modal.MedicineSearchResponse;
import com.healthsaverz.healthmobile.medicine.reminder.controller.ReminderActivity;

import java.util.ArrayList;
import java.util.Calendar;

public class MedicineSearchActivity extends Activity implements ParserListener, SearchView.OnQueryTextListener, View.OnTouchListener {

    //private RelativeLayout firstBlock;
    private RelativeLayout secondBlock;
    private ListView listView;
    private ProgressBar locationProgress;
    private CheckBox saltCheck;
    private TextView searchName;
    private Parser parser;
    private MedicineSearchResponse searchResponse;
    private MedicineRequest medicineRequest;
    private String searchText;
    private RelativeLayout emptyText;
    private Button emailUs;
    private Button addReminderButton;
    private App app;
    private SearchView searchView;
    private boolean isform = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        searchText = getIntent().getStringExtra("searchName");
        isform = getIntent().getBooleanExtra("isform", false);
//        if (isform){
//            getActionBar().setTitle("");
//        }
        app = (App) this.getApplicationContext();
        listView = (ListView) findViewById(R.id.medic_list);
        emptyText = (RelativeLayout)findViewById(R.id.emptyText);
        addReminderButton = (Button)findViewById(R.id.addReminderButton);
        addReminderButton.setOnTouchListener(this);
        emailUs = (Button)findViewById(R.id.emailUs);
        emailUs.setOnTouchListener(this);
        listView.setEmptyView(null);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                searchResponse = (MedicineSearchResponse)adapterView.getItemAtPosition(i);
                if (searchResponse != null){
                    if (app.isMed){
                        app.medName = searchResponse.brandsName;
                        app.strengthName = ((searchResponse.strengthName.equals("0"))?"0":searchResponse.strengthName+" ")
                                + ((searchResponse.strengthUnitName.equals("0"))?"":searchResponse.strengthUnitName);
//                        app.isMed = false;
                        finish();
                    }
                    else {
                        Intent intent = new Intent(MedicineSearchActivity.this, MedicineDetailActivity.class);
                        intent.putExtra("medicine", searchResponse);
                        intent.putExtra("product", medicineRequest);
                        startActivity(intent);
                        isform = false;
                    }

                }
            }
        });
        //firstBlock = (RelativeLayout)findViewById(R.id.firstBlock);
        secondBlock = (RelativeLayout)findViewById(R.id.secondBlock);
        secondBlock.setVisibility(View.GONE);
        saltCheck = (CheckBox)findViewById(R.id.saltCheck);
        searchName = (TextView)findViewById(R.id.searchName);
        getParser();
        locationProgress = (ProgressBar) findViewById(R.id.progressbar);
//        if (isform){
//            firstBlock.setVisibility(View.GONE);
//        }else {
//            firstBlock.setVisibility(View.VISIBLE);
//        }
    }

    public Parser getParser() {
        if (parser == null) {
            parser = new Parser(this);
            parser.setListener(this);
        }
        return parser;
    }

    @Override
    public void onBackPressed() {

        if (app.isMed && searchView != null){
            app.medName = searchView.getQuery().toString();

            isform = false;
        }
        if (parser != null){
            parser.cancel(true);
            parser = null;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search, menu);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(this);
        searchView.onActionViewExpanded();
        if (searchText != null && !searchText.equals("")){
            searchView.setQuery(searchText, true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()){
            case R.id.action_settings:
                return true;
            case R.id.action_search:
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void didReceivedError(AsyncLoader.Status errorCode) {
        if (parser != null){
            parser.cancel(true);
            parser = null;
        }
        Toast.makeText(this, "Check your Internet Connection ", Toast.LENGTH_SHORT).show();
        listView.setAdapter(null);
        listView.setEmptyView(emptyText);
        locationProgress.setVisibility(View.GONE);
    }

    @Override
    public void didReceivedData(Object result) {
        if (result != null) {
            ArrayList<MedicineSearchResponse> medicineSearchResponses = (ArrayList<MedicineSearchResponse>) result;
            if (medicineSearchResponses.size() > 0){
                MedicineAdapter medicineAdapter = new MedicineAdapter(medicineSearchResponses,this);
                secondBlock.setVisibility(View.VISIBLE);
                listView.setAdapter(medicineAdapter);
                listView.setEmptyView(null);
                locationProgress.setVisibility(View.GONE);
            }
            else {
                //Toast.makeText(this, "No result Found ", Toast.LENGTH_SHORT).show();
                listView.setAdapter(null);
                listView.setEmptyView(emptyText);
                locationProgress.setVisibility(View.GONE);
            }
        }
        else {
            //Toast.makeText(this, "No result Found ", Toast.LENGTH_SHORT).show();
            listView.setAdapter(null);
            listView.setEmptyView(emptyText);
            locationProgress.setVisibility(View.GONE);
        }
        if (parser != null){
            parser.cancel(true);
            parser = null;
        }
    }

    @Override
    public void didReceivedProgress(double progress) {

    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        //newText = newText.isEmpty() ? "" : "Query so far: " + newText;
        if (newText.length() > 1) {
            emptyText.setVisibility(View.GONE);
            if (isform){
                searchName.setText("If medicine not found, then go back to add " +"\""+ newText.trim()+"\" as medicine reminder");
                addReminderButton.setVisibility(View.GONE);
            }
            else {
                searchName.setText("Drugs Matching " +"\""+ newText.trim()+"\"");
                addReminderButton.setVisibility(View.VISIBLE);
            }

            medicineRequest = new MedicineRequest(newText.trim());
            //medicineRequest.clientID = PreferenceManager.getStringForKey(this, "clientID", "null");
            medicineRequest.sessionID = PreferenceManager.getStringForKey(this, "sessionID", "null");
            if (parser != null){
                parser.cancel(true);
                parser = null;
            }
            if (saltCheck.isChecked()){
                getParser().searchProductGenericName(medicineRequest);
            }
            else {
                getParser().searchProduct(medicineRequest);
            }
            //firstBlock.setVisibility(View.GONE);
            secondBlock.setVisibility(View.VISIBLE);
            locationProgress.setVisibility(View.VISIBLE);
            searchText = newText.trim();
        }
        else if (newText.length() < 1){
            if (parser != null){
                parser.cancel(true);
                parser = null;
            }
            listView.setAdapter(null);
            emptyText.setVisibility(View.VISIBLE);
            secondBlock.setVisibility(View.GONE);
//            if (!isform){
//                firstBlock.setVisibility(View.VISIBLE);
//            }
            locationProgress.setVisibility(View.GONE);
        }
        return true;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(v, null);
            return true;
        } else if (event.getActionMasked() == MotionEvent.ACTION_UP) {
            ViewHelper.fadeIn(v, null);
            switch (v.getId()){
                case R.id.emailUs:
                    Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto", AppConstants.email_us, null));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Need Medicine "+searchText);
                    startActivity(Intent.createChooser(intent, "New Medicine"));
                    return true;
                case R.id.addReminderButton:
                    if (searchText != null && !searchText.equals("")) {
                        App app = (App) this.getApplicationContext();
                        app.isMed = true;
                        app.medName = searchText;
                        app.strengthName = "";
                        intent = new Intent(this, ReminderActivity.class);
                        intent.putExtra("CurrCal", Calendar.getInstance().getTimeInMillis());
                        startActivity(intent);
                    }
                    else {
                        Toast.makeText(this, "Type a Medicine name", Toast.LENGTH_SHORT).show();
                    }
                    return true;
            }

        }
        return false;
    }
}