package com.healthsaverz.healthmobile.medicine.uploadPrescription.model;

/**
 * Created by DG on 11/6/2014.
 */
public class UploadPresciptionResponse {
    public  static final String SUCCESS = "you are activated valid user";
    public  static final String FAILURE = "User is not valid";

    public String status ="";
    public String sessionID ="";
    public String clientID = "";




    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }
}
