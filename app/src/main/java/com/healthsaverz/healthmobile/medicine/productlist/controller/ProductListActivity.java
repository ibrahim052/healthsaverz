package com.healthsaverz.healthmobile.medicine.productlist.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.filters.controller.FilterActivity;

import java.util.ArrayList;

public class ProductListActivity extends Activity {

    int categoryID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        categoryID = getIntent().getIntExtra("categoryID",-1);
        String categoryName = getIntent().getStringExtra("categoryName");

        TextView categoryTitleTextView = ((TextView)findViewById(R.id.category_menu_title));
        categoryTitleTextView.setText(categoryName);

        Button filterButton = (Button)findViewById(R.id.action_filters);
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductListActivity.this, FilterActivity.class);
                intent.putExtra("categoryId",categoryID);
                //startActivity(intent);
                startActivityForResult(intent, 1);
            }
        });


    }

    //1 -> Result code is set inside the FilterActivity when ApplyButton is pressed.
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1 && resultCode == 1){

            ArrayList<Integer> filterids = data.getIntegerArrayListExtra("filterids");

            for(Integer integer:filterids){
                Log.d("onActivityResult", "filter id " + integer);
            }
        }
    }

}
