package com.healthsaverz.healthmobile.medicine.bookaservice.controller;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoader;
import com.healthsaverz.healthmobile.medicine.asyncloader.ParserListener;
import com.healthsaverz.healthmobile.medicine.bookaservice.model.Deal;
import com.healthsaverz.healthmobile.medicine.global.Parser;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;

import java.util.ArrayList;

public class DealsDetailActivity extends Activity implements ParserListener, View.OnTouchListener {
    private Parser parser;
    private ProgressBar progressBar;
    private TextView descriptionText;
    private int dealID;

    public Parser getParser() {
        if (parser == null) {
            parser = new Parser(this);
            parser.setListener(this);
        }
        return parser;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deals_detail);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        descriptionText = (TextView) findViewById(R.id.descriptionText);
        findViewById(R.id.selectButton).setOnTouchListener(this);

        dealID = getIntent().getIntExtra("DealID", -1);
        if (dealID!= -1){
            getParser().getDealDetail(dealID);
            progressBar = (ProgressBar)findViewById(R.id.progressbar);
            progressBar.setVisibility(View.VISIBLE);
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_deals_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            Intent intent = new Intent();
            //intent.putExtra("checked", true);
            intent.putExtra("dealID", -1);
            setResult(69, intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void didReceivedError(AsyncLoader.Status errorCode) {
        if (parser!= null){
            parser.cancel(true);
            parser = null;
        }
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void didReceivedData(Object result) {
        if (parser!= null){
            parser.cancel(true);
            parser = null;
        }
        progressBar.setVisibility(View.GONE);
        String dealDetail = (String) result;
        //DealsListAdapter dealsListAdapter = new DealsListAdapter(this, dealArrayList);
        descriptionText.setText(dealDetail);
    }

    @Override
    public void didReceivedProgress(double progress) {
        if (parser!= null){
            parser.cancel(true);
            parser = null;
        }
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, "Still Loading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(view, null);
            if (view.getId() == R.id.selectButton){
                Intent intent = new Intent();
                //intent.putExtra("checked", true);
                intent.putExtra("dealID", dealID);
                setResult(69, intent);
                finish();
            }
            return true;
        }
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
            ViewHelper.fadeIn(view, null);
            return true;
        }
        return false;
    }
}
