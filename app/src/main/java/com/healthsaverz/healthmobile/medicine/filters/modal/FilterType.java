package com.healthsaverz.healthmobile.medicine.filters.modal;

import java.util.ArrayList;

/**
 * Created by priyankranka on 12/11/14.
 */
public class FilterType {

    public String attributeType; //unique filter category name
    public String attributeCategoryType; //unique filter category name

    public ArrayList<Filter> filters;
}
