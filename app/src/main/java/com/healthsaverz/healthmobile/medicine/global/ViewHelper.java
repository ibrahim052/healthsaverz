package com.healthsaverz.healthmobile.medicine.global;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.healthsaverz.healthmobile.medicine.R;

import java.util.ArrayList;

/**
 * Created by montydudani on 29/08/14.
 */
public class ViewHelper {

    public static void fadeInTogether(View... views) {
        ArrayList<Animator> animators = new ArrayList<Animator>();
        for (View view : views) {
            view.setVisibility(View.VISIBLE);
            final ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, "alpha", .0f, 1f);
            objectAnimator.setDuration(1000);
            animators.add(objectAnimator);
        }
        final AnimatorSet mAnimationSet = new AnimatorSet();
        mAnimationSet.playTogether(animators);
        mAnimationSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
            }
        });
        mAnimationSet.start();
    }

    public static void fadeIn(View view, AnimatorListenerAdapter animatorListenerAdapter) {
        view.setVisibility(View.VISIBLE);
        final ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, "alpha", .3f, 1f);
        objectAnimator.setDuration(300);
        final AnimatorSet mAnimationSet = new AnimatorSet();
        mAnimationSet.play(objectAnimator);
        if (null != animatorListenerAdapter)
        mAnimationSet.addListener(animatorListenerAdapter);
        mAnimationSet.start();
    }

    public static void fadeOut(View view, AnimatorListenerAdapter animatorListenerAdapter) {
        view.setVisibility(View.VISIBLE);
        final ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, "alpha", 1f, .3f);
        objectAnimator.setDuration(300);
        final AnimatorSet mAnimationSet = new AnimatorSet();
        mAnimationSet.play(objectAnimator);
        if (null != animatorListenerAdapter)
        mAnimationSet.addListener(animatorListenerAdapter);
        mAnimationSet.start();
    }

    public static void vibrate(Context context, View view) {
        //view.setBackgroundResource(R.drawable.bordered_error);
        Animation shake = AnimationUtils.loadAnimation(context, R.anim.vibrate);
        view.startAnimation(shake);
    }

    public static void scaleIn(Context context, View view) {
        //view.setBackgroundResource(R.drawable.bordered_error);
        Animation shake = AnimationUtils.loadAnimation(context, R.anim.scale_down);
        view.startAnimation(shake);
    }

    public static void scaleOut(Context context, View view) {
        //view.setBackgroundResource(R.drawable.bordered_error);
        Animation shake = AnimationUtils.loadAnimation(context, R.anim.scale_up);
        view.startAnimation(shake);
    }

    public static void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = viewGroup.getChildAt(i);
            view.setEnabled(enabled);
            if (view instanceof ViewGroup) {
                enableDisableViewGroup((ViewGroup) view, enabled);
            }
        }
    }
    public static void enableDisableView(View view, boolean enabled) {
        //int childCount = view.getChildCount();
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            enableDisableViewGroup((ViewGroup) view, enabled);
        }
        if (!enabled)
            fadeOut(view, null);
        else
            fadeIn(view, null);
    }

    public static void hideShowViewGroup(ViewGroup viewGroup, boolean enabled) {
//        int childCount = viewGroup.getChildCount();
//        for (int i = 0; i < childCount; i++) {
//            View view = viewGroup.getChildAt(i);
//            if (enabled){
//                view.setVisibility(View.VISIBLE);
//            }
//            else {
//                view.setVisibility(View.GONE);
//            }
//            //view.setEnabled(enabled);
//            if (view instanceof ViewGroup) {
//                hideShowViewGroup((ViewGroup) view, enabled);
//            }
//            //viewGroup.setVisibility();
//        }
        if (enabled){
            viewGroup.setVisibility(View.VISIBLE);
        }
        else {
            viewGroup.setVisibility(View.GONE);
        }
        //scrollView.scrollTo(scrollView_X, scrollView_Y);
    }
    public static void hideShowView(View view, boolean enabled) {
            if (enabled){
                view.setVisibility(View.VISIBLE);
            }
            else {
                view.setVisibility(View.GONE);
            }
            //view.setEnabled(enabled);
//            if (view instanceof ViewGroup) {
//                showViewGroup((ViewGroup) view, enabled);
//            }
            //viewGroup.setVisibility();
        //scrollView.scrollTo(scrollView_X, scrollView_Y);
    }

}
