package com.healthsaverz.healthmobile.medicine.api;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

/**
 * Created by priyankranka on 02/05/14.
 */
public class ProcessImage extends AsyncTask<byte[], Void, Bitmap> {


    public interface ProcessImageListener {

        public void didReceivedProcessedImage(Bitmap image, int imageIndex, byte[] actualData);
    }

    private ProcessImageListener listener;
    private int width;
    private int height;
    private int imageIndex;
    private byte[] actualData;

    public byte[] getActualData() {
        return actualData;
    }

    public void setActualData(byte[] actualData) {
        this.actualData = actualData;
    }

    public ProcessImageListener getListener() {
        return listener;
    }

    public void setListener(ProcessImageListener listener) {
        this.listener = listener;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getImageIndex() {
        return imageIndex;
    }

    public void setImageIndex(int imageIndex) {
        this.imageIndex = imageIndex;
    }

    @Override
    protected Bitmap doInBackground(byte[]... params) {


        if (!isCancelled()) {
            Bitmap bitmap;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            BitmapFactory.decodeByteArray(params[0], 0, params[0].length, options);

            int sampleSize = calculateInSampleSize(options, width, height);
            options.inSampleSize = sampleSize;
            options.inJustDecodeBounds = false;

            bitmap = BitmapFactory.decodeByteArray(params[0], 0, params[0].length, options);

            return bitmap;
        }


        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);

        if(this.listener != null)
        {
            this.listener.didReceivedProcessedImage(bitmap,this.imageIndex,this.actualData);
        }
    }

    //local methods
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
