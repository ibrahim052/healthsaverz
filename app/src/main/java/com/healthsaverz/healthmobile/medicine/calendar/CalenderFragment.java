package com.healthsaverz.healthmobile.medicine.calendar;


import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.cabinet.controller.CabinetActivity;
import com.healthsaverz.healthmobile.medicine.global.AnimatedExpandableListView;
import com.healthsaverz.healthmobile.medicine.global.AnimatedExpandableListView.AnimatedExpandableListAdapter;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.DialogProgressFragment;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.MedFriend;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.SmsParser;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.UpdateMedFriendParser;
import com.healthsaverz.healthmobile.medicine.medicine.controller.MedicineDBHandler;
import com.healthsaverz.healthmobile.medicine.medicine.modal.AlarmReceiver;
import com.healthsaverz.healthmobile.medicine.medicine.modal.Medicine;
import com.healthsaverz.healthmobile.medicine.medicine.modal.ReminderModal;
import com.healthsaverz.healthmobile.medicine.medicine.modal.UpdateTakeSkipCountParser;
import com.healthsaverz.healthmobile.medicine.medicineSearch.controller.MedicineSearchActivity;
import com.healthsaverz.healthmobile.medicine.reminder.controller.RefillActivity;
import com.healthsaverz.healthmobile.medicine.reminder.controller.ReminderActivity;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.TimeZone;


/**
 * A simple {@link android.app.Fragment} subclass.
 *
 */
public class CalenderFragment extends Fragment implements ExpandableListView.OnChildClickListener, SmsParser.SmsParserListener, UpdateMedFriendParser.UpdateFriendParserListener {

    private static final String TAG = "CalenderFragment";

    private int[] mCards = {
            0,
            0,
            R.drawable.badge1,
            R.drawable.badge2,
            R.drawable.badge3,
            R.drawable.capsule,
            R.drawable.badge5,
            R.drawable.badge6,
            R.drawable.badge7,
            R.drawable.badge8,
            R.drawable.badge9,
            R.drawable.badge10,
            R.drawable.badge11,
            R.drawable.badge12,
            R.drawable.badge14,
            R.drawable.badge15,
            R.drawable.badge16,
            R.drawable.badge17,
            R.drawable.badge18,
            R.drawable.badge19,
            R.drawable.badge20,
            R.drawable.badge21,
            R.drawable.badge22,
            R.drawable.badge23,
            R.drawable.badge24,
            R.drawable.badge25,
            R.drawable.badge26,
            R.drawable.badge27,
            R.drawable.badge28,
            0,
            0};

    private int[] mColors = {
            0,
            0,
            Color.parseColor("#3cbaff"),
            Color.parseColor("#2797d0"),
            Color.parseColor("#005c94"),
            Color.parseColor("#d4ff5f"),
            Color.parseColor("#a9e327"),
            Color.parseColor("#27a227"),
            Color.parseColor("#ffff5c"),
            Color.parseColor("#ff9900"),
            Color.parseColor("#ff7d27"),
            Color.parseColor("#efd597"),
            Color.parseColor("#dba747"),
            Color.parseColor("#936827"),
            Color.parseColor("#ff4141"),
            Color.parseColor("#e12727"),
            Color.parseColor("#c02727"),
            Color.parseColor("#ffae8e"),
            Color.parseColor("#ffa2b2"),
            Color.parseColor("#e9967a"),
            Color.parseColor("#fb9388"),
            Color.parseColor("#f3d2ff"),
            Color.parseColor("#dd82ff"),
            Color.parseColor("#c527ff"),
            Color.parseColor("#2d2d2d"),
            Color.parseColor("#7d7d7d"),
            Color.parseColor("#a9a9a9"),
            Color.parseColor("#d4d4d4"),
            Color.parseColor("#ffffff"),
            Color.parseColor("#000000"),
            0,0};

    private static final int START_INDEX = 31;
    private int index = -1;
    public int dateDifference;

    private AnimatedExpandableListView calendarListView = null;
    private ArrayList<CalenderReminder> calenderReminders = null;
    private ArrayList<CalenderReminder> headers = null;
    private HashMap<String,ArrayList<CalenderReminder>> medicines = null;

    MedicineDBHandler dbHandler;
    ReminderModal reminderModal1;

    CalendarExpandableListAdapter   calendarExpandableListAdapter;
    TextView textView;
    int currentDayOfMonth;
    int currentMonth;
    int currentYear;
    private int start =0;
    private SmsParser smsParser;
    private ArrayList<MedFriend> medFriends;
    private DialogProgressFragment progressFragment;
    private UpdateTakeSkipCountParser updateTakeSkipCountParser;

    public SmsParser getSmsParser(){
        if (smsParser == null){
            smsParser = new SmsParser(getActivity());
            smsParser.setSmsParserListener(this);
        }
        return smsParser;
    }

    public CalenderFragment() {
        // Required empty public constructor
    }

    public static CalenderFragment newInstance(int position){

        CalenderFragment calenderFragment = new CalenderFragment();

        Bundle bundle = new Bundle();
        bundle.putInt("index",position);

        calenderFragment.setArguments(bundle);

        return calenderFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getActionBar().show();
        setHasOptionsMenu(true);
        getActivity().getActionBar().setTitle("Med Schedule");
        index = getArguments().getInt("index");

        Log.d("CalenderFragment", "onCreate " + index);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        Log.d("CalenderFragment", "onCreateView " + index);
        setHasOptionsMenu(true);
        getActivity().getActionBar().show();
        View view = inflater.inflate(R.layout.fragment_calender, container, false);

        dateDifference = index - START_INDEX;

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND,0);
        calendar.add(Calendar.DAY_OF_YEAR, dateDifference);

        currentMonth = calendar.get(Calendar.MONTH) + 1;
        currentYear = calendar.get(Calendar.YEAR);
        int currentDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        currentDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        int currentMin  = calendar.get(Calendar.MINUTE);


        String monthString = "";
        String dayOfWeekString = "";
        String timeOfDay = ""+currentHour+":"+currentMin;

        switch (currentMonth){
            case 1:
                monthString = "JAN";
                break;
            case 2:
                monthString = "FEB";
                break;
            case 3:
                monthString = "MAR";
                break;
            case 4:
                monthString = "APR";
                break;
            case 5:
                monthString = "MAY";
                break;
            case 6:
                monthString = "JUN";
                break;
            case 7:
                monthString = "JUL";
                break;
            case 8:
                monthString = "AUG";
                break;
            case 9:
                monthString = "SEP";
                break;
            case 10:
                monthString = "OCT";
                break;
            case 11:
                monthString = "NOV";
                break;
            case 12:
                monthString = "DEC";
                break;
        }

        switch (currentDayOfWeek) {
            case 1:
                dayOfWeekString = "SUN";
                break;
            case 2:
                dayOfWeekString = "MON";
                break;
            case 3:
                dayOfWeekString = "TUE";
                break;
            case 4:
                dayOfWeekString = "WED";
                break;
            case 5:
                dayOfWeekString = "THU";
                break;
            case 6:
                dayOfWeekString = "FRI";
                break;
            case 7:
                dayOfWeekString = "SAT";
                break;
        }


        textView = (TextView) view.findViewById(R.id.calendar_view_title);
        TextView leftArrowTextView =  (TextView) view.findViewById(R.id.calendar_view_title_left_arrow);
        leftArrowTextView.setText("<");
        leftArrowTextView.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                if(index > 0)
                    ((CalendarActivity)getActivity()).navigatePage(index-1);
            }
        });

        TextView rightArrowTextView =  (TextView) view.findViewById(R.id.calendar_view_title_right_arrow);
        rightArrowTextView.setText(">");
        rightArrowTextView.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                if(index < 62)
                    ((CalendarActivity)getActivity()).navigatePage(index+1);
            }
        });

        textView.setText(monthString + " " + currentDayOfMonth + ", " + dayOfWeekString);

        CalenderReminderProvider provider = new CalenderReminderProvider(getActivity());

        calenderReminders   =   provider.getReminders(index);
        headers             =   provider.getHeaders(calenderReminders);
        medicines           =   provider.getMedicines(calenderReminders);

        //Log.d("onCreateView", "full lenght " +calenderReminders.size() + "headers "+ headers.size()+"medicines  "+medicines.size());

        calendarListView    =   (AnimatedExpandableListView) view.findViewById(R.id.calendar_list);
        calendarExpandableListAdapter   =   new CalendarExpandableListAdapter();

        calendarListView.setAdapter(calendarExpandableListAdapter);
        calendarListView.setGroupIndicator(null);
        calendarListView.setOnChildClickListener(this);
        calendarListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if (calendarListView.isGroupExpanded(groupPosition)) {
                    calendarListView.collapseGroupWithAnimation(groupPosition);
                } else {
                    calendarListView.expandGroupWithAnimation(groupPosition);
                }
                return true;
            }
        });


        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        try {
            Date startTime  =    sdf.parse("05:01");
            Date endTime    =    sdf.parse("12:00");
            Date startTime1  =    sdf.parse("12:01");
            Date endTime1    =    sdf.parse("16:00");
            Date startTime2  =    sdf.parse("16:01");
            Date endTime2    =    sdf.parse("20:00");
            Date startTime3  =    sdf.parse("20:01");
            Date endTime3    =    sdf.parse("24:00");

            Date reminderTime =  sdf.parse(timeOfDay);

            if((reminderTime.compareTo(startTime) == 1 || reminderTime.compareTo(startTime) == 0) &&
                    (reminderTime.compareTo(endTime) == -1) || reminderTime.compareTo(endTime) == 0){

                calendarListView.expandGroup(0,true);
            }
            else if((reminderTime.compareTo(startTime1) == 1 || reminderTime.compareTo(startTime1) == 0) &&
                    (reminderTime.compareTo(endTime1) == -1) || reminderTime.compareTo(endTime1) == 0){

                calendarListView.expandGroup(1,true);
            }
            else if((reminderTime.compareTo(startTime2) == 1 || reminderTime.compareTo(startTime2) == 0) &&
                    (reminderTime.compareTo(endTime2) == -1) || reminderTime.compareTo(endTime2) == 0){

                calendarListView.expandGroup(2,true);
            }
            else if((reminderTime.compareTo(startTime3) == 1 || reminderTime.compareTo(startTime3) == 0) &&
                    (reminderTime.compareTo(endTime3) == -1) || reminderTime.compareTo(endTime3) == 0){

                calendarListView.expandGroup(3,true);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }


        calendarExpandableListAdapter.notifyDataSetChanged();

        return view;
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        //super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.menu_calendar, menu);
//        this.menu = menu;
//    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()){
            case R.id.action_add:
                Calendar calendar = AppConstants.getCurrentCalendar();
                Calendar currentCal = new GregorianCalendar(currentYear, currentMonth-1, currentDayOfMonth, calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), 0);
                currentCal.set(Calendar.MILLISECOND, 0);
                if (currentCal.before(calendar) && !(calendar.getTimeInMillis() - currentCal.getTimeInMillis() == 43200000)) {   // )

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    // Setting Dialog Title
                    alertDialog.setTitle("Validation Error");
                    // Setting Dialog Message
                    alertDialog.setMessage("Cannot set reminder on " + textView.getText().toString());

                    // Setting Positive "OK" Btn
                    alertDialog.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Write your code here to execute after dialog
                                    dialog.cancel();
                                }
                            });
                    // Showing Alert Dialog
                    alertDialog.show();
                } else {
                    Intent intent = new Intent(getActivity(), ReminderActivity.class);
                    intent.putExtra("CurrCal", currentCal.getTimeInMillis());
                    startActivity(intent);
                    CalendarActivity.addButtonClicked    =   true;
                }

                break;
            case android.R.id.home:
                getActivity().finish();
                break;
            case R.id.action_cabinet:
                startActivity(new Intent(getActivity(), CabinetActivity.class));
                break;
        }
        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Log.d("CalenderFragment", "onDestroyView " + index);
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().invalidateOptionsMenu();
        Log.d("CalenderFragment", "onDestroy ");
        calenderReminders = null;
        headers = null;
        medicines = null;
    }

    @Override
    public void onResume() {
        super.onResume();
//        getActivity().getActionBar().show();
        getActivity().openOptionsMenu();
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
//        setHasOptionsMenu(true);
        //onPrepareOptionsMenu(menu);
    }
    CalenderReminder calenderReminderOnMenuItemClick;
    int onMenuItemClickGroupPosition;
    int onMenuItemClickChildPosition;
    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, final int childPosition, long id) {

        Log.d("onChildClick","groupPosition "+groupPosition+" & "+"childPosition "+childPosition);

        onMenuItemClickGroupPosition    =   groupPosition;
        onMenuItemClickChildPosition    =   childPosition;
        calenderReminderOnMenuItemClick =   null;

        if(groupPosition == 0)
        {
            calenderReminderOnMenuItemClick =   medicines.get("morning").get(childPosition);
        }
        else if(groupPosition == 1)
        {
            calenderReminderOnMenuItemClick =   medicines.get("afternoon").get(childPosition);
        }
        else if(groupPosition == 2)
        {
            calenderReminderOnMenuItemClick =   medicines.get("evening").get(childPosition);
        }
        else if(groupPosition == 3)
        {
            calenderReminderOnMenuItemClick =   medicines.get("night").get(childPosition);
        }

        dbHandler = new MedicineDBHandler(getActivity());
        //reminderModal1 = new ReminderModal();

        reminderModal1 = dbHandler.findSpecificReminder(calenderReminderOnMenuItemClick.reminderID);


        if(calenderReminderOnMenuItemClick.medicineID == -1)// No medicine available
        {
            return false;
        }
        else
        {
            PopupMenu popup = new PopupMenu(this.getActivity(),v);//v is the cell

            if(dateDifference > 0) // different date not current date
            {
                popup.getMenuInflater().inflate(R.menu.menu_other_date,popup.getMenu());
            }
            else if (dateDifference < 0){
                popup.getMenuInflater().inflate(R.menu.menu_previous_date,popup.getMenu());
            }
            else
            {
                Calendar calendar2 = AppConstants.getCurrentCalendar();
                int hour1 = calendar2.get(Calendar.HOUR_OF_DAY);
                int minute1 = calendar2.get(Calendar.MINUTE);

                Calendar calendar3 = new GregorianCalendar();
                calendar3.setTimeInMillis(Long.parseLong(reminderModal1.get_time()));

                int hour2 = calendar3.get(Calendar.HOUR_OF_DAY);
                int minute2 = calendar3.get(Calendar.MINUTE);

                if (((hour2*60)+minute2) > ((hour1*60)+minute1))
                {
                    popup.getMenuInflater().inflate(R.menu.menu_other_date,popup.getMenu());
                }
                else if(calenderReminderOnMenuItemClick.medicineStatus == 1 || calenderReminderOnMenuItemClick.medicineStatus == 2)// Medicine taken
                {
                    popup.getMenuInflater().inflate(R.menu.menu_other_date,popup.getMenu());
                }
                else if (calenderReminderOnMenuItemClick.medicineStatus == 0)//Medicine missed
                {
                    popup.getMenuInflater().inflate(R.menu.menu_take,popup.getMenu());
                }
                else//medicine default
                {
                    popup.getMenuInflater().inflate(R.menu.menu_take_skip,popup.getMenu());
                }
            }
            popup.show();//showing popup menu





            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener(){
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    Medicine medicine = dbHandler.findMedicinefromID(calenderReminderOnMenuItemClick.medicineID);
                    updateTakeSkipCountParser = new UpdateTakeSkipCountParser(getActivity());
                    Calendar calendar1 = AppConstants.getCurrentCalendar();
                    float finalPillCount;
                    switch (item.getItemId()){
                        case R.id.action_take:
                            if (medicine.get_pills() != -1.0f){

                                float v = (float) (medicine.get_pills() - ((reminderModal1.get_quantity() != null) ? Float.parseFloat(reminderModal1.get_quantity()) : 0.0));

                                finalPillCount = medicine.get_pills() - Float.parseFloat(reminderModal1.get_quantity());

                                Log.d("setOnMenuItemClckListnr","finalPillCount : "+finalPillCount);

                                Medicine medicine1 = new Medicine(medicine.get_medID(), medicine.get_medName(), medicine.get_patientName(),
                                        medicine.get_shape(), medicine.get_color1(), medicine.get_color2(), medicine.get_startDate(),
                                        medicine.get_endDate(), medicine.get_dose(), medicine.get_unit(), medicine.get_status(),
                                        medicine.get_description(),finalPillCount);
                                medicine1._threshold = medicine._threshold;
                                medicine1._tempEndDate  =   medicine._tempEndDate;

                                dbHandler.updateMedicine(medicine1);
                                reminderModal1._taken_time = AppConstants.getCurrentCalendar().getTimeInMillis()+"";
                                if (Long.parseLong(reminderModal1._taken_time) - Long.parseLong(reminderModal1.get_time()) < AppConstants.TIME_DIFFERENCE_MINUTES )
                                    reminderModal1._taken = 1;
                                else
                                    reminderModal1._taken = 2;
                                reminderModal1._snooze = -1;
                                dbHandler.updateReminder(reminderModal1);

                                if (v <= medicine._threshold) {
                                    Intent intent = new Intent(getActivity(),RefillActivity.class);
                                    //intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                                    intent.putExtra("reminder_id",calenderReminderOnMenuItemClick.reminderID);
                                    intent.putExtra("medicine_id",calenderReminderOnMenuItemClick.medicineID);
                                    intent.putExtra("time",reminderModal1.get_time());
                                    intent.putExtra("notificationtype",2);// 2 for calendar view

                                    startActivity(intent);
                                }
                            }
                            else
                            {
                                reminderModal1._taken_time = calendar1.getTimeInMillis()+"";
                                if (Long.parseLong(reminderModal1._taken_time) - Long.parseLong(reminderModal1.get_time()) < AppConstants.TIME_DIFFERENCE_MINUTES )
                                    reminderModal1._taken = 1;
                                else
                                    reminderModal1._taken = 2;
                                dbHandler.updateReminder(reminderModal1);
                                finalPillCount = medicine.get_pills();
                            }
                            updateTakeSkipCountParser.updateReminder(reminderModal1, medicine.get_medName(), medicine.get_dose(), finalPillCount);

                            /*
                                Once we have taken the pill we need to the remover the shceduler.
                             */
                            AppConstants.cancelNotificationScheduler(getActivity(), reminderModal1, 10);

                            calenderReminderOnMenuItemClick.medicineStatus =   1;
                            calenderReminderOnMenuItemClick.medicineTakenTime = calendar1.get(Calendar.HOUR_OF_DAY) + ":" + calendar1.get(Calendar.MINUTE);

                            calendarExpandableListAdapter.notifyDataSetChanged();
                            break;
                        case R.id.action_skip:
                            reminderModal1._taken = 0;
                            reminderModal1._taken_time = AppConstants.getCurrentCalendar().getTimeInMillis()+"";
                            reminderModal1._snooze = -1;
                            dbHandler.updateReminder(reminderModal1);
                            updateTakeSkipCountParser.updateReminder(reminderModal1, medicine.get_medName(), medicine.get_dose(), medicine.get_pills());
                            medFriends = dbHandler.getAllMedFriendsForMedID(medicine.get_medID());
                            if (medFriends != null && medFriends.size() > start){
                                for (MedFriend medFriend : medFriends){
                                    medFriend._skipCount = medFriend._skipCount+1;
                                }
                                getSmsParser().getsms(medFriends.get(start),reminderModal1.get_medID(), reminderModal1.get_remindID());
                                progressFragment =  new DialogProgressFragment();
                                progressFragment.show(getActivity().getFragmentManager(), TAG);
                                progressFragment.setCancelable(false);
                            }

                            /*
                                Once we have clicked skip the pill we need to the remover the shceduler as SMS will go immediately.
                             */
                            AppConstants.cancelNotificationScheduler(getActivity(), reminderModal1, 10);

                            calenderReminderOnMenuItemClick.medicineStatus =   0;
                            calenderReminderOnMenuItemClick.medicineTakenTime  =   calendar1.get(Calendar.HOUR_OF_DAY) + ":" + calendar1.get(Calendar.MINUTE);

                            calendarExpandableListAdapter.notifyDataSetChanged();
                            break;
                        case R.id.action_delete_dose:
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            // Setting Dialog Title
                            alertDialog.setTitle("Delete Dose!");
                            // Setting Dialog Message
                            alertDialog.setMessage("Are you sure ?");
                            alertDialog.setCancelable(false);

                            // Setting Positive "OK" Btn
                            alertDialog.setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // Write your code here to execute after dialog
                                            if(dbHandler.deleteReminder(calenderReminderOnMenuItemClick.reminderID)){

                                /*
                                We need to first remove the alarm from for the schedule medicine
                                For that we need to make Pending Intent and pass the same intent to cancel.
                                But there can be a case where notification has fired and user had call delete in that
                                case we need to make sure notification is not create when broadcast recieves the notification.
                                 */
                                                Intent intent = new Intent(getActivity(), AlarmReceiver.class);
                                                //intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                                                intent.putExtra("medicine_id",reminderModal1.get_medID());
                                                intent.putExtra("reminder_id",reminderModal1.get_remindID());
                                                if(reminderModal1.get_qty_type() == 0)
                                                    intent.putExtra("notificationtype",1);// 1 mean it is for pill reminder.
                                                else
                                                    intent.putExtra("notificationtype",3);// 3 mean it is for RX reminder.

                                                PendingIntent pendingIntentDeleteDose = PendingIntent.getBroadcast(getActivity().getApplicationContext(), reminderModal1.get_remindID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

                                                AlarmManager alarmManager = (AlarmManager) getActivity().getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                                                alarmManager.cancel(pendingIntentDeleteDose);
                                                pendingIntentDeleteDose.cancel();

                                                /*
                                                One need to remove scheduler as well for the pill dose
                                                 */
                                                AppConstants.cancelNotificationScheduler(getActivity().getApplicationContext(), reminderModal1, 10);


                                                if(onMenuItemClickGroupPosition == 0)
                                                {
                                                    medicines.get("morning").remove(onMenuItemClickChildPosition);
                                                }
                                                else if(onMenuItemClickGroupPosition == 1)
                                                {
                                                    medicines.get("afternoon").remove(onMenuItemClickChildPosition);
                                                }
                                                else if(onMenuItemClickGroupPosition == 2)
                                                {
                                                    medicines.get("evening").remove(onMenuItemClickChildPosition);
                                                }
                                                else if(onMenuItemClickGroupPosition == 3)
                                                {
                                                    medicines.get("night").remove(onMenuItemClickChildPosition);
                                                }

                                                ((CalendarActivity)getActivity()).refreshCalendar();
                                            }
                                            dialog.cancel();
                                        }
                                    });
                            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            });
                            // Showing Alert Dialog
                            alertDialog.show();

                            break;
                        case R.id.action_delete_medication:
                            /*
                            We need to delete all the reminders associated with the medicine
                             */

                            //First delete the shcedule from the DB
                            alertDialog = new AlertDialog.Builder(getActivity());
                            // Setting Dialog Title
                            alertDialog.setTitle("Delete Medicine!");
                            // Setting Dialog Message
                            alertDialog.setMessage("Are you sure ?");
                            alertDialog.setCancelable(false);

                            // Setting Positive "OK" Btn
                            alertDialog.setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // Write your code here to execute after dialog
                                            // we have to update the flags for update and delete where 1 is for update and -1 is for delete
                                            dbHandler.updateDeleteMedicineFlag("-1", calenderReminderOnMenuItemClick.medicineID);
                                            //Once schedule is deleted we need to remove all the alaram
                                            ArrayList<ReminderModal> reminderModals = dbHandler.findReminder(calenderReminderOnMenuItemClick.medicineID);

                                            for (ReminderModal modal: reminderModals) {
                                /*
                                We need to delete all the reminders which are scheduled on the AlarmManager along with the scheduler
                                 */
                                                Intent intent = new Intent(getActivity(), AlarmReceiver.class);
                                                intent.putExtra("medicine_id",modal.get_medID());
                                                intent.putExtra("reminder_id",modal.get_remindID());
                                                if(modal.get_qty_type() == 0)
                                                    intent.putExtra("notificationtype",1);// 1 mean it is for pill reminder.
                                                else
                                                    intent.putExtra("notificationtype",3);// 3 mean it is for RX  reminder.

                                                PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity().getApplicationContext(), modal.get_remindID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                                                AlarmManager alarmManager = (AlarmManager) getActivity().getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                                                alarmManager.cancel(pendingIntent);
                                                pendingIntent.cancel();

                                                AppConstants.cancelNotificationScheduler(getActivity().getApplicationContext(), modal, 10);
                                            }
                                            //finally delete the medicine from the medicine table for future dates
                                            //dbHandler.deleteReminderWithMedicineID(medID);
                                            dbHandler.deleteReminderWithMedicineIDForFutureDate(calenderReminderOnMenuItemClick.medicineID);
                                            ((CalendarActivity)getActivity()).refreshCalendar();
                                            dialog.cancel();
                                        }
                                    });
                            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            });
                            // Showing Alert Dialog
                            alertDialog.show();


                            break;
                        case R.id.action_medication_info:
                            if (medicine != null){
                                Intent intent = new Intent(getActivity(), MedicineSearchActivity.class);
                                intent.putExtra("searchName", medicine.get_medName());
                                startActivity(intent);
                            }
                            break;
                    }
                    return false;
                }
            });

            return true;
        }


    }


    //Local methods

    @Override
    public void smsParserDidReceiveData(String message) {
        if (message != null && message.equals("1701")){
            UpdateMedFriendParser medFriendParser = new UpdateMedFriendParser(getActivity());
            medFriendParser.setMedFriendParserListener(this);
            medFriendParser.updateMedFriend(medFriends.get(start));

        }
        else  {
            start++;
            if (medFriends != null && medFriends.size() > start){
                getSmsParser().getsms(medFriends.get(start),reminderModal1.get_medID(), reminderModal1.get_remindID());
            }
            else {
                if (progressFragment != null){
                    progressFragment.dismiss();
                    progressFragment = null;
                }
                //getAlert("Something went wrong");
            }
        }
    }

    @Override
    public void smsParserDidReceivedConnectionError(AsyncLoaderNew.Status status) {
        start++;
        if (medFriends != null && medFriends.size() > start){
            getSmsParser().getsms(medFriends.get(start),reminderModal1.get_medID(), reminderModal1.get_remindID());
        }
        else {
            if (progressFragment != null){
                progressFragment.dismiss();
                progressFragment = null;
            }

            //getAlert(status.toString());
        }
    }

    @Override
    public void smsParserDidReceivedProcessingError(String message) {
        start++;
        if (medFriends != null && medFriends.size() > start){
            getSmsParser().getsms(medFriends.get(start),reminderModal1.get_medID(), reminderModal1.get_remindID());
        }
        else {
            if (progressFragment != null){
                progressFragment.dismiss();
                progressFragment = null;
            }

            //getAlert(message);

        }
        Log.d("NotifictionActivity", message);
    }

    @Override
    public void updateFriendParserDidReceiveData(Object message) {
        dbHandler.updateMedFriend(medFriends.get(start));
        start++;
        if (medFriends != null && medFriends.size() > start){
            getSmsParser().getsms(medFriends.get(start),reminderModal1.get_medID(), reminderModal1.get_remindID());
        }
        else {
            if (progressFragment != null){
                progressFragment.dismiss();
                progressFragment = null;
            }
            Toast.makeText(getActivity(), "We'hve informed your friends", Toast.LENGTH_SHORT).show();
        }
    }

    public static class ViewHolderHeader{

        TextView leftTitleTextView;
        ImageView rightImageView;
    }

    public static class ViewHolderMedicine{

        ImageView leftMedicineIcon;
        TextView rightMedicineNameTextView;
        TextView rightMedicineScheduleTextView;
        TextView centerNoMedicineTextView;
    }
    @TargetApi(16)
    private void setBackgroundV16Plus(View view, Drawable drawable) {
        view.setBackground(drawable);

    }

    @SuppressWarnings("deprecation")
    private void setBackgroundV16Minus(View view, Drawable drawable) {
        view.setBackgroundDrawable(drawable);
    }

    public class CalendarExpandableListAdapter extends AnimatedExpandableListAdapter {

        LayoutInflater layoutInflater;

        int paddingDP;

        public CalendarExpandableListAdapter(){

            layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            paddingDP       =  (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getActivity().getResources().getDisplayMetrics());
        }

        @Override
        public int getGroupCount() {
            return headers.size();
        }

        @Override
        public int getRealChildrenCount(int i) {
            if(i == 0)
            {
                return  medicines.get("morning").size();
            }
            else if(i == 1)
            {
                return  medicines.get("afternoon").size();
            }
            else if(i == 2)
            {
                return  medicines.get("evening").size();
            }
            else if(i == 3)
            {
                return  medicines.get("night").size();
            }
            return 0;
        }
        @Override
        public Object getGroup(int i) {
            return headers.get(i);
        }

        @Override
        public Object getChild(int i, int i2) {
            if(i == 0)
            {
                return  medicines.get("morning").get(i2);
            }
            else if(i == 1)
            {
                return  medicines.get("afternoon").get(i2);
            }
            else if(i == 2)
            {
                return  medicines.get("evening").get(i2);
            }
            else if(i == 3)
            {
                return  medicines.get("night").get(i2);
            }

            return null;
        }

        @Override
        public long getGroupId(int i) {
            return i;
        }

        @Override
        public long getChildId(int i, int i2) {
            return i2;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {

            if(view == null)
            {
                ViewHolderHeader viewHolderHeader = new ViewHolderHeader();
                view = layoutInflater.inflate(R.layout.cell_calendar_header,null);
                viewHolderHeader.leftTitleTextView  =   (TextView)view.findViewById(R.id.cell_header_left_title);
                viewHolderHeader.rightImageView  =   (ImageView)view.findViewById(R.id.cell_header_right_icon);

                view.setTag(viewHolderHeader);
            }

            CalenderReminder calenderReminder = headers.get(i);

            if(calendarListView.isGroupExpanded(i)){
                if (android.os.Build.VERSION.SDK_INT >= 16){
                    setBackgroundV16Plus(view, getActivity().getResources().getDrawable(R.drawable.onlytopcornerrounded));
                }
                else{
                    setBackgroundV16Minus(view, getActivity().getResources().getDrawable(R.drawable.onlytopcornerrounded));
                }
                //view.setBackground(getActivity().getResources().getDrawable(R.drawable.onlytopcornerrounded));
            }
            else
            {
                if (android.os.Build.VERSION.SDK_INT >= 16){
                    setBackgroundV16Plus(view, getActivity().getResources().getDrawable(R.drawable.allcornerrounded));
                }
                else{
                    setBackgroundV16Minus(view, getActivity().getResources().getDrawable(R.drawable.allcornerrounded));
                }
                //view.setBackground(getActivity().getResources().getDrawable(R.drawable.allcornerrounded));
            }

            ViewHolderHeader viewHolderHeader = (ViewHolderHeader)view.getTag();

            viewHolderHeader.leftTitleTextView.setText(calenderReminder.headerTitle);
            viewHolderHeader.rightImageView.setImageBitmap(calenderReminder.headerIcon);

            return view;
        }

        @Override
        public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            if(convertView == null){

                ViewHolderMedicine viewHolderMedicine = new ViewHolderMedicine();
                convertView = layoutInflater.inflate(R.layout.cell_calendar_medicine,null);

                viewHolderMedicine.leftMedicineIcon =   (ImageView)convertView.findViewById(R.id.medicine_icon);
                viewHolderMedicine.rightMedicineScheduleTextView  =   (TextView)convertView.findViewById(R.id.medicine_take_time);
                viewHolderMedicine.rightMedicineNameTextView  =   (TextView)convertView.findViewById(R.id.medicine_name_dose);
                viewHolderMedicine.centerNoMedicineTextView =   (TextView)convertView.findViewById(R.id.medicine_no_schedule);

                convertView.setTag(viewHolderMedicine);
            }

            CalenderReminder calenderReminder = null;

            boolean isLastRow = false;

            if(groupPosition == 0)
            {
                calenderReminder =   medicines.get("morning").get(childPosition);
                isLastRow = childPosition == medicines.get("morning").size() - 1;
            }
            else if(groupPosition == 1)
            {
                calenderReminder =   medicines.get("afternoon").get(childPosition);
                isLastRow = childPosition == medicines.get("afternoon").size() - 1;
            }
            else if(groupPosition == 2)
            {
                calenderReminder =   medicines.get("evening").get(childPosition);
                isLastRow = childPosition == medicines.get("evening").size() - 1;
            }
            else if(groupPosition == 3)
            {
                calenderReminder =   medicines.get("night").get(childPosition);
                isLastRow = childPosition == medicines.get("night").size() - 1;
            }

            ViewHolderMedicine viewHolderMedicine = (ViewHolderMedicine)convertView.getTag();

            if(calendarListView.isGroupExpanded(groupPosition)){

                if(isLastRow) {
                    if (android.os.Build.VERSION.SDK_INT >= 16){
                        setBackgroundV16Plus(convertView, getActivity().getResources().getDrawable(R.drawable.onlybottomcornerrounded));
                    }
                    else{
                        setBackgroundV16Minus(convertView, getActivity().getResources().getDrawable(R.drawable.onlybottomcornerrounded));
                    }
                    //convertView.setBackground(getActivity().getResources().getDrawable(R.drawable.onlybottomcornerrounded));
                }
                else
                {
                    if (android.os.Build.VERSION.SDK_INT >= 16){
                        setBackgroundV16Plus(convertView, getActivity().getResources().getDrawable(R.drawable.nocornerrounded));
                    }
                    else{
                        setBackgroundV16Minus(convertView, getActivity().getResources().getDrawable(R.drawable.nocornerrounded));
                    }
                    //convertView.setBackground(getActivity().getResources().getDrawable(R.drawable.nocornerrounded));
                }
            }
            else
            {
                if (android.os.Build.VERSION.SDK_INT >= 16){
                    setBackgroundV16Plus(convertView, getActivity().getResources().getDrawable(R.drawable.nocornerrounded));
                }
                else{
                    setBackgroundV16Minus(convertView, getActivity().getResources().getDrawable(R.drawable.nocornerrounded));
                }
                //convertView.setBackground(getActivity().getResources().getDrawable(R.drawable.nocornerrounded));
            }

            assert calenderReminder != null;
            if(calenderReminder.medicineID == -1)//this indicate no medine schedule
            {
                viewHolderMedicine.leftMedicineIcon.setVisibility(View.INVISIBLE);
                viewHolderMedicine.rightMedicineScheduleTextView.setVisibility(View.INVISIBLE);
                viewHolderMedicine.rightMedicineNameTextView.setVisibility(View.INVISIBLE);
                viewHolderMedicine.centerNoMedicineTextView.setVisibility(View.VISIBLE);
            }
            else
            {

                viewHolderMedicine.leftMedicineIcon.setVisibility(View.VISIBLE);

                if (calenderReminder.medicineShape == 5)//Capsule with 2 layers
                {
                    viewHolderMedicine.leftMedicineIcon.setColorFilter(null);
                    viewHolderMedicine.leftMedicineIcon.setImageBitmap(mergeBitmap(calenderReminder.medicineColor1,calenderReminder.medicineColor2));
                }
                else if(calenderReminder.medicineShape == -1)//Default shape
                {
                    viewHolderMedicine.leftMedicineIcon.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(),mCards[2]));
                    viewHolderMedicine.leftMedicineIcon.setColorFilter(null);
                    viewHolderMedicine.leftMedicineIcon.setColorFilter(mColors[28], PorterDuff.Mode.MULTIPLY);
                }
                else {
                    viewHolderMedicine.leftMedicineIcon.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(),mCards[calenderReminder.medicineShape]));
                    viewHolderMedicine.leftMedicineIcon.setColorFilter(null);
                    viewHolderMedicine.leftMedicineIcon.setColorFilter(mColors[calenderReminder.medicineColor1], PorterDuff.Mode.MULTIPLY);
                }

                viewHolderMedicine.rightMedicineScheduleTextView.setVisibility(View.VISIBLE);
                viewHolderMedicine.rightMedicineNameTextView.setVisibility(View.VISIBLE);
                viewHolderMedicine.centerNoMedicineTextView.setVisibility(View.INVISIBLE);

                viewHolderMedicine.rightMedicineNameTextView.setText(calenderReminder.medicineName + " " + calenderReminder.medicineDose);
                DecimalFormat mFormat= new DecimalFormat("00");
                String time = calenderReminder.medicineTime;
                String[] hourMin = time.split(":");
                time = mFormat.format(Double.parseDouble(hourMin[0].equals("")?"00":hourMin[0]))+":"+mFormat.format(Double.parseDouble(hourMin[1].equals("")?"00":hourMin[1]));
                //Integer.parseInt(hourMin[1]);
                if(calenderReminder.medicineStatus == -1)
                {
                    viewHolderMedicine.rightMedicineScheduleTextView.setText("Take " + calenderReminder.medicineScheduleDose + " At " + time);
                }
                else if (calenderReminder.medicineStatus == 0)
                {
                    String timeTaken = calenderReminder.medicineTakenTime;
                    hourMin = timeTaken.split(":");
                    timeTaken = mFormat.format(Double.parseDouble(hourMin[0].equals("")?"00":hourMin[0]))+":"+mFormat.format(Double.parseDouble(hourMin[1].equals("")?"00":hourMin[1]));
                    viewHolderMedicine.rightMedicineScheduleTextView.setText("Skipped " + calenderReminder.medicineScheduleDose + " At " + timeTaken +"\n(Scheduled for " +time + " )");
                }
                else
                {
                    String timeTaken = calenderReminder.medicineTakenTime;
                    hourMin = timeTaken.split(":");
                    timeTaken = mFormat.format(Double.parseDouble(hourMin[0].equals("")?"00":hourMin[0]))+":"+mFormat.format(Double.parseDouble(hourMin[1].equals("")?"00":hourMin[1]));
                    viewHolderMedicine.rightMedicineScheduleTextView.setText("Taken " + calenderReminder.medicineScheduleDose + " At " + timeTaken +"\n(Scheduled for " +time + " )");
                }
            }
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int i, int i2) {
            return true;
        }
    }
    public Bitmap mergeBitmap(int colo1,int colo2)
    {

        Bitmap firstBitmap = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.badge3left);
        Bitmap secondBitmap = BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.badge3right);

        Bitmap comboBitmap;

        float width, height;

        width = firstBitmap.getWidth() + secondBitmap.getWidth();
        height = firstBitmap.getHeight();

        comboBitmap = Bitmap.createBitmap((int)(width/2), (int)height, Bitmap.Config.ARGB_4444);

        Paint firstPaint = new Paint();
        firstPaint.setColorFilter(new PorterDuffColorFilter(mColors[colo1], PorterDuff.Mode.MULTIPLY));

        Paint secondPaint = new Paint();
        secondPaint.setColorFilter(new PorterDuffColorFilter(mColors[colo2], PorterDuff.Mode.MULTIPLY));

        Canvas comboImage = new Canvas(comboBitmap);

        comboImage.drawBitmap(firstBitmap, 0f, 0f, firstPaint);
        comboImage.drawBitmap(secondBitmap,0f,0f, secondPaint);

        return comboBitmap;

    }
}
