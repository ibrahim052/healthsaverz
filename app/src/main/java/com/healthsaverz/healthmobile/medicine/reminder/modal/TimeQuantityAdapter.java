package com.healthsaverz.healthmobile.medicine.reminder.modal;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Ibrahim on 18-10-2014.
 */
public class TimeQuantityAdapter extends ArrayAdapter<ReminderTime> {
    private List<ReminderTime> cells;
    LayoutInflater layoutInflater;
    //private SparseBooleanArray mSelectedItemsIds;
    View.OnClickListener deleteClick;
    public TimeQuantityAdapter(Context context, int resource, List<ReminderTime> objects, View.OnClickListener deleteClick) {
        super(context, resource, objects);
        this.cells = objects;
        this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.deleteClick = deleteClick;
        //mSelectedItemsIds = new SparseBooleanArray();
    }

//    public void toggleSelection(int position) {
//        selectView(position, !mSelectedItemsIds.get(position));
//    }
//    public void selectView(int position, boolean value) {
//        if (value)
//            mSelectedItemsIds.put(position, true);
//        else
//            mSelectedItemsIds.delete(position);
//
//        notifyDataSetChanged();
//    }
//    public int getSelectedCount() {
//        return mSelectedItemsIds.size();
//    }
//    public void removeSelection() {
//        mSelectedItemsIds = new SparseBooleanArray();
//        notifyDataSetChanged();
//    }
//    public SparseBooleanArray getSelectedIds() {
//        return mSelectedItemsIds;
//    }
    @Override
    public int getCount() {
        return cells.size();
    }

    @Override
    public ReminderTime getItem(int index) {
        return cells.get(index);
    }

    @Override
    public long getItemId(int index) {
        return index;
    }

    @Override
    public View getView(int i, View convertview, ViewGroup viewGroup) {
        if (convertview == null){

            convertview = layoutInflater.inflate(R.layout.list_time_quantity, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.deleteImage = (ImageView)convertview.findViewById(R.id.deleteImage);
            viewHolder.deleteImage.setOnClickListener(deleteClick);
            viewHolder.time = (TextView)convertview.findViewById(R.id.time);
            viewHolder.quantity = (TextView)convertview.findViewById(R.id.quantity);
            convertview.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder)convertview.getTag();
        final ReminderTime cell = cells.get(i);
        Calendar calendar = cell.getCalendar();
        int hour = calendar.get(Calendar.HOUR);
        if (hour == 0){
            hour = 12;
        }
        int minute = calendar.get(Calendar.MINUTE);
        String am_pm = calendar.get(Calendar.AM_PM) == 1?"PM":"AM";
        holder.time.setText(String.format("%02d", hour)+":"+String.format("%02d", minute)+" "+am_pm);
        holder.quantity.setText("(take "+cell.getQuantity()+")");

        return convertview;
    }
    public class ViewHolder {
        public ImageView deleteImage;
        public TextView time;
        public TextView quantity;
    }

}

