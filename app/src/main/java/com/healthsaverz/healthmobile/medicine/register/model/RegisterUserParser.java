package com.healthsaverz.healthmobile.medicine.register.model;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by priyankranka on 07/11/14.
 */
public class RegisterUserParser extends AsyncTask <String,Void,String> implements AsyncLoaderNew.Listener {

    /*
    Start from registerUser method
    Takes String as input and out is based on RegisterUserParserListener
     */

    private AsyncLoaderNew asyncLoaderNew;
    private Context context;

    public RegisterUserParser(Context context){

        this.context = context;
    }

    public interface RegisterUserParserListener{

        void registerUserParserDidUserRegistered(String message); //message as 100 for success
        void registerUserParserDidReceivedConnectionError(AsyncLoaderNew.Status status);// AsyncLoaderNew.Status
        void registerUserParserDidReceivedProcessingError(String message);//server message for unsuccessful
    }

    private RegisterUserParserListener registerUserListener;

    public RegisterUserParserListener getRegisterUserListener() {
        return registerUserListener;
    }

    public void setRegisterUserListener(RegisterUserParserListener registerUserListener) {
        this.registerUserListener = registerUserListener;
    }

    private static final String METHOD_REGISTER_USER = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/UserServices/addUserMobile";

    public void registerUser(String password, String firstName, String lastName, String gender,String postalCode, String cellNumber, String emailAddress){

        asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.POST_HEADER);

        TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);

        if (cellNumber.length() == 10){
            cellNumber = "91"+cellNumber;
        }
        String requestXML = "<user>" +
                "<clientID>"+"healthsaverz"+"</clientID>" +
                "<userName>"+emailAddress+"</userName>" +
                "<password>"+password+"</password>" +
                "<firstName>"+firstName+"</firstName>" +
                "<lastName>"+lastName+"</lastName>" +
                "<gender>"+gender+"</gender>" +
                "<postalCode>"+postalCode+"</postalCode>" +
                "<userGroupId>"+5+"</userGroupId>" +
                "<cellNumber>"+cellNumber+"</cellNumber>" +
                "<emailAddress>"+emailAddress+"</emailAddress>" +
                "<deviceToken>"+AppConstants.getDeviceToken()+"</deviceToken>" +
                "<deviceUdid>"+telephonyManager.getDeviceId()+"</deviceUdid>" +
                "<osType>"+"A"+"</osType>"+
                "<deviceUDID>"+telephonyManager.getDeviceId()+"</deviceUDID>" +
                "<osVersion>"+AppConstants.osVersion()+"</osVersion>" +
                "<deviceName>"+ AppConstants.deviceName() +"</deviceName>"+
                "<uniqueNumber></uniqueNumber>"+
                "<vochuerFlag>N</vochuerFlag>"+
                "</user>";

        Log.d("requestXML ","requestXML "+requestXML);

        asyncLoaderNew.requestMessage   =   requestXML;
        asyncLoaderNew.executeRequest(METHOD_REGISTER_USER);
        asyncLoaderNew.setListener(this);
    }
    //AsyncLoaderNew.Listener method implementation
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {

        Log.d("RegisterUserParser","didReceivedError error status "+status);
        if(registerUserListener != null){
            registerUserListener.registerUserParserDidReceivedConnectionError(status);
        }
    }
    @Override
    public void didReceivedData(byte[] data) {
        String responseString = new String(data);
        Log.d("RegisterUserParser","didReceivedData is "+responseString);
        execute(responseString);
    }
    @Override
    public void didReceivedProgress(Double progress) {
    }
    //Current Thread Stack
    @Override
    protected String doInBackground(String... params) {
        if(!isCancelled())
        {
            try {
            /*
            {"sessionID":"","clientID":"","message":"100","gender":"Male",
            "deviceUdid":"353743054004708","deviceToken":"DEVICE_TOKEN","osType":"A",
            "verificationCode":"7031","verificationFlag":"","fileDetailList":null,"state":"",
            "country":"","id":361,"password":"","userName":"priyank@nimapinfotech.com","status":"I",
            "firstName":"","lastName":"","address1":"","address2":"","postalCode":"","emailAddress":"priyank@nimapinfotech.com",
            "subUserId":0,"userGroupId":3,"phoneNumber":"","cellNumber":"+919869357889","created_By":0,"city":""}
             */
                JSONObject jsonObject = new JSONObject(params[0]);

                if(jsonObject.getString("message").equals("100") || jsonObject.getString("message").equals("100")){
                    // indicates success in registering the user. We need to add to persistent storage so that it can be used later on when ever needed.
                    PreferenceManager.saveStringForKey(context,PreferenceManager.SESSION_ID,jsonObject.getString("sessionID"));//jsonObject.getString("sessionID") "test"
                    PreferenceManager.saveStringForKey(context,PreferenceManager.CLIENT_ID,jsonObject.getString("clientID"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.MESSAGE,jsonObject.getString("message"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.GENDER,jsonObject.getString("gender"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.DEVICE_TOKEN,jsonObject.getString("deviceToken"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.OS_TYPE,jsonObject.getString("osType"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.VERIFICATION_CODE,jsonObject.getString("verificationCode"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.VERIFICATION_FLAG,jsonObject.getString("verificationFlag"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.FILE_DETAIL_LIST,jsonObject.getString("fileDetailList"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.STATE,jsonObject.getString("state"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.STATUS,jsonObject.getString("status"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.COUNTRY,jsonObject.getString("country"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.ID,jsonObject.getString("id"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.USERNAME,jsonObject.getString("userName"));
//                    PreferenceManager.saveStringForKey(context,PreferenceManager.PASSWORD,jsonObject.getString("password"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.ADDRESS1,jsonObject.getString("address1"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.ADDRESS2,jsonObject.getString("address2"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.POSTAL_CODE,jsonObject.getString("postalCode"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.EMAIL_ADDRESS,jsonObject.getString("emailAddress"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.SUB_USER_ID,jsonObject.getString("subUserId"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.USER_GROUP_ID,jsonObject.getString("userGroupId"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.PHONE_NUMBER,jsonObject.getString("phoneNumber"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.CELL_NUMBER,jsonObject.getString("cellNumber"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.CREATED_BY,jsonObject.getString("created_By"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.CITY,jsonObject.getString("city"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.OS_VERSION,jsonObject.getString("osVersion"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.DEVICE_NAME,jsonObject.getString("deviceName"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.DEVICE_UDID,jsonObject.getString("deviceUDID"));
                    return "100";
                }
                else
                {
                    return jsonObject.getString("message");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            cancel(true);
        }
        return "Something went wrong. Please contact our administrator";
    }
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(this.registerUserListener != null){
            if(s.equals("100")){
                registerUserListener.registerUserParserDidUserRegistered(s);
            }
            else
            {
                registerUserListener.registerUserParserDidReceivedProcessingError(s);
            }
        }
    }
    @Override
    protected void onCancelled() {
        super.onCancelled();

        if (this.registerUserListener != null) {
            this.registerUserListener.registerUserParserDidReceivedProcessingError("Registration Abrupted");
        }
    }
}
