package com.healthsaverz.healthmobile.medicine.pieChart.modal;

import com.healthsaverz.healthmobile.medicine.medicine.modal.Medicine;

/**
 * Created by DELL PC on 3/3/2015.
 */
public class MedicineReportModel {
    public int medID;
    public String medicineName;
    public int valueCount;
    public int totalCount;
    public int shape;
    public int color1;
    public int color2;

    public MedicineReportModel(int medID, String medicineName, int valueCount, int totalCount, int shape, int color1, int color2) {
        this.medID = medID;
        this.medicineName = medicineName;
        this.valueCount = valueCount;
        this.totalCount = totalCount;
        this.shape = shape;
        this.color1 = color1;
        this.color2 = color2;
    }

    public MedicineReportModel() {
    }
}
