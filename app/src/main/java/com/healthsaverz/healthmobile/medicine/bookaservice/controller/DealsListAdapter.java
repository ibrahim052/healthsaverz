package com.healthsaverz.healthmobile.medicine.bookaservice.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.bookaservice.model.Deal;
import java.util.ArrayList;
/**
 * Created by healthsaverz5 on 4/9/2015.
 */
public class DealsListAdapter extends BaseAdapter {

    private static final String TAG = "CustomTestListAdapter";
    Context mContext;
    LayoutInflater inflater;
    private ArrayList<Deal> mainDataList;
    public boolean isSelectedByUser  = false;
    public boolean userSelectedValue = false;

    public DealsListAdapter(Context context, ArrayList<Deal> mainDataList) {
        this.mContext = context;
        this.mainDataList = mainDataList;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mainDataList.size();
    }

    @Override
    public Deal getItem(int position) {
        return mainDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            ViewHolder holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_testlist_layout, null);
            holder.individualTest = (RelativeLayout) convertView.findViewById(R.id.individualTest);
            holder.name = (TextView) convertView.findViewById(R.id.testname);
            holder.description = (TextView) convertView.findViewById(R.id.testdesc);
            holder.testfees = (TextView) convertView.findViewById(R.id.testfees);
            holder.check = (CheckBox) convertView.findViewById(R.id.testcheck);
            convertView.setTag(holder);
        }
        ViewHolder holder = (ViewHolder)convertView.getTag();
        final Deal deal = mainDataList.get(position);
        holder.name.setText(deal.name);
        holder.description.setText("Valid till: "+deal.endDate);
        holder.testfees.setText(mContext.getResources().getString(R.string.rs)+deal.amount);
        holder.check.setChecked(deal.isChecked);
        holder.check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSelectedByUser)
                    deal.isChecked = true;
                else
                    deal.isChecked = false;
            }
        });
        holder.check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isSelectedByUser = isChecked;
            }
        });
        //VITAL PART!!! Set the state of the
        //CheckBox using the boolean array
        return convertView;
    }

    static class ViewHolder {
        protected TextView name;
        protected TextView description;
        protected CheckBox check;
        protected RelativeLayout individualTest;
        protected TextView testfees;
    }

}
