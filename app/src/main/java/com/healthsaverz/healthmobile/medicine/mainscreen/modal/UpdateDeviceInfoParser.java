package com.healthsaverz.healthmobile.medicine.mainscreen.modal;

import android.content.Context;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;

/**
 * Created by DELL PC on 2/19/2015.
 */
public class UpdateDeviceInfoParser extends AsyncTask<String,Void,String> implements AsyncLoaderNew.Listener {
     /*
    Start from updateDevice method
    Takes String as input and out is based on VerifyUserParserListener
     */

    private Context context;

    public UpdateDeviceInfoParser(Context context) {

        this.context = context;
    }

    public interface UpdateDeviceParserListener {

        void updateDeviceParserDidDeviceChanged(String message); //message as 100 for success

        void updateDeviceParserDidReceivedConnectionError(AsyncLoaderNew.Status status);// AsyncLoaderNew.Status

        void updateDeviceParserDidReceivedProcessingError(String message);//server message for unsuccessful
    }

    private UpdateDeviceParserListener updateDeviceListener;

    public UpdateDeviceParserListener getUpdateDeviceListener() {
        return updateDeviceListener;
    }

    public void setUpdateDeviceListener(UpdateDeviceParserListener updateDeviceListener) {
        this.updateDeviceListener = updateDeviceListener;
    }

    private static final String METHOD_UPDATE_DEVICE = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/MappUserDeviceInfoServices/addMappUserDeviceInfo";

    //local method
    public void updateDevice() {

        AsyncLoaderNew asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.POST_HEADER);
        TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        String userID = PreferenceManager.getStringForKey(context, PreferenceManager.ID, "-1");
        String sessionID = PreferenceManager.getStringForKey(context, PreferenceManager.SESSION_ID, "-1");

      
        String requestXML = "<mappUserDeviceInfo>" +
                "<clientID>healthsaverz</clientID>" +
                "<sessionID>"+sessionID+"</sessionID>" +
                "<userId>"+userID+"</userId>" +
                "<deviceUDID>"+telephonyManager.getDeviceId()+"</deviceUDID>" +
                "<osVersion>"+AppConstants.osVersion()+"</osVersion>" +
                "<deviceName>"+ AppConstants.deviceName() +"</deviceName>"+
                "<status>A</status>" +
                "<createdBy>"+userID+"</createdBy>" +
                "<extra_1></extra_1>" +
                "<extra_2></extra_2>" +
                "<extra_3></extra_3>" +
                "<extra_4></extra_4>" +
                "<extra_5></extra_5>" +
                "</mappUserDeviceInfo>";

        Log.d("requestXML ", "requestXML " + requestXML);

        asyncLoaderNew.requestMessage   =   requestXML;
        asyncLoaderNew.executeRequest(METHOD_UPDATE_DEVICE);
        asyncLoaderNew.setListener(this);


    }

    @Override
    protected String doInBackground(String... params) {

        return params[0];
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (this.updateDeviceListener != null) {

            if (s != null && s.equals("100")) {
                updateDeviceListener.updateDeviceParserDidDeviceChanged(s);
            } else if (s != null && s.equals("101")) {
                updateDeviceListener.updateDeviceParserDidReceivedProcessingError("Device Not Updated");
            } else {
                updateDeviceListener.updateDeviceParserDidReceivedProcessingError(s);
            }
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();

        if (this.updateDeviceListener != null) {
            this.updateDeviceListener.updateDeviceParserDidReceivedProcessingError("Verification Abrupted");
        }
    }


    //AsyncLoaderNew.Listener method implementation
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {
        if (updateDeviceListener != null) {
            updateDeviceListener.updateDeviceParserDidReceivedConnectionError(status);
        }
    }

    @Override
    public void didReceivedData(byte[] data) {
        String s = new String(data);
        Log.d("UpdateDeviceParser", "didReceivedData is " + s);
        //execute(responseString);
        if (this.updateDeviceListener != null) {

            if (s != null && s.equals("100")) {
                TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
                PreferenceManager.saveStringForKey(context,PreferenceManager.OS_VERSION,AppConstants.osVersion());
                PreferenceManager.saveStringForKey(context,PreferenceManager.DEVICE_NAME,AppConstants.deviceName());
                PreferenceManager.saveStringForKey(context,PreferenceManager.DEVICE_UDID,telephonyManager.getDeviceId());
                updateDeviceListener.updateDeviceParserDidDeviceChanged(s);
            } else if (s != null && s.equals("101")) {
                updateDeviceListener.updateDeviceParserDidReceivedProcessingError("Device Not Updated");
            } else {
                updateDeviceListener.updateDeviceParserDidReceivedProcessingError(s);
            }
        }
    }

    @Override
    public void didReceivedProgress(Double progress) {

    }
}