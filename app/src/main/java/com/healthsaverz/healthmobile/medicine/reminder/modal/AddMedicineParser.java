package com.healthsaverz.healthmobile.medicine.reminder.modal;

import android.content.Context;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;
import com.healthsaverz.healthmobile.medicine.medicine.modal.Medicine;
import com.healthsaverz.healthmobile.medicine.medicine.modal.ReminderModal;
import com.healthsaverz.healthmobile.medicine.medicine.modal.Schedule;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 * Created by DELL PC on 2/13/2015.
 */
public class AddMedicineParser extends AsyncTask<String,Void,String> implements AsyncLoaderNew.Listener {

    /*
    Start from addMedicine method
    Takes String as input and out is based on addMedicineParserListener
     */

    private AsyncLoaderNew asyncLoaderNew;
    private Context context;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public AddMedicineParser(Context context){

        this.context = context;
    }

    public interface AddMedicineParserListener{

        void addMedicineParserDidUserRegistered(String message); //message as 100 for success
        void addMedicineParserDidReceivedConnectionError(AsyncLoaderNew.Status status);// AsyncLoaderNew.Status
        void addMedicineParserDidReceivedProcessingError(String message);//server message for unsuccessful
    }

    private AddMedicineParserListener addMedicineParserListener;

    public AddMedicineParserListener getAddMedicineParserListener() {
        return addMedicineParserListener;
    }

    public void setAddMedicineParserListener(AddMedicineParserListener addMedicineParserListener) {
        this.addMedicineParserListener = addMedicineParserListener;
    }

    private static final String METHOD_ADD_MEDICINE = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/AppUserMedicineDataServices/addAppUserMedicineData";

    public void addMedicine(Medicine medicine){
        asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.POST_HEADER);
        //TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        ArrayList<ReminderModal> reminderModals = medicine.get_reminderModals();
        Schedule schedule = medicine.get_schedule();
        String sessionID = PreferenceManager.getStringForKey(context, PreferenceManager.SESSION_ID, "");
        String clientID = PreferenceManager.getStringForKey(context, PreferenceManager.CLIENT_ID, "");
        String userId = PreferenceManager.getStringForKey(context, PreferenceManager.ID, "");
        String endDate = medicine.get_endDate();
        if (!endDate.equals("N/A")){
            endDate = sdf.format(Long.parseLong(medicine.get_endDate()));
        }
        String tempEndDate = medicine._tempEndDate;
        if (tempEndDate == null)
            tempEndDate = "";
        else
            tempEndDate = sdf.format(Long.parseLong(tempEndDate));
        //String medicineRequest = medicine.toString();
        StringBuilder timePipe = new StringBuilder();
        for (ReminderModal reminderModal:reminderModals){
            timePipe.append(sdf.format(Long.parseLong(reminderModal.get_time())));
            timePipe.append(",");
            timePipe.append(reminderModal.get_quantity());
            timePipe.append("|");
            //DateFormat.format(sdf.formatToCharacterIterator(), Long.parseLong(reminderModal.get_time()));
        }
        Random r = new Random();
        StringBuilder random = new StringBuilder();
        String alphabet = "0123456789qwertyuiopasdfghjklzxcvbnm";
        for (int i = 0; i < 6; i++) {
            random.append(alphabet.charAt(r.nextInt(alphabet.length())));
        }
        String requestXML =
                "<appUserMedicineData>" +

                    "<sessionID>"+sessionID+"</sessionID>" +
                    "<clientID>"+clientID+"</clientID>" +
                    "<userId>"+userId+"</userId>" +
                    "<requestId>"+random.toString()+"</requestId>" +
                    "<status>A</status>" +
                    "<createdBy>"+userId+"</createdBy>" +
                    "<extra_1></extra_1>" +
                    "<extra_2></extra_2>" +
                    "<extra_3></extra_3>" +
                    "<extra_4></extra_4>" +
                    "<extra_5></extra_5>" +

                    "<medicineInfo>" +
                        "<sessionID>"+sessionID+"</sessionID>" +
                        "<clientID>"+clientID+"</clientID>" +
                        "<userId>"+userId+"</userId>" +
                        "<medicineName>"+medicine.get_medName()+"</medicineName>" +
                        "<shape>"+medicine.get_shape()+"</shape>" +
                        "<color1>"+medicine.get_color1()+"</color1>" +
                        "<color2>"+medicine.get_color2()+"</color2>" +
                        "<dose>"+medicine.get_dose()+"</dose>" +
                        "<unit>"+medicine.get_unit()+"</unit>" +
                        "<pills>"+medicine.get_pills()+"</pills>" +
                        "<startDate>"+sdf.format(Long.parseLong(medicine.get_startDate()))+"</startDate>" +
                        "<endDate>"+endDate+"</endDate>" +
                        "<description>"+medicine.get_description()+"</description>" +
                        "<totalPills>"+medicine._threshold+"</totalPills>" +
                        "<tempEndDate>"+tempEndDate+"</tempEndDate>" +
                        "<forWhom>"+medicine.get_patientName()+"</forWhom>" +
                        "<updateDeleteFlag>"+medicine.updateDeleteFlag+"</updateDeleteFlag>" +
                        "<updateDeleteTime>"+medicine.updateDeleteDate+"</updateDeleteTime>" +
                        "<status>"+"A"+"</status>" +
                        "<createdBy>"+userId+"</createdBy>" +
                        "<extra_1></extra_1>" +
                        "<extra_2></extra_2>" +
                        "<extra_3></extra_3>" +
                        "<extra_4></extra_4>" +
                        "<extra_5></extra_5>" +
                    "</medicineInfo>" +


                    "<reminder>" +
                        "<sessionID>"+sessionID+"</sessionID>" +
                        "<clientID>"+clientID+"</clientID>" +
                        "<medcineInfoId>"+medicine.get_medID()+"</medcineInfoId>" +
                        "<quantity>"+0+"</quantity>" +
                        "<time>"+timePipe+"</time>" +
                        "<quantityType>"+reminderModals.get(0).get_qty_type()+"</quantityType>" +
                        "<reminderStatus>"+reminderModals.get(0)._taken+"</reminderStatus>" +
                        "<takenTime>"+0+"</takenTime>" +
                        "<snoozed>"+0+"</snoozed>" +
                        "<snoozedTime></snoozedTime>" +
                        "<status>A</status>" +
                        "<createdBy>"+userId+"</createdBy>" +
                        "<extra_1></extra_1>" +
                        "<extra_2></extra_2>" +
                        "<extra_3></extra_3>" +
                        "<extra_4></extra_4>" +
                        "<extra_5></extra_5>" +
                    "</reminder>" +

                    "<schedule>" +
                        "<sessionID>"+sessionID+"</sessionID>" +
                        "<clientID>"+clientID+"</clientID>" +
                        "<action>"+schedule.get_action()+"</action>" +
                        "<type>"+schedule.get_type()+"</type>" +
                        "<medicineInfoId>"+schedule.get_medID()+"</medicineInfoId>" +
                        "<status>A</status>" +
                        "<createdBy>"+userId+"</createdBy>" +
                        "<extra_1></extra_1>" +
                        "<extra_2></extra_2>" +
                        "<extra_3></extra_3>" +
                        "<extra_4></extra_4>" +
                        "<extra_5></extra_5>" +
                    "</schedule>" +

                    "<mappMedFriend>" +
                        "<sessionID>"+sessionID+"</sessionID>" +
                        "<clientID>"+clientID+"</clientID>" +
                        "<medFriendId></medFriendId>" +
                        "<medcineInfoId></medcineInfoId>" +
                        "<status>A</status>" +
                        "<createdBy>"+userId+"</createdBy>" +
                        "<extra_1></extra_1>" +
                        "<extra_2></extra_2>" +
                        "<extra_3></extra_3>" +
                        "<extra_4></extra_4>" +
                        "<extra_5></extra_5>" +
                    "</mappMedFriend>" +

                "</appUserMedicineData>";

        Log.d("requestXML ", "requestXML " + requestXML);

        asyncLoaderNew.requestMessage   =   requestXML;
        asyncLoaderNew.executeRequest(METHOD_ADD_MEDICINE);
        asyncLoaderNew.setListener(this);

    }


    //AsyncLoaderNew.Listener method implementation
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {

        Log.d("addMedicineParser","didReceivedError error status "+status);
        if(addMedicineParserListener != null){
            addMedicineParserListener.addMedicineParserDidReceivedConnectionError(status);
        }
    }

    @Override
    public void didReceivedData(byte[] data) {

        String responseString = new String(data);
        Log.d("addMedicineParser","didReceivedData is "+responseString);
        execute(responseString);
    }

    @Override
    public void didReceivedProgress(Double progress) {
    }


    //Current Thread Stack
    @Override
    protected String doInBackground(String... params) {
        return null;
    }
}