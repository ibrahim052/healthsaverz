package com.healthsaverz.healthmobile.medicine.charting.interfaces;

import com.healthsaverz.healthmobile.medicine.charting.components.YAxis.AxisDependency;
import com.healthsaverz.healthmobile.medicine.charting.utils.Transformer;

public interface BarLineScatterCandleDataProvider extends ChartInterface {

    public Transformer getTransformer(AxisDependency axis);
    public int getMaxVisibleCount();
}
