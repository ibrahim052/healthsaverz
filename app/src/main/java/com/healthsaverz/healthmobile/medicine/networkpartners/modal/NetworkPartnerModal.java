package com.healthsaverz.healthmobile.medicine.networkpartners.modal;

import android.content.Context;

/**
 * Created by healthsaverz5 on 3/25/2015.
 */
public class NetworkPartnerModal {
    /*
    [{"sessionID":"",
    "clientID":"",
    "message":"",
    "id":1,
    "name":"Reliance",
    "description":"Network",
    "status":"A",
    "created_By":1,
    "extra_1":null,
    "extra_2":null,
    "extra_3":null,
    "extra_4":null,"extra_5":null}]
     */

    public static final String sessionID = "sessionID";
    public static final String clientID = "clientID";
    public static String message = "message";
    public static String name = "name";
    public static String status = "status";
    public static String description = "description";
    public static int id = 1;

    public String extra_3 = "";
    public String extra_4 = "";
    public String extra_1 = "";
    public String extra_5 = "";
    public String extra_2 = "";
    public int created_By = 0;
    //    public int id ;
    private  Context context;


    public NetworkPartnerModal(Context context) {
        this.context = context;
    }

    public NetworkPartnerModal(Context context, String message, String name, String status, String description, int id) {
        this.context = context;
        this.message = message;
        this.name = name;
        this.status = status;
        this.description = description;
        this.id = id;
    }


    public NetworkPartnerModal() {

    }
}
