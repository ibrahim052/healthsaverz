package com.healthsaverz.healthmobile.medicine.medfriend.modal;

/**
 * Created by Ibrahim on 24-11-2014.
 */
public class MedFriend {
    public boolean ischecked;
    public String clientID = "";
    public String sessionID = "";
    public int _local_id;
    public int _medFriendID ;
    public int _userID ;
    public String _friendName = "";
    public String _friendEmail = "";
    public String _friendMobile = "";
    public int _missCount;
    public int _skipCount;
    public String extra_1 = "";
    public String extra_2 = "";
    public String extra_3 = "";
    public String extra_4 = "";
    public String extra_5 = "";

    public MedFriend(int _medFriendID, int _userID, String _friendName, String _friendEmail, String _friendMobile, int _missCount, int _skipCount, String extra_1, String extra_2, String extra_3, String extra_4, String extra_5) {
        this._medFriendID = _medFriendID;
        this._userID = _userID;
        this._friendName = _friendName;
        this._friendEmail = _friendEmail;
        this._friendMobile = _friendMobile;
        this._missCount = _missCount;
        this._skipCount = _skipCount;
        this.extra_1 = extra_1;
        this.extra_2 = extra_2;
        this.extra_3 = extra_3;
        this.extra_4 = extra_4;
        this.extra_5 = extra_5;
    }

    public MedFriend() {

    }
}
