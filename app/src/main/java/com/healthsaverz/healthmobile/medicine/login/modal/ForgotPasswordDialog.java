package com.healthsaverz.healthmobile.medicine.login.modal;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoader;
import com.healthsaverz.healthmobile.medicine.asyncloader.ParserListener;
import com.healthsaverz.healthmobile.medicine.global.Parser;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;

/**
 * Created by Ibrahim on 25-11-2014.
 */
public class ForgotPasswordDialog extends DialogFragment implements ParserListener {

    public static final String TAG = "AddMedFriendDialog";
    private Context context;

    private Parser parser;
    private EditText email;
    ForgotPassListener forgotListener;
    public interface ForgotPassListener{
        void forgotPassword(ForgotPassword password);
    }
    private ProgressBar progressBar;
    public ProgressBar getProgressBar() {
        if (progressBar == null) {
            progressBar = (ProgressBar)getView().findViewById(R.id.progressbar);
        }
        return progressBar;
    }

    public EditText getEmail() {
        if (email == null) {
            email = (EditText) getView().findViewById(R.id.email);
            AccountManager accountManager = AccountManager.get(getActivity());
            Account[] accounts = accountManager.getAccountsByType("com.google");

            Account account = (accounts.length > 0)?accounts[0]:null;
            if (account != null) {
                email.setText("");
                email.append(account.name);
            }
        }
        return email;
    }
    private Button setButton;
    public Button getVerifyButton() {
        if (setButton == null) {
            setButton = (Button) getView().findViewById(R.id.setButton);
            setButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                        ViewHelper.fadeOut(view, null);
                        return true;
                    } else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                        ViewHelper.fadeIn(view, null);
                        if (validateForm()){
                            ForgotPassword forgotPassword = new ForgotPassword();
                            forgotPassword.emailID = getEmail().getText().toString();
                            if (forgotListener != null){
                                forgotListener.forgotPassword(forgotPassword);
                            }
                            dismiss();
                            }

                        }

                        return true;
                    }
            });
        }
        return setButton;
    }

    private Button cancelButton;
    public Button getCancelButton() {
        if (cancelButton == null) {
            cancelButton = (Button) getView().findViewById(R.id.cancelButton);
            cancelButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {

                    dismiss();
                    return true;
                }
            });
        }
        return cancelButton;
    }

    public static ForgotPasswordDialog newInstance(ForgotPassListener context){
        ForgotPasswordDialog addMedFriendDialog = new ForgotPasswordDialog();
        addMedFriendDialog.forgotListener = context;
        return addMedFriendDialog;
    }

    public ForgotPasswordDialog() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dialog_forgot_password, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getVerifyButton();
        getCancelButton();
        getEmail();

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return dialog;
    }

    public  boolean validateForm(){

        String errorMessage = null;
        boolean isErrorOccured = false;

        if(email.getText().toString().trim().equals("")){

            errorMessage = "Email id can not be empty.";
            isErrorOccured = true;
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString().trim()).matches())
        {
            errorMessage = "Invalid email id.";
            isErrorOccured = true;
        }

        if(isErrorOccured){

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            // Setting Dialog Title
            alertDialog.setTitle("Validation Error");
            // Setting Dialog Message
            alertDialog.setMessage(errorMessage);

            // Setting Positive "OK" Btn
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            dialog.cancel();
                        }
                    });
            // Showing Alert Dialog
            alertDialog.show();

            return false;
        }
        else {

            return true;
        }
    }
    @Override
    public void didReceivedError(AsyncLoader.Status errorCode) {
        getProgressBar().setVisibility(View.GONE);
        getalert("Failure", errorCode.toString());
        if (parser != null){
            parser.cancel(true);
            parser = null;
        }
    }
    @Override
    public void didReceivedData(Object result) {
        if (result != null){
            getalert("Success", result.toString());
        }
        getProgressBar().setVisibility(View.GONE);
        if (parser != null){
            parser.cancel(true);
            parser = null;
        }

    }
    @Override
    public void didReceivedProgress(double progress) {

    }
    private void getalert(String title, String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Positive "OK" Btn
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                        dismiss();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }

}
