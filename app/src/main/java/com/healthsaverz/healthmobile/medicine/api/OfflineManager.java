package com.healthsaverz.healthmobile.medicine.api;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by priyankranka on 21/04/14.
 */
public class OfflineManager {

    private static final String TAG = "OfflineManager";
    private Context context;

    private static final OfflineManager INSTANCE = new OfflineManager();

    private OfflineManager() {


    }

    public static OfflineManager getInstance() {
        return INSTANCE;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    static String hashKeyForDisk(String key) {
        String cacheKey = null;

        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(key.getBytes());
            cacheKey = bytesToHexString(messageDigest.digest());

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            cacheKey = String.valueOf(key.hashCode());
        }

        return cacheKey;
    }

    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public static String getDiskCacheDirPath(Context context) {
        // Check if media is mounted or storage is built-in, if so, try and use external cache dir
        // otherwise use internal cache dir

        /*
        This method check where cache path is created. First it check the external SD card is present build in or removable SD card
        is present. If yes it will give Cache Directory on the external storage. If not than cache directory from internal storage is given.
         */

        String cachePath = null;

       Log.d(TAG, "getDiskCacheDirPath" + context);

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) || !Environment.isExternalStorageRemovable()) {
            cachePath = context.getExternalCacheDir().getPath();
        } else {
            cachePath = context.getCacheDir().getPath();
        }


        return cachePath;
        //return new String(cachePath + File.separator + uniqueName);
    }

    public boolean writeDataFromURL(String url, byte[] data) {

        String filePath = getFilePathURLCacheDir(url);

        FileOutputStream fileOutputStream = null;
        File file = null;

        try {
            file = new File(filePath);
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(data);
            fileOutputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            file.delete();
            return false;
        }

        return true;

    }

    public boolean isFileExistAtPathFromURL(String url) {

        String filePath = getFilePathURLCacheDir(url);

        File file = new File(filePath);
        if (file.exists())
            return true;
        else
            return false;
    }

    public byte[] readFileAtPathFromURL(String url) {
        String filePath = getFilePathURLCacheDir(url);

        File file = new File(filePath);

        Log.d(TAG,"readFileAtPathFromURL"+filePath);

        byte[] readData = new byte[(int) file.length()];
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            fileInputStream.read(readData);
            fileInputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return readData;
    }

    public String getFilePathURLCacheDir(String url) {
        String fileName = hashKeyForDisk(url);
        String filePath = getDiskCacheDirPath(context) + File.separator + fileName;

        return filePath;
    }

    public void deleteCacheDir() {

        Log.d(TAG,"deleteCacheDir");
        String filePath = getDiskCacheDirPath(context);

        deleteFile(new File(filePath));

    }

    public boolean deleteFile(File file)
    {
        if (file.isDirectory()) {
            String[] children = file.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteFile(new File(file, children[i]));
                if (!success) {
                    // return false;
                }
            }
        }

        // The directory is now empty so delete it
        return file.delete();
    }
}
