package com.healthsaverz.healthmobile.medicine.healthshopmenu.controller;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.healthshopmenu.modal.ProductMenu;
import com.healthsaverz.healthmobile.medicine.healthshopmenu.modal.ProductMenuParser;
import com.healthsaverz.healthmobile.medicine.productlist.controller.ProductListActivity;

import java.util.ArrayList;

public class HealthShopHomeActivity extends Activity implements ProductMenuParser.ProductMenuParserListener, AdapterView.OnItemClickListener, HealthShopHomeFragment.HealthShopHomeFragmentListener {

    private DrawerLayout mDrawerLayout;

    HealthShopHomeFragment healthShopHomeFragment;

    ProductMenuParser productMenuParser = null;

    ArrayList<ProductMenu> productMenus = null;
    ArrayList<ProductMenu> currentProductMenus = null;

    public ListView productMenuListView = null;

    ProductMenuListViewAdapter productMenuListViewAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_shop_home);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        productMenuListView = (ListView)findViewById(R.id.product_menu);

        healthShopHomeFragment = new HealthShopHomeFragment();
        healthShopHomeFragment.setListener(this);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.heath_shop_main_container,healthShopHomeFragment) ;
        fragmentTransaction.commit();

        if(productMenuParser != null)
        {
            productMenuParser.cancel(true);
            productMenuParser = null;
        }

        productMenuParser = new ProductMenuParser(this);
        productMenuParser.setProductMenuParserListener(this);
        productMenuParser.getProductMenu();


    }

    //ProductMenuParser.ProductMenuParserListener
    @Override
    public void productMenuParserDidReceivedMenu(ArrayList<ProductMenu> productMenus1) {

        if(this.productMenus != null)
        {
            this.productMenus = null;
        }
        if(currentProductMenus != null)
        {
            currentProductMenus = null;
        }

        this.productMenus = productMenus1;

        currentProductMenus = new ArrayList<ProductMenu>();

        for(ProductMenu productMenu:productMenus){
            if(productMenu.show)
            {
                currentProductMenus.add(productMenu);
            }
        }

        if(productMenuListViewAdapter != null)
        {
            productMenuListView.removeAllViews();
            productMenuListViewAdapter = null;
        }
        productMenuListViewAdapter = new ProductMenuListViewAdapter();

        productMenuListView.setAdapter(productMenuListViewAdapter);
        productMenuListView.setOnItemClickListener(this);
    }

    @Override
    public void productMenuDidReceivedConnectionError(AsyncLoaderNew.Status status) {

    }

    @Override
    public void productMenuDidReceivedProcessingError(String message) {

    }

    //HealthShopHomeFragment.HealthShopHomeFragmentListener implements
    @Override
    public void didDrawerButtonPressed() {
        mDrawerLayout.openDrawer(productMenuListView);
    }

    //AdapterView.OnItemClickListener implementation
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        ProductMenu productMenu = currentProductMenus.get(position);
        ArrayList<ProductMenu> subMenuProducts = new ArrayList<ProductMenu>();

        if(productMenu.isExpandable)
        {
            if(!productMenu.isExpanded){

                for(ProductMenu productMenu1:productMenus){

                    if(productMenu1.masterCategory == productMenu.productCategoryId){
                        productMenu1.show = true;

                        productMenu1.level = productMenu.level+1;
                        Log.d("for", "adding level is " + productMenu1.level);
                        subMenuProducts.add(productMenu1);
                    }
                }

                if(subMenuProducts.size() > 0){
                    productMenu.isExpanded = true;
                    currentProductMenus.addAll(position+1,subMenuProducts);
                }
                else
                {
                    Log.d("onItemClick","Final row encountered at position "+ position);
                    Intent intent = new Intent(this, ProductListActivity.class);
                    intent.putExtra("categoryID",productMenu.productCategoryId);
                    intent.putExtra("categoryName",productMenu.name);
                    startActivity(intent);

                    mDrawerLayout.closeDrawer(productMenuListView);
                }
            }
            else
            {
                /*
                This means user has tab on the cell to close the list. You need to remove all the cells from the array and set them to
                ideal state
                 */

                productMenu.isExpanded = false;

                int index = position+1;

                while(true){

                    ProductMenu productMenu1 = currentProductMenus.get(index);

                    if(productMenu1.level > productMenu.level){
                        productMenu1.level = -1;
                        productMenu1.show = false;
                        productMenu1.isExpanded = false;

                        currentProductMenus.remove(index);
                    }
                    else {
                        break;
                    }
                }
            }
        }
        else
        {
            Log.d("onItemClick","Go to Product List page with category id "+productMenu.productCategoryId);
            Intent intent = new Intent(this, ProductListActivity.class);
            intent.putExtra("categoryID",productMenu.productCategoryId);
            intent.putExtra("categoryName",productMenu.name);
            startActivity(intent);

            mDrawerLayout.closeDrawer(productMenuListView);
        }


        productMenuListViewAdapter.notifyDataSetChanged();


    }

    //ListView Adapter

    public static class ProductMenuViewHolder{
        TextView productMenuTextView;
        ImageView indicatorImageView;

    }

    public class ProductMenuListViewAdapter extends BaseAdapter {

        LayoutInflater layoutInflater;

        public static final int LEVEL_0_CELL     =   0;
        public static final int LEVEL_1_CELL     =   1;
        public static final int LEVEL_2_CELL     =   2;

        public ProductMenuListViewAdapter(){
            layoutInflater = (LayoutInflater)HealthShopHomeActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return currentProductMenus.size();
        }

        @Override
        public Object getItem(int position) {
            //return productMenus.get(position);
            return currentProductMenus.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView == null){


                //Need to first check which type of cell is need to be configured

                if(getItemViewType(position) == LEVEL_0_CELL){

                    convertView = layoutInflater.inflate(R.layout.cell_category_menu_level_0,null);

                    ProductMenuViewHolder viewHolder = new ProductMenuViewHolder();
                    viewHolder.productMenuTextView = (TextView) convertView.findViewById(R.id.category_menu_title);
                    viewHolder.indicatorImageView = (ImageView) convertView.findViewById(R.id.category_menu_indicator);

                    convertView.setTag(viewHolder);


                }
                else if(getItemViewType(position) == LEVEL_1_CELL){

                    convertView = layoutInflater.inflate(R.layout.cell_category_menu_level_1,null);

                    ProductMenuViewHolder viewHolder = new ProductMenuViewHolder();
                    viewHolder.productMenuTextView = (TextView) convertView.findViewById(R.id.category_menu_title);
                    viewHolder.indicatorImageView = (ImageView) convertView.findViewById(R.id.category_menu_indicator);

                    convertView.setTag(viewHolder);


                }
                else if(getItemViewType(position) == LEVEL_2_CELL){

                    convertView = layoutInflater.inflate(R.layout.cell_category_menu_level_2,null);

                    ProductMenuViewHolder viewHolder = new ProductMenuViewHolder();
                    viewHolder.productMenuTextView = (TextView) convertView.findViewById(R.id.category_menu_title);
                    viewHolder.indicatorImageView = (ImageView) convertView.findViewById(R.id.category_menu_indicator);

                    convertView.setTag(viewHolder);

                }

            }

            //ProductMenu productMenu = productMenus.get(position);
            ProductMenu productMenu = currentProductMenus.get(position);

            if(getItemViewType(position) == LEVEL_0_CELL){

                ProductMenuViewHolder viewHolder = (ProductMenuViewHolder)convertView.getTag();
                viewHolder.productMenuTextView.setText(productMenu.name);

                if(productMenu.isExpandable && !productMenu.isExpanded){
                    viewHolder.indicatorImageView.setVisibility(View.VISIBLE);
                    viewHolder.indicatorImageView.setImageBitmap(BitmapFactory.decodeResource(HealthShopHomeActivity.this.getResources(), android.R.drawable.ic_input_add));
                }
                else if(productMenu.isExpandable && productMenu.isExpanded){
                    viewHolder.indicatorImageView.setVisibility(View.VISIBLE);
                    viewHolder.indicatorImageView.setImageBitmap(BitmapFactory.decodeResource(HealthShopHomeActivity.this.getResources(),android.R.drawable.ic_delete));
                }
                else {
                    viewHolder.indicatorImageView.setVisibility(View.INVISIBLE);
                }
            }
            else if(getItemViewType(position) == LEVEL_1_CELL){

                ProductMenuViewHolder viewHolder = (ProductMenuViewHolder)convertView.getTag();
                viewHolder.productMenuTextView.setText(productMenu.name);

                if(productMenu.isExpandable && !productMenu.isExpanded){
                    viewHolder.indicatorImageView.setVisibility(View.VISIBLE);
                    viewHolder.indicatorImageView.setImageBitmap(BitmapFactory.decodeResource(HealthShopHomeActivity.this.getResources(),android.R.drawable.ic_input_add));
                }
                else if(productMenu.isExpandable && productMenu.isExpanded){
                    viewHolder.indicatorImageView.setVisibility(View.VISIBLE);
                    viewHolder.indicatorImageView.setImageBitmap(BitmapFactory.decodeResource(HealthShopHomeActivity.this.getResources(),android.R.drawable.ic_delete));
                }
                else {
                    viewHolder.indicatorImageView.setVisibility(View.INVISIBLE);
                }
            }
            else if(getItemViewType(position) == LEVEL_2_CELL){

                ProductMenuViewHolder viewHolder = (ProductMenuViewHolder)convertView.getTag();
                viewHolder.productMenuTextView.setText(productMenu.name);

                if(productMenu.isExpandable && !productMenu.isExpanded){
                    viewHolder.indicatorImageView.setVisibility(View.VISIBLE);
                    viewHolder.indicatorImageView.setImageBitmap(BitmapFactory.decodeResource(HealthShopHomeActivity.this.getResources(),android.R.drawable.ic_input_add));
                }
                else if(productMenu.isExpandable && productMenu.isExpanded){
                    viewHolder.indicatorImageView.setVisibility(View.VISIBLE);
                    viewHolder.indicatorImageView.setImageBitmap(BitmapFactory.decodeResource(HealthShopHomeActivity.this.getResources(),android.R.drawable.ic_delete));
                }
                else {
                    viewHolder.indicatorImageView.setVisibility(View.INVISIBLE);
                }
            }

            return convertView;
        }

        //Following methods are needed for returning multiple cells in the list view
        @Override
        public int getViewTypeCount() {
            return 3;
        }

        @Override
        public int getItemViewType(int position) {

            ProductMenu productMenu = currentProductMenus.get(position);

            if(productMenu.level == 0)
                return LEVEL_0_CELL;
            else if(productMenu.level == 1)
                return LEVEL_1_CELL;
            else if(productMenu.level == 2)
                return LEVEL_2_CELL;
            else
                return LEVEL_2_CELL;
        }
    }

}
