package com.healthsaverz.healthmobile.medicine.reminder.modal;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;

/**
 * Created by Ibrahim on 17-10-2014.
 */
public class DaysIntervalDialog extends DialogFragment implements View.OnTouchListener {

    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String TAG = "DaysIntervalDialog";
    public String[] daysOfWeek = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
    private Context context;

    DaysIntervalListener daysIntervalListener;




    public interface DaysIntervalListener {
        void onDayIntervalChange(String quantity);
    }

    private TextView textShow;

    public TextView getTextShow() {
        if (textShow == null) {
            textShow = (TextView) getView().findViewById(R.id.textShow);
        }
        return textShow;
    }

    private ImageView minus;
    public ImageView getMinus() {
        if (minus == null) {
            minus = (ImageView) getView().findViewById(R.id.minus);
        }
        return minus;
    }

    private ImageView plus;
    public ImageView getPlus() {
        if (plus == null) {
            plus = (ImageView) getView().findViewById(R.id.plus);

        }
        return plus;
    }

    private TextView quantityText;
    public TextView getQuantityText() {
        if (quantityText == null) {
            quantityText = (TextView) getView().findViewById(R.id.quantityText);

        }
        return quantityText;
    }

    private Button setButton;
    public Button getVerifyButton() {
        if (setButton == null) {
            setButton = (Button) getView().findViewById(R.id.setButton);
            setButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                        ViewHelper.fadeOut(view, null);
                        return true;
                    } else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                        final int viewID = view.getId();
                        ViewHelper.fadeIn(view, null);

                        if (daysIntervalListener != null){
                            daysIntervalListener.onDayIntervalChange(quantityText.getText().toString());
                        }
                        dismiss();
                        return true;


                    }
                    return false;
                }
            });
        }
        return setButton;
    }

    private Button cancelButton;
    public Button getCancelButton() {
        if (cancelButton == null) {
            cancelButton = (Button) getView().findViewById(R.id.cancelButton);
            cancelButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    //UseCases.saveRegistrationStatus(getActivity(), false);
                    if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                        ViewHelper.fadeOut(view, null);
                        return true;
                    } else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                        final int viewID = view.getId();
                        ViewHelper.fadeIn(view, null);

                        if (daysIntervalListener != null){
                            daysIntervalListener.onDayIntervalChange(null);
                        }
                        dismiss();
                        return true;
                    }
                    return false;
                }
            });
        }
        return cancelButton;
    }

    public static DaysIntervalDialog newInstance(DaysIntervalListener timeSetlistener){
        DaysIntervalDialog daysIntervalDialog = new DaysIntervalDialog();
        daysIntervalDialog.daysIntervalListener = timeSetlistener;
        return daysIntervalDialog;
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dialog_days_interval, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getVerifyButton();
        getCancelButton();
        //code = getQuantityText().getText().toString().trim();
        getPlus().setOnTouchListener(this);
        getMinus().setOnTouchListener(this);
        getQuantityText();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_SmileWindow;
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return dialog;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(view, null);
            return true;
        } else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
            final int viewID = view.getId();
            ViewHelper.fadeIn(view, null);
            String numberOnly= quantityText.getText().toString().replaceAll("[^0-9]", "");
            switch (view.getId()){
                case R.id.plus:
                    int add = Integer.parseInt(numberOnly);
                    add = add + 1;
                    quantityText.setText("every "+String.valueOf(add) + " days");
//                    int i = 7/add;
//                    getTextShow().setText();
                    break;
                case R.id.minus:
                    int sub = Integer.parseInt(numberOnly);
                    if (sub > 1){
                        sub = sub - 1;
                        quantityText.setText("every "+String.valueOf(sub) + " days");
                    }
                    break;

            }
//            if (validate()){
//                final String code = verifyCode.getText().toString().trim();
//                //final String systemCode = PreferenceManager.getStringForKey(getActivity(), PreferenceManager.VERIFICATION_CODE, "");
//                if (!code.equals(systemCode)) {
//                    //Toast.makeText(getActivity(), "Invalid Verification Code", Toast.LENGTH_SHORT).show();
//                    invalidVerificationCode();
//                } else {
//                    PreferenceManager.saveStringForKey(getActivity(), PreferenceManager.IS_VERIFICATION_STATUS, VerifyFragment.TRUE);
//                    //Toast.makeText(getActivity(), "Successfully Registered", Toast.LENGTH_SHORT).show();
//                    callbacks.didPressView(viewID);
//                    dismiss();
//                    return true;
//                }
//            }


        }
        return false;
    }
}

