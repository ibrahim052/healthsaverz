package com.healthsaverz.healthmobile.medicine.bookaservice.controller;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
import com.healthsaverz.healthmobile.medicine.networkpartners.modal.LocationPartners;
import com.healthsaverz.healthmobile.medicine.networkpartners.modal.NetworkPartnerModal;
import com.healthsaverz.healthmobile.medicine.networkpartners.modal.NetworkPartnerParser;
import com.healthsaverz.healthmobile.medicine.pieChart.modal.DemoBase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by healthsaverz5 on 3/24/2015.
 *
 * @author: Priyanka Kale
 */
public class BookAServiceActivity extends DemoBase implements View.OnTouchListener, View.OnClickListener, DateTimeDialog.TimeSetlistener, NetworkPartnerParser.NetworkPartnerParserLitnr, AsyncLoaderNew.Listener, AdapterView.OnItemSelectedListener {

    private static final String TAG = "BookServiceActivity";
    private static final String METHOD_GET_LOCATION_FORPARTNER = AppConstants.WEB_DOMAIN+"/sHealthSaverz/jaxrs/MappNetworkPartnerDetailsServices/getMappNetworkPartnerDetailsOnId";// uncomment after live
    //private static final String METHOD_GET_LOCATION_FORPARTNER = "http://192.168.2.139:8080/sHealthSaverz/jaxrs/MappNetworkPartnerDetailsServices/getMappNetworkPartnerDetailsOnId";
       private static final String METHOD_BOOK_A_SERVICE = AppConstants.WEB_DOMAIN+"/sHealthSaverz/jaxrs/BookAServiceServices/addBookAService";
    //private static final String METHOD_BOOK_A_SERVICE = "http://192.168.2.139:8080/sHealthSaverz/jaxrs/BookAServiceServices/addBookAService";

    private List<Integer> listNetworkPartner;
    private List<LocationPartners> listLocationPartnersList;
    private List<String> listLocationPartnersNameList;
    private int partnerIdSelected, partnerLocationIdSelected;
    private List<String> listNetworkPartnerNList;
    private ArrayAdapter<String> datalocAdapter;

    private String patientName, patientMobileNumber;
    private String format = "";
    private boolean isSubmitRequestExecuted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookaservice);
        getActionBar().setTitle("Book A Service");
        getNetworkPartnersFromSever();
        initializeViews();
    }

    public void getNetworkPartnersFromSever() {
        /**
         *  get network partners from server
         *
         *  Initial Service called
         */
        NetworkPartnerParser networkParser = new NetworkPartnerParser(this);
        networkParser.setNetworkPartnerParserLitnr(this);
        networkParser.getNetworkPartnerFromSever();

    }

    private void initializeViews() {
        lblSelectedDateTime();
        getNetworkPartners();
        getLocation();
        listLocationPartnersNameList = new ArrayList<String>();
        listNetworkPartnerNList = new ArrayList<String>();

        if (listLocationPartnersNameList != null) {
            listLocationPartnersNameList.add(0, spnLocation.getPrompt().toString());
        }

        datalocAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listLocationPartnersNameList);
        datalocAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnLocation.setAdapter(datalocAdapter);


        selectDateAndTime();
        getSelectionYes();
        getSelectionNo();
        getPatientDetails();
        selectDateAndTime().setOnClickListener(this);
        getSelectionYes().setOnTouchListener(this);
        getSelectionNo().setOnTouchListener(this);
        submitDetails();
        submitDetails().setOnClickListener(this);
    }

    private RelativeLayout linearLytPDetails;

    private RelativeLayout getPatientDetails() {
        if (linearLytPDetails == null) {
            linearLytPDetails = (RelativeLayout) findViewById(R.id.linearlytPdetails);
        }
        return linearLytPDetails;
    }


    private RadioButton radioYes;

    public RadioButton getSelectionYes() {
        if (radioYes == null) {
            radioYes = (RadioButton) findViewById(R.id.radioYes);
        }
        return radioYes;
    }

    private RadioButton radioNo;

    public RadioButton getSelectionNo() {
        if (radioNo == null) {
            radioNo = (RadioButton) findViewById(R.id.radioNo);
        }
        return radioNo;
    }


    private Spinner spnNetworkPartner;

    private Spinner getNetworkPartners() {
        if (spnNetworkPartner == null) {
            spnNetworkPartner = (Spinner) this.findViewById(R.id.spnNtwkPartner);
        }
        return spnNetworkPartner;
    }

    private Spinner spnLocation;

    private Spinner getLocation() {
        if (spnLocation == null) {
            spnLocation = (Spinner) this.findViewById(R.id.spnLocation);
        }
        return spnLocation;
    }

    private ImageButton imgBtnCaldt;

    private ImageButton selectDateAndTime() {
        if (imgBtnCaldt == null) {
            imgBtnCaldt = (ImageButton) this.findViewById(R.id.imgBtnCaldt);
        }
        return imgBtnCaldt;
    }

    private EditText edtMobile;

    private EditText getPatientMobile() {
        if (edtMobile == null) {
            edtMobile = (EditText) this.findViewById(R.id.edtCellNo);
        }
        return edtMobile;
    }

    private EditText edtName;

    private EditText getPatientName() {
        if (edtName == null) {
            edtName = (EditText) this.findViewById(R.id.edtFullName);
        }
        return edtName;
    }

    private Button btnSubmit;

    private Button submitDetails() {
        if (btnSubmit == null) {
            btnSubmit = (Button) this.findViewById(R.id.btnSubmit);
        }
        return btnSubmit;
    }

    private EditText selectedDateTimeTxt;

    private EditText lblSelectedDateTime() {
        if (selectedDateTimeTxt == null) {
            selectedDateTimeTxt = (EditText) this.findViewById(R.id.lblSelectedDateTime);
        }
        return selectedDateTimeTxt;
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(v, null);
            return true;
        }
        if (event.getActionMasked() == MotionEvent.ACTION_UP) {
            ViewHelper.fadeIn(v, null);
            getPatientName();
            getPatientMobile();
            switch (v.getId()) {
                case R.id.radioNo:
                    radioNo.setChecked(true);
                    linearLytPDetails.setVisibility(View.VISIBLE);
                    break;
                case R.id.radioYes:
                    radioYes.setChecked(true);
                    linearLytPDetails.setVisibility(View.GONE);
                    break;
            }
            return true;
        }
        return false;
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imgBtnCaldt) {
            DateTimeDialog.newInstance(BookAServiceActivity.this).show(getFragmentManager(), TAG);

        } else if (v.getId() == R.id.btnSubmit) {
            bookAService();
        }
    }

    private void bookAService() {

        AsyncLoaderNew asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.POST_HEADER);
        String status = "A";
        int created_By = Integer.parseInt(PreferenceManager.getStringForKey(this, PreferenceManager.ID, ""));
        if (edtName != null) {
            patientName = edtName.getText().toString();
        } else {
            patientName = "";
        }
        if (edtMobile != null) {
            patientMobileNumber = edtMobile.getText().toString();
        } else {
            patientMobileNumber = "";

        }
        String preferredDateAndTime = selectedDateTimeTxt.getText().toString();
        asyncLoaderNew.requestMessage = "<bookAService>" +
                "<clientID>" + "healthsaverz" + "</clientID>" +
                "<sessionID>" + PreferenceManager.getStringForKey(this, PreferenceManager.SESSION_ID, "") + "</sessionID>" +
                "<partnerId>" + partnerIdSelected + "</partnerId>" +//loc
                "<partnerLocationId>" + partnerLocationIdSelected + " </partnerLocationId>" +//loc id
                " <preferredDateAndTime>" + preferredDateAndTime + " </preferredDateAndTime>" +
                "<patientName>" + patientName + " </patientName>" +
                "<patientMobileNumber>" + patientMobileNumber + " </patientMobileNumber>" +
                "<patientAddress>" + "" + " </patientAddress>" +
                "<source>" + "A" + " </source>" +
                "<bookingStatus>" + "N" + "</bookingStatus>" +
                "<actualBookingDateAndTime>" + "N" + " </actualBookingDateAndTime>" +
                " <testCompletedStatus>" + "N" + " </testCompletedStatus>" +
                "<testCompletedDateAndTime>" + "N" + " </testCompletedDateAndTime>" +
                "<testResultUploadedStatus> " + "N" + "</testResultUploadedStatus>" +
                " <testResultUploadedDateAndTime>" + "N" + " </testResultUploadedDateAndTime>" +
                "<reportEmail>" + "N" + " </reportEmail>" +
                "<reportEmailDateAndTime>" + "N" + " </reportEmailDateAndTime>" +
                "<comment>" + " " + " </comment>" +
                "<status>" + status + "</status>" +
                "<created_By>" + created_By + "</created_By>" +
                "<extra_1>" + " " + "</extra_1>" +
                "<extra_2>" + " " + "</extra_2>" +
                "<extra_3>" + " " + "</extra_3>" +
                "<extra_4>" + " " + "</extra_4>" +
                "<extra_5>" + "" + "</extra_5>" +
                " </bookAService>";

        Log.d(TAG, "REQUEST-------" + asyncLoaderNew.requestMessage);

        String value = null;


        if ((partnerIdSelected == 0 || partnerLocationIdSelected == 0) || (selectedDateTimeTxt.getText().length() == 0)) {
            showAlertDialog();
            Log.d(TAG + "", "4444444");

        } else {
            if (linearLytPDetails.getVisibility() == View.VISIBLE) {
                if (patientName.equalsIgnoreCase("") || patientMobileNumber.equalsIgnoreCase("")) {
                    showAlertDialog();

                } else {
                    isSubmitRequestExecuted = true;
                    asyncLoaderNew.setListener(this);
                    asyncLoaderNew.executeRequest(METHOD_BOOK_A_SERVICE);
                }
            } else {

                isSubmitRequestExecuted = true;
                asyncLoaderNew.setListener(this);
                asyncLoaderNew.executeRequest(METHOD_BOOK_A_SERVICE);
            }
        }

    }

    private void showAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Please Fill Details");
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }

    /*
       overridden methods of networkparserlistner
     */
    @Override
    public void networkPartnerParserDidReceivedMenu(NetworkPartnerModal netwkPartner, List<String> listNetworkPartnerNameList, List<Integer> listNetworkPartnerIDList, List<NetworkPartnerModal> listNetworkPartnerList) {
        listNetworkPartner = listNetworkPartnerIDList;
        listNetworkPartnerNList = listNetworkPartnerNameList;

        if (listNetworkPartnerNList != null) {
            listNetworkPartnerNList.add(0, spnNetworkPartner.getPrompt().toString());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listNetworkPartnerNList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnNetworkPartner.setAdapter(dataAdapter);
        dataAdapter.notifyDataSetChanged();
        spnNetworkPartner.setOnItemSelectedListener(this);
    }

    @Override
    public void networkPartnerParserDidReceivedConnectionError(AsyncLoaderNew.Status status) {
        Log.d(TAG, " networkPartnerParserDidReceivedConnectionError:::::" + status);

    }

    @Override
    public void networkPartnerParserDidReceivedProcessingError(String message) {
        Log.d(TAG, " networkPartnerParserDidReceivedProcessingError:::::::" + message);

    }

    /**
     * @param status // asyncLoader.executeRequest(METHOD_GET_LOCATION_FORPARTNER
     */
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {
        Log.d(TAG, " didReceivedError:::::::" + status);

    }

    @Override
    public void didReceivedData(byte[] data) {

        if (isSubmitRequestExecuted) {
            String responseString = new String(data);
            Log.d(TAG, "isSubmitRequestExecuted::" + responseString);
            isSubmitRequestExecuted = false;
            Toast.makeText(this, responseString, Toast.LENGTH_SHORT).show();
            finish();

        } else {
        /*
            getting response of URL METHOD_GET_LOCATION_FORPARTNER (2nd hit data received)
         */
            String responseString = new String(data);
            listLocationPartnersList = new ArrayList<LocationPartners>();

            try {
                JSONArray jsonArray = new JSONArray(responseString);

                listLocationPartnersNameList.clear();
                listLocationPartnersNameList.add(0, spnLocation.getPrompt().toString());
                spnLocation.setSelection(0);

                for (int i = 0; i < jsonArray.length(); i++) {

                    LocationPartners locationPartners = new LocationPartners();
                    JSONObject jsonObj = jsonArray.getJSONObject(i);
                    locationPartners.id = jsonObj.getInt("id");
                    locationPartners.location = jsonObj.getString("location");
                    locationPartners.status = jsonObj.getString("status");
                    locationPartners.partnerId = jsonObj.getInt("partnerId");
                    listLocationPartnersNameList.add(locationPartners.location);
                    listLocationPartnersList.add(locationPartners);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            datalocAdapter.notifyDataSetChanged();
            spnLocation.setOnItemSelectedListener(this);
        }
    }

    @Override
    public void didReceivedProgress(Double progress) {
        Toast.makeText(this, "" + progress, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

         /*  if (parent.getChildAt(0) != null) {
            ((TextView) parent.getChildAt(0)).setTextColor(Color.RED);
         }*/
        if (parent.getId() == R.id.spnNtwkPartner) {

            if (position > 0) {
                int ntwkPartnerId = listNetworkPartner.get(position - 1);
        /*
           get Location for Network Partners from Server (2nd HIT on page)
        */
                AsyncLoaderNew asyncLoader = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.GET);
                String sessionID = PreferenceManager.getStringForKey(this, PreferenceManager.SESSION_ID, "");
                String clientID = "healthsaverz";
                asyncLoader.setListener(this);
                asyncLoader.executeRequest(METHOD_GET_LOCATION_FORPARTNER + "/" + clientID + "/" + sessionID + "/PartnerId" + "/" + ntwkPartnerId);
            }
        } else if (parent.getId() == R.id.spnLocation) {
            if (position > 0) {
                partnerIdSelected = listLocationPartnersList.get(position - 1).partnerId;
                partnerLocationIdSelected = listLocationPartnersList.get(position - 1).id;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onTimeChange(Calendar calendar, Integer currentHour, Integer currentMinute) {

        showTime(calendar, currentHour, currentMinute);
    }

    public void showTime(Calendar calendar, int hour, int min) {
        if (hour == 0) {
            hour += 12;
            format = "AM";
        } else if (hour == 12) {
            format = "PM";
        } else if (hour > 12) {
            hour -= 12;
            format = "PM";
        } else {
            format = "AM";
        }

     /*   time.setText(new StringBuilder().append(hour).append(" : ").append(min)
                .append(" ").append(format));*/
        selectedDateTimeTxt.setText(calendar.get(Calendar.DAY_OF_MONTH) + "-" + calendar.get(Calendar.MONTH) + "-" + calendar.get(Calendar.YEAR) + " " + new StringBuilder().append(hour).append(":").append(min).append(" ").append(format));
    }
}

