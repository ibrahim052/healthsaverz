package com.healthsaverz.healthmobile.medicine.uploadPrescription.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.healthsaverz.healthmobile.medicine.R;

import java.util.ArrayList;

/**
 * Created by Ibrahim on 06-11-2014.
 */
public class GridImageAdapter extends ArrayAdapter<Bitmap> {
    ArrayList<Bitmap> arrayList;
    private Context context ;
    private LayoutInflater inflater;
    View.OnClickListener deleteClick;

    public GridImageAdapter(Context context,int resource, ArrayList<Bitmap> arrayList, View.OnClickListener deleteClick) {
        super(context, resource, arrayList);
        this.arrayList = arrayList;
        this.context = context;
        this.deleteClick = deleteClick;
        this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Bitmap getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (convertView == null){
            convertView = inflater.inflate(R.layout.cell_image_grid, null);
            holder = new ViewHolder();
            holder.image = (ImageView) convertView.findViewById(R.id.gridImage);
            holder.closeImage = (ImageView) convertView.findViewById(R.id.deleteImage);
            holder.closeImage.setOnClickListener(deleteClick);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.image.setImageBitmap(arrayList.get(i));
        return convertView;
    }
    static class ViewHolder {
        ImageView image;
        ImageView closeImage;
    }
}
