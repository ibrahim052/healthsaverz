package com.healthsaverz.healthmobile.medicine.pieChart.controller;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.global.AnimatedExpandableListView;
import com.healthsaverz.healthmobile.medicine.medicine.controller.MedicineDBHandler;
import com.healthsaverz.healthmobile.medicine.medicine.modal.ReminderModal;
import com.healthsaverz.healthmobile.medicine.pieChart.modal.MedicineReportAdapter;
import com.healthsaverz.healthmobile.medicine.pieChart.modal.MedicineReportModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

public class  AdherenceReportActivity extends Activity {

    private ArrayList<ReminderModal> reminders;
    HashMap<MedicineReportModel, List<String>> listDataChild;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_adherence_report);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        SimpleDateFormat formatter = new SimpleDateFormat("MMM-dd EEE hh:mm a");
        SimpleDateFormat formatter1 = new SimpleDateFormat("hh:mm a");
        getActionBar().setTitle("Adherence Report");
        ExpandableListView reportList = (ExpandableListView) findViewById(R.id.reportList);
        TextView titleText = (TextView) findViewById(R.id.titleText);
        int days = getIntent().getIntExtra("daysResult", 0);
        int type = getIntent().getIntExtra("type", 3);

        switch (days){
            case 15:
                titleText.setText("15 DAYS");
                break;
            case 30:
                titleText.setText("30 DAYS");
                break;
            case 91:
                titleText.setText("3 MONTHS");
                break;
            case 183:
                titleText.setText("6 MONTHS");
                break;
            default:
                titleText.setText("");
                break;
        }
        switch (type){
            case 0:
                titleText.append(" (MISSED)");
                break;
            case 1:
                titleText.append(" (ON TIME)");
                break;
            case 2:
                titleText.append(" (LATE)");
                break;
            default:

                break;
        }
        MedicineDBHandler medicineDBHandler = new MedicineDBHandler(getApplicationContext());
        listDataChild = new HashMap<>();
        ArrayList<MedicineReportModel> medicineReportModelArrayList = medicineDBHandler.getAllMedicinesWITHReminderCountForFixedDays(days, type);
        Calendar calendar = new GregorianCalendar();
        Calendar calendar2 = new GregorianCalendar();
        for (MedicineReportModel reportModel: medicineReportModelArrayList){
            ArrayList<ReminderModal> reminderModals = medicineDBHandler.getRemindersForPastDates(days, reportModel.medID);
            ArrayList<String> times = new ArrayList<>();
            for (ReminderModal modal : reminderModals){
                calendar.setTimeInMillis(Long.parseLong(modal.get_time()));
                String time = formatter.format(calendar.getTime());
                String finalTime;
                if (!modal._taken_time.equals("")){
                    calendar2.setTimeInMillis(Long.parseLong(modal._taken_time));
                    String timeTaken = formatter1.format(calendar2.getTime());
                    finalTime = time+" | "+timeTaken;
                }
                else {
                    finalTime = time;
                }
                times.add(finalTime);
            }
            listDataChild.put(reportModel, times);
        }
        MedicineReportAdapter reportAdapter = new MedicineReportAdapter(this, medicineReportModelArrayList, listDataChild);
        reportList.setAdapter(reportAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_adherence_report, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            //Toast.makeText(this, "Setting clicked", Toast.LENGTH_SHORT).show();
            finish();
            return true;
        }
        return false;
    }
}
