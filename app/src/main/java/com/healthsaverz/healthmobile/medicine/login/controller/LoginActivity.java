package com.healthsaverz.healthmobile.medicine.login.controller;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.DialogProgressFragment;
import com.healthsaverz.healthmobile.medicine.global.UseCases;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
import com.healthsaverz.healthmobile.medicine.login.modal.ForgotPassword;
import com.healthsaverz.healthmobile.medicine.login.modal.ForgotPasswordDialog;
import com.healthsaverz.healthmobile.medicine.login.modal.ForgotPasswordParser;
import com.healthsaverz.healthmobile.medicine.login.modal.LoginUserParser;
import com.healthsaverz.healthmobile.medicine.mainscreen.controller.MainScreenActivity;
import com.healthsaverz.healthmobile.medicine.register.controller.RegisterActvity;

public class LoginActivity extends Activity implements View.OnTouchListener, LoginUserParser.LoginUserParserListener, ForgotPasswordDialog.ForgotPassListener, ForgotPasswordParser.MedFriendParserListener {

    EditText userName;
    EditText password;

    //Button action_cancel;
    Button action_login;
    Button action_signUp;
    Button forgotPass;

    DialogProgressFragment progressFragment;

    public static final String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getActionBar().hide();
        if(password == null){
            password = (EditText)findViewById(R.id.password);
        }
        if(userName == null){
            userName = (EditText)findViewById(R.id.userName);

            AccountManager accountManager = AccountManager.get(this);
            Account[] accounts = accountManager.getAccountsByType("com.google");

            Account account = (accounts.length > 0)?accounts[0]:null;
            if (account != null) {
                userName.setText("");
                userName.append(account.name);
            }
        }
//        if(action_cancel == null){0
//            action_cancel = (Button)findViewById(R.id.action_cancel);
//            action_cancel.setOnTouchListener(this);
//        }
        if(action_login == null){
            action_login = (Button)findViewById(R.id.action_login);
            action_login.setOnTouchListener(this);
        }
        if(action_signUp == null){
            action_signUp = (Button)findViewById(R.id.action_signUp);
            action_signUp.setOnTouchListener(this);
        }
        if(forgotPass == null){
            forgotPass = (Button)findViewById(R.id.forgotPass);
            forgotPass.setOnTouchListener(this);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(v, null);
            return true;
        }
        if (event.getActionMasked() == MotionEvent.ACTION_UP) {
            ViewHelper.fadeIn(v, null);
            if(v.getId() == R.id.action_login) {

                boolean isValid =  validateForm();
                if(isValid)
                {
                    progressFragment =  new DialogProgressFragment();
                    progressFragment.show(getFragmentManager(), TAG);
                    progressFragment.setCancelable(false);

                    LoginUserParser loginUserParser = new LoginUserParser(this);
                    loginUserParser.loginUser(userName.getText().toString().trim(),password.getText().toString().trim());
                    loginUserParser.setLoginUserListener(this);
                }

            }
            else if(v.getId() == R.id.action_signUp)
            {
                startActivity(new Intent(this, RegisterActvity.class));
            }
            else if(v.getId() == R.id.action_cancel) {

                onBackPressed();
            }
            else if(v.getId() == R.id.forgotPass) {
                ForgotPasswordDialog.newInstance(this).show(getFragmentManager(), "LoginActivity");
            }
            return true;
        }
        return false;
    }

    public  boolean validateForm(){

        String errorMessage = null;
        boolean isErrorOccured = false;

        if(userName.getText().toString().trim().equals("")){

            errorMessage = "Email id can not be empty.";
            isErrorOccured = true;
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(userName.getText().toString().trim()).matches())
        {
            errorMessage = "Invalid email id.";
            isErrorOccured = true;
        }
        else if(password.getText().toString().trim().equals("")){

            errorMessage = "Password can not be empty";
            isErrorOccured = true;
        }
//        else if (password.getText().toString().trim().length() <= 6)
//        {
//            errorMessage = "Password should have atleast 6 characters.";
//            isErrorOccured = true;
//        }

        if(isErrorOccured){

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            // Setting Dialog Title
            alertDialog.setTitle("Validation Error");
            // Setting Dialog Message
            alertDialog.setMessage(errorMessage);

            // Setting Positive "OK" Btn
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            dialog.cancel();
                        }
                    });
            // Showing Alert Dialog
            alertDialog.show();

            return false;
        }
        else {

            return true;
        }


    }

    //LoginUserParser.LoginUserParserListener implements
    @Override
    public void loginUserParserDidUserLoggedIn(String message) {

        if(message.equals("100")){
            progressFragment.dismiss();
            progressFragment = null;

            UseCases.startActivityByClearingStack(this, MainScreenActivity.class);
            Toast.makeText(this, "User Logged In", Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    @Override
    public void loginUserParserDidReceivedConnectionError(AsyncLoaderNew.Status status) {
        if (progressFragment != null){
            progressFragment.dismiss();
            progressFragment = null;
        }



        //Log.d("registerUserParserDidReceivedConnectionError"," "+status);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Login Error");
        alertDialog.setMessage(AppConstants.ConnectionError(status));
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }

    @Override
    public void loginUserParserDidReceivedProcessingError(String message) {
        progressFragment.dismiss();
        progressFragment = null;

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Login Error");
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }

    @Override
    public void forgotPassword(ForgotPassword password) {
        if (password != null){
            progressFragment =  new DialogProgressFragment();
            progressFragment.show(getFragmentManager(), TAG);
            progressFragment.setCancelable(false);

            ForgotPasswordParser parser = new ForgotPasswordParser(this);
            parser.setMedFriendParserListener(this);
            parser.forgotPassword(password);
        }
    }

    private void getalert(String title, String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Positive "OK" Btn
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }

    @Override
    public void medFriendParserDidReceiveData(Object message) {
        progressFragment.dismiss();
        progressFragment = null;
        if (message != null){
            String j = (String)message;
            switch (j) {
                case "Please check mail, password has been sent.":
                    getalert("Success", j);
                    break;
                case "Please enter valid username and email address":
                    getalert("Failure", j);
                    break;
                default:
                    getalert("Failure", "Something went wrong");
                    break;
            }
        }
    }

    @Override
    public void medFriendParserDidReceivedConnectionError(AsyncLoaderNew.Status status) {
        progressFragment.dismiss();
        progressFragment = null;
        getalert("Failure", "Connection Error");

    }

    @Override
    public void medFriendParserDidReceivedProcessingError(String message) {
        progressFragment.dismiss();
        progressFragment = null;
        getalert("Failure", "Something went wrong");

    }
}
