package com.healthsaverz.healthmobile.medicine.reminder.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
import com.healthsaverz.healthmobile.medicine.medicine.controller.MedicineDBHandler;
import com.healthsaverz.healthmobile.medicine.medicine.modal.Medicine;
import com.healthsaverz.healthmobile.medicine.medicine.modal.ReminderModal;
import com.healthsaverz.healthmobile.medicine.reminder.modal.RefillPillsParser;

public class RefillActivity extends Activity implements View.OnTouchListener {

    int medicine_id;
    int reminder_id;
    int notificationtype;

    private MedicineDBHandler dbHandler;
    private Medicine medicine;
    private String[] months = {
            "Jan", "Feb", "Mar", "Apr",
            "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec"
    };


    private ImageView minus;
    private float threshold = -1;

    public ImageView getMinus() {
        if (minus == null) {
            minus = (ImageView) this.findViewById(R.id.minus);

        }
        return minus;
    }

    private Button callUsButton;
    public Button getCallUsButton(){
        if (callUsButton == null){
            callUsButton = (Button)findViewById(R.id.callUsButton);
        }
        return callUsButton;
    }
    private ImageView plus;

    public ImageView getPlus() {
        if (plus == null) {
            plus = (ImageView) this.findViewById(R.id.plus);

        }
        return plus;
    }

    private TextView medicineName;

    public TextView getMedicineName() {
        if (medicineName == null) {
            medicineName = (TextView) this.findViewById(R.id.medicineName);

        }
        return medicineName;
    }

    private TextView medDetail;

    public TextView getMedDetail() {
        if (medDetail == null) {
            medDetail = (TextView) this.findViewById(R.id.medDetail);

        }
        return medDetail;
    }
    private ImageView medImage;
    public ImageView getMedImage() {
        if (medImage == null) {
            medImage = (ImageView) this.findViewById(R.id.medImage);

        }
        return medImage;
    }
    private TextView medTime;

    public TextView getMedTime() {
        if (medTime == null) {
            medTime = (TextView) this.findViewById(R.id.medTime);

        }
        return medTime;
    }

    private TextView quantity;

    public TextView getQuantity() {
        if (quantity == null) {
            quantity = (TextView) this.findViewById(R.id.quantity);

        }
        return quantity;
    }

    private Button setButton;

    private EditText quantityText;

    public EditText getQuantityText() {
        if (quantityText == null) {
            quantityText = (EditText) findViewById(R.id.quantityText);
        }
        return quantityText;
    }


    public Button getVerifyButton() { //refill button
        if (setButton == null) {
            setButton = (Button) this.findViewById(R.id.setButton);
            setButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                        ViewHelper.fadeOut(view, null);
                        return true;
                    } else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                        final int viewID = view.getId();
                        ViewHelper.fadeIn(view, null);
                        //int i = timepicker.getCurrentHour();

                        //calendar.set(Calendar.MINUTE, );
                        if (medicine.get_pills() != -1) {
                            if (Float.parseFloat(quantityText.getText().toString()) <= threshold ){
                                getAlert("Invalid  Value");
                            }
                            else {
                                Medicine medicine1 = new Medicine(medicine.get_medID(), medicine.get_medName(), medicine.get_patientName(),
                                        medicine.get_shape(), medicine.get_color1(), medicine.get_color2(), medicine.get_startDate(),
                                        medicine.get_endDate(), medicine.get_dose(), medicine.get_unit(), medicine.get_status(),
                                        medicine.get_description(), Float.parseFloat(quantityText.getText().toString()));
                                medicine1._threshold = medicine._threshold;
                                medicine1._tempEndDate = medicine._tempEndDate;

                                dbHandler.updateMedicine(medicine1);
                                if (reminder_id != -1){
                                    ReminderModal reminderModal = dbHandler.findSpecificReminder(reminder_id);
                                    RefillPillsParser refillPillsParser = new RefillPillsParser(RefillActivity.this);
                                    refillPillsParser.updatePills(reminderModal.get_time(), medicine);
                                }

                                finish();
                            }
                        }
                        return true;
                    }
                    return false;
                }
            });
        }
        return setButton;
    }

    private Button cancelButton;

    public Button getCancelButton() {
        if (cancelButton == null) {
            cancelButton = (Button) this.findViewById(R.id.cancelButton);
            cancelButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    //UseCases.saveRegistrationStatus(getActivity(), false);
                    finish();
                    return true;
                }
            });
        }
        return cancelButton;
    }

    private int[] mCards = {
            0,
            0,
            R.drawable.badge1,
            R.drawable.badge2,
            R.drawable.badge3,
            R.drawable.capsule,
            R.drawable.badge5,
            R.drawable.badge6,
            R.drawable.badge7,
            R.drawable.badge8,
            R.drawable.badge9,
            R.drawable.badge10,
            R.drawable.badge11,
            R.drawable.badge12,
            R.drawable.badge14,
            R.drawable.badge15,
            R.drawable.badge16,
            R.drawable.badge17,
            R.drawable.badge18,
            R.drawable.badge19,
            R.drawable.badge20,
            R.drawable.badge21,
            R.drawable.badge22,
            R.drawable.badge23,
            R.drawable.badge24,
            R.drawable.badge25,
            R.drawable.badge26,
            R.drawable.badge27,
            R.drawable.badge28,
            0,
            0};

    private int[] mColors = {
            0,
            0,
            Color.parseColor("#3cbaff"),
            Color.parseColor("#2797d0"),
            Color.parseColor("#005c94"),
            Color.parseColor("#d4ff5f"),
            Color.parseColor("#a9e327"),
            Color.parseColor("#27a227"),
            Color.parseColor("#ffff5c"),
            Color.parseColor("#ff9900"),
            Color.parseColor("#ff7d27"),
            Color.parseColor("#efd597"),
            Color.parseColor("#dba747"),
            Color.parseColor("#936827"),
            Color.parseColor("#ff4141"),
            Color.parseColor("#e12727"),
            Color.parseColor("#c02727"),
            Color.parseColor("#ffae8e"),
            Color.parseColor("#ffa2b2"),
            Color.parseColor("#e9967a"),
            Color.parseColor("#fb9388"),
            Color.parseColor("#f3d2ff"),
            Color.parseColor("#dd82ff"),
            Color.parseColor("#c527ff"),
            Color.parseColor("#2d2d2d"),
            Color.parseColor("#7d7d7d"),
            Color.parseColor("#a9a9a9"),
            Color.parseColor("#d4d4d4"),
            Color.parseColor("#ffffff"),
            Color.parseColor("#000000"),
            0, 0};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refill);

        medicine_id = this.getIntent().getIntExtra("medicine_id", -1);
        //long i = this.getIntent().getLongExtra("time", -1);
        reminder_id = this.getIntent().getIntExtra("reminder_id",-1);
        notificationtype    =   this.getIntent().getIntExtra("notificationtype",-1);
//        Calendar calendar = new GregorianCalendar();
//        calendar.setTimeInMillis(i);
//        calendar.set(Calendar.SECOND, 0);
//        calendar.set(Calendar.MILLISECOND, 0);
        if (medicine_id != -1) {
            dbHandler = new MedicineDBHandler(this);
            medicine = dbHandler.findMedicinefromID(medicine_id);
            threshold = medicine._threshold;
            if (medicine.get_shape() == 5) {

                getMedImage().setColorFilter(null);
                getMedImage().setImageBitmap(mergeBitmap(medicine.get_color1(),medicine.get_color2()));

            }else if(medicine.get_shape() == -1){
                getMedImage().setImageBitmap(BitmapFactory.decodeResource(this.getResources(), mCards[2]));
                getMedImage().setColorFilter(null);
                getMedImage().setColorFilter(mColors[28], PorterDuff.Mode.MULTIPLY);
            }
            else {
                getMedImage().setImageBitmap(BitmapFactory.decodeResource(this.getResources(),mCards[medicine.get_shape()]));
                getMedImage().setColorFilter(null);
                getMedImage().setColorFilter(mColors[medicine.get_color1()], PorterDuff.Mode.MULTIPLY);
            }
            getMedicineName().setText(medicine.get_medName());
            getMedTime().setText("You currently have " + medicine.get_pills());
            getQuantityText().setText("");
            getQuantityText().append(String.valueOf(medicine.get_pills()));

        }
        getVerifyButton();
        getCancelButton();
        getPlus().setOnTouchListener(this);
        getMinus().setOnTouchListener(this);
        getCallUsButton().setOnTouchListener(this);
    }
    public Bitmap mergeBitmap(int colo1,int colo2)
    {

        Bitmap firstBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.badge3left);
        Bitmap secondBitmap = BitmapFactory.decodeResource(this.getResources(),R.drawable.badge3right);

        Bitmap comboBitmap;

        float width, height;

        width = firstBitmap.getWidth() + secondBitmap.getWidth();
        height = firstBitmap.getHeight();

        comboBitmap = Bitmap.createBitmap((int)(width/2), (int)height, Bitmap.Config.ARGB_4444);

        Paint firstPaint = new Paint();
        firstPaint.setColorFilter(new PorterDuffColorFilter(mColors[colo1], PorterDuff.Mode.MULTIPLY));

        Paint secondPaint = new Paint();
        secondPaint.setColorFilter(new PorterDuffColorFilter(mColors[colo2], PorterDuff.Mode.MULTIPLY));

        Canvas comboImage = new Canvas(comboBitmap);
        comboImage.drawBitmap(firstBitmap, 0f, 0f, firstPaint);
        comboImage.drawBitmap(secondBitmap,0f,0f, secondPaint);

        return comboBitmap;

    }
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(view, null);
            return true;
        } else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
            final int viewID = view.getId();
            ViewHelper.fadeIn(view, null);
            switch (view.getId()) {
                case R.id.plus:
                    float add = Float.parseFloat(quantityText.getText().toString());
                    add = add + 1.0f;
                    quantityText.setText("");
                    quantityText.append(String.valueOf(add));
                    return true;
                case R.id.minus:
                    float sub = Float.parseFloat((quantityText.getText().toString()));
                    if (sub > 1.0f) {
                        sub = sub - 1.0f;
                        quantityText.setText("");
                        quantityText.append(String.valueOf(sub));
                    }
                    return true;
                case R.id.callUsButton:
                    String uri = "tel:" + AppConstants.call_us.trim() ;
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(uri));
                    startActivity(intent);
                    return true;
            }
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //m.stop();
    }
    public void getAlert(String message){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle("Validation Error");
        // Setting Dialog Message
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);

        // Setting Positive "OK" Btn
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }
}
