package com.healthsaverz.healthmobile.medicine.global;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by Ibrahim on 30-12-2014.
 */
public class MyCustomRelativeLayout extends RelativeLayout {
    public MyCustomRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        this.setMeasuredDimension(parentWidth, parentHeight*2/3);
        this.setLayoutParams(new RelativeLayout.LayoutParams(parentWidth, parentHeight * 2 / 3));
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
