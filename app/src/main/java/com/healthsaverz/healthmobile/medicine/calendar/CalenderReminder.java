package com.healthsaverz.healthmobile.medicine.calendar;

import android.graphics.Bitmap;

/**
 * Created by priyankranka on 23/10/14.
 */
public class CalenderReminder {

    public boolean isHeader;//
    public String headerTitle;//
    public Bitmap headerIcon;//

    public int medicineID;
    public int reminderID;
    public String medicineName;
    public String medicineDose;
    public String medicineTime;
    public String medicineScheduleDose;
    public int  medicineShape;
    public int  medicineColor1;
    public int  medicineColor2;
    public int medicineStatus; //0- missed and 1-taken

    public String medicineTakenTime;

//Parsecable
    //add time taken
}
