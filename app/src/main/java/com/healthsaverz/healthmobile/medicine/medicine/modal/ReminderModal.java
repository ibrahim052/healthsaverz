package com.healthsaverz.healthmobile.medicine.medicine.modal;

/**
 * Created by Ibrahim on 09-10-2014.
 */
public class ReminderModal {
    private int _remindID;
    private int _medID;
    private String _quantity;
    private String _time;
    private int _qty_type;
    public int _taken;
    /*
    taken =1 taken
    taken =0 missed and default
    taken =-1 skipped
    taken =2 On time
     */
    public String _taken_time;
    public int _snooze = -1;

    public ReminderModal(int _medID, String _quantity, String _time, int _qty_type) {
        this._medID = _medID;
        this._quantity = _quantity;
        this._time = _time;
        this._qty_type = _qty_type;
    }

    public ReminderModal(int _remindID, int _medID, String _quantity, String _time, int _qty_type) {
        this._remindID = _remindID;
        this._medID = _medID;
        this._quantity = _quantity;
        this._time = _time;
        this._qty_type = _qty_type;
    }

    public ReminderModal() {

    }

    public int get_remindID() {
        return _remindID;
    }

    public void set_remindID(int _remindID) {
        this._remindID = _remindID;
    }

    public int get_medID() {
        return _medID;
    }

    public void set_medID(int _medID) {
        this._medID = _medID;
    }

    public String get_quantity() {
        return _quantity;
    }

    public void set_quantity(String _quantity) {
        this._quantity = _quantity;
    }

    public String get_time() {
        return _time;
    }

    public void set_time(String _time) {
        this._time = _time;
    }

    public int get_qty_type() {
        return _qty_type;
    }

    public void set_qty_type(int _qty_type) {
        this._qty_type = _qty_type;
    }
}
