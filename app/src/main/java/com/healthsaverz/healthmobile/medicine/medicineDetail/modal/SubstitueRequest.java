package com.healthsaverz.healthmobile.medicine.medicineDetail.modal;

/**
 * Created by DG on 10/17/2014.
 */
public class SubstitueRequest {



    public String clientID = "healthsaverzm";
    public String sessionID = "test";
   // public String SearchTitle = "";
    public String MedicineName ="";
    public String Strength ="0";
    public String StrengthUnit="0";

    public String StartRow = "0";
    public String EndRow = "10";

    public String productPackagingName	       = "";
    public String packagingSize = "0";
    public String packagingUnit	       = "0";



    public SubstitueRequest(String strength, String strengthUnit, String packagingSize, String packagingUnit, String productPackagingName) {
        Strength = strength;
        StrengthUnit = strengthUnit;
        this.packagingSize = packagingSize;
        this.packagingUnit = packagingUnit;
        this.productPackagingName = productPackagingName;
    }

    public SubstitueRequest(String clientID, String sessionID, String medicineName, String strength, String strengthUnit,
                            String startRow, String endRow, String productPackagingName, String packagingSize, String packagingUnit) {
        this.clientID = clientID;
        this.sessionID = sessionID;
        MedicineName = medicineName;
        Strength = strength;
        StrengthUnit = strengthUnit;
        StartRow = startRow;
        EndRow = endRow;
        this.productPackagingName = productPackagingName;
        this.packagingSize = packagingSize;
        this.packagingUnit = packagingUnit;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

//    public String getSearchTitle() {
//        return SearchTitle;
//    }
//
//    public void setSearchTitle(String searchTitle) {
//        SearchTitle = searchTitle;
//    }


    public String getStartRow() {
        return StartRow;
    }

    public void setStartRow(String startRow) {
        StartRow = startRow;
    }

    public String getEndRow() {
        return EndRow;
    }

    public void setEndRow(String endRow) {
        EndRow = endRow;
    }

    public String getMedicineName() {
        return MedicineName;
    }

    public void setMedicineName(String medicineName) {
        MedicineName = medicineName;
    }

    public String getProductPackagingName() {
        return productPackagingName;
    }

    public void setProductPackagingName(String productPackagingName) {
        this.productPackagingName = productPackagingName;
    }

    public String getPackagingSize() {
        return packagingSize;
    }

    public void setPackagingSize(String packagingSize) {
        this.packagingSize = packagingSize;
    }

    public String getPackagingUnit() {
        return packagingUnit;
    }

    public void setPackagingUnit(String packagingUnit) {
        this.packagingUnit = packagingUnit;
    }

    public String getStrength() {
        return Strength;
    }

    public void setStrength(String strength) {
        Strength = strength;
    }

    public String getStrengthUnit() {
        return StrengthUnit;
    }

    public void setStrengthUnit(String strengthUnit) {
        StrengthUnit = strengthUnit;
    }
}

