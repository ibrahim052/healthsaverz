package com.healthsaverz.healthmobile.medicine.cabinet.controller;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.cabinet.modal.CabinetAdapter;
import com.healthsaverz.healthmobile.medicine.calendar.CalendarActivity;
import com.healthsaverz.healthmobile.medicine.calendar.CalenderReminderProvider;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.medicine.controller.MedicineDBHandler;
import com.healthsaverz.healthmobile.medicine.medicine.modal.AlarmReceiver;
import com.healthsaverz.healthmobile.medicine.medicine.modal.Medicine;
import com.healthsaverz.healthmobile.medicine.medicine.modal.ReminderModal;
import com.healthsaverz.healthmobile.medicine.medicineSearch.controller.MedicineSearchActivity;
import com.healthsaverz.healthmobile.medicine.reminder.controller.ReminderActivity;

import java.util.ArrayList;

public class CabinetActivity extends Activity implements  AdapterView.OnItemClickListener {

    private ListView medCabinetList;
    ArrayList<Medicine> medicines;
    CabinetAdapter cabinetAdapter;
    ActionBar actionBar;
    private Medicine medicine;
    private ProgressDialog emptyProgress;

//    public ProgressBar getEmpty() {
//        if (emptyProgress == null){
//            emptyProgress = (ProgressBar) findViewById(R.id.empty);
//        }
//        return emptyProgress;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cabinet);
        actionBar = getActionBar();
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("MyMeds");
        medCabinetList = (ListView) findViewById(R.id.medCabinetList);
        medCabinetList.setOnItemClickListener(this);
        //CabinetAdapter cabinetAdapter = new CabinetAdapter();
        //getEmpty();
        medicines           =   getAllMedicines();
        if (medicines.size() > 0){
            cabinetAdapter = new CabinetAdapter(this,0, medicines);
            medCabinetList.setAdapter(cabinetAdapter);
        }
    }

    public ArrayList<Medicine> getAllMedicines() {
        ArrayList<Medicine> calenderReminders;
        MedicineDBHandler dbHandler = new MedicineDBHandler(this);
        calenderReminders = dbHandler.findAllActiveMedicines(); //findAllActiveMedicines
        return calenderReminders;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cabinet, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if (id == android.R.id.home){
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (cabinetAdapter != null){
            medicines           =   getAllMedicines();
            if (medicines.size() > 0){
                cabinetAdapter = new CabinetAdapter(this,0, medicines);
                medCabinetList.setAdapter(cabinetAdapter);
            }
        }
        actionBar.show();
    }
    public void getConfirm(final DialogInterface dialogInterface){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle("Confirm Delete");
        // Setting Dialog Message
        alertDialog.setMessage("Are you sure?");
        alertDialog.setCancelable(false);
// Setting Positive "OK" Btn
        alertDialog.setNegativeButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        //getEmpty();
                        dialog.cancel();
                        dialogInterface.cancel();
                        //emptyProgress.setVisibility(View.VISIBLE); //emptyProgress.getVisibility() == View.VISIBLE?View.GONE:View.VISIBLE
                        emptyProgress = ProgressDialog.show(CabinetActivity.this, "Working", "Deleting Data...", true, false);
                        new DeleteMedicine().execute(medicine.get_medID());

                        //emptyProgress.setVisibility(View.GONE);
                    }
                });
        // Setting Positive "OK" Btn
        alertDialog.setPositiveButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                        dialogInterface.cancel();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        medicine =  (Medicine) adapterView.getItemAtPosition(i);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(CabinetActivity.this);
        // Setting Dialog Title
        alertDialog.setTitle("Medicine");
        // Setting Dialog Message
        alertDialog.setMessage("Choose what to do with this medicine");

        // Setting Positive "OK" Btn
        alertDialog.setPositiveButton("Delete",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        getConfirm(dialog);
                    }
                });
        // Showing Alert Dialog
        alertDialog.setNegativeButton("Edit Medicine",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        ReminderActivity.inEditMode = true;
                        Intent intent = new Intent(CabinetActivity.this, ReminderActivity.class);
                        intent.putExtra("id", medicine.get_medID());
                        startActivity(intent);
                        dialog.cancel();
                    }
                });
        alertDialog.setNeutralButton("Search for Generic name",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(CabinetActivity.this, MedicineSearchActivity.class);
                intent.putExtra("searchName", medicine.get_medName());
                startActivity(intent);
                dialog.cancel();
            }
        });
        alertDialog.show();
    }
    private class DeleteMedicine extends AsyncTask<Integer, Void, Object> {

        @Override
        protected Object doInBackground(Integer... params) {
            MedicineDBHandler dbHandler = new MedicineDBHandler(CabinetActivity.this);
            // we have to update the flags for update and delete where 1 is for update and -1 is for delete
            dbHandler.updateDeleteMedicineFlag("-1", medicine.get_medID());
            //Once schedule is deleted we need to remove all the alaram and than delete the future reminders for medicine
            //Once schedule is deleted we need to remove all the alaram
            ArrayList<ReminderModal> reminderModals = dbHandler.findReminder(medicine.get_medID());

            for (ReminderModal modal: reminderModals) {
                                /*
                                We need to delete all the reminders which are scheduled on the AlarmManager along with the scheduler
                                 */
                Intent intent = new Intent(CabinetActivity.this, AlarmReceiver.class);
                intent.putExtra("medicine_id",modal.get_medID());
                intent.putExtra("reminder_id",modal.get_remindID());
                if(modal.get_qty_type() == 0)
                    intent.putExtra("notificationtype",1);// 1 mean it is for pill reminder.
                else
                    intent.putExtra("notificationtype",3);// 3 mean it is for RX  reminder.

                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), modal.get_remindID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                alarmManager.cancel(pendingIntent);
                pendingIntent.cancel();

                AppConstants.cancelNotificationScheduler(getApplicationContext(), modal, 10);
            }
            //finally delete the medicine from the medicine table for future dates
            //dbHandler.deleteReminderWithMedicineID(medID);
            dbHandler.deleteReminderWithMedicineIDForFutureDate(medicine.get_medID());

            return null;
        }

        protected void onPostExecute(Object result) {
            // Pass the result data back to the main activity
            CalendarActivity.addButtonClicked = true;
            cabinetAdapter.remove(medicine);
            cabinetAdapter.notifyDataSetChanged();
            if (emptyProgress != null) {
                emptyProgress.dismiss();
            }
        }
    }
}
