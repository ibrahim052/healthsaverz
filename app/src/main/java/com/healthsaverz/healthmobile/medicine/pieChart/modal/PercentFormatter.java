package com.healthsaverz.healthmobile.medicine.pieChart.modal;

import com.healthsaverz.healthmobile.medicine.charting.utils.ValueFormatter;

import java.text.DecimalFormat;

public class PercentFormatter implements ValueFormatter {

    private DecimalFormat mFormat;
    
    public PercentFormatter() {
        mFormat = new DecimalFormat("#,##0");
    } //#,##0.0
    
    @Override
    public String getFormattedValue(float value) {
        return mFormat.format(value);
    }  // + " %"

}
