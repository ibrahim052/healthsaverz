package com.healthsaverz.healthmobile.medicine.medicine.modal;

import com.healthsaverz.healthmobile.medicine.medfriend.modal.MedFriend;

import java.util.ArrayList;

/**
 * Created by Ibrahim on 09-10-2014.
 */
public class Medicine {

    private int _medID;
    private String _medName = "";
    private String _patientName = "";
    private int _shape;
    private int _color1;
    private int _color2;
    private String _startDate;
    private String _endDate;
    private double _dose;
    private String _unit = "";
    private int _status = 0;
    private String _description = "";
    private ArrayList<ReminderModal> _reminderModals;
    private ArrayList<MedFriend> _medFriends;
    private Schedule _schedule;
    private float _pills;
    public float _threshold;
    public String _tempEndDate;
    public String updateDeleteFlag = "0";
    public String updateDeleteDate = "";
    public String extra_1 = "";
    public String extra_2 = "";
    public String extra_3 = "";
    public String extra_4 = "";
    public String extra_5 = "";


    public Medicine(String _medName, String _patientName, int _shape, int _color1, int _color2, String _startDate, String _endDate, double _dose, String _unit, int _status, String _description, float _pills) {
        this._medName = _medName;
        this._patientName = _patientName;
        this._shape = _shape;
        this._color1 = _color1;
        this._color2 = _color2;
        this._startDate = _startDate;
        this._endDate = _endDate;
        this._dose = _dose;
        this._unit = _unit;
        this._status = _status;
        this._description = _description;
        this._pills = _pills;
    }

    public Medicine(int _medID, String _medName, String _patientName, int _shape, int _color1, int _color2, String _startDate, String _endDate, double _dose, String _unit, int _status, String _description, float _pills) {
        this._medID = _medID;
        this._medName = _medName;
        this._patientName = _patientName;
        this._shape = _shape;
        this._color1 = _color1;
        this._color2 = _color2;
        this._startDate = _startDate;
        this._endDate = _endDate;
        this._dose = _dose;
        this._unit = _unit;
        this._status = _status;
        this._description = _description;
        this._pills = _pills;
    }

    public Medicine() {

    }

    public int get_medID() {
        return _medID;
    }

    public void set_medID(int _medID) {
        this._medID = _medID;
    }

    public String get_medName() {
        return _medName;
    }

    public void set_medName(String _medName) {
        this._medName = _medName;
    }

    public String get_patientName() {
        return _patientName;
    }

    public void set_patientName(String _patientName) {
        this._patientName = _patientName;
    }

    public int get_shape() {
        return _shape;
    }

    public void set_shape(int _shape) {
        this._shape = _shape;
    }

    public int get_color1() {
        return _color1;
    }

    public void set_color1(int _color1) {
        this._color1 = _color1;
    }

    public int get_color2() {
        return _color2;
    }

    public void set_color2(int _color2) {
        this._color2 = _color2;
    }

    public String get_startDate() {
        return _startDate;
    }

    public void set_startDate(String _startDate) {
        this._startDate = _startDate;
    }

    public String get_endDate() {
        return _endDate;
    }

    public void set_endDate(String _endDate) {
        this._endDate = _endDate;
    }

    public double get_dose() {
        return _dose;
    }

    public void set_dose(double _dose) {
        this._dose = _dose;
    }

    public String get_unit() {
        return _unit;
    }

    public void set_unit(String _unit) {
        this._unit = _unit;
    }

    public int get_status() {
        return _status;
    }

    public void set_status(int _status) {
        this._status = _status;
    }

    public String get_description() {
        return _description;
    }

    public void set_description(String _description) {
        this._description = _description;
    }

    public ArrayList<ReminderModal> get_reminderModals() {
        return _reminderModals;
    }

    public void set_reminderModals(ArrayList<ReminderModal> _reminderModals) {
        this._reminderModals = _reminderModals;
    }

    public Schedule get_schedule() {
        return _schedule;
    }

    public void set_schedule(Schedule _schedule) {
        this._schedule = _schedule;
    }

    public float get_pills() {
        return _pills;
    }

    public void set_pills(float _pills) {
        this._pills = _pills;
    }

}
