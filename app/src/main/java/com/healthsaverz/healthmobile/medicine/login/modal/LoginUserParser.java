package com.healthsaverz.healthmobile.medicine.login.modal;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by priyankranka on 08/11/14.
 */
public class LoginUserParser extends AsyncTask <String,Void,String> implements AsyncLoaderNew.Listener {
     /*
    Start from loginUser method
    Takes String as input and out is based on VerifyUserParserListener
     */

    private Context context;

    public LoginUserParser(Context context){

        this.context = context;
    }

    public interface LoginUserParserListener{

        void loginUserParserDidUserLoggedIn(String message); //message as 100 for success
        void loginUserParserDidReceivedConnectionError(AsyncLoaderNew.Status status);// AsyncLoaderNew.Status
        void loginUserParserDidReceivedProcessingError(String message);//server message for unsuccessful
    }

    private LoginUserParserListener loginUserListener;

    public LoginUserParserListener getLoginUserListener() {
        return loginUserListener;
    }

    public void setLoginUserListener(LoginUserParserListener loginUserListener) {
        this.loginUserListener = loginUserListener;
    }

    private static final String METHOD_LOGIN_USER = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/LoginServices/loginForMobile/";

    //local method
    public void loginUser(String username, String password){

        AsyncLoaderNew asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.GET);

        String userClientID = PreferenceManager.getStringForKey(context, PreferenceManager.CLIENT_ID, "-1");

        if(userClientID.equals("") || userClientID.equals("-1")){
            userClientID = "healthsaverz";
        }

        asyncLoaderNew.setListener(this);
        asyncLoaderNew.executeRequest(METHOD_LOGIN_USER + userClientID + "/" + username + "/" + password);

    }


    @Override
    protected String doInBackground(String... params) {
        if(!isCancelled())
        {
            try {
            /*
            {"sessionID":"","clientID":"","message":"100","gender":"Male",
            "deviceUdid":"353743054004708","deviceToken":"DEVICE_TOKEN","osType":"A",
            "verificationCode":"7031","verificationFlag":"","fileDetailList":null,"state":"",
            "country":"","id":361,"password":"","userName":"priyank@nimapinfotech.com","status":"I",
            "firstName":"","lastName":"","address1":"","address2":"","postalCode":"","emailAddress":"priyank@nimapinfotech.com",
            "subUserId":0,"userGroupId":3,"phoneNumber":"","cellNumber":"+919869357889","created_By":0,"city":""}
             */
                JSONObject jsonObject = new JSONObject(params[0]);
                String k = jsonObject.getString("id");
                if(jsonObject.getString("message").equals("100") || jsonObject.getString("message").equals("you are activated valid user")){
                    // indicates success in registering the user. We need to add to persistent storage so that it can be used later on when ever needed.
                    PreferenceManager.saveStringForKey(context,PreferenceManager.SESSION_ID,"test");//jsonObject.getString("sessionID")
                    PreferenceManager.saveStringForKey(context,PreferenceManager.CLIENT_ID,jsonObject.getString("clientID"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.MESSAGE,jsonObject.getString("message"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.GENDER,jsonObject.getString("gender"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.DEVICE_UDID,jsonObject.getString("deviceUdid"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.DEVICE_TOKEN,jsonObject.getString("deviceToken"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.OS_TYPE,jsonObject.getString("osType"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.VERIFICATION_CODE,jsonObject.getString("verificationCode"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.VERIFICATION_FLAG,jsonObject.getString("verificationFlag"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.FILE_DETAIL_LIST,jsonObject.getString("fileDetailList"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.STATE,jsonObject.getString("state"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.STATUS,jsonObject.getString("status"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.COUNTRY,jsonObject.getString("country"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.ID,jsonObject.getString("id"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.USERNAME,jsonObject.getString("userName"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.PASSWORD,jsonObject.getString("password"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.ADDRESS1,jsonObject.getString("address1"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.ADDRESS2,jsonObject.getString("address2"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.POSTAL_CODE,jsonObject.getString("postalCode"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.EMAIL_ADDRESS,jsonObject.getString("emailAddress"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.SUB_USER_ID,jsonObject.getString("subUserId"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.USER_GROUP_ID,jsonObject.getString("userGroupId"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.PHONE_NUMBER,jsonObject.getString("phoneNumber"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.CELL_NUMBER,jsonObject.getString("cellNumber"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.CREATED_BY,jsonObject.getString("created_By"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.CITY,jsonObject.getString("city"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.OS_VERSION,jsonObject.getString("osVersion"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.DEVICE_NAME,jsonObject.getString("deviceName"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.DEVICE_UDID,jsonObject.getString("deviceUDID"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.DOCTORID,jsonObject.getString("doctorId"));
                    PreferenceManager.saveStringForKey(context,PreferenceManager.DOCTORNAME,jsonObject.getString("doctorName"));
                    String e = PreferenceManager.getStringForKey(context, "id", "a");
                    return "100";
                }
                else
                {
                    return jsonObject.getString("message");
                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
            return "Something went wrong. Please contact our administrator";
        }
        else {
            cancel(true);
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(this.loginUserListener != null){

            if(s != null && s.equals("100")){
                loginUserListener.loginUserParserDidUserLoggedIn(s);
            }
            else if (s!= null && s.equals("")){
                loginUserListener.loginUserParserDidReceivedProcessingError("User Not Validated");
            }
            else
            {
                loginUserListener.loginUserParserDidReceivedProcessingError(s);
            }
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();

        if (this.loginUserListener != null) {
            this.loginUserListener.loginUserParserDidReceivedProcessingError("Verification Abrupted");
        }
    }


    //AsyncLoaderNew.Listener method implementation
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {
        if(loginUserListener != null){
            loginUserListener.loginUserParserDidReceivedConnectionError(status);
        }
    }

    @Override
    public void didReceivedData(byte[] data) {
        String responseString = new String(data);
        Log.d("LoginUserParser", "didReceivedData is " + responseString);
        execute(responseString);
    }

    @Override
    public void didReceivedProgress(Double progress) {

    }
}
