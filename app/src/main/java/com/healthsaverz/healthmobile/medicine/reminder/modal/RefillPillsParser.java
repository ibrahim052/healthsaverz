package com.healthsaverz.healthmobile.medicine.reminder.modal;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;
import com.healthsaverz.healthmobile.medicine.medicine.modal.Medicine;

import java.text.SimpleDateFormat;

/**
 * Created by DELL PC on 3/17/2015.
 */
public class RefillPillsParser extends AsyncTask<String,Void,String> implements AsyncLoaderNew.Listener {

    /*
    Start from addMedicine method
    Takes String as input and out is based on updatePills
     */

    private AsyncLoaderNew asyncLoaderNew;
    private Context context;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public RefillPillsParser(Context context){

        this.context = context;
    }

    private static final String METHOD_UPDATE_PILLS = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/ReminderServices/updatePills";

    public void updatePills(String reminderTime, Medicine medicine){
        asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.POST_HEADER);
        String sessionID = PreferenceManager.getStringForKey(context, PreferenceManager.SESSION_ID, "");
        String clientID = PreferenceManager.getStringForKey(context, PreferenceManager.CLIENT_ID, "");
        String userId = PreferenceManager.getStringForKey(context, PreferenceManager.ID, "");


        String requestXML = "<reminder>" +
                "<sessionID>"+sessionID+"</sessionID>" +
                "<clientID>"+clientID+"</clientID>" +
                "<userId>"+userId+"</userId>" +
                "<medicineName>"+medicine.get_medName()+"</medicineName>" +
                "<dose>"+medicine.get_dose()+"</dose> " +
                "<time>"+sdf.format(Long.parseLong(reminderTime))+"</time>" +
                "<pills>"+medicine.get_pills()+"</pills>" +
                "<status>A</status>" +
                "<createdBy>"+userId+"</createdBy>" +
                "<extra_1></extra_1>" +
                "<extra_2></extra_2>" +
                "<extra_3></extra_3>" +
                "<extra_4></extra_4>" +
                "<extra_5></extra_5>" +
                "</reminder>";
//        "\"<reminder>\"\n" +
//                "                           \"<clientID>%@</clientID>\"\n" +
//                "                           \"<sessionID>%@</sessionID>\"\n" +
//                "                           \"<userId>%@</userId>\"\n" +
//                "                           \"<medicineName>%@</medicineName>\"\n" +
//                "                           \"<dose>%d</dose>\"\n" +
//                "                           \"<time>%@</time>\"\n" +
//                "                           \"<status>A</status>\"\n" +
//                "                           \"<createdBy>%@</createdBy>\"\n" +
//                "                           \"<pills>%0.2f</pills>\"\n" +
//                "                           \"<extra_1></extra_1>\"\n" +
//                "                           \"<extra_2></extra_2>\"\n" +
//                "                           \"<extra_3></extra_3>\"\n" +
//                "                           \"<extra_4></extra_4>\"\n" +
//                "                           \"<extra_5></extra_5>\"\n" +
//                "                           \"</reminder>\""
        Log.d("requestXML ", "requestXML " + requestXML);
        asyncLoaderNew.requestMessage   =   requestXML;
        asyncLoaderNew.executeRequest(METHOD_UPDATE_PILLS);
        asyncLoaderNew.setListener(this);

    }


    //AsyncLoaderNew.Listener method implementation
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {


    }

    @Override
    public void didReceivedData(byte[] data) {

        String responseString = new String(data);
        Log.d("addMedicineParser","didReceivedData is "+responseString);
        execute(responseString);
    }

    @Override
    public void didReceivedProgress(Double progress) {
    }
    //Current Thread Stack
    @Override
    protected String doInBackground(String... params) {
        return null;
    }
}
