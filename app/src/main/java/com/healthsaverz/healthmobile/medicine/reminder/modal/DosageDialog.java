package com.healthsaverz.healthmobile.medicine.reminder.modal;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;

/**
 * Created by Ibrahim on 18-10-2014.
 */
public class DosageDialog extends DialogFragment implements View.OnTouchListener, AdapterView.OnItemSelectedListener {

    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String TAG = "DosageDialog";
    private Context context;

    DosageListener dosageListener;
    private String code;

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        code = String.valueOf(adapterView.getItemAtPosition(i));
        Log.d(TAG, code);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        code = "mg";
    }


    public interface DosageListener {
        void onDosageChange(String quantity);
    }

    private Spinner unitSpinner;

    public Spinner getUnitSpinner() {
        if (unitSpinner == null) {
            unitSpinner = (Spinner) getView().findViewById(R.id.unitSpinner);
        }
        return unitSpinner;
    }

    private ImageView minus;
    public ImageView getMinus() {
        if (minus == null) {
            minus = (ImageView) getView().findViewById(R.id.minus);

        }
        return minus;
    }

    private ImageView plus;
    public ImageView getPlus() {
        if (plus == null) {
            plus = (ImageView) getView().findViewById(R.id.plus);

        }
        return plus;
    }

    private EditText quantityText;
    public EditText getQuantityText() {
        if (quantityText == null) {
            quantityText = (EditText) getView().findViewById(R.id.quantityText);

        }
        return quantityText;
    }

    private Button setButton;
    public Button getVerifyButton() {
        if (setButton == null) {
            setButton = (Button) getView().findViewById(R.id.setButton);
            setButton.setOnTouchListener(this);
        }
        return setButton;
    }

    private Button cancelButton;
    public Button getCancelButton() {
        if (cancelButton == null) {
            cancelButton = (Button) getView().findViewById(R.id.cancelButton);
            cancelButton.setOnTouchListener(this);
        }
        return cancelButton;
    }

    public static DosageDialog newInstance(DosageListener timeSetlistener){
        DosageDialog dosageDialog = new DosageDialog();
        dosageDialog.dosageListener = timeSetlistener;
        return dosageDialog;
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.dialog_dosage, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getVerifyButton();
        getCancelButton();
        //getUnitSpinner();
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.units_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        getUnitSpinner().setAdapter(adapter);
        getUnitSpinner().setSelection(4);
        getUnitSpinner().setOnItemSelectedListener(this);

        getPlus().setOnTouchListener(this);
        getMinus().setOnTouchListener(this);
        getQuantityText().setText("");
        getQuantityText().append("100.00");
        getQuantityText().requestFocus();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_SmileWindow;
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return dialog;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(view, null);
            return true;
        } else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
            final int viewID = view.getId();
            ViewHelper.fadeIn(view, null);
            if (quantityText.getText().toString().equals("") || quantityText.getText().toString().equals("0")
                    || Double.parseDouble(quantityText.getText().toString()) < 1) {

                quantityText.setText("1.00");
            }
            switch (view.getId()){
                case R.id.plus:
                    double add = Double.parseDouble((quantityText.getText().toString()));
                    add = add + 1;
                    quantityText.setText("");
                    quantityText.append(String.format("%.2f",add));
                    return true;
                case R.id.minus:
                    double sub = Double.parseDouble((quantityText.getText().toString()));
                    if (sub > 1){
                        sub = sub - 1;
                        quantityText.setText("");
                        quantityText.append(String.format("%.2f",sub));
                    }
                    return true;
                case R.id.setButton:
                    if (dosageListener != null){
                        dosageListener.onDosageChange(quantityText.getText().toString() +" "+ code);
                    }
                    dismiss();
                    return true;

                case R.id.cancelButton:
                    if (dosageListener != null){
                        dosageListener.onDosageChange(null);
                    }
                    dismiss();
                    return true;

            }
//            if (validate()){
//                final String code = verifyCode.getText().toString().trim();
//                //final String systemCode = PreferenceManager.getStringForKey(getActivity(), PreferenceManager.VERIFICATION_CODE, "");
//                if (!code.equals(systemCode)) {
//                    //Toast.makeText(getActivity(), "Invalid Verification Code", Toast.LENGTH_SHORT).show();
//                    invalidVerificationCode();
//                } else {
//                    PreferenceManager.saveStringForKey(getActivity(), PreferenceManager.IS_VERIFICATION_STATUS, VerifyFragment.TRUE);
//                    //Toast.makeText(getActivity(), "Successfully Registered", Toast.LENGTH_SHORT).show();
//                    callbacks.didPressView(viewID);
//                    dismiss();
//                    return true;
//                }
//            }


        }
        return false;
    }

}
