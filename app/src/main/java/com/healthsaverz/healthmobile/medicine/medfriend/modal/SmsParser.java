package com.healthsaverz.healthmobile.medicine.medfriend.modal;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.medicine.controller.MedicineDBHandler;
import com.healthsaverz.healthmobile.medicine.medicine.modal.Medicine;
import com.healthsaverz.healthmobile.medicine.medicine.modal.ReminderModal;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by DG on 11/23/2014.
 */
public class SmsParser extends AsyncTask<String,Void,String> implements AsyncLoaderNew.Listener{

    private static final String TAG = "SmsParser";
    private Context context;
    private String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec" };
    public SmsParser(Context context){
        this.context = context;
    }
    public interface SmsParserListener{

        void smsParserDidReceiveData(String message); //message as 100 for success
        void smsParserDidReceivedConnectionError(AsyncLoaderNew.Status status);// AsyncLoaderNew.Status
        void smsParserDidReceivedProcessingError(String message);//server message for unsuccessful
    }
    private SmsParserListener smsParserListener;

    public void setSmsParserListener(SmsParserListener smsParserListener) {
        this.smsParserListener = smsParserListener;
    }
    private static final String METHOD_SEND_SMS = "http://sms6.routesms.com:8080/bulksms/bulksms?";
    public void getsms(MedFriend medFriend, int medID, int remindID) {


        AsyncLoaderNew asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.GET);
        MedicineDBHandler dbHandler = new MedicineDBHandler(context);
        Medicine medicine = dbHandler.findMedicinefromID(medID);
        ReminderModal reminderModal = dbHandler.findSpecificReminder(remindID);
        String userName = "saverz";
        String password = "sav63erz";
        int getType = 0;
        int getDlr = 1;
        String getSource = "HEALTH";
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(Long.parseLong(reminderModal.get_time()));
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        //String message = "Dear "+medFriend._friendName+" Thank you for choosing HEALTHSAVERZ as your partner in health 1234 Do call us on 022 6169 9494 to get started in the meantime.";
        String getMessage = "Dear "+medFriend._friendName+" , your friend "+medicine.get_patientName()+" has missed to take the medicine "
                +medicine.get_medName()+" on "+AppConstants.mFormat.format((double) calendar.get(Calendar.DAY_OF_MONTH))
                +"-"+months[calendar.get(Calendar.MONTH)]+" at "+AppConstants.mFormat.format((double) calendar.get(Calendar.HOUR_OF_DAY))+":"
                +AppConstants.mFormat.format((double) calendar.get(Calendar.MINUTE))+".";
        //Dear <variable> , your friend <variable> has missed to take the medicine <variable>.
        asyncLoaderNew.setListener(this);
        asyncLoaderNew.executeRequest(METHOD_SEND_SMS + "username=" + userName + "&password=" + password + "&type=" + getType + "&dlr=" + getDlr + "&destination=" + medFriend._friendMobile + "&source=" + getSource + "&message=" + getMessage);
    }


    /*
    @priyanka kale
     */
    public void getSmsToReferrals( String friendNumber , String docName,String docAddres,String docNumber){
        /**
         * Sent sms to Friend
         */
        AsyncLoaderNew asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.GET);
        String userName = "saverz";
        String password = "sav63erz";
        int getType = 0;
        int getDlr = 1;
        String getSource = "HEALTH";

        String message = "For your reference Doctor Details are "+"\n"+" Doctor Name : "+ "Dr."+docName+"\n Doctor Address : "+ docAddres +"\n Doctor Number : "+docNumber;
        Log.d("SmsParser","getSmsToReferrals  SENT SMS Friend"+friendNumber);
        asyncLoaderNew.setListener(this);
        asyncLoaderNew.executeRequest(METHOD_SEND_SMS + "username=" + userName + "&password=" + password + "&type=" + getType + "&dlr=" + getDlr + "&destination=" + friendNumber + "&source=" + getSource + "&message=" + message);
    }

    /*
  @priyanka kale
   */
    public void getSmsToDoctor( String docNum ,String refereBy,String cellNumberr, String referTo, String name,String mobile){
        /*
        Sent sms to Doctor
         */
        AsyncLoaderNew asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.GET);
        String userName = "saverz";
        String password = "sav63erz";
        int getType = 0;
        int getDlr = 1;
        String getSource = "HEALTH";
        String message =  "Patient  Details :\n"+"\nName : "+ name +"\nMobile : "+mobile + "\n has been referred by "+refereBy + "\nCell Number :"+cellNumberr;
        Log.d("SmsParser","getSmsToDoctor  SENT SMS doctorNumber"+docNum);
        asyncLoaderNew.setListener(this);
        asyncLoaderNew.executeRequest(METHOD_SEND_SMS + "username=" + userName + "&password=" + password + "&type=" + getType + "&dlr=" + getDlr + "&destination=" + docNum + "&source=" + getSource + "&message=" + message);

    }

    @Override
    protected String doInBackground(String... params) {
        return null;
    }

    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {
        if(smsParserListener != null){
            smsParserListener.smsParserDidReceivedConnectionError(status);
        }
    }

    @Override
    public void didReceivedData(byte[] data) {
        String response = new String(data);

        String newStr = response.substring(0, Math.min(response.length(), 4));
        Log.d("SMSParser", "didReceivedData is " + newStr+" and "+ response);
        if (this.smsParserListener != null){
            switch (newStr) {
                case "1701":
                    smsParserListener.smsParserDidReceiveData(newStr);

                    break;
                case "1706":
                    smsParserListener.smsParserDidReceivedProcessingError("Invalid Phone No.");
                    break;
                default:
                    smsParserListener.smsParserDidReceivedProcessingError("Something went wrong");
                    break;
            }
        }
        //execute(responseString);

    }

    @Override
    public void didReceivedProgress(Double progress) {

    }


}