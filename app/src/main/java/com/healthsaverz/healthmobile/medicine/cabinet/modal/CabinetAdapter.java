package com.healthsaverz.healthmobile.medicine.cabinet.modal;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.medicine.modal.Medicine;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Ibrahim on 13-11-2014.
 */
public class CabinetAdapter extends ArrayAdapter<Medicine> {
    ArrayList<Medicine> arrayList;
    private Context context ;
    private LayoutInflater inflater;
    //View.OnClickListener deleteClick;

    public Bitmap mergeBitmap(int colo1,int colo2)
    {

        Bitmap firstBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.badge3left);
        Bitmap secondBitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.badge3right);

        Bitmap comboBitmap;

        float width, height;

        width = firstBitmap.getWidth() + secondBitmap.getWidth();
        height = firstBitmap.getHeight();

        comboBitmap = Bitmap.createBitmap((int)(width/2), (int)height, Bitmap.Config.ARGB_4444);

        Paint firstPaint = new Paint();
        firstPaint.setColorFilter(new PorterDuffColorFilter(mColors[colo1], PorterDuff.Mode.MULTIPLY));

        Paint secondPaint = new Paint();
        secondPaint.setColorFilter(new PorterDuffColorFilter(mColors[colo2], PorterDuff.Mode.MULTIPLY));

        Canvas comboImage = new Canvas(comboBitmap);

        comboImage.drawBitmap(firstBitmap, 0f, 0f, firstPaint);
        comboImage.drawBitmap(secondBitmap,0f,0f, secondPaint);

        return comboBitmap;

    }

    public CabinetAdapter(Context context,int resource, ArrayList<Medicine> arrayList) {
            super(context, resource, arrayList);
            this.arrayList = arrayList;
            this.context = context;
            //this.deleteClick = deleteClick;
            this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }

    @Override
    public int getCount() {
            return arrayList.size();
            }

    @Override
    public Medicine getItem(int i) {
            return arrayList.get(i);
            }

    @Override
    public long getItemId(int i) {
            return i;
            }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
            ViewHolder holder;
            if (convertView == null){
                convertView = inflater.inflate(R.layout.cell_med_cabinet, null);
                holder = new ViewHolder();
                holder.medicine_icon = (ImageView) convertView.findViewById(R.id.medicine_icon);
                holder.medicineName = (TextView) convertView.findViewById(R.id.medicineName);
                holder.strength = (TextView) convertView.findViewById(R.id.strength);
                holder.patientName = (TextView) convertView.findViewById(R.id.patientName);
                //holder.closeImage.setOnClickListener(deleteClick);
                convertView.setTag(holder);
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }
        Medicine medicine = arrayList.get(i);
        if (medicine.get_shape() == 5) {

            //PriyankShape
//            Resources r = context.getResources();
//            Drawable[] layers = new Drawable[2];
//
//            layers[0] = r.getDrawable(R.drawable.badge3left);
//            layers[0].setColorFilter(mColors[medicine.get_color1()], PorterDuff.Mode.MULTIPLY);
//
//            layers[1] = r.getDrawable(R.drawable.badge3right);
//            layers[1].setColorFilter(mColors[medicine.get_color2()], PorterDuff.Mode.MULTIPLY);
//
//            LayerDrawable layerDrawable = new LayerDrawable(layers);
//            //layerDrawable.setBounds(0,0,convertToDPUnit(getActivity(),64),convertToDPUnit(getActivity(),32));
//            holder.medicine_icon.setImageDrawable(layerDrawable);

            holder.medicine_icon.setColorFilter(null);
            holder.medicine_icon.setImageBitmap(mergeBitmap(medicine.get_color1(),medicine.get_color2()));

        }
        else if(medicine.get_shape() == -1){

            //PriyankShape
//            Resources r = context.getResources();
//            Drawable[] layers = new Drawable[1];
//
//            layers[0] = r.getDrawable(mCards[2]);
//            layers[0].setColorFilter(mColors[28], PorterDuff.Mode.MULTIPLY);
//            LayerDrawable layerDrawable = new LayerDrawable(layers);
//            holder.medicine_icon.setImageDrawable(layerDrawable);

            holder.medicine_icon.setImageBitmap(BitmapFactory.decodeResource(context.getResources(),mCards[2]));
            holder.medicine_icon.setColorFilter(null);
            holder.medicine_icon.setColorFilter(mColors[28], PorterDuff.Mode.MULTIPLY);
        }
        else {

            //PriyankShape
//            Resources r = context.getResources();
//            Drawable[] layers = new Drawable[1];
//
//            layers[0] = r.getDrawable(mCards[medicine.get_shape()]);
//            layers[0].setColorFilter(mColors[medicine.get_color1()], PorterDuff.Mode.MULTIPLY);
//            LayerDrawable layerDrawable = new LayerDrawable(layers);
//            holder.medicine_icon.setImageDrawable(layerDrawable);
//            //viewHolderMedicine.leftMedicineIcon.setColorFilter(mColors[calenderReminder.medicineColor1], PorterDuff.Mode.MULTIPLY);

            holder.medicine_icon.setImageBitmap(BitmapFactory.decodeResource(context.getResources(),mCards[medicine.get_shape()]));
            holder.medicine_icon.setColorFilter(null);
            holder.medicine_icon.setColorFilter(mColors[medicine.get_color1()], PorterDuff.Mode.MULTIPLY);
        }
        holder.medicineName.setText(medicine.get_medName());
        Long time = Long.parseLong(medicine.get_startDate());
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(time);
        if (medicine.get_dose() != 0.0){
            holder.strength.setText(medicine.get_dose() +" "+ medicine.get_unit());
        }
        else {
            holder.strength.setText("100 MG");
        }

        holder.patientName.setText(medicine.get_patientName());
        //convertView.setBackgroundColor((i%2 == 0)?0x9934B5E4:Color.WHITE);
        return convertView;
    }
    static class ViewHolder {
        ImageView medicine_icon;
        TextView medicineName;
        TextView strength;
        TextView patientName;
    }
    private int[] mColors = {
            0,
            0,
            Color.parseColor("#3cbaff"),
            Color.parseColor("#2797d0"),
            Color.parseColor("#005c94"),
            Color.parseColor("#d4ff5f"),
            Color.parseColor("#a9e327"),
            Color.parseColor("#27a227"),
            Color.parseColor("#ffff5c"),
            Color.parseColor("#ff9900"),
            Color.parseColor("#ff7d27"),
            Color.parseColor("#efd597"),
            Color.parseColor("#dba747"),
            Color.parseColor("#936827"),
            Color.parseColor("#ff4141"),
            Color.parseColor("#e12727"),
            Color.parseColor("#c02727"),
            Color.parseColor("#ffae8e"),
            Color.parseColor("#ffa2b2"),
            Color.parseColor("#e9967a"),
            Color.parseColor("#fb9388"),
            Color.parseColor("#f3d2ff"),
            Color.parseColor("#dd82ff"),
            Color.parseColor("#c527ff"),
            Color.parseColor("#2d2d2d"),
            Color.parseColor("#7d7d7d"),
            Color.parseColor("#a9a9a9"),
            Color.parseColor("#d4d4d4"),
            Color.parseColor("#ffffff"),
            Color.parseColor("#000000"),
            0,0};
    private int[] mCards = {
            0,
            0,
            R.drawable.badge1,
            R.drawable.badge2,
            R.drawable.badge3,
            R.drawable.capsule,
            R.drawable.badge5,
            R.drawable.badge6,
            R.drawable.badge7,
            R.drawable.badge8,
            R.drawable.badge9,
            R.drawable.badge10,
            R.drawable.badge11,
            R.drawable.badge12,
            R.drawable.badge14,
            R.drawable.badge15,
            R.drawable.badge16,
            R.drawable.badge17,
            R.drawable.badge18,
            R.drawable.badge19,
            R.drawable.badge20,
            R.drawable.badge21,
            R.drawable.badge22,
            R.drawable.badge23,
            R.drawable.badge24,
            R.drawable.badge25,
            R.drawable.badge26,
            R.drawable.badge27,
            R.drawable.badge28,
            0,
            0};
}