package com.healthsaverz.healthmobile.medicine.medfriend.modal;

/**
 * Created by Ibrahim on 24-11-2014.
 */
public class MapMedFriend {
    public int _map_id;
    public int _medID ;
    public int _medFriendID ;

    public MapMedFriend(int _medID, int _medFriendID) {
        this._medID = _medID;
        this._medFriendID = _medFriendID;
    }
}
