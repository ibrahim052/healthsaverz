package com.healthsaverz.healthmobile.medicine.healthshopmenu.controller;



import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;


/**
 * A simple {@link android.app.Fragment} subclass.
 *
 */
public class HealthShopHomeFragment extends Fragment {


    public HealthShopHomeFragment() {
        // Required empty public constructor
    }

    public interface HealthShopHomeFragmentListener{

        void didDrawerButtonPressed();
    }

    private HealthShopHomeFragmentListener listener;

    public HealthShopHomeFragmentListener getListener() {
        return listener;
    }

    public void setListener(HealthShopHomeFragmentListener listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_health_shop_home, container, false);

        TextView textView = (TextView)view.findViewById(R.id.action_dummy);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(listener != null){
                    listener.didDrawerButtonPressed();
                }
            }
        });

        return view;
    }


}
