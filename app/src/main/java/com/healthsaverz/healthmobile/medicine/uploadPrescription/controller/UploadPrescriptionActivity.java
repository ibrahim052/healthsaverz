package com.healthsaverz.healthmobile.medicine.uploadPrescription.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoader;
import com.healthsaverz.healthmobile.medicine.asyncloader.ParserListener;
import com.healthsaverz.healthmobile.medicine.global.App;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.Parser;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
import com.healthsaverz.healthmobile.medicine.uploadPrescription.model.GridImageAdapter;
import com.healthsaverz.healthmobile.medicine.uploadPrescription.model.MyGridView;
import com.healthsaverz.healthmobile.medicine.uploadPrescription.model.Prescription;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class UploadPrescriptionActivity extends FragmentActivity implements View.OnTouchListener, ParserListener {

    public  static final String SUCCESS = "100";
    private static final int REQUEST_IMAGE_CAPTURE = 5;
    private TextView prescriptionText;
    private MyGridView gridView;
    ArrayList<Bitmap> bitmaps;
    private Parser parser;
    private int i;
    private Prescription prescription;
    private GridImageAdapter adapter;
    private ProgressDialog progressDialog;
    private App app;
    private String mLastPhotoPath = "";
    private int imageCount = 1;
    String memberid = "";
    String sessionID = "test";
    String clientID = "healthsaverzm";
    private int totalCount;

//    private ParserUtils parserUtils;

    public Parser getParser() {
        if (parser == null) {
            parser = new Parser(this);
            parser.setListener(this);
        }
        return parser;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_prescription);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        memberid = PreferenceManager.getStringForKey(this, "id", "null");
        sessionID = PreferenceManager.getStringForKey(this, "sessionID", "null");
        //clientID = PreferenceManager.getStringForKey(this, "clientID", "null");
        gridView = (MyGridView)findViewById(R.id.gridView);
        bitmaps = new ArrayList<>();
        prescriptionText = (TextView)findViewById(R.id.prescriptionText);
        app = (App) getApplicationContext();
        Button cameraButton = (Button) findViewById(R.id.cameraButton);
        cameraButton.setOnTouchListener(this);
        Button galleryButton = (Button) findViewById(R.id.galleryButton);
        galleryButton.setOnTouchListener(this);
        Button submitButton = (Button) findViewById(R.id.submitButton);
        submitButton.setOnTouchListener(this);
        if (savedInstanceState != null){
            bitmaps = savedInstanceState.getParcelableArrayList("PhotoBitmap");
        }
        if (bitmaps != null && bitmaps.size()> 0){
            adapter = new GridImageAdapter(this, 0, bitmaps, deleteClick);
            gridView.setAdapter(adapter);
            prescriptionText.setVisibility(View.INVISIBLE);

        }
        dispatchTakePictureIntent();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (bitmaps != null){
            outState.putParcelableArrayList("PhotoBitmap", bitmaps);
            //outState.putParcelable("GridView", gridView.onSaveInstanceState());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_upload_prescription, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()){
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                finish();
                return true;
            case R.id.callUsButton:
                String uri = "tel:" + AppConstants.call_us.trim() ;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(uri));
                startActivity(intent);
                return true;
        }
        return false;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(v, null);
            return true;
        } else if (event.getActionMasked() == MotionEvent.ACTION_UP) {
            ViewHelper.fadeIn(v, null);
            switch (v.getId()){
                case R.id.cameraButton:
                    //ImagePickerFragment.newInstance().show(getSupportFragmentManager(), ImagePickerFragment.TAG);
                    dispatchTakePictureIntent();
                    return true;
                case R.id.galleryButton:
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK);
                    galleryIntent.setType("image/*");
                    galleryIntent.putExtra("return-data", true);
                    startActivityForResult(galleryIntent, 3);
                    return true;
                case R.id.submitButton:
                    i = bitmaps.size()-1;
                    totalCount = bitmaps.size();
                    if (i>= 0){
                        progressDialog = new ProgressDialog(this);
                        progressDialog.setCanceledOnTouchOutside(false);
                        progressDialog.setMessage("uploading " + imageCount + " of " + totalCount);
                        progressDialog.setButton(ProgressDialog.BUTTON_NEUTRAL, "Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                progressDialog.dismiss();
                                if (parser != null) {
                                    parser.cancel(true);
                                    parser = null;
                                }
                            }
                        });
                        progressDialog.show();
                        String ic = encodeToBase64(bitmaps.get(i));
                        prescription = new Prescription();
                        prescription.fileEncodedValue = ic;
                        prescription.memberId = memberid;
                        prescription.sessionID = sessionID;
                        prescription.clientID = clientID;
                        getParser().getUploadPrescription(prescription);
                    }
                    return true;
            }

        }
        return false;
    }
    public static String encodeToBase64(Bitmap image)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File file;
            if (isExternalStorageWritable()) {
                /*
                if you use Environment.getExternalStorageDirectory() then your folder will remain in the sd card even if you delete the app

                if you use getFilesDir() then your folder and data inside internal memory will delete automatically on deleting the app
                if you use getExternalFilesDir() then your folder and data inside sd card will delete automatically on deleting the app.
                 */
                file=new File(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES),"/HealthSaverz/");
            } else {
                file = new File(getApplicationContext().getFilesDir(),"/HealthSaverz/"); //getDir("Health", Context.MODE_PRIVATE)
            }
            if (!file.exists()) {
                file.mkdirs();
            }
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Uri mImageCaptureUri = Uri.fromFile(new File(file.getAbsolutePath(), "prescription_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            app.mCurrentPhotoPath = mImageCaptureUri.getEncodedPath();
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private View.OnClickListener deleteClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final int position = gridView.getPositionForView(v);
            if (position != ListView.INVALID_POSITION) {
                adapter.remove(adapter.getItem(position));
                adapter.notifyDataSetChanged();
                if (adapter.getCount() > 0) {
                    prescriptionText.setVisibility(View.INVISIBLE);
                }
                else {
                    prescriptionText.setVisibility(View.VISIBLE);
                }
            }
        }
    };
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_IMAGE_CAPTURE:
                    if(mLastPhotoPath != null && !mLastPhotoPath.equals("")){
                        File file = new File(mLastPhotoPath);
                        //file.delete();
                    }
                    mLastPhotoPath = app.mCurrentPhotoPath;
                    setPic();
                    break;
                case 3:
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(
                            selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    app.mCurrentPhotoPath = "";
                    app.mCurrentPhotoPath = cursor.getString(columnIndex);
                    cursor.close();
                    mLastPhotoPath = app.mCurrentPhotoPath;
                    setPic();
                    break;
            }
        }
    }
    private void setPic() {
        int targetW = 150;
        int targetH = 150;

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(app.mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;


        Bitmap bitmap = BitmapFactory.decodeFile(app.mCurrentPhotoPath, bmOptions);
        ////////////////////////////////////
        int rotate = 0;
        try {
            ExifInterface exif = new ExifInterface(app.mCurrentPhotoPath);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }

            Log.v("", "Exif orientation: " + orientation);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        bitmaps.add(bitmap);
        adapter = new GridImageAdapter(this, 0, bitmaps, deleteClick);
        gridView.setAdapter(adapter);
        if (bitmaps.size() > 0) {
            prescriptionText.setVisibility(View.INVISIBLE);
        }
        else {
            prescriptionText.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void didReceivedError(AsyncLoader.Status errorCode) {
        progressDialog.dismiss();
        if (parser != null){
            parser.cancel(true);
            parser = null;
        }
    }

    @Override
    public void didReceivedData(Object result) {

        if (result != null && result.equals(SUCCESS)){
            adapter.remove(bitmaps.get(i));
            adapter.notifyDataSetChanged();
            imageCount ++;
            i--;
            if (i>= 0){
                String ic = encodeToBase64(bitmaps.get(i));
                prescription = new Prescription();
                prescription.fileEncodedValue = ic;
                prescription.memberId = memberid;
                prescription.sessionID = sessionID;
                prescription.clientID = clientID;
                if (parser != null){
                    parser.cancel(true);
                    parser = null;
                }
                getParser().getUploadPrescription(prescription);
                if (progressDialog != null){
                    progressDialog.setMessage("uploading "+imageCount+" of "+ totalCount);
                }
            }
            else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setTitle("Success");
                alertDialog.setMessage("Uploaded Successfully!\n" +
                        "You can check your prescription in EHR");

                alertDialog.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                finish();
                            }
                        });
                alertDialog.show();
                progressDialog.dismiss();
                if (parser != null){
                    parser.cancel(true);
                    parser = null;
                }
                totalCount = 0;
                imageCount = 1;
                i = 0;

            }
        }
        else {
            progressDialog.dismiss();
            if (parser != null){
                parser.cancel(true);
                parser = null;
            }
            Toast.makeText(this, "Error in Uploading", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void didReceivedProgress(double progress) {

    }
}
