package com.healthsaverz.healthmobile.medicine.ehr.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by priyankranka on 21/04/14.
 */
public class EmrParcelable implements Parcelable {

    public static final String PRESCRIPTION_FILES = "prescriptionFiles";
    public static final String PRESCRIPTION_FILE = "prescriptionFile";
    public static final String CLIENT_ID = "healthsaverzm";
    public static final String MESSAGE = "message";
    public static final String SESSION_ID = "sessionID";
    public static final String CREATED_BY = "created_By";
    public static final String CREATED_ON = "created_On";
    public static final String MEMBER_ID = "memberId";
    public static final String PRESCRIPTION_ID = "prescriptionId";
    public static final String PRESCRIPTION_IMAGE_URL = "prescriptionImageUrl";
    public static final String STATUS = "status";



    public String clientID;
    public String message;
    public String sessionID;
    public String created_By;
    public String created_On;
    public String memberId;
    public String prescriptionId;
    public String prescriptionImageUrl;
    public String status;

    public EmrParcelable(HashMap<String, String> hashMap)
    {
        this.clientID = hashMap.get(CLIENT_ID);
        this.message = hashMap.get(MESSAGE);
        this.sessionID = hashMap.get(SESSION_ID);
        this.created_By = hashMap.get(CREATED_BY);
        this.created_On = hashMap.get(CREATED_ON);
        this.memberId = hashMap.get(MEMBER_ID);
        this.prescriptionId = hashMap.get(PRESCRIPTION_ID);
        this.prescriptionImageUrl = hashMap.get(PRESCRIPTION_IMAGE_URL);
        this.status = hashMap.get(STATUS);
    }

    public EmrParcelable(Parcel source) {

        clientID = source.readString();
        message = source.readString();
        sessionID = source.readString();
        created_By = source.readString();
        memberId = source.readString();
        prescriptionId = source.readString();
        created_On = source.readString();
        prescriptionImageUrl = source.readString();
        status = source.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(clientID);
        dest.writeString(message);
        dest.writeString(sessionID);
        dest.writeString(created_By);
        dest.writeString(memberId);
        dest.writeString(prescriptionId);
        dest.writeString(created_On);
        dest.writeString(prescriptionImageUrl);
        dest.writeString(status);
    }

    public static final Creator<EmrParcelable> CREATOR = new Creator<EmrParcelable>() {
        @Override
        public EmrParcelable createFromParcel(Parcel source) {
            return new EmrParcelable(source);
        }

        @Override
        public EmrParcelable[] newArray(int size) {
            return new EmrParcelable[size];
        }
    };

}
