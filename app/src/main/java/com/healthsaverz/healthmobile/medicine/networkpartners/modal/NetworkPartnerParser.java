package com.healthsaverz.healthmobile.medicine.networkpartners.modal;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by healthsaverz5 on 3/25/2015.
 */
public class NetworkPartnerParser extends AsyncTask<String, Void, NetworkPartnerModal> implements AsyncLoaderNew.Listener {


    private static final String METHOD_GETNETWORK_PARTNER = AppConstants.WEB_DOMAIN+"/sHealthSaverz/jaxrs/NetworkPartnerDetailsServices/getNetworkPartnersDetails";
    private static final String TAG = "NETWORK-PARSER";
    private final Context context;
    private AsyncLoaderNew asyncLoaderNew;
    private List<NetworkPartnerModal> listNetworkPartnerList;
    private List<Integer> listNetworkPartnerIDList;
    private List<String> listNetworkPartnerNameList;

    /*.
     default constructor
     */
    public NetworkPartnerParser(Context context) {
        this.context = context;
    }

    /*
     setter getter
     */
    private NetworkPartnerParserLitnr networkPartnerParserLitnr;

    public NetworkPartnerParserLitnr getNetworkPartnerParserLitnr() {
        return networkPartnerParserLitnr;
    }

    public void setNetworkPartnerParserLitnr(NetworkPartnerParserLitnr networkPartnerParserLitnr) {
        this.networkPartnerParserLitnr = networkPartnerParserLitnr;
    }

    /**
     * interface declarations
     */

    public interface NetworkPartnerParserLitnr {
        void networkPartnerParserDidReceivedMenu(NetworkPartnerModal netwkPartner, List<String> listNetworkPartnerNameList, List<Integer> listNetworkPartnerIDList, List<NetworkPartnerModal> listNetworkPartnerList);

        void networkPartnerParserDidReceivedConnectionError(AsyncLoaderNew.Status status);// AsyncLoaderNew.Status

        void networkPartnerParserDidReceivedProcessingError(String message);//server message for unsuccessful
    }


    /**
     * public method to get details from server
     */
    public void getNetworkPartnerFromSever() {
        /**
         *  get Network Partner from server
         */
        Log.d(TAG, "in parser class");
        asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.GET);
        asyncLoaderNew.setListener(this);
        String sessionID = PreferenceManager.getStringForKey(context, PreferenceManager.SESSION_ID, "");
        String clientID = "healthsaverz";

        asyncLoaderNew.executeRequest(METHOD_GETNETWORK_PARTNER + "/" + clientID + "/" + sessionID);

    }


    /**
     * ***************************************************
     */
    @Override
    protected NetworkPartnerModal doInBackground(String... params) {
        String k = params[0];
        Log.d(TAG, "parser :doInBackground-----METHOD_GETNETWORK_PARTNER processing---------::" + k);
        NetworkPartnerModal networkPartnerModal = new NetworkPartnerModal();
        try {
            listNetworkPartnerList = new ArrayList<NetworkPartnerModal>();
            listNetworkPartnerIDList = new ArrayList<Integer>();
            listNetworkPartnerNameList = new ArrayList<String>();
            JSONArray jsonArray = new JSONArray(params[0]);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObj = jsonArray.getJSONObject(i);
                networkPartnerModal.id = jsonObj.getInt("id");
                networkPartnerModal.name = jsonObj.getString("name");
                networkPartnerModal.status = jsonObj.getString("status");
                networkPartnerModal.description = jsonObj.getString("description");
                Log.i("Name-----ID-", networkPartnerModal.name + "networkPartnerModal.id" + networkPartnerModal.id);
                listNetworkPartnerIDList.add(networkPartnerModal.id);
                listNetworkPartnerNameList.add(networkPartnerModal.name);
                listNetworkPartnerList.add(networkPartnerModal);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return networkPartnerModal;

    }

    @Override
    protected void onPostExecute(NetworkPartnerModal s) {
        super.onPostExecute(s);
        if (s == null) {
            if (networkPartnerParserLitnr != null) {
                networkPartnerParserLitnr.networkPartnerParserDidReceivedProcessingError("Invalid Data");
            }
        } else {
            if (networkPartnerParserLitnr != null) {
                networkPartnerParserLitnr.networkPartnerParserDidReceivedMenu(s, listNetworkPartnerNameList, listNetworkPartnerIDList, listNetworkPartnerList);

            }
        }
    }

    /**
     * ***************************************************
     */

    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {
        Log.d(TAG, "didReceivedError============" + status);

    }

    @Override
    public void didReceivedData(byte[] data) {
        String responseString = new String(data);
        Log.d(TAG, "DATA:" + responseString);
        execute(responseString);

    }

    @Override
    public void didReceivedProgress(Double progress) {
        Log.d(TAG, "didReceivedProgress============" + progress);

    }
}
