package com.healthsaverz.healthmobile.medicine.asyncloader;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;


import com.healthsaverz.healthmobile.medicine.BuildConfig;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by montydudani on 18/02/14.
 */

public class AsyncLoader extends AsyncTask<String, Double, byte[]> {

    public Context context;

    public enum RequestMethod {
        GET,
        POST,
        REST_GET,
        REST_POST,
        SOAP_GET,
        SOAP_POST,
        PUT
    }

    public enum DownloadState {
        NOT_DOWNLOADED,
        DOWNLOADING,
        DOWNLOADED;
    }

    public enum Status {
        IDEAL,
        SUCCESS,
        CONNECTIVITY_ERROR,
        HTTP_404,
        MALFORMED_URL,
        URI_SYNTAX_ERROR,
        UNSUPPORTED_ENCODING,
        PULL_PARSER_EXCEPTION,
        PARSING_EXCEPTION,
        IO_EXCEPTION,
        FAILED,
        NOT_DEFINED
    }

    public interface Listener {
        public void didReceivedError(Status errorCode);

        public void didReceivedData(byte[] data);

        public void didReceivedProgress(double progress);
    }

    public static final String TAG = "AsyncLoader";
    public String requestMessage = "";
    public HashMap<String, String> headers;

    public HashMap<String, String> getHeaders() {
        if (headers == null) {
            headers = new HashMap<String, String>();
        }
        return headers;
    }

    private RequestMethod requestMethod;
    private DownloadState state;
    private Status status;
    private Listener listener;

    public AsyncLoader(RequestMethod requestMethod) {
        this.requestMethod = requestMethod;
    }

    public Listener getListener() {
        return listener;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void executeRequest(String requestURL) {
        try {
            requestURL = requestURL.trim();
            if (requestURL.indexOf("%", 0) == -1) {
                URL url = new URL(requestURL);
                URI uri = null;
                uri = new URI(url.getProtocol(), url.getUserInfo(),
                        url.getHost(), url.getPort(), url.getPath(),
                        url.getQuery(), url.getRef());

                url = uri.toURL();
                requestURL = String.valueOf(url);
            }
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Input URL -> " + requestURL);
            }

        } catch (URISyntaxException | MalformedURLException e) {
            e.printStackTrace();
        }
        this.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, requestURL);
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (!ConnectionDetector.isConnectingToInternet(context)) {
            if (this.listener != null) {
                this.listener.didReceivedError(Status.CONNECTIVITY_ERROR);
            }
            this.cancel(true);
        }
    }

    @Override
    protected byte[] doInBackground(String... params) {
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse httpResponse = null;
            if (!this.isCancelled()) {
                if (this.requestMethod.equals(RequestMethod.REST_GET) || this.requestMethod.equals(
                        RequestMethod.GET)) {
                    HttpGet httpGet = new HttpGet(params[0]);
                    httpResponse = httpClient.execute(httpGet);
                } else if (this.requestMethod.equals(RequestMethod.REST_POST) ||
                        this.requestMethod.equals(RequestMethod.POST)) {
                    HttpPost httpPost = new HttpPost(params[0]);
                    httpResponse = httpClient.execute(httpPost);
                } else if (this.requestMethod.equals(RequestMethod.SOAP_POST)) {
                    HttpPost httpPost = new HttpPost(params[0]);
                    for (String key : getHeaders().keySet()) {
                        httpPost.setHeader(key, getHeaders().get(key));
                    }
                    httpPost.setEntity(new StringEntity(this.requestMessage));
                    httpResponse = httpClient.execute(httpPost);
                } else if (this.requestMethod.equals(RequestMethod.PUT)) {
                    HttpPut httpPut = new HttpPut(params[0]);
                    for (String key : getHeaders().keySet()) {
                        httpPut.setHeader(key, getHeaders().get(key));
                    }
                    httpPut.setEntity(new StringEntity(this.requestMessage));
                    httpResponse = httpClient.execute(httpPut);
                }
                state = DownloadState.DOWNLOADED;
                status = Status.SUCCESS;
                return EntityUtils.toByteArray(httpResponse.getEntity());
            }
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, e.toString());
            status = Status.UNSUPPORTED_ENCODING;

        } catch (ClientProtocolException e) {
            Log.e(TAG, e.toString());
            status = Status.HTTP_404;
        } catch (IOException e) {
            Log.e(TAG, e.toString());
            status = Status.CONNECTIVITY_ERROR;

        }
        state = DownloadState.NOT_DOWNLOADED;
        this.cancel(true);
        return null;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if (this.listener != null) {
            this.listener.didReceivedError(status);
        }
    }

    @Override
    protected void onPostExecute(byte[] bytes) {
        super.onPostExecute(bytes);
        if (this.listener != null) {
            if (status.equals(Status.SUCCESS)) {
                this.listener.didReceivedData(bytes);
            } else {
                this.listener.didReceivedError(status);
            }
        }
    }

    // If % character is not found that means we need to perform escape of
    // the percentage characters
    // so that url is properly formatted. if percentage character is found
    // that means url string already contains
    // escape character so we ignore injecting the percentage character
    @Override
    protected void onProgressUpdate(Double... values) {
        super.onProgressUpdate(values);
        if (this.listener != null) {
            this.listener.didReceivedProgress(values[0]);
        }
    }
}