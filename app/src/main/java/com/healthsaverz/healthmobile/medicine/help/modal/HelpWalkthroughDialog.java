package com.healthsaverz.healthmobile.medicine.help.modal;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;

/**
 * Created by Ibrahim on 30-12-2014.
 */
public class HelpWalkthroughDialog extends DialogFragment implements View.OnTouchListener, ViewPager.OnPageChangeListener {

    public static final String TRUE = "true";
    public static final String TAG = "HelpWalkthroughDialog";
    private Context context;

    private ViewPager helpViewer;
    private Button skipButton;
    private int[] mCards = {

            R.drawable.screen1, //page_1_6
            R.drawable.screen2, //screen_temp
            R.drawable.screen3, //page_34
            R.drawable.screen4, //page_35
            R.drawable.screen5,
            R.drawable.screen6
    };
    public ViewPager getHelpViewer() {
        if (helpViewer == null){
            helpViewer = (ViewPager) getView().findViewById(R.id.helpViewer);
        }
        return helpViewer;
    }

    public Button getSkipButton() {
        if (skipButton == null){
            skipButton = (Button) getView().findViewById(R.id.skipButton);
        }
        return skipButton;
    }


    public static HelpWalkthroughDialog newInstance(Context context){
        HelpWalkthroughDialog helpWalkthroughDialog = new HelpWalkthroughDialog();
        helpWalkthroughDialog.context = context;
        return helpWalkthroughDialog;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_help_walkthrough, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getSkipButton().setOnTouchListener(this);
        HelpViewPagerAdapter helpViewPagerAdapter = new HelpViewPagerAdapter();
        getHelpViewer().setAdapter(helpViewPagerAdapter);
        getHelpViewer().setOnPageChangeListener(this);

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_SmileWindow;
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        Window window = dialog.getWindow();
//        window.setLayout(100, 50);
//        window.setGravity(Gravity.CENTER);
        return dialog;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN)
        {
            ViewHelper.fadeOut(view, null);
            return true;
        }
        else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP)
        {
            ViewHelper.fadeIn(view, null);
            dismiss();
            return true;
        }
        return false;
    }

    //ViewPager.OnPageChangeListener Implementation
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        if (position == mCards.length-1) {
            getSkipButton().setText("Get Started");
        }
        else {
            getSkipButton().setText("Skip");
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public class HelpViewPagerAdapter extends PagerAdapter {


        @Override
        public int getCount() {
            return mCards.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView imageView = new ImageView(context);
//            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(convertToDPUnit(getActivity(), 24), convertToDPUnit(getActivity(), 24));
//            imageView.setLayoutParams(layoutParams);
//            int padding = context.getResources().getDimensionPixelSize(R.dimen.standard_margin);
//            imageView.setPadding(padding, padding, padding, padding);
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imageView.setImageResource(mCards[position]);
            container.addView(imageView, 0);
            return imageView;
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((ImageView) object);
        }
    }
}
