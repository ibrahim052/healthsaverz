package com.healthsaverz.healthmobile.medicine.bookaservice.controller;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoader;
import com.healthsaverz.healthmobile.medicine.asyncloader.ParserListener;
import com.healthsaverz.healthmobile.medicine.bookaservice.model.Deal;
import com.healthsaverz.healthmobile.medicine.global.Parser;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;

import java.util.ArrayList;

public class DealsActivity extends Activity implements View.OnTouchListener, ParserListener, AdapterView.OnItemClickListener {

    private ListView dealsList;
    private Parser parser;
    private ProgressBar progressBar;
    private DealsListAdapter dealsListAdapter;
    private ArrayList<Deal> dealArrayList;

    public Parser getParser() {
        if (parser == null) {
            parser = new Parser(this);
            parser.setListener(this);
        }
        return parser;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deals);
        progressBar = (ProgressBar)findViewById(R.id.progressbar);
        progressBar.setVisibility(View.VISIBLE);
        dealsList = (ListView) findViewById(R.id.dealsList);
        Button tabLocation = (Button) findViewById(R.id.tabLocation);
        Button tabPartner = (Button) findViewById(R.id.tabPartner);
        Button tabCategory = (Button) findViewById(R.id.tabCategory);
        Button tabDeals = (Button) findViewById(R.id.tabDeals);
        Button doneButton = (Button) findViewById(R.id.doneButton);
        dealsList.setOnItemClickListener(this);
        tabLocation.setOnTouchListener(this);
        tabPartner.setOnTouchListener(this);
        tabCategory.setOnTouchListener(this);
        tabDeals.setOnTouchListener(this);
        doneButton.setOnTouchListener(this);
        getParser().getAllDeals();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_deals, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(view, null);
            return true;
        }
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
            ViewHelper.fadeIn(view, null);
            switch (view.getId()){
                case R.id.doneButton:
                    
                return true;
            }

        }
        return false;
    }

    @Override
    public void didReceivedError(AsyncLoader.Status errorCode) {
        if (parser!= null){
            parser.cancel(true);
            parser = null;
        }
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void didReceivedData(Object result) {
        if (parser!= null){
            parser.cancel(true);
            parser = null;
        }
        progressBar.setVisibility(View.GONE);
        dealArrayList = (ArrayList<Deal>) result;
        dealsListAdapter = new DealsListAdapter(this, dealArrayList);
        dealsList.setAdapter(dealsListAdapter);
    }

    @Override
    public void didReceivedProgress(double progress) {
        if (parser!= null){
            parser.cancel(true);
            parser = null;
        }
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, "Still Loading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Deal deal = (Deal) parent.getItemAtPosition(position);
        Intent intent = new Intent(this, DealsDetailActivity.class);
        intent.putExtra("DealID", deal.bookAserviceDealId);
        startActivityForResult(intent, 69);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==69) {
            int dealID = data.getIntExtra("dealID", -1);
            if (dealID != -1) {
                for (Deal deal : dealArrayList) {
                    if (deal.bookAserviceDealId == dealID) {
                        deal.isChecked = true;
                    }
                }
            }
            //Log.d("TAGLKJSKJD", dealArrayList.toString());
            dealsListAdapter.notifyDataSetChanged();
        }
    }
}
