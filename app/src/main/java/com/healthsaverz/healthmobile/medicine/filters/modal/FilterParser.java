package com.healthsaverz.healthmobile.medicine.filters.modal;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.filters.controller.FilterActivity;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by DG on 11/10/2014.
 */
public class FilterParser extends AsyncTask<String, Void, FilterMaster> implements AsyncLoaderNew.Listener  {


    private static final String METHOD_FILTER_CATEGORY_ID = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/FilterService/getFiltersByCategory/";
    private final Context context;

    public FilterParser(Context context) {
        this.context = context;
    }


    public interface FilterParserListener{
        void filterParserDidReceivedFilter(FilterMaster filterMaster);
        void filterParserDidReceivedConnectionError(AsyncLoaderNew.Status status);// AsyncLoaderNew.Status
        void filterParserDidReceivedProcessingError(String message);//server message for unsuccessful
    }

    private FilterParserListener filterParserListener;

    public void setFilterParserListener(FilterParserListener filterParserListener) {
        this.filterParserListener = filterParserListener;
    }




    //Local methods
    public  void validateCategeroy(String categoryId) {

        AsyncLoaderNew asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.GET);


        String userClientID ="healthsaverzn";
        String userSessionID = "test";
        asyncLoaderNew.setListener(this);
        asyncLoaderNew.executeRequest(METHOD_FILTER_CATEGORY_ID + userSessionID + "/" + userClientID + "/" + categoryId);




    }
    //AsyncTask Stack
    @Override
    protected FilterMaster doInBackground(String... strings) {
        FilterMaster filterMaster = null;
        FilterType filterType;
        Filter filter;

        if (!isCancelled()) {
            try {
                JSONArray jsonArray = new JSONArray(strings[0]);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    filter = new Filter();
                    filter.filterId = jsonObject1.getInt("filterId");
                    filter.attributeValue   =   jsonObject1.getString("attributeValue");
                    filter.attributeDisplayType   =   jsonObject1.getString("attributeDisplayType");
                    filter.isChecked                =   false;

                    if(filterMaster == null){
                        filterMaster = new FilterMaster();
                        filterMaster.categoryid =   jsonObject1.getInt("categoryId");
                        filterMaster.filterTypes = new ArrayList<>();
                    }

                    if(filterMaster.filterTypes.size() == 0){
                        filterType  =   new FilterType();
                        filterType.attributeType = jsonObject1.getString("attributeType");
                        filterType.attributeCategoryType = jsonObject1.getString("attributeCategoryType");
                        filterType.filters = new ArrayList<>();
                        filterType.filters.add(filter);
                        filterMaster.filterTypes.add(filterType);
                    }
                    else
                    {
                        int count = 0;
                        for(FilterType filterType1:filterMaster.filterTypes){

                            if(filterType1.attributeType.equals(jsonObject1.getString("attributeType"))){
                                /*
                                If BRAND is already added than you need to add filter directly.
                                 */
                                filterType1.filters.add(filter);
                                break;
                            }
                            else {
                                count++;
                            }
                        }

                        //Log.d("FilterParser","count is "+count+" and size is "+filterMaster.filterTypes.size());
                        if(count == filterMaster.filterTypes.size()){
                            /*
                            this means type is not added we need to make new filter type.
                             */
                            filterType  =   new FilterType();
                            filterType.attributeType = jsonObject1.getString("attributeType");
                            filterType.attributeCategoryType = jsonObject1.getString("attributeCategoryType");
                            filterType.filters = new ArrayList<>();
                            filterType.filters.add(filter);
                            filterMaster.filterTypes.add(filterType);
                        }
                    }

                }

                return filterMaster;

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        else{
            return null;
        }
        return null;


    }

    @Override
    protected void onPostExecute(FilterMaster filterMaster) {
        super.onPostExecute(filterMaster);

        if(filterMaster == null){
            if(filterParserListener != null){
                filterParserListener.filterParserDidReceivedProcessingError("Invalid Data");
            }
        }
        else
        {
            if(filterParserListener != null){
                filterParserListener.filterParserDidReceivedFilter(filterMaster);
            }
        }

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }


    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {

        if(filterParserListener != null){
            filterParserListener.filterParserDidReceivedConnectionError(status);
        }
    }

    @Override
    public void didReceivedData(byte[] data) {
        String responseString = new String(data);
        Log.d("FilterByCategoryParser", "didReceivedData is " + responseString);
        execute(responseString);

    }

    @Override
    public void didReceivedProgress(Double progress) {

    }
}
