package com.healthsaverz.healthmobile.medicine.register.controller;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.DialogProgressFragment;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;
import com.healthsaverz.healthmobile.medicine.global.UseCases;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
import com.healthsaverz.healthmobile.medicine.login.modal.LoginUserParser;
import com.healthsaverz.healthmobile.medicine.mainscreen.controller.MainScreenActivity;
import com.healthsaverz.healthmobile.medicine.register.model.RegisterUserParser;
import com.healthsaverz.healthmobile.medicine.register.model.VerifyUserParser;

public class RegisterActvity extends Activity implements View.OnTouchListener, RegisterUserParser.RegisterUserParserListener {

    private Button signUP;

    EditText firstName;
//    EditText lastName;
    EditText email;
    EditText password;
    EditText cellNumber;
    EditText postalCode;
    TextView termsText;
//    ImageView   male_check;
//    ImageView   female_check;
    Button action_signUp;


    boolean isMale = true; // default male radio button is active

    public static final String TAG = RegisterActvity.class.getSimpleName();

    RegisterUserParser registerUserParser = null;
    VerifyUserParser verifyUserParser   =   null;
    LoginUserParser loginUserParser = null;

    DialogProgressFragment progressFragment;


    public Button getSignUP() {
        if (signUP == null) {
            signUP = (Button) this.findViewById(R.id.signUP);
            signUP.setOnTouchListener(this);
        }
        return signUP;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getActionBar().hide();
        //getSignUP();

        if(firstName == null){
            firstName = (EditText)findViewById(R.id.firstName);
        }

        if(email == null){
            email = (EditText)findViewById(R.id.email);

            AccountManager accountManager = AccountManager.get(this);
            Account[] accounts = accountManager.getAccountsByType("com.google");

            Account account = (accounts.length > 0)?accounts[0]:null;
            if (account != null) {
                email.setText("");
                email.append(account.name);
            }
        }
        if(password == null){
            password = (EditText)findViewById(R.id.password);
        }
        if(cellNumber == null){
            cellNumber = (EditText)findViewById(R.id.cellNumber);
            TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
            String number = tm.getLine1Number();
            if(number != null)
            {
                cellNumber.setText("");
                cellNumber.append(number);
            }
        }
        if(postalCode == null){
            postalCode = (EditText)findViewById(R.id.postalCode);
        }
        if (termsText == null){
            termsText = (TextView)findViewById(R.id.termsText);
            Spanned spanned = Html.fromHtml(getString(R.string.terms_policy));
            termsText.setMovementMethod(LinkMovementMethod.getInstance());
            termsText.setText(spanned);


        }
        if(action_signUp == null){
            action_signUp = (Button)findViewById(R.id.action_signUp);
            action_signUp.setOnTouchListener(this);
        }

        int showDialog = getIntent().getIntExtra("showVerificationDialog",-1);

        if(showDialog == 1)
        {
//            DialogVerificationCodeFragment dialogVerificationCodeFragment =DialogVerificationCodeFragment.newInstance(this);
//            dialogVerificationCodeFragment.show(getFragmentManager(), TAG);
//            dialogVerificationCodeFragment.setCancelable(false);
            startActivity(new Intent(this, VerificationCodeActivity.class));
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(v, null);
            return true;
        }
        if (event.getActionMasked() == MotionEvent.ACTION_UP) {
            ViewHelper.fadeIn(v, null);
            if(v.getId() == R.id.action_signUp) {
                //UseCases.startActivityByClearingStack(RegisterActvity.this, MainScreenActivity.class);

                boolean isValid =  validateForm();
                if(isValid){
                    //We will call the parser and register on the server

                    if(registerUserParser != null)
                    {
                        registerUserParser.cancel(true);
                        registerUserParser = null;
                    }

                    registerUserParser = new RegisterUserParser(this);
                    registerUserParser.setRegisterUserListener(this);
                    String gender = null;
                    if(isMale)
                        gender  = "Male";
                    else
                        gender  = "Female";

                    if(progressFragment != null)
                    {
                        progressFragment = null;
                    }
                    progressFragment =  new DialogProgressFragment();
                    progressFragment.show(getFragmentManager(), TAG);
                    progressFragment.setCancelable(false);
                    PreferenceManager.saveStringForKey(this,PreferenceManager.PASSWORD,password.getText().toString().trim());
                    registerUserParser.registerUser(password.getText().toString().trim(),firstName.getText().toString().trim(),
                            "",gender,postalCode.getText().toString().trim(),cellNumber.getText().toString().trim(),
                            email.getText().toString().trim());
                }

            }
            return true;
        }
        return false;
    }

    //local methods for validation

    public  boolean validateForm(){

        String errorMessage = null;
        boolean isErrorOccured = false;

        if(email.getText().toString().trim().equals("")){

            errorMessage = "Email id can not be empty.";
            isErrorOccured = true;
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString().trim()).matches())
        {
            errorMessage = "Invalid email id.";
            isErrorOccured = true;
        }
        else if(cellNumber.getText().toString().trim().equals("")){

            errorMessage = "Cell phone can not be empty.";
            isErrorOccured = true;
        }
        else if (!Patterns.PHONE.matcher(cellNumber.getText().toString().trim()).matches())
        {
            errorMessage = "Invalid Cell Number.";
            isErrorOccured = true;
        }
        else if (cellNumber.getText().toString().trim().length() <= 9)
        {
            errorMessage = "Cell Number should have country code followed by 10 digit number.";
            isErrorOccured = true;
        }
        else if(password.getText().toString().trim().equals("")){

            errorMessage = "Password can not be empty";
            isErrorOccured = true;
        }
        else if (password.getText().toString().trim().length() < 6)
        {
            errorMessage = "Password should have at least 6 characters.";
            isErrorOccured = true;
        }
        else if (!firstName.getText().toString().equals("")&& firstName.getText().toString().trim().length() < 3)
        {
            errorMessage = "Enter a valid Name.";
            isErrorOccured = true;
        }
        if(isErrorOccured){

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            // Setting Dialog Title
            alertDialog.setTitle("Validation Error");
            // Setting Dialog Message
            alertDialog.setMessage(errorMessage);

            // Setting Positive "OK" Btn
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            dialog.cancel();
                        }
                    });
            // Showing Alert Dialog
            alertDialog.show();

            return false;
        }
        else {
            return true;
        }
    }

    //Callbacks for RegisterUserParser
    @Override
    public void registerUserParserDidUserRegistered(String message) {

        //Log.d("registerUserParserDidUserRegistered"," "+message);
        progressFragment.dismiss();
        progressFragment = null;

        if(message.equals("100"))//this means success we need to open dialog box for entering verification code
        {
//            DialogVerificationCodeFragment dialogVerificationCodeFragment =DialogVerificationCodeFragment.newInstance(this);
//            dialogVerificationCodeFragment.show(getFragmentManager(), TAG);
//            dialogVerificationCodeFragment.setCancelable(false);
            startActivity(new Intent(this, VerificationCodeActivity.class));
        }
        else if(message.equals("101"))//this means success we need to open dialog box for entering verification code
        {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Registration Error");
            alertDialog.setMessage("Invalid Verification Code");
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            dialog.cancel();
                        }
                    });
            // Showing Alert Dialog
            alertDialog.show();
        }
        else
        {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Registration Error");
            alertDialog.setMessage(message);
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            dialog.cancel();
                        }
                    });
            // Showing Alert Dialog
            alertDialog.show();
        }
    }

    @Override
    public void registerUserParserDidReceivedConnectionError(AsyncLoaderNew.Status status) {

        progressFragment.dismiss();
        progressFragment = null;
        //Log.d("registerUserParserDidReceivedConnectionError"," "+status);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Verification Error");
        alertDialog.setMessage(AppConstants.ConnectionError(status));
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }

    @Override
    public void registerUserParserDidReceivedProcessingError(String message) {
        //Log.d("registerUserParserDidReceivedProcessingError"," "+message);

        progressFragment.dismiss();
        progressFragment = null;

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Registration Error");
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }

}
