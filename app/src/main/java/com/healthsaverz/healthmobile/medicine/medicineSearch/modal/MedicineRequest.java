package com.healthsaverz.healthmobile.medicine.medicineSearch.modal;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DG on 10/10/2014.
 */
public class MedicineRequest implements Parcelable {

    public String clientID = "healthsaverzm";
    public String sessionID = "test";
    public String SearchTitle = "";
    public int StartRow =0;
    public int EndRow = 10;

    public MedicineRequest(String searchTitle) {
        SearchTitle = searchTitle;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getSearchTitle() {
        return SearchTitle;
    }

    public void setSearchTitle(String searchTitle) {
        SearchTitle = searchTitle;
    }

    public int getStartRow() {
        return StartRow;
    }

    public void setStartRow(int startRow) {
        StartRow = startRow;
    }

    public int getEndRow() {
        return EndRow;
    }

    public void setEndRow(int endRow) {
        EndRow = endRow;
    }




public MedicineRequest(Context context, String clientID, String sessionID, String searchTitle, int StartRow, int EndRow)
{
    this.sessionID = sessionID;
    this.clientID =clientID;
    this.SearchTitle= searchTitle;
    this.StartRow =StartRow;
    this.EndRow =EndRow;

}
    private MedicineRequest(Parcel in){
        sessionID = in.readString();
        clientID = in.readString();
        SearchTitle = in.readString();
        StartRow = in.readInt();
        EndRow = in.readInt();

    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(sessionID);
        out.writeString(clientID);
        out.writeString(SearchTitle);
        out.writeInt(StartRow);
        out.writeInt(EndRow);

    }

    public static final Creator<MedicineRequest> CREATOR = new Creator<MedicineRequest>() {
        public MedicineRequest createFromParcel(Parcel in) {
            return new MedicineRequest(in);
        }

        public MedicineRequest[] newArray(int size) {
            return new MedicineRequest[size];
        }
    };
}


