package com.healthsaverz.healthmobile.medicine.networkpartners.modal;


import android.content.Context;

/**
 * Created by Priyanka Kale
 * 25/3/2015
 */
public class LocationPartners {

    /*
    [{"sessionID":"","clientID":"","message":"","id":3,"partnerId":2,"partnerName":null,"location":"Delhi","address":"Delhi West","contactNo":"798192781","email":"raj123@gmail.com","status":"A","created_By":1,"extra_1":null,"extra_2":null,"extra_3":null,"extra_4":null,"extra_5":null},{"sessionID":"","clientID":"","message":"","id":6,"partnerId":2,"partnerName":null,"location":"Navi Mumbai","address":"BelaPur","contactNo":"9869933815","email":"ranjeet@healthsaverz.com","status":"A","created_By":1,"extra_1":null,"extra_2":null,"extra_3":null,"extra_4":null,"extra_5":null}]
     */
    public  int partnerId = 0;
    public  String partnerName = "partnerName";
    public  String location = "location";
    public  String address = "address";
    public  String contactNo = "contactNo";
    public  String email = "email";
    public  String sessionID = "sessionID";
    public  String clientID = "clientID";
    public  String message = "message";
    public  String status = "status";
    public  int id = 1;
    public String extra_3 = "";
    public String extra_4 = "";
    public String extra_1 = "";
    public String extra_5 = "";
    public String extra_2 = "";
    public int created_By = 0;
    private Context context;


    public LocationPartners(Context context) {
        this.context = context;
    }

    public LocationPartners(Context context, String message, int partnerId, String partnerName, String email, String location, String contactNo, String status, int id) {
        this.context = context;
        this.message = message;
        this.location = location;
        this.status = status;
        this.contactNo = contactNo;
        this.email = email;
        this.id = id;
        this.partnerName = partnerName;
        this.partnerId = partnerId;
    }


    public LocationPartners() {

    }

}