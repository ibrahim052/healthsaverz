package com.healthsaverz.healthmobile.medicine.global;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

/**
 * Created by montydudani on 29/08/14.
 */
public class UseCases {

    public static boolean isUserRegistered(Context context) {
        return  PreferenceManager.getBooleanForKey(context, PreferenceManager.IS_REGISTRATION_STATUS, false);
    }

    public static void saveRegistrationStatus(Context context, boolean status) {
        PreferenceManager.saveBooleanForKey(context, PreferenceManager.IS_REGISTRATION_STATUS, status);
    }

    public static boolean isContactsSynced(Context context) {
        return  PreferenceManager.getBooleanForKey(context, PreferenceManager.IS_CONTACTS_SYNCED, false);
    }

    public static void saveContactsSynced(Context context, boolean status) {
        PreferenceManager.saveBooleanForKey(context, PreferenceManager.IS_CONTACTS_SYNCED, status);
    }

    public static void saveCountryCode(Context context, String code) {
        PreferenceManager.saveStringForKey(context,PreferenceManager.COUNTRY_CODE, code);
    }

    public static String getCountryCode(Context context) {
        return PreferenceManager.getStringForKey(context, PreferenceManager.COUNTRY_CODE, "+1");
    }

    public static void saveUserID(Context context, String userID) {
        PreferenceManager.saveStringForKey(context,PreferenceManager.ID, userID);
    }

    public static String getUserID(Context context) {
        return PreferenceManager.getStringForKey(context, PreferenceManager.ID, "-1");
    }

    public static void saveUserName(Context context, String userName) {
        PreferenceManager.saveStringForKey(context,PreferenceManager.USER_NAME, userName);
    }

    public static String getUserName(Context context) {
        return PreferenceManager.getStringForKey(context, PreferenceManager.USER_NAME, "");
    }

    public static void saveEmail(Context context, String email) {
        PreferenceManager.saveStringForKey(context,PreferenceManager.EMAIL, email);
    }

    public static String getEmail(Context context) {
        return PreferenceManager.getStringForKey(context, PreferenceManager.EMAIL, "");
    }

    public static void saveVerificationCode(Context context, String verificationCode) {
        PreferenceManager.saveStringForKey(context,PreferenceManager.VERIFICATION_CODE, verificationCode);
    }

    public static String getVerificationCode(Context context) {
        return PreferenceManager.getStringForKey(context, PreferenceManager.VERIFICATION_CODE, "1234");
    }

    public static String encodeToBase64(Bitmap image)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    public static void startActivityByClearingStack(Context context, Class<?> aClass) {
        Intent intent = new Intent(context, aClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

}
