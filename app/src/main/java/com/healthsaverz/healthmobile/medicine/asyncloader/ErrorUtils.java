package com.healthsaverz.healthmobile.medicine.asyncloader;

import android.app.AlertDialog;
import android.content.Context;

/**
 * Created by montydudani on 06/05/14.
 */
public class ErrorUtils {
    public static void showError(Context context, AsyncLoader.Status errorCode) {
        String msg = "";
        if (errorCode == AsyncLoader.Status.CONNECTIVITY_ERROR ||
                errorCode == AsyncLoader.Status.HTTP_404 ||
                errorCode == AsyncLoader.Status.MALFORMED_URL ||
                errorCode == AsyncLoader.Status.URI_SYNTAX_ERROR ||
                errorCode == AsyncLoader.Status.UNSUPPORTED_ENCODING) {
            msg = "Error occured while connecting";
        } else if (errorCode == AsyncLoader.Status.PULL_PARSER_EXCEPTION ||
                errorCode == AsyncLoader.Status.PARSING_EXCEPTION ||
                errorCode == AsyncLoader.Status.IO_EXCEPTION) {
            msg = "Error occured while processing";
        }

        new AlertDialog.Builder(context)
                .setMessage(msg)
                .setPositiveButton("OK", null)
                .show();
    }

}
