package com.healthsaverz.healthmobile.medicine.mainscreen.controller;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.DialogProgressFragment;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;
import com.healthsaverz.healthmobile.medicine.global.UseCases;
import com.healthsaverz.healthmobile.medicine.login.controller.LoginActivity;
import com.healthsaverz.healthmobile.medicine.mainscreen.modal.UpdateDeviceInfoParser;
import com.healthsaverz.healthmobile.medicine.mainscreen.modal.UserMedicineNameParser;

import java.util.ArrayList;

public class MainScreenActivity extends FragmentActivity implements UpdateDeviceInfoParser.UpdateDeviceParserListener, UserMedicineNameParser.UserMedicineNameParserListener {

    private static final String TAG = "MainScreenActivity";
    private ProgressDialog progressFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        getActionBar().setTitle("");
        getActionBar().setIcon(R.drawable.myhealth);
        Intent intent = getIntent().getParcelableExtra("resultintent");
        if (intent != null){
            startActivity(intent);
        }
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, MainScreenFragment.newInstance(), MainScreenFragment.TAG)
                    .commit();
        }
//        TelephonyManager telephonyManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
//        String id1 = PreferenceManager.getStringForKey(this, PreferenceManager.DEVICE_UDID, "");
//        String id2 = telephonyManager.getDeviceId();
//        if (!PreferenceManager.getStringForKey(this, PreferenceManager.DEVICE_UDID, "").equals(telephonyManager.getDeviceId())){
//            getAlert("Updating Device...");
//
//        }
    }

    private void getAlert(final String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle("Device Changed!");
        // Setting Dialog Message
        alertDialog.setMessage("Your device has been changed, Do you want to save this device for future Operations?");
        alertDialog.setCancelable(false);
        // Setting Positive "OK" Btn
        alertDialog.setNegativeButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
//                            Write your code here to execute after dialog
                        if(progressFragment != null)
                        {
                            progressFragment = null;
                        }
                        progressFragment = ProgressDialog.show(MainScreenActivity.this, "Working", message, true, false);
                        UpdateDeviceInfoParser updateDeviceInfoParser = new UpdateDeviceInfoParser(MainScreenActivity.this);
                        updateDeviceInfoParser.setUpdateDeviceListener(MainScreenActivity.this);
                        updateDeviceInfoParser.updateDevice();
                        dialog.cancel();
                    }
                });
        // Setting Positive "OK" Btn
        alertDialog.setPositiveButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        PreferenceManager.remove(MainScreenActivity.this, PreferenceManager.ID);
                        PreferenceManager.remove(MainScreenActivity.this, PreferenceManager.SESSION_ID);
                        PreferenceManager.remove(MainScreenActivity.this, PreferenceManager.ADDRESS1);
                        PreferenceManager.remove(MainScreenActivity.this, PreferenceManager.ADDRESS2);
                        PreferenceManager.remove(MainScreenActivity.this, PreferenceManager.CELL_NUMBER);
                        PreferenceManager.remove(MainScreenActivity.this, PreferenceManager.CITY);
                        PreferenceManager.remove(MainScreenActivity.this, PreferenceManager.CLIENT_ID);
                        PreferenceManager.remove(MainScreenActivity.this, PreferenceManager.COUNTRY);
                        PreferenceManager.remove(MainScreenActivity.this, PreferenceManager.CREATED_BY);
                        PreferenceManager.remove(MainScreenActivity.this, PreferenceManager.EMAIL);
                        PreferenceManager.remove(MainScreenActivity.this, PreferenceManager.VERIFICATION_FLAG);
                        PreferenceManager.remove(MainScreenActivity.this, PreferenceManager.VERIFICATION_CODE);
                        PreferenceManager.remove(MainScreenActivity.this, PreferenceManager.USER_NAME);
                        PreferenceManager.remove(MainScreenActivity.this, PreferenceManager.EMAIL_ADDRESS);
                        PreferenceManager.remove(MainScreenActivity.this, PreferenceManager.GENDER);
                        UseCases.startActivityByClearingStack(MainScreenActivity.this, LoginActivity.class);
                        dialog.cancel();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }

    @Override
    public void updateDeviceParserDidDeviceChanged(String message) {
        if (progressFragment != null) {
            progressFragment.dismiss();
        }
        Toast.makeText(this, "Device Updated Successfully", Toast.LENGTH_SHORT).show();
        UserMedicineNameParser userMedicineNameParser = new UserMedicineNameParser(this);
        userMedicineNameParser.setUserMedicineNameListener(this);
        userMedicineNameParser.userMedicineName();
        progressFragment =  ProgressDialog.show(MainScreenActivity.this, "Working", "Fetching medicine data", true, false);
    }

    @Override
    public void updateDeviceParserDidReceivedConnectionError(AsyncLoaderNew.Status status) {
        if (progressFragment != null) {
            progressFragment.dismiss();
        }
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Error Updating Device");
        alertDialog.setMessage("Check your Internet Connection");
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                        getAlert("Updating Device...");
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }

    @Override
    public void updateDeviceParserDidReceivedProcessingError(String message) {
        if (progressFragment != null) {
            progressFragment.dismiss();
        }
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Error Updating Device");
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                        getAlert("Updating Device...");
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }

    @Override
    public void userMedicineNameParserDidRecieveData(ArrayList<String> message) {
        if (progressFragment != null) {
            progressFragment.dismiss();
        }
        if (message.size()>0){
            Toast.makeText(this, "Medicine Data Fetched", Toast.LENGTH_SHORT).show();
            //progressFragment =  ProgressDialog.show(MainScreenActivity.this, "Working", "message", true, false);
            Intent intent = new Intent(this, MedicineSelectlistActivity.class);
            intent.putStringArrayListExtra("medList", message);
            startActivity(intent);
        }
        else
        Toast.makeText(this, "No medicine found", Toast.LENGTH_SHORT).show();
        //progressFragment =  ProgressDialog.show(MainScreenActivity.this, "Working", "message", true, false);
        Intent intent = new Intent(this, MedicineSelectlistActivity.class);
        intent.putExtra("medList", message);
        startActivity(intent);
    }

    @Override
    public void userMedicineNameParserDidReceivedConnectionError(AsyncLoaderNew.Status status) {
        if (progressFragment != null) {
            progressFragment.dismiss();
        }
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Error Fetching Medicines");
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                        //getAlert("Updating Device...");
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }

    @Override
    public void userMedicineNameParserDidReceivedProcessingError(String message) {
        if (progressFragment != null) {
            progressFragment.dismiss();
        }
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Error Fetching Medicines");
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                        //getAlert("Updating Device...");
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }
}
