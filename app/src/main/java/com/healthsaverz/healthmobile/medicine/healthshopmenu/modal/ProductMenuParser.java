package com.healthsaverz.healthmobile.medicine.healthshopmenu.modal;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by priyankranka on 14/11/14.
 */
public class ProductMenuParser extends AsyncTask <String,Void,ArrayList<ProductMenu>> implements AsyncLoaderNew.Listener {


    private AsyncLoaderNew asyncLoaderNew;
    private Context context;

    private static final String METHOD_PRODUCT_MENU = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/ProductCategoryServices/getProductCategories/";


    public interface ProductMenuParserListener{
        void productMenuParserDidReceivedMenu(ArrayList<ProductMenu> productMenus1);
        void productMenuDidReceivedConnectionError(AsyncLoaderNew.Status status);// AsyncLoaderNew.Status
        void productMenuDidReceivedProcessingError(String message);//server message for unsuccessful
    }

    private ProductMenuParserListener productMenuParserListener;

    public ProductMenuParserListener getProductMenuParserListener() {
        return productMenuParserListener;
    }

    public void setProductMenuParserListener(ProductMenuParserListener productMenuParserListener) {
        this.productMenuParserListener = productMenuParserListener;
    }

    public ProductMenuParser(Context context) {

        this.context = context;
    }


    //Local methods
    public  void getProductMenu() {

        asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.GET);


        String userClientID ="healthsaverzn";
        String userSessionID = "test";

//        String userClientID = PreferenceManager.getStringForKey(context, PreferenceManager.CLIENT_ID, "-1");
//        String userSessionID = PreferenceManager.getStringForKey(context, PreferenceManager.SESSION_ID, "-1");

//        if(userClientID.equals("") || userClientID.equals("-1")) {
//            userClientID = "healthsaverzn";
//        }
//
//        if(userSessionID.equals("") || userClientID.equals("-1")) {
//            userSessionID = "test";
//        }

        asyncLoaderNew.setListener(this);
        asyncLoaderNew.executeRequest(METHOD_PRODUCT_MENU+userSessionID+"/"+userClientID+"/");

    }

    //AsyncLoaderNew.Listener implementation
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {
        if(productMenuParserListener != null){
            productMenuParserListener.productMenuDidReceivedConnectionError(status);
        }
    }

    @Override
    public void didReceivedData(byte[] data) {

        String responseString = new String(data);
        Log.d("ProductMenuParser", "didReceivedData is " + responseString);
        execute(responseString);
    }

    @Override
    public void didReceivedProgress(Double progress) {


    }

    //ProductMenuParser AsyncTask Stack
    @Override
    protected ArrayList<ProductMenu> doInBackground(String... params) {

        ArrayList<ProductMenu> productMenus = new ArrayList<ProductMenu>();

        if(!isCancelled()) {
            try {
                JSONArray jsonArray = new JSONArray(params[0]);


                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    ProductMenu productMenu = new ProductMenu();

                    productMenu.name                =   jsonObject1.getString("name");
                    productMenu.type                =   jsonObject1.getInt("type");
                    productMenu.masterCategory      =   jsonObject1.getInt("masterCategory");
                    productMenu.productCategoryId   =   jsonObject1.getInt("productCategoryId");

                    //At this point we can define only root levels. Other levels will be define when
                    //user will select the categories at run time.
                    if(productMenu.masterCategory == 0)//this means it is the root category which needs to be displayed
                    {
                        productMenu.show = true;
                        productMenu.level   =   0;
                    }
                    else
                    {
                        productMenu.isExpanded = false;
                        productMenu.level       =   -1;
                    }

                    productMenus.add(productMenu);

                }

                /*
                We need to mark whether they are expandiable or not by checking they have any child node.
                 */
                for(ProductMenu productMenu:productMenus){

                    for(ProductMenu productMenu1:productMenus){

                        if(productMenu1.masterCategory == productMenu.productCategoryId){
                            productMenu.isExpandable = true;
                            break;
                        }
                    }
                }

                return productMenus;

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        else{
            return null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<ProductMenu> productMenus) {
        super.onPostExecute(productMenus);
        if(productMenus == null){
            if(productMenuParserListener != null){
                productMenuParserListener.productMenuDidReceivedProcessingError("Invalid Data");
            }
        }
        else
        {
            if(productMenuParserListener != null){
                productMenuParserListener.productMenuParserDidReceivedMenu(productMenus);
            }
        }
    }

    @Override
    protected void onCancelled(ArrayList<ProductMenu> productMenus) {
        super.onCancelled(productMenus);

        if(productMenuParserListener != null){
            productMenuParserListener.productMenuDidReceivedProcessingError("Processing Abrupt!!");
        }
    }
}
