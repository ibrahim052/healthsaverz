package com.healthsaverz.healthmobile.medicine.pieChart.controller;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Telephony;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.charting.charts.PieChart;
import com.healthsaverz.healthmobile.medicine.charting.data.DataSet;
import com.healthsaverz.healthmobile.medicine.charting.data.Entry;
import com.healthsaverz.healthmobile.medicine.charting.data.PieData;
import com.healthsaverz.healthmobile.medicine.charting.data.PieDataSet;
import com.healthsaverz.healthmobile.medicine.charting.interfaces.OnChartValueSelectedListener;
import com.healthsaverz.healthmobile.medicine.charting.utils.ColorTemplate;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
import com.healthsaverz.healthmobile.medicine.medicine.controller.MedicineDBHandler;
import com.healthsaverz.healthmobile.medicine.medicine.modal.Medicine;
import com.healthsaverz.healthmobile.medicine.medicine.modal.ReminderModal;
import com.healthsaverz.healthmobile.medicine.pieChart.modal.DemoBase;
import com.healthsaverz.healthmobile.medicine.pieChart.modal.PercentFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PieChartActivity extends DemoBase implements OnChartValueSelectedListener, View.OnTouchListener, AdapterView.OnItemSelectedListener {

    private static final int LATE_INDEX = 0;
    private static final int ON_TIME_INDEX = 1;
    private static final int MISS_INDEX = 2;
    private static final int FIFTEEN_DAYS = 15;
    private static final int THIRTY_DAYS = 30;
    private static final int THREE_MONTHS = 91;
    private static final int SIX_MONTHS = 183;
    private static final int ONE_YEAR = 365;
    private static final String PATIENTS = "Patients";
    private static final String MEDICINES = "Medicines";
    private PieChart mChart;
    private Spinner patientSpinner;
    private Spinner daysSpinner;
    private Spinner medSpinner;

    private ArrayList<ReminderModal> reminders;
    private String latePer;
    private String missPer;
    private String onTimePer;
    private int dayInterval;
    private String patientName = "";
    private String medName = "";
    private MedicineDBHandler medicineDBHandler;
    private List<String> medNames;
    private ArrayList<String> patientList;
    private ArrayAdapter<String> medAdapter;
    private ArrayAdapter<String> patientAdapter;
    private ArrayList<String> medList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_piechart);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("Adherence Score");
        medicineDBHandler = new MedicineDBHandler(this);
        reminders = medicineDBHandler.getAllRemindersForDatesPatiMed(FIFTEEN_DAYS, medName, patientName);
        patientList = new ArrayList<>();
        patientList.add(PATIENTS);
        medList = new ArrayList<>();
        medList.add(MEDICINES);
        ArrayList<Medicine> hashlist;
        medNames = new ArrayList<>();
        hashlist = medicineDBHandler.getAllPatientList("p", medName);
        for (Medicine medicine: hashlist){
            patientList.add(medicine.get_patientName());
        }
        hashlist = medicineDBHandler.getAllPatientList("m", patientName);
        for (Medicine medicine: hashlist){
            medList.add(medicine.get_medName()+" "+medicine.get_dose()+" "+medicine.get_unit());
            medNames.add(medicine.get_medName());
        }
        String[] dayList= {
                "15 DAYS",
                "30 DAYS",
                "03 MONTHS",
                "06 MONTHS",
                "01 YEAR"
        };

        dayInterval = FIFTEEN_DAYS;
        mChart = (PieChart) findViewById(R.id.chart1);
        patientSpinner = (Spinner) findViewById(R.id.patientSpinner);
        patientSpinner.setOnItemSelectedListener(this);
        daysSpinner = (Spinner) findViewById(R.id.daysSpinner);
        daysSpinner.setOnItemSelectedListener(this);
        medSpinner = (Spinner) findViewById(R.id.medSpinner);
        medSpinner.setOnItemSelectedListener(this);

        ArrayAdapter<String> dayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, dayList);
        dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        daysSpinner.setAdapter(dayAdapter);

        patientAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, patientList);
        patientAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        patientSpinner.setAdapter(patientAdapter);

        medAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, medList);
        medAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        medSpinner.setAdapter(medAdapter);

        findViewById(R.id.firstShare).setOnTouchListener(this);
        findViewById(R.id.secondShare).setOnTouchListener(this);
        findViewById(R.id.thirdShare).setOnTouchListener(this);
        mChart.setValueFormatter(new PercentFormatter());
        mChart.setUsePercentValues(true);
        mChart.setRotationEnabled(false);
        mChart.setValueTextColor(Color.BLACK);

        mChart.setHoleColorTransparent(true);

        Typeface tf = Typeface.createFromAsset(getAssets(), "OpenSans-Bold.ttf");//OpenSans-Bold.ttf  OpenSans-Regular.ttf

        mChart.setValueTypeface(tf);
        mChart.setCenterTextTypeface(Typeface.createFromAsset(getAssets(), "OpenSans-Semibold.ttf")); //OpenSans-Semibold.ttf   OpenSans-Light.ttf

        mChart.setHoleRadius(60f);
        mChart.setHighlightEnabled(true);
        mChart.setDescription("");

        mChart.setDrawCenterText(true);

        mChart.setDrawHoleEnabled(true);
        mChart.getRadius();

        mChart.setRotationAngle(0);
        mChart.invalidate();
        // add a selection listener
        mChart.setOnChartValueSelectedListener(this);

        setData(FIFTEEN_DAYS, medName, patientName);

        mChart.animateXY(1500, 1500);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.pie, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case android.R.id.home:
                    finish();
                    break;
                case R.id.actionToggleValues: {
                    for (DataSet<?> set : mChart.getData().getDataSets())
                        set.setDrawValues(!set.isDrawValuesEnabled());
                    mChart.invalidate();
                    break;
                }
                case R.id.actionToggleHole: {
                    if (mChart.isDrawHoleEnabled())
                        mChart.setDrawHoleEnabled(false);
                    else
                        mChart.setDrawHoleEnabled(true);
                    mChart.invalidate();
                    break;
                }
                case R.id.actionDrawCenter: {
                    if (mChart.isDrawCenterTextEnabled())
                        mChart.setDrawCenterText(false);
                    else
                        mChart.setDrawCenterText(true);
                    mChart.invalidate();
                    break;
                }
                case R.id.actionToggleXVals: {
                    mChart.setDrawSliceText(!mChart.isDrawSliceTextEnabled());
                    mChart.invalidate();
                    break;
                }
                case R.id.actionSave: {
                    mChart.saveToPath("title" + System.currentTimeMillis(), "");
                    break;
                }
                case R.id.actionTogglePercent:
                    mChart.setUsePercentValues(!mChart.isUsePercentValuesEnabled());
                    mChart.invalidate();
                    break;
                case R.id.animateX: {
                    mChart.animateX(1800);
                    break;
                }
                case R.id.animateY: {
                    mChart.animateY(1800);
                    break;
                }
                case R.id.animateXY: {
                    mChart.animateXY(1800, 1800);
                    break;
                }
            }
        return super.onOptionsItemSelected(item);
    }

    private void setData(int daysBetween, String medName, String patientName) {
        dayInterval = daysBetween;
        ArrayList<String> xVals = new ArrayList<String>();//Text
        ArrayList<Entry> yVals1 = new ArrayList<Entry>();// values
        ArrayList<Integer> colors = new ArrayList<Integer>();// colors
//        for (int c : ColorTemplate.HEALTH_COLORS)
//            colors.add(c);
        int takeCount = medicineDBHandler.getAllRemindersForFilterDatesPatiMed(daysBetween, 2, medName, patientName).size();
        int onTimeCount = medicineDBHandler.getAllRemindersForFilterDatesPatiMed(daysBetween, 1, medName, patientName).size();
        int missCount = medicineDBHandler.getAllRemindersForFilterDatesPatiMed(daysBetween, -1, medName, patientName).size();
        latePer = mChart.getValueFormatter().getFormattedValue((float)takeCount * 100 / (float)reminders.size())+" %";
        onTimePer = mChart.getValueFormatter().getFormattedValue((float)onTimeCount * 100 / (float)reminders.size())+" %";
        missPer = mChart.getValueFormatter().getFormattedValue((float)missCount * 100 / (float)reminders.size())+" %";
        int index = -1;
        if (takeCount >= 1) {
            yVals1.add(new Entry(takeCount, ++index));
            xVals.add(mStatus[0]);
            colors.add(ColorTemplate.HEALTH_COLORS[0]);
        }
        if (onTimeCount >= 1) {
            yVals1.add(new Entry(onTimeCount, ++index));
            xVals.add(mStatus[1]);
            colors.add(ColorTemplate.HEALTH_COLORS[1]);
        }
        if (missCount >= 1) {
            yVals1.add(new Entry(missCount, ++index));
            xVals.add(mStatus[2]);
            colors.add(ColorTemplate.HEALTH_COLORS[2]);
        }
        if (reminders.size()>0) {
            String score = String.valueOf(Integer.parseInt(mChart.getValueFormatter().getFormattedValue((float)onTimeCount * 100 / (float)reminders.size()))
                    + Integer.parseInt(mChart.getValueFormatter().getFormattedValue((float)takeCount * 100 / (float)reminders.size())))+"%";
            mChart.setCenterText(score); //+"\n On Time"

            mChart.setCenterTextSize(70);
        }
        else {
            mChart.setCenterText(getString(R.string.no_medicine_score_text));
            mChart.setCenterTextSize(20);
        }

        PieDataSet dataSet = new PieDataSet(yVals1, "");
        colors.add(ColorTemplate.getHoloBlue());
        dataSet.setColors(colors);
        dataSet.setSelectionShift(10f);
        PieData data = new PieData(xVals, dataSet);
        mChart.setData(data);

        // undo all highlights
        mChart.highlightValues(null);
        mChart.invalidate();
    }

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, int xIndex) {
        if (e == null)
            return;
        PieData pieData = mChart.getData();
        PieDataSet dataSet = pieData.getDataSetByIndex(dataSetIndex);
        //dataSet.get(15f);
        //dataSet.sli
        if (dataSet.getSliceSpace()==0f && dataSet.getSelectionShift() ==0f)
            dataSet.setSliceSpace(0f);
        else
            dataSet.setSliceSpace(3f);
        int type ;
        if (mChart.getXValue(xIndex).equals(mStatus[0])){
            type = 2;
        }
        else if (mChart.getXValue(xIndex).equals(mStatus[1])){
            type = 1;
        }
        else
            type = 0;

        Intent intent = new Intent(this, AdherenceReportActivity.class);
        intent.putExtra("daysResult", dayInterval);
        intent.putExtra("type", type);
        startActivity(intent);
    }

    @Override
    public void onNothingSelected() {
        PieData pieData = mChart.getData();
        PieDataSet dataSet = pieData.getDataSet();
        dataSet.setSliceSpace(0f);
        Log.i("PieChart", "nothing selected");
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(v, null);
            return true;
        }
        if (event.getActionMasked() == MotionEvent.ACTION_UP) {
            ViewHelper.fadeIn(v, null);
            String centre = mChart.getCenterText();
            switch (v.getId()){
                case R.id.firstShare:
                    if (centre.equals(getString(R.string.no_medicine_score_text))){
                        Toast.makeText(PieChartActivity.this, centre, Toast.LENGTH_SHORT).show();
                        break;
                    }
                    try {
                        Bitmap screenShot = mChart.saveToPath("screenshot" + System.currentTimeMillis(), "");
                        if (screenShot != null) {
                            String pathofBmp = MediaStore.Images.Media.insertImage(getContentResolver(), screenShot, "", null);
                            Uri bmpUri = Uri.parse(pathofBmp);
                            String uriText = "mailto:" + Uri.encode("") +
                                    "?subject=" + Uri.encode("My Adherence report") +
                                    "&body=" + Uri.encode("");
                            Uri uri = Uri.parse(uriText);
                            Intent intent = new Intent(Intent.ACTION_SENDTO);
                            intent.setData(uri);
                            intent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                            startActivity(Intent.createChooser(intent, "Send mail..."));
                        }

                    }catch (Throwable t) {
                        Toast.makeText(this,
                                "Request failed try again: " + t.toString(),
                                Toast.LENGTH_LONG).show();
                    }
                    break;
                case R.id.secondShare:
                    if (centre.equals(getString(R.string.no_medicine_score_text))){
                        Toast.makeText(PieChartActivity.this, centre, Toast.LENGTH_SHORT).show();
                        break;
                    }
                    String body = "Medicine Adherence Score is "+mChart.getCenterText()+" % \n \nLate: "+
                            latePer+"\nOnTime: "+onTimePer+"\nMissed: "+missPer;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) //At least KitKat
                    {
                        String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this); //Need to change the build to API 19

                        Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
                        //sendIntent.setType("text/plain");
                        sendIntent.setData(Uri.parse("smsto:"));
                        sendIntent.putExtra("sms_body", body);

                        if (defaultSmsPackageName != null)//Can be null in case that there is no default, then the user would be able to choose any app that support this intent.
                        {
                            sendIntent.setPackage(defaultSmsPackageName);
                        }
                        startActivity(sendIntent);

                    }
                    else //For early versions, do what worked for you before.
                    {
                        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                        //sendIntent.setData(Uri.parse("sms:"));
                        sendIntent.putExtra("sms_body", body);
                        sendIntent.setType("vnd.android-dir/mms-sms");
                        startActivity(sendIntent);
                    }

                    break;
                case R.id.thirdShare:
                    if (centre.equals(getString(R.string.no_medicine_score_text))){
                        Toast.makeText(PieChartActivity.this, centre, Toast.LENGTH_SHORT).show();
                        break;
                    }
                    try {
                        Bitmap screenShot = mChart.saveToPath("screenshot" + System.currentTimeMillis(), "");
                        String pathofBmp = MediaStore.Images.Media.insertImage(getContentResolver(), screenShot,"", null);
                        Uri bmpUri = Uri.parse(pathofBmp);
                        PackageManager pm=getPackageManager();
                        Intent waIntent = new Intent(Intent.ACTION_SEND);
                        PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                        waIntent.setPackage("com.whatsapp");
                        waIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                        waIntent.setType("image/png");
                        startActivity(Intent.createChooser(waIntent, "Share with"));
                    }
                    catch (PackageManager.NameNotFoundException e) {
                        Toast.makeText(this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                            .show();
                    }
                    catch (Throwable t) {
                        Toast.makeText(this,
                                "Request failed try again: " + t.toString(),
                                Toast.LENGTH_LONG).show();
                    }
                break;
            }
            mChart.animateXY(1500, 1500);
            return true;
        }
        return false;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selection = (String) parent.getItemAtPosition(position);
        switch (parent.getId()){
            case R.id.daysSpinner:
                switch (position) {
                    case 0:
                        dayInterval = FIFTEEN_DAYS;
                        break;
                    case 1:
                        dayInterval = THIRTY_DAYS;
                        break;
                    case 2:
                        dayInterval = THREE_MONTHS;
                        break;
                    case 3:
                        dayInterval = SIX_MONTHS;
                        break;
                    case 4:
                        dayInterval = ONE_YEAR;
                        break;
                }
                reminders = medicineDBHandler.getAllRemindersForDatesPatiMed(dayInterval, medName, patientName);
                setData(dayInterval, medName, patientName);
                break;
            case R.id.patientSpinner:
                    if (position == 0)
                        patientName = "";
                    else
                        patientName = selection;

                reminders = medicineDBHandler.getAllRemindersForDatesPatiMed(dayInterval, medName, patientName);
                setData(dayInterval, medName, patientName);
                medList = new ArrayList<>();
                medAdapter.clear();
                medList.add(MEDICINES);
                medNames = new ArrayList<>();
                ArrayList<Medicine> hashlist = medicineDBHandler.getAllPatientList("m", patientName);
                for (Medicine medicine: hashlist){
                    medList.add(medicine.get_medName()+" "+medicine.get_dose()+" "+medicine.get_unit());
                    medNames.add(medicine.get_medName());
                }

                medAdapter.addAll(medList);
                medAdapter.notifyDataSetChanged();
                break;
            case R.id.medSpinner:
                if (position == 0)
                    medName = "";
                else
                    medName = medNames.get(position-1);
                reminders = medicineDBHandler.getAllRemindersForDatesPatiMed(dayInterval, medName, patientName);
                setData(dayInterval, medName, patientName);
                patientList = new ArrayList<>();
                patientAdapter.clear();
                patientList.add(PATIENTS);
                hashlist = medicineDBHandler.getAllPatientList("p", medName);
                for (Medicine medicine: hashlist){
                    patientList.add(medicine.get_patientName());
                }

                patientAdapter.addAll(patientList);
                patientAdapter.notifyDataSetChanged();
                break;
        }
        mChart.animateXY(1500, 1500);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
//    final class MedNameExtractor {
//        private Pattern pattern;
//        private Matcher matcher;
//
//        private static final String MEDICINE_PATTERN =
//                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
//                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
//
//        public MedNameExtractor() {
//            pattern = Pattern.compile(MEDICINE_PATTERN);
//        }
//
//        public boolean validate(final String hex) {
//
//            matcher = pattern.matcher(hex);
//            matcher.replaceAll("/\s[a-z:]+\s[a-z]+$/i");
//            return matcher.matches();
//
//        }
//    }
}
