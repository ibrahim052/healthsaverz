package com.healthsaverz.healthmobile.medicine.asyncloader;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

/**
 * Created by montydudani on 06/05/14.
 */
public class ParserUtils {

    private ProgressDialog progressDialog;

    private static final ParserUtils INSTANCE = new ParserUtils();

    private ParserUtils() {

    }

    public static ParserUtils getInstance() {
        return INSTANCE;
    }

    public void dismissLoading() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    public void showLoading(Context context, final AsyncTask<?, ?, ?> asyncTask) {
        if (progressDialog != null) {
            if (!progressDialog.isShowing()) {
                progressDialog = ProgressDialog.show(context, "", "Please wait...", true,
                        true,
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                asyncTask.cancel(true);
                                dialog.dismiss();
                            }
                        }
                );
            }
        } else {
            progressDialog = ProgressDialog.show(context, "", "Please wait...", true,
                    true,
                    new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            asyncTask.cancel(true);
                            dialog.dismiss();
                        }
                    }
            );
        }
    }

    public void showDialog(Context context, String msg, String positiveTitle, String negativeTitle, final Runnable runnable) {
        new AlertDialog.Builder(context)
                .setMessage(msg)
                .setPositiveButton(positiveTitle, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        runnable.run();
                    }
                })
                .setNegativeButton(negativeTitle, null)
                .show();
    }
}
