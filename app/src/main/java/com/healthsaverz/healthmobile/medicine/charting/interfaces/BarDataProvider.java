package com.healthsaverz.healthmobile.medicine.charting.interfaces;

import com.healthsaverz.healthmobile.medicine.charting.data.BarData;

public interface BarDataProvider extends BarLineScatterCandleDataProvider {

    public BarData getBarData();
    public boolean isDrawBarShadowEnabled();
    public boolean isDrawValueAboveBarEnabled();
    public boolean isDrawHighlightArrowEnabled();
    public boolean isDrawValuesForWholeStackEnabled();
}
