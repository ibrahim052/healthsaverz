package com.healthsaverz.healthmobile.medicine.ehr.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.api.AsyncLoaderEmr;
import com.healthsaverz.healthmobile.medicine.api.OfflineManager;
import com.imagezoom.ImageAttacher;
import com.imagezoom.ImageAttacher.OnMatrixChangedListener;
import com.imagezoom.ImageAttacher.OnPhotoTapListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class GridDetailFragment extends Fragment implements AsyncLoaderEmr.Listener  {

    private  static final String TAG = "CenterGridDetailFragmnt";
    public static final String IMAGE_URL = "imageurl";
    public static final String CREATED_ON = "created_on";

    private String imageURL;
    private AsyncLoaderEmr asyncLoader;
    private ImageView imageView;
    private boolean isLoadingDone = false;
    private Bitmap bitmap;

    public static Fragment newInstance(String imageURL, String created_on)
    {
        Bundle bundle = new Bundle();

        bundle.putString(IMAGE_URL,imageURL);
        bundle.putString(CREATED_ON,created_on);

        GridDetailFragment centerGridDetailFragment = new GridDetailFragment();
        centerGridDetailFragment.setArguments(bundle);
        return centerGridDetailFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_grid_detail, container, false);
        this.imageURL = getArguments().getString(IMAGE_URL);
        String name = getArguments().getString(CREATED_ON);
        TextView pictureName = (TextView) view.findViewById(R.id.picture_name);
        /*
        2014-12-30 02:47:07.0
         */
        //String dtStart = "2010-10-15T09:27:37Z";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        Date date;
        String nameString = "";
        try {
            date = format.parse(name.trim());
            nameString = "Uploaded Date: " + new SimpleDateFormat("dd-MMM-yyyy").format(date);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (!nameString.equals(""))
        pictureName.setText(nameString);
        if(isLoadingDone)
        {
            imageView =  (ImageView)view.findViewById(R.id.center_detail_picture);
            imageView.setImageBitmap(bitmap);

            //name
            view.findViewById(R.id.progressbar).setVisibility(View.GONE);

        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        if(!isLoadingDone)
        {
            /*
            First check whether file exist on harddrive. If yes load from there else download the file from the server
             */

            OfflineManager offlineManager = OfflineManager.getInstance();
            offlineManager.setContext(this.getActivity());

            if(offlineManager.isFileExistAtPathFromURL(imageURL))
            {
                byte[] imageData = offlineManager.readFileAtPathFromURL(imageURL);
                bitmap = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
                imageView =  (ImageView)getView().findViewById(R.id.center_detail_picture);
                imageView.setImageBitmap(bitmap);
                /**
                 * Use Simple ImageView
                 */
                usingSimpleImage(imageView);
//                pictureName = (TextView)getView().findViewById(R.id.picture_name);
//                pictureName.setText(name);
                getView().findViewById(R.id.progressbar).setVisibility(View.GONE);
            }
            else
            {
                if(asyncLoader != null)
                    asyncLoader = null;

                asyncLoader = new AsyncLoaderEmr();
                asyncLoader.getData(AsyncLoaderEmr.RequestMethod.GET,0);
                asyncLoader.setContext(this.getActivity());
                asyncLoader.setListener(this);
                asyncLoader.executeRequest(this.imageURL);
            }

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        if(asyncLoader != null)
        {
            if(!asyncLoader.isCancelled())
            {
                asyncLoader.cancel(true);
                isLoadingDone = false;
            }
        }
    }

    @Override
    public void didReceivedError(AsyncLoaderEmr.Status status) {
        isLoadingDone = false;
        asyncLoader = null;
    }

    @Override
    public void didReceivedData(byte[] data, int dataIndex) {
        OfflineManager offlineManager = OfflineManager.getInstance();
        offlineManager.writeDataFromURL(imageURL,data);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeByteArray(data, 0, data.length, options);

        ImageView thumbnail_icon1 = (ImageView) getActivity().findViewById(R.id.center_detail_picture);

        options.inSampleSize = calculateInSampleSize(options, thumbnail_icon1.getWidth(), thumbnail_icon1.getHeight());
        options.inJustDecodeBounds = false;

        this.bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);
        thumbnail_icon1.setImageBitmap(this.bitmap);
        usingSimpleImage(thumbnail_icon1);
        getActivity().findViewById(R.id.progressbar).setVisibility(View.GONE);

        isLoadingDone = true;
        asyncLoader = null;
    }

    @Override
    public void didReceivedProgress(Double progress) {

    }
    //local methods
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
    public void usingSimpleImage(ImageView imageView) {
        ImageAttacher mAttacher = new ImageAttacher(imageView);
        ImageAttacher.MAX_ZOOM = 3.0f; // Double the current Size
        ImageAttacher.MIN_ZOOM = 1.0f; // Half the current Size
        MatrixChangeListener mMaListener = new MatrixChangeListener();
        mAttacher.setOnMatrixChangeListener(mMaListener);
        PhotoTapListener mPhotoTap = new PhotoTapListener();
        mAttacher.setOnPhotoTapListener(mPhotoTap);
    }

    private class PhotoTapListener implements OnPhotoTapListener {

        @Override
        public void onPhotoTap(View view, float x, float y) {
        }
    }

    private class MatrixChangeListener implements OnMatrixChangedListener {

        @Override
        public void onMatrixChanged(RectF rect) {

        }
    }
}
