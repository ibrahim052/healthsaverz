package com.healthsaverz.healthmobile.medicine.charting.interfaces;

import com.healthsaverz.healthmobile.medicine.charting.data.ScatterData;

public interface ScatterDataProvider extends BarLineScatterCandleDataProvider {

    public ScatterData getScatterData();
    
}
