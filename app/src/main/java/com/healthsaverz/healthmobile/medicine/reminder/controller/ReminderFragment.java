package com.healthsaverz.healthmobile.medicine.reminder.controller;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoader;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.asyncloader.ParserListener;
import com.healthsaverz.healthmobile.medicine.calendar.CalendarActivity;
import com.healthsaverz.healthmobile.medicine.global.App;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.DialogProgressFragment;
import com.healthsaverz.healthmobile.medicine.global.Parser;
import com.healthsaverz.healthmobile.medicine.global.UseCases;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
import com.healthsaverz.healthmobile.medicine.mainscreen.controller.MainScreenActivity;
import com.healthsaverz.healthmobile.medicine.medfriend.controller.AddMedFriendDialog;
import com.healthsaverz.healthmobile.medicine.medfriend.controller.UpdateMedFriendDialog;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.AddMedFriendParser;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.DeleteMedFriendParser;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.FriendListAdapter;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.MapMedFriend;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.MedFriend;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.UpdateMedFriendParser;
import com.healthsaverz.healthmobile.medicine.medicine.controller.MedicineDBHandler;
import com.healthsaverz.healthmobile.medicine.medicine.modal.AlarmReceiver;
import com.healthsaverz.healthmobile.medicine.medicine.modal.Medicine;
import com.healthsaverz.healthmobile.medicine.medicine.modal.ReminderModal;
import com.healthsaverz.healthmobile.medicine.medicine.modal.Schedule;
import com.healthsaverz.healthmobile.medicine.medicineSearch.controller.MedicineSearchActivity;
import com.healthsaverz.healthmobile.medicine.reminder.modal.AddMedicineParser;
import com.healthsaverz.healthmobile.medicine.reminder.modal.DaysIntervalDialog;
import com.healthsaverz.healthmobile.medicine.reminder.modal.DaysSpecificDialog;
import com.healthsaverz.healthmobile.medicine.reminder.modal.DosageDialog;
import com.healthsaverz.healthmobile.medicine.reminder.modal.DurationScheduleDialog;
import com.healthsaverz.healthmobile.medicine.reminder.modal.ExpandableListView;
import com.healthsaverz.healthmobile.medicine.reminder.modal.MedicineInfo;
import com.healthsaverz.healthmobile.medicine.reminder.modal.RefillDialog;
import com.healthsaverz.healthmobile.medicine.reminder.modal.ReminderTime;
import com.healthsaverz.healthmobile.medicine.reminder.modal.TimeQuantityAdapter;
import com.healthsaverz.healthmobile.medicine.reminder.modal.TimeQuantityDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;


/**
 * A simple {@link android.app.Fragment} subclass.
 *
 */
public class ReminderFragment extends Fragment implements
        View.OnTouchListener, TimeQuantityDialog.TimeSetlistener,
        DurationScheduleDialog.DaysListener, DaysSpecificDialog.DaySpecificListener,
        View.OnClickListener, DaysIntervalDialog.DaysIntervalListener,
        DosageDialog.DosageListener, RefillDialog.RefillListener, DatePickerDialog.OnDateSetListener,
        ViewPager.OnPageChangeListener, AddMedFriendDialog.AddFriendListener, AddMedFriendParser.MedFriendParserListener,
        ParserListener, DeleteMedFriendParser.DeleteFriendParserListener, UpdateMedFriendDialog.UpdateFriendListener,
        UpdateMedFriendParser.UpdateFriendParserListener {


    public static final String TAG = ReminderFragment.class.getSimpleName();
    public static final String REMIND_ME_REFILL ="Remind me to refill current RX in";
    public static final String REMIND_ME_PILLS ="I currently have";
    public static final String REMIND_DAYS_1 ="15 Days";
    public static final String REMIND_DAYS_2 ="28 Days";
    public static final String REMIND_PILLS_1 ="23 Pills";
    public static final String REMIND_PILLS_2 ="20 Pills";
    public static final String SPECIFIC_DAYS_WEEK ="Specific days of week: ";
    private static final int COUNT = 30;


    private String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec" };

    private int[] mCards = {
            0,
            0,
            R.drawable.badge1,
            R.drawable.badge2,
            R.drawable.badge3,
            R.drawable.capsule,
            R.drawable.badge5,
            R.drawable.badge6,
            R.drawable.badge7,
            R.drawable.badge8,
            R.drawable.badge9,
            R.drawable.badge10,
            R.drawable.badge11,
            R.drawable.badge12,
            R.drawable.badge14,
            R.drawable.badge15,
            R.drawable.badge16,
            R.drawable.badge17,
            R.drawable.badge18,
            R.drawable.badge19,
            R.drawable.badge20,
            R.drawable.badge21,
            R.drawable.badge22,
            R.drawable.badge23,
            R.drawable.badge24,
            R.drawable.badge25,
            R.drawable.badge26,
            R.drawable.badge27,
            R.drawable.badge28,
            0,
            0};

    private int[] mColors = {
            0,
            0,
            Color.parseColor("#3cbaff"),
            Color.parseColor("#2797d0"),
            Color.parseColor("#005c94"),
            Color.parseColor("#d4ff5f"),
            Color.parseColor("#a9e327"),
            Color.parseColor("#27a227"),
            Color.parseColor("#ffff5c"),
            Color.parseColor("#ff9900"),
            Color.parseColor("#ff7d27"),
            Color.parseColor("#efd597"),
            Color.parseColor("#dba747"),
            Color.parseColor("#936827"),
            Color.parseColor("#ff4141"),
            Color.parseColor("#e12727"),
            Color.parseColor("#c02727"),
            Color.parseColor("#ffae8e"),
            Color.parseColor("#ffa2b2"),
            Color.parseColor("#e9967a"),
            Color.parseColor("#fb9388"),
            Color.parseColor("#f3d2ff"),
            Color.parseColor("#dd82ff"),
            Color.parseColor("#c527ff"),
            Color.parseColor("#2d2d2d"),
            Color.parseColor("#7d7d7d"),
            Color.parseColor("#a9a9a9"),
            Color.parseColor("#d4d4d4"),
            Color.parseColor("#ffffff"),
            Color.parseColor("#000000"),
            0,0};

    private ScrollView scrollView;
    private TextView nameText;
    private TextView reminderTimeText;
    private TextView shapeColorText;
    private TextView scheduleText;
    private TextView textDosage;
    private TextView instructionText;
    private TextView refillreminderText;
    private TextView patientNameText;
    private TextView medFriendText;

    private RelativeLayout hideView1;
    private RelativeLayout hideView2;
    private RelativeLayout hideView3;
    private RelativeLayout hideView4;
    private RelativeLayout hideView5;
    private RelativeLayout hideView6;
    private RelativeLayout hideView7;
    private RelativeLayout hideView8;
    private RelativeLayout hideView9;
    private RelativeLayout hideView10;

    private TextView detailName;
    private TextView detailReminderTime;
    private TextView detailShapeColor;
    private TextView detailSchedule;
    private TextView detailDosage;
    private TextView detailInstructions;
    private TextView detailRefillReminder;
    private TextView detailPatientName;
    private TextView detailMedFriend;

    private ImageView detailNameIcon;
    private ImageView detailReminderTimeIcon;
    private ImageView detailShapeColorIcon;
    private ImageView detailScheduleIcon;
    private ImageView detailDosageIcon;
    private ImageView detailInstructionsIcon;
    private ImageView detailRefillReminderIcon;
    private ImageView detailPatientNameIcon;
    private ImageView detailMedFriendIcon;


    private EditText medicineName;
    private EditText patientName;

    private ImageView pagerImage;
    private ViewPager pagerMedicine;
    private ViewPager pagerPrimaryColor;
    private ViewPager pagerSecondaryColor;

    private boolean isShapeColorUpdated;

    private TextView addTime;
    private TextView addFriend;
    private ExpandableListView reminderList;
    private ExpandableListView medFriendList;
    private TextView selectedDate;
    //    private RadioGroup radioDuration;
//    private RadioGroup radioDays;
    private RadioButton radioDaysEveryDay;
    private RadioButton radioDurationNumberOfDays;
    private RadioButton radioDurationContinious;
    private RadioButton radioDaysSpecificDays;
    private RadioButton radioDaysDaysInterval;
    private TextView textMedicineDoseTap;
    private EditText instructionName;
    private RadioGroup radioInstruction;
    private RadioButton radioInstructionNeverMind;
    private RadioButton radioInstructionAfter;
    private RadioButton radioInstructionWith;
    private RadioButton radioInstructionbefore;
    private RadioButton radioRefillDays;
    private RadioButton radioRefillPills;
    private TextView textReminder1;
    private TextView textReminderTap1;
    private TextView textReminderTap2;
    private CheckBox checkBoxRefillReminder;
    private ArrayList<ReminderTime> reminderTimes = new ArrayList<>();
    private boolean repeat = false;
    private int mm = -1;
    private int yy = -1;
    private int dd = -1;
    private ImageView imageView;
    private App app;
    Calendar calendar = AppConstants.getCurrentCalendar();
    private Calendar initCalendar;
    private int medID = -1;
    private Calendar editCalendar1;
    private int day1 = -1;
    private int day2 = -1;
    private int pill1 = -1;
    private int pill2 = -1;
    private ArrayList<MedFriend> medFriends = new ArrayList<>();
    private MedicineDBHandler dbHandler;
    private FriendListAdapter friendListAdapter;
    private DialogProgressFragment progressFragment;
    private ProgressDialog emptyProgress;
    private Parser parser;
    private MedFriend medFriend;
    private MedFriend medFriendAfter;

    public Parser getParser() {
        if (parser == null) {
            parser = new Parser(getActivity());
            //parser.setListener(this);
        }
        return parser;
    }
    public TextView getMedFriendText() {
        if (medFriendText == null){
            medFriendText = (TextView) getActivity().findViewById(R.id.medFriendText);
        }
        return medFriendText;
    }
    public RelativeLayout getHideView10() {
        if (hideView10 == null){
            hideView10 = (RelativeLayout) getActivity().findViewById(R.id.hideView10);
        }
        return hideView10;
    }
    public TextView getDetailMedFriend() {
        if (detailMedFriend == null){
            detailMedFriend = (TextView) getActivity().findViewById(R.id.detailMedFriend);
        }
        return detailMedFriend;
    }
    public ImageView getDetailMedFriendIcon() {
        if (detailMedFriendIcon == null){
            detailMedFriendIcon = (ImageView) getActivity().findViewById(R.id.detailMedFriendIcon);
        }
        return detailMedFriendIcon;
    }

    public TextView getAddFriend() {
        if (addFriend == null){
            addFriend = (TextView) getActivity().findViewById(R.id.addFriend);
        }
        return addFriend;
    }
//    public RadioGroup getRadioDays() {
//        if (radioDays == null){
//            radioDays = (RadioGroup) getActivity().findViewById(R.id.radioDays);
//        }
//        return radioDays;
//    }

    public TextView getPatientNameText() {
        if (patientNameText == null){
            patientNameText = (TextView) getActivity().findViewById(R.id.patientNameText);
        }
        return patientNameText;
    }
    public RelativeLayout getHideView9() {
        if (hideView9 == null){
            hideView9 = (RelativeLayout) getActivity().findViewById(R.id.hideView9);
        }
        return hideView9;
    }
    public TextView getDetailPatientName() {
        if (detailPatientName == null){
            detailPatientName = (TextView) getActivity().findViewById(R.id.detailPatientName);
        }
        return detailPatientName;
    }
    public ImageView getDetailPatientNameIcon() {
        if (detailPatientNameIcon == null){
            detailPatientNameIcon = (ImageView) getActivity().findViewById(R.id.detailPatientNameIcon);
        }
        return detailPatientNameIcon;
    }
    public EditText getPatientName() {
        if (patientName == null){
            patientName = (EditText) getActivity().findViewById(R.id.patientName);
        }
        return patientName;
    }
    public CheckBox getCheckBoxRefillReminder() {
        if (checkBoxRefillReminder == null){
            checkBoxRefillReminder = (CheckBox) getActivity().findViewById(R.id.checkBoxRefillReminder);
        }
        return checkBoxRefillReminder;
    }

    public RadioGroup getRadioInstruction() {
        if (radioInstruction == null){
            radioInstruction = (RadioGroup) getActivity().findViewById(R.id.radioInstruction);
        }
        return radioInstruction;
    }

    public RadioButton getRadioRefillDays() {
        if (radioRefillDays == null){
            radioRefillDays = (RadioButton) getActivity().findViewById(R.id.radioRefillDays);
        }
        return radioRefillDays;
    }

    public RadioButton getRadioRefillPills() {
        if (radioRefillPills == null){
            radioRefillPills = (RadioButton) getActivity().findViewById(R.id.radioRefillPills);
        }
        return radioRefillPills;
    }

    public RadioButton getRadioInstructionNeverMind() {
        if (radioInstructionNeverMind == null){
            radioInstructionNeverMind = (RadioButton) getActivity().findViewById(R.id.radioInstructionNeverMind);
        }
        return radioInstructionNeverMind;
    }

    public RadioButton getRadioInstructionAfter() {
        if (radioInstructionAfter == null){
            radioInstructionAfter = (RadioButton) getActivity().findViewById(R.id.radioInstructionAfter);
        }
        return radioInstructionAfter;
    }

    public RadioButton getRadioInstructionWith() {
        if (radioInstructionWith == null){
            radioInstructionWith = (RadioButton) getActivity().findViewById(R.id.radioInstructionWith);
        }
        return radioInstructionWith;
    }

    public RadioButton getRadioInstructionbefore() {
        if (radioInstructionbefore == null){
            radioInstructionbefore = (RadioButton) getActivity().findViewById(R.id.radioInstructionbefore);
        }
        return radioInstructionbefore;
    }

    public RadioButton getRadioDaysSpecificDays() {
        if (radioDaysSpecificDays == null){
            radioDaysSpecificDays = (RadioButton) getActivity().findViewById(R.id.radioDaysSpecificDays);
        }
        return radioDaysSpecificDays;
    }

    public RadioButton getRadioDaysEveryDay() {
        if (radioDaysEveryDay == null){
            radioDaysEveryDay = (RadioButton) getActivity().findViewById(R.id.radioDaysEveryDay);
        }
        return radioDaysEveryDay;
    }

    public RadioButton getRadioDaysDaysInterval() {
        if (radioDaysDaysInterval == null){
            radioDaysDaysInterval = (RadioButton) getActivity().findViewById(R.id.radioDaysDaysInterval);
        }
        return radioDaysDaysInterval;
    }

    public RadioButton getRadioDurationContinious() {
        if (radioDurationContinious == null){
            radioDurationContinious = (RadioButton) getActivity().findViewById(R.id.radioDurationContinious);
        }
        return radioDurationContinious;
    }


    public RadioButton getRadioDurationNumberOfDays() {
        if (radioDurationNumberOfDays == null){
            radioDurationNumberOfDays = (RadioButton) getActivity().findViewById(R.id.radioDurationNumberOfDays);
        }
        return radioDurationNumberOfDays;
    }

    public TextView getTextReminder1() {
        if (textReminder1 == null){
            textReminder1 = (TextView) getActivity().findViewById(R.id.textReminder1);
        }
        return textReminder1;
    }

    public TextView getTextReminderTap1() {
        if (textReminderTap1 == null){
            textReminderTap1 = (TextView) getActivity().findViewById(R.id.textReminderTap1);
        }
        return textReminderTap1;
    }

    public TextView getTextReminderTap2() {
        if (textReminderTap2 == null){
            textReminderTap2 = (TextView) getActivity().findViewById(R.id.textReminderTap2);
        }
        return textReminderTap2;
    }

    public TextView getTextMedicineDoseTap() {
        if (textMedicineDoseTap == null){
            textMedicineDoseTap = (TextView) getActivity().findViewById(R.id.textMedicineDoseTap);
        }
        return textMedicineDoseTap;
    }

    public TextView getSelectedDate() {
        if (selectedDate == null){
            selectedDate = (TextView) getActivity().findViewById(R.id.selectedDate);
        }
        return selectedDate;
    }

    /*
    These variables are used to manage the ReminderList
     */

    boolean isReminderEdited = false;
    int editReminderIndex;

    public ExpandableListView getReminderList() {
        if (reminderList == null){
            reminderList = (ExpandableListView) getActivity().findViewById(R.id.reminderList);
            reminderList.setOnItemClickListener(new ListView.OnItemClickListener(){

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    TimeQuantityAdapter adapter = (TimeQuantityAdapter)getReminderList().getAdapter();
//                    if (position != ListView.INVALID_POSITION) {
//                        adapter.remove(adapter.getItem(position));
//                        adapter.notifyDataSetChanged();
//                    }
                    ReminderTime time = (ReminderTime) parent.getItemAtPosition(position);
                    isReminderEdited    =   true;
                    editReminderIndex   =   position;

                    TimeQuantityDialog.newInstance(ReminderFragment.this, time.getCalendar() ).show(getFragmentManager(), TAG);
                }
            });
        }
        return reminderList;
    }
    public ExpandableListView getMedFriendList() {
        if (medFriendList == null){
            medFriendList = (ExpandableListView) getActivity().findViewById(R.id.medFriendList);
            medFriendList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                    for (int j= 0; j<adapterView.getCount(); j++){
//                        friendListAdapter.getItem(j).ischecked = false;
//                    }
//                    friendListAdapter.getItem(i).ischecked = !friendListAdapter.getItem(i).ischecked;
//                    friendListAdapter.notifyDataSetChanged();
                    medFriend = (MedFriend) adapterView.getItemAtPosition(i);
//                    isReminderEdited    =   true;
//                    editReminderIndex   =   position;

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    // Setting Dialog Title
                    alertDialog.setTitle(medFriend._friendName);
                    // Setting Dialog Message
                    alertDialog.setMessage("Choose what to do with this Buddy");

                    // Setting Positive "OK" Btn
                    alertDialog.setPositiveButton("Delete",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Write your code here to execute after dialog
                                    getConfirm(dialog);
                                }
                            });
                    // Showing Alert Dialog
                    alertDialog.setNegativeButton("Edit Details",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Write your code here to execute after dialog
//                                    ReminderActivity.inEditMode = true;
//                                    Intent intent = new Intent(getActivity(), ReminderActivity.class);
//                                    intent.putExtra("id", medicine.get_medID());
//                                    startActivity(intent);
                                    UpdateMedFriendDialog.newInstance(ReminderFragment.this, medFriend).show(getFragmentManager(), TAG);
                                    dialog.cancel();
                                }
                            });

                    alertDialog.show();


                }
            });
        }
        return medFriendList;
    }
    public void getConfirm(final DialogInterface dialogInterface){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // Setting Dialog Title
        alertDialog.setTitle("Confirm Delete");
        // Setting Dialog Message
        alertDialog.setMessage("Are you sure?");
        alertDialog.setCancelable(false);
// Setting Positive "OK" Btn
        alertDialog.setNegativeButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        //getEmpty();
                        dialog.cancel();
                        dialogInterface.cancel();
                        //emptyProgress.setVisibility(View.VISIBLE); //emptyProgress.getVisibility() == View.VISIBLE?View.GONE:View.VISIBLE
                        emptyProgress = ProgressDialog.show(getActivity(), "Working", "Deleting Data...", true, false);
                        DeleteMedFriendParser deleteMedFriendParser = new DeleteMedFriendParser(getActivity());
                        deleteMedFriendParser.setMedFriendParserListener(ReminderFragment.this);
                        deleteMedFriendParser.deleteMedFriend(medFriend);

                        //emptyProgress.setVisibility(View.GONE);
                    }
                });
        // Setting Positive "OK" Btn
        alertDialog.setPositiveButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                        dialogInterface.cancel();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }
    public EditText getInstructionName() {
        if (instructionName == null){
            instructionName = (EditText) getActivity().findViewById(R.id.instructionName);
        }
        return instructionName;
    }

    public TextView getAddTime() {
        if (addTime == null){
            addTime = (TextView) getActivity().findViewById(R.id.addTime);
        }
        return addTime;
    }

    public ViewPager getPagerMedicine() {
        if (pagerMedicine == null){
            pagerMedicine = (ViewPager) getActivity().findViewById(R.id.pagerMedicine);
        }
        return pagerMedicine;
    }

    public ViewPager getPagerMedicinePrimaryColor() {
        if (pagerPrimaryColor == null){
            pagerPrimaryColor = (ViewPager) getActivity().findViewById(R.id.pagerMedicinePrimaryColor);
        }
        return pagerPrimaryColor;
    }

    public ViewPager getPagerMedicineSecondaryColor() {
        if (pagerSecondaryColor == null){
            pagerSecondaryColor = (ViewPager) getActivity().findViewById(R.id.pagerMedicineSecondaryColor);
        }
        return pagerSecondaryColor;
    }

    public ImageView getPagerImage() {
        if (pagerImage == null){
            pagerImage = (ImageView) getActivity().findViewById(R.id.pagerImage);
        }
        return pagerImage;
    }

    public ScrollView getScrollView() {
        if (scrollView == null){
            scrollView = (ScrollView) getActivity().findViewById(R.id.scrollView);
        }
        return scrollView;
    }

    public TextView getNameText() {
        if (nameText == null){
            nameText = (TextView) getActivity().findViewById(R.id.nameText);
        }
        return nameText;
    }

    public TextView getReminderTimeText() {
        if (reminderTimeText == null){
            reminderTimeText = (TextView) getActivity().findViewById(R.id.reminderTimeText);
        }
        return reminderTimeText;
    }

    public TextView getShapeColorText() {
        if (shapeColorText == null){
            shapeColorText = (TextView) getActivity().findViewById(R.id.shapeColorText);
        }
        return shapeColorText;
    }

    public TextView getScheduleText() {
        if (scheduleText == null){
            scheduleText = (TextView) getActivity().findViewById(R.id.scheduleText);
        }
        return scheduleText;
    }

    public TextView getTextDosage() {
        if (textDosage == null){
            textDosage = (TextView) getActivity().findViewById(R.id.textDosage);
        }
        return textDosage;
    }

    public TextView getInstructionText() {
        if (instructionText == null){
            instructionText = (TextView) getActivity().findViewById(R.id.instructionText);
        }
        return instructionText;
    }

    public TextView getRefillreminderText() {
        if (refillreminderText == null){
            refillreminderText = (TextView) getActivity().findViewById(R.id.refillreminderText);
        }
        return refillreminderText;
    }

    public RelativeLayout getHideView1() {
        if (hideView1 == null){
            hideView1 = (RelativeLayout) getActivity().findViewById(R.id.hideView1);
        }
        return hideView1;
    }

    public RelativeLayout getHideView2() {
        if (hideView2 == null){
            hideView2 = (RelativeLayout) getActivity().findViewById(R.id.hideView2);
        }
        return hideView2;
    }

    public RelativeLayout getHideView3() {
        if (hideView3 == null){
            hideView3 = (RelativeLayout) getActivity().findViewById(R.id.hideView3);
        }
        return hideView3;
    }

    public RelativeLayout getHideView4() {
        if (hideView4 == null){
            hideView4 = (RelativeLayout) getActivity().findViewById(R.id.hideView4);
        }
        return hideView4;
    }

    public RelativeLayout getHideView5() {
        if (hideView5 == null){
            hideView5 = (RelativeLayout) getActivity().findViewById(R.id.hideView5);
        }
        return hideView5;
    }

    public RelativeLayout getHideView6() {
        if (hideView6 == null){
            hideView6 = (RelativeLayout) getActivity().findViewById(R.id.hideView6);
        }
        return hideView6;
    }

    public RelativeLayout getHideView7() {
        if (hideView7 == null){
            hideView7 = (RelativeLayout) getActivity().findViewById(R.id.hideView7);
        }
        return hideView7;
    }

    public RelativeLayout getHideView8() {
        if (hideView8 == null){
            hideView8 = (RelativeLayout) getActivity().findViewById(R.id.hideView8);
        }
        return hideView8;
    }

    public TextView getDetailName() {
        if (detailName == null){
            detailName = (TextView) getActivity().findViewById(R.id.detailName);
        }
        return detailName;
    }

    public TextView getDetailReminderTime() {
        if (detailReminderTime == null){
            detailReminderTime = (TextView) getActivity().findViewById(R.id.detailReminderTime);
        }
        return detailReminderTime;
    }

    public TextView getDetailShapeColor() {
        if (detailShapeColor == null){
            detailShapeColor = (TextView) getActivity().findViewById(R.id.detailShapeColor);
        }
        return detailShapeColor;
    }

    public TextView getDetailSchedule() {
        if (detailSchedule == null){
            detailSchedule = (TextView) getActivity().findViewById(R.id.detailSchedule);
        }
        return detailSchedule;
    }

    public TextView getDetailDosage() {
        if (detailDosage == null){
            detailDosage = (TextView) getActivity().findViewById(R.id.detailDosage);
        }
        return detailDosage;
    }

    public TextView getDetailInstructions() {
        if (detailInstructions == null){
            detailInstructions = (TextView) getActivity().findViewById(R.id.detailInstructions);
        }
        return detailInstructions;
    }

    public TextView getDetailRefillReminder() {
        if (detailRefillReminder == null){
            detailRefillReminder = (TextView) getActivity().findViewById(R.id.detailRefillReminder);
        }
        return detailRefillReminder;
    }

    public ImageView getDetailNameIcon() {
        if (detailNameIcon == null){
            detailNameIcon = (ImageView) getActivity().findViewById(R.id.detailNameIcon);
        }
        return detailNameIcon;
    }

    public ImageView getDetailReminderTimeIcon() {
        if (detailReminderTimeIcon == null){
            detailReminderTimeIcon = (ImageView) getActivity().findViewById(R.id.detailReminderTimeIcon);
        }
        return detailReminderTimeIcon;
    }

    public ImageView getDetailShapeColorIcon() {
        if (detailShapeColorIcon == null){
            detailShapeColorIcon = (ImageView) getActivity().findViewById(R.id.detailShapeColorIcon);
        }
        return detailShapeColorIcon;
    }

    public ImageView getDetailScheduleIcon() {
        if (detailScheduleIcon == null){
            detailScheduleIcon = (ImageView) getActivity().findViewById(R.id.detailScheduleIcon);
        }
        return detailScheduleIcon;
    }

    public ImageView getDetailDosageIcon() {
        if (detailDosageIcon == null){
            detailDosageIcon = (ImageView) getActivity().findViewById(R.id.detailDosageIcon);
        }
        return detailDosageIcon;
    }

    public ImageView getDetailInstructionsIcon() {
        if (detailInstructionsIcon == null){
            detailInstructionsIcon = (ImageView) getActivity().findViewById(R.id.detailInstructionsIcon);
        }
        return detailInstructionsIcon;
    }

    public ImageView getDetailRefillReminderIcon() {
        if (detailRefillReminderIcon == null){
            detailRefillReminderIcon = (ImageView) getActivity().findViewById(R.id.detailRefillReminderIcon);
        }
        return detailRefillReminderIcon;
    }

    public EditText getMedicineName() {
        if (medicineName == null){
            medicineName = (EditText) getActivity().findViewById(R.id.medicineName);
        }
        return medicineName;
    }

    public ReminderFragment() {
        // Required empty public constructor
    }
    public static Fragment newInstance(Calendar calendar) {
        ReminderFragment fragment = new ReminderFragment();
        fragment.initCalendar = calendar;
        return fragment;
    }

//    public static Fragment newInstance(String medicineName, String dose) {
//        medicineNameText = medicineName;
//        doseText = dose;
//        return new ReminderFragment();
//    }
    public static Fragment newInstance(int med_id) {
        ReminderFragment fragment = new ReminderFragment();
        fragment.medID = med_id;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reminder, container, false);

        setHasOptionsMenu(true);
        getPagerImage();
        getPagerMedicine();
        getPagerMedicinePrimaryColor();
        getPagerMedicineSecondaryColor();
        getScrollView();
        getNameText();
        getInstructionText();
        getRefillreminderText();
        getScheduleText();
        getReminderTimeText();
        getShapeColorText();
        getTextDosage();

        getDetailDosage();
        getDetailInstructions();
        getDetailName();
        getDetailRefillReminder();
        getDetailReminderTime();
        getDetailSchedule();
        getDetailShapeColor();
        getDetailPatientName();
        getDetailMedFriend();

        getDetailDosageIcon();
        getDetailInstructionsIcon();
        getDetailNameIcon();
        getDetailRefillReminderIcon();
        getDetailReminderTimeIcon();
        getDetailScheduleIcon();
        getDetailShapeColorIcon();
        getDetailPatientNameIcon();
        getDetailMedFriendIcon();

        getHideView1();
        getHideView2();
        getHideView3();
        getHideView4();
        getHideView5();
        getHideView6();
        getHideView7();
        getHideView9();
        getHideView10();

        getMedicineName();
        getPatientName();
        //timeQuantityDialog = new TimeQuantityDialog(getActivity());
        //timeQuantityDialog.setTimeSetlistener(this);
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        onCreateOptionsMenu(menu, getActivity().getMenuInflater());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.reminder, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String errorMessage = null;
        boolean isErrorOccured = false;

        switch (item.getItemId()){
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.action_save:

                getAddTime().setError(null);
                //getMedicineName().setError(null);
                if (getMedicineName().getText().toString().equals("")){
                    getMedicineName().setError("Cannot be empty");
                    getHideView1().setVisibility(View.VISIBLE);
                    getScrollView().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getScrollView().fullScroll(ScrollView.FOCUS_UP);
                        }
                    },100);
                }
                else if (reminderTimes.size()< 1){
                    getAddTime().setError("Select time and quantity");
                    getHideView2().setVisibility(View.VISIBLE);
                    ViewHelper.vibrate(getActivity(), getAddTime());
                    getScrollView().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getScrollView().fullScroll(ScrollView.FOCUS_UP);
                        }
                    },100);
                }
                else if (getPatientName().getText().toString().trim().equals("")){
                    getPatientName().setError("Cannot be Empty");
                    getHideView9().setVisibility(View.VISIBLE);
                    ViewHelper.vibrate(getActivity(), getPatientName());
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    // Setting Dialog Title
                    alertDialog.setTitle("Validation Error");
                    // Setting Dialog Message
                    alertDialog.setMessage("Patient Name cannot be empty");

                    // Setting Positive "OK" Btn
                    alertDialog.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Write your code here to execute after dialog
                                    dialog.cancel();
                                    getScrollView().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            getScrollView().fullScroll(ScrollView.FOCUS_DOWN);
                                        }
                                    },100);
                                }
                            });
                    // Showing Alert Dialog
                    alertDialog.show();
                }
                else {
                    if (getRadioRefillPills().isChecked()){
                        if (Integer.parseInt(getTextReminderTap1().getText().toString().replaceAll("[^0-9]", ""))
                                < Integer.parseInt(getTextReminderTap2().getText().toString().replaceAll("[^0-9]", ""))){
                            errorMessage = "Rx Reminder for pills should be less than your current pills";
                            isErrorOccured = true;
                        }
                    }
                    if(isErrorOccured){
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        // Setting Dialog Title
                        alertDialog.setTitle("Validation Error");
                        // Setting Dialog Message
                        alertDialog.setMessage(errorMessage);
                        // Setting Positive "OK" Btn
                        alertDialog.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Write your code here to execute after dialog
                                        dialog.cancel();
                                        getScrollView().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                getScrollView().fullScroll(ScrollView.FOCUS_DOWN);
                                            }
                                        },100);
                                    }
                                });
                        // Showing Alert Dialog
                        alertDialog.show();
                    }
                    else if (medID == -1){
                        item.setEnabled(false);
                        //finalEntry();
                        emptyProgress = ProgressDialog.show(getActivity(), "Creating", "Saving Data...", true, false);
                        new CreateMedicine().execute();
                    }
                    else {
                        emptyProgress = ProgressDialog.show(getActivity(), "Updating", "Saving Data...", true, false);
                        new CreateMedicine().execute();

                    }
                    //Toast.makeText(getActivity(), "Successfully Created", Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.action_showHelp:
                toggleShow(getDetailName());
                toggleShow(getDetailReminderTime());
                toggleShow(getDetailShapeColor());
                toggleShow(getDetailSchedule());
                toggleShow(getDetailDosage());
                toggleShow(getDetailInstructions());
                toggleShow(getDetailRefillReminder());
                toggleShow(getDetailPatientName());
                toggleShow(getDetailMedFriend());
                return true;
        }
        return false;
    }

    public Bitmap mergeBitmap(int colo1,int colo2)
    {

        Bitmap firstBitmap = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.badge3left);
        Bitmap secondBitmap = BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.badge3right);

        Bitmap comboBitmap;

        float width, height;

        width = firstBitmap.getWidth() + secondBitmap.getWidth();
        height = firstBitmap.getHeight();

        comboBitmap = Bitmap.createBitmap((int)(width/2), (int)height, Bitmap.Config.ARGB_4444);

        Paint firstPaint = new Paint();
        firstPaint.setColorFilter(new PorterDuffColorFilter(mColors[colo1], PorterDuff.Mode.MULTIPLY));

        Paint secondPaint = new Paint();
        secondPaint.setColorFilter(new PorterDuffColorFilter(mColors[colo2], PorterDuff.Mode.MULTIPLY));

        Canvas comboImage = new Canvas(comboBitmap);

        comboImage.drawBitmap(firstBitmap, 0f, 0f, firstPaint);
        comboImage.drawBitmap(secondBitmap,0f,0f, secondPaint);

        return comboBitmap;

    }

    private void toggleShow(View view) {
        view.setVisibility(view.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageView = (ImageView) view.findViewById(R.id.mainImage);
        imageView.setVisibility(View.GONE);
        getPagerImage().setOnTouchListener(this);
        getPagerMedicine().setAdapter(new CardsPagerAdapter());

        getPagerMedicine().setOffscreenPageLimit(5);
        getPagerMedicine().setOnPageChangeListener(this);
        //getPagerMedicine().setLayerPaint(null);
        getPagerMedicinePrimaryColor().setAdapter(new PrimaryColorPagerAdapter());
        getPagerMedicinePrimaryColor().setOffscreenPageLimit(5);
        pagerPrimaryColor.setCurrentItem(16);

        getPagerMedicinePrimaryColor().setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {
                switch (i) {
                    case ViewPager.SCROLL_STATE_IDLE:
                        updateImage();
                        break;
                    case ViewPager.SCROLL_STATE_DRAGGING:
                        isShapeColorUpdated =   true;//this is check shape and color has been updated
                        break;
                    case ViewPager.SCROLL_STATE_SETTLING:
                        isShapeColorUpdated =   true;//this is check shape and color has been updated
                        break;
                    default:
                        break;
                }
            }
        });

        getPagerMedicineSecondaryColor().setAdapter(new SecondaryColorPagerAdapter());
        getPagerMedicineSecondaryColor().setOffscreenPageLimit(5);
        pagerSecondaryColor.setCurrentItem(16);
        getPagerMedicineSecondaryColor().setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {
                switch (i) {
                    case ViewPager.SCROLL_STATE_IDLE:
                        updateImage();
                        break;
                    case ViewPager.SCROLL_STATE_DRAGGING:
                        isShapeColorUpdated =   true;//this is check shape and color has been updated
                        break;
                    case ViewPager.SCROLL_STATE_SETTLING:
                        isShapeColorUpdated =   true;//this is check shape and color has been updated
                        break;
                    default:
                        break;
                }
            }
        });
        updateImage();
        dbHandler = new MedicineDBHandler(getActivity());
        medFriends = dbHandler.getAllMedFriends();
        if (medID != -1){
            Medicine medicine = dbHandler.findMedicinefromID(medID);
            if (medicine != null){
                getMedicineName().setText("");
                getMedicineName().append(medicine.get_medName());
                getPatientName().setText("");
                getPatientName().append(medicine.get_patientName());
                ArrayList<MedFriend> friends = dbHandler.getAllMedFriendsForMedID(medID);
                if (medFriends!= null && friends != null){
                    for (MedFriend medf :medFriends){
                        for (MedFriend friend:friends){
                            if (medf._medFriendID == friend._medFriendID){
                                medf.ischecked = true;
                            }
                        }
                    }
                }
                if (medicine.get_dose() > 0 && !medicine.get_unit().equals("")){
                    getTextMedicineDoseTap().setText(medicine.get_dose() + " "+medicine.get_unit());
                }
                String strtdate = medicine.get_startDate();
                editCalendar1 = AppConstants.getCurrentCalendar();
                Calendar editCalendar2 = new GregorianCalendar();
                if (strtdate!= null){
                    //editCalendar1.setTimeInMillis(Long.parseLong(strtdate));
                    getSelectedDate().setText(months[editCalendar1.get(Calendar.MONTH)] + " " + editCalendar1.get(Calendar.DATE) + ", " + editCalendar1.get(Calendar.YEAR));
                    mm = editCalendar1.get(Calendar.MONTH);
                    yy = editCalendar1.get(Calendar.YEAR);
                    dd = editCalendar1.get(Calendar.DAY_OF_MONTH);
                }
                if (medicine.get_endDate().equals("N/A")){
                    getRadioDurationContinious().setChecked(true);
                }else {
                    editCalendar2.setTimeInMillis(Long.parseLong(medicine.get_endDate()));
                    long oneDay = 1000 * 60 * 60 * 24;
                    long daycount = (editCalendar2.getTimeInMillis() - editCalendar1.getTimeInMillis())/oneDay;

                    getRadioDurationNumberOfDays().setChecked(true);
                    getRadioDurationNumberOfDays().setText("Fixed number of days: "+ daycount);
                }
                ArrayList<ReminderModal> editReminderModals = dbHandler.findRemindersForTime(medicine.get_medID(), 0);
                // here -1 indicates the snooze time because we need to forbid the snooze time from displaying.
                if (editReminderModals != null && editReminderModals.size() > 0){
                    for (ReminderModal reminderModal: editReminderModals){
                        Calendar calendar1 = new GregorianCalendar();
                        calendar1.setTimeInMillis(Long.parseLong(reminderModal.get_time()));
                        ReminderTime reminderTime = new ReminderTime(calendar1, reminderModal.get_quantity(),
                                calendar1.get(Calendar.HOUR_OF_DAY), calendar1.get(Calendar.MINUTE));
                        for (ReminderTime time : reminderTimes){
                            if (time.getHours()+time.getMinutes() == calendar1.get(Calendar.HOUR_OF_DAY)+calendar1.get(Calendar.MINUTE) && time.getQuantity().equals(reminderModal.get_quantity())){
                                repeat = true;
                            }
                        }
                        if (!repeat) {
                            reminderTimes.add(reminderTime);
                            repeat = false;
                        }
                    }
                }

                TimeQuantityAdapter timeQuantityAdapter = new TimeQuantityAdapter(getActivity(), 0, reminderTimes, deleteClick);
                getReminderList().setAdapter(timeQuantityAdapter);
                Schedule schedule = dbHandler.findScheduleWithMedicinID(medID);
                if (schedule != null){
                    switch (schedule.get_type()){
                        case 1:
                            getRadioDaysEveryDay().setChecked(true);
                            break;
                        case 2:
                            getRadioDaysSpecificDays().setChecked(true);
                            getRadioDaysSpecificDays().setText(SPECIFIC_DAYS_WEEK + schedule.get_action());
                            break;
                        case 3:
                            getRadioDaysDaysInterval().setChecked(true);
                            getRadioDaysDaysInterval().setText("Days interval: "+schedule.get_action());
                            break;
                    }

                }
                if (medicine.get_shape() == 5) {

                    //Code Added by PriyankShape
//                    Resources r = getActivity().getResources();
//                    Drawable[] layers = new Drawable[2];
//
//                    layers[0] = r.getDrawable(R.drawable.badge3left);
//                    layers[0].setColorFilter(mColors[medicine.get_color1()], PorterDuff.Mode.MULTIPLY);
//
//                    layers[1] = r.getDrawable(R.drawable.badge3right);
//                    layers[1].setColorFilter(mColors[medicine.get_color2()], PorterDuff.Mode.MULTIPLY);
//
//                    LayerDrawable layerDrawable = new LayerDrawable(layers);
//                    //layerDrawable.setBounds(0,0,convertToDPUnit(getActivity(),64),convertToDPUnit(getActivity(),32));
//                    getPagerImage().setImageDrawable(layerDrawable);
//                    getPagerMedicine().setCurrentItem(medicine.get_shape()-2);
//                    getPagerMedicinePrimaryColor().setCurrentItem(medicine.get_color1()-2);
//                    getPagerMedicineSecondaryColor().setCurrentItem(medicine.get_color2()-2);
                    //updateImage();


                    getPagerImage().setColorFilter(null);
                    getPagerImage().setImageBitmap(mergeBitmap(medicine.get_color1(),medicine.get_color2()));
                    getPagerMedicine().setCurrentItem(medicine.get_shape()-2);
                    getPagerMedicinePrimaryColor().setCurrentItem(medicine.get_color1()-2);
                    getPagerMedicineSecondaryColor().setCurrentItem(medicine.get_color2()-2);

                }
                else if(medicine.get_shape() == -1){

                    //Code Added by PriyankShape
//                    Resources r = getActivity().getResources();
//                    Drawable[] layers = new Drawable[1];
//
//                    layers[0] = r.getDrawable(mCards[2]);
//                    layers[0].setColorFilter(mColors[28], PorterDuff.Mode.MULTIPLY);
//                    LayerDrawable layerDrawable = new LayerDrawable(layers);
//                    getPagerImage().setImageDrawable(layerDrawable);
//                    //updateImage();

                    getPagerImage().setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(),mCards[2]));
                    getPagerImage().setColorFilter(null);
                    getPagerImage().setColorFilter(mColors[28], PorterDuff.Mode.MULTIPLY);

                }
                else {

                    //Code Added by PriyankShape
//                    Resources r = getActivity().getResources();
//                    Drawable[] layers = new Drawable[1];
//
//                    layers[0] = r.getDrawable(mCards[medicine.get_shape()]);
//                    layers[0].setColorFilter(mColors[medicine.get_color1()], PorterDuff.Mode.MULTIPLY);
//                    LayerDrawable layerDrawable = new LayerDrawable(layers);
//                    getPagerImage().setImageDrawable(layerDrawable);
//                    getPagerMedicine().setCurrentItem(medicine.get_shape()-2);
//                    getPagerMedicinePrimaryColor().setCurrentItem(medicine.get_color1()-2);
//                    getPagerMedicineSecondaryColor().setCurrentItem(medicine.get_color2()-2);
//                    //updateImage();
//                    //viewHolderMedicine.leftMedicineIcon.setColorFilter(mColors[calenderReminder.medicineColor1], PorterDuff.Mode.MULTIPLY);

                    getPagerImage().setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(),mCards[medicine.get_shape()]));
                    getPagerImage().setColorFilter(null);
                    getPagerImage().setColorFilter(mColors[medicine.get_color1()], PorterDuff.Mode.MULTIPLY);
                    getPagerMedicine().setCurrentItem(medicine.get_shape()-2);
                    getPagerMedicinePrimaryColor().setCurrentItem(medicine.get_color1()-2);
                    getPagerMedicineSecondaryColor().setCurrentItem(medicine.get_color2()-2);
                }
                isShapeColorUpdated =   true;//this is check shape and color has been updated
                String desc = medicine.get_description();
                String[] descarr = desc.split(":");
                switch (descarr[0]) {
                    case "Before food":
                        getRadioInstructionbefore().setChecked(true);
                        break;
                    case "After food":
                        getRadioInstructionAfter().setChecked(true);
                        break;
                    case "With food":
                        getRadioInstructionWith().setChecked(true);
                        break;
                    default:
                        getRadioInstructionNeverMind().setChecked(true);
                        break;
                }
                if (descarr.length > 1){
                    getInstructionName().setText("");
                    getInstructionName().append(descarr[1]);
                }
                else if (descarr.length == 1 && (descarr[0].equals("No Food Instructions") || descarr[0].equals("Before food")
                        || descarr[0].equals("After food")|| descarr[0].equals("With food"))){
                    getInstructionName().setText("");
                }
                else {
                    getInstructionName().setText("");
                    getInstructionName().append(descarr[0]);
                }
                // type 0 - normal reminder
                // type 1 - rx reminder
                // type 2 - no reminder
                ArrayList<ReminderModal> rXReminderModals = dbHandler.findRemindersForRX(medicine.get_medID(),  1);
                if (rXReminderModals != null && rXReminderModals.size() == 2){
                    getCheckBoxRefillReminder().setChecked(true);
                    rxcheck();
                    //getHideView8().setVisibility(View.VISIBLE);
                    getRadioRefillDays().setChecked(true);
                    Calendar calendar1 = new GregorianCalendar();
                    calendar1.setTimeInMillis(Long.parseLong(rXReminderModals.get(0).get_time()));
                    long oneDay = 1000 * 60 * 60 * 24;
                    day1 = (int) ((calendar1.getTimeInMillis() - AppConstants.getCurrentCalendar().getTimeInMillis())/oneDay)+1;
                    getTextReminderTap1().setText(day1+" Days");

                    calendar1 = new GregorianCalendar();
                    calendar1.setTimeInMillis(Long.parseLong(rXReminderModals.get(1).get_time()));
                    day2 = (int) ((calendar1.getTimeInMillis() - AppConstants.getCurrentCalendar().getTimeInMillis())/oneDay)-day1+1;
                    getTextReminderTap2().setText(day2+" Days");
                }
                else if (medicine.get_pills() > -1){
                    pill1 = (int) medicine.get_pills();
                    pill2 = (int) medicine._threshold;
                    getCheckBoxRefillReminder().setChecked(true);
                    rxcheck();
                    getRadioRefillPills().setChecked(true);
                    getTextReminder1().setText(REMIND_ME_PILLS);
                    getTextReminderTap1().setText(pill1+" Pills");
                    getTextReminderTap2().setText(pill2+" Pills");
//                    case R.id.radioRefillDays:
//                        getTextReminder1().setText(REMIND_ME_REFILL);
//                        if (day1 != -1 && day2 != -1){
//                            getTextReminderTap1().setText(day1+" Days");
//                            getTextReminderTap2().setText(day2+" Days");
//                        }
//                        else {
//                            getTextReminderTap1().setText(REMIND_DAYS_1);
//                            getTextReminderTap2().setText(REMIND_DAYS_2);
//                        }
//                        break;
//                    case R.id.radioRefillPills:
//                        getTextReminder1().setText(REMIND_ME_PILLS);
//                        if (pill1 != -1 && pill2 != -1){
//                            getTextReminderTap1().setText(pill1+" Pills");
//                            getTextReminderTap2().setText(pill2+" Pills");
//                        }
//                        else {
//                            getTextReminderTap1().setText(REMIND_PILLS_1);
//                            getTextReminderTap2().setText(REMIND_PILLS_2);
//                        }
//                        break;
                }
                else {
                    getHideView8().setVisibility(View.GONE);
                }

            }

        }
        else {
            getHideView4().setVisibility(View.GONE);
            //getHideView5().setVisibility(View.GONE);
            getHideView6().setVisibility(View.GONE);
            getHideView7().setVisibility(View.GONE);
            getHideView9().setVisibility(View.GONE);
            getHideView10().setVisibility(View.GONE);
            TimeQuantityAdapter timeQuantityAdapter = new TimeQuantityAdapter(getActivity(), 0, reminderTimes, deleteClick);
            getReminderList().setAdapter(timeQuantityAdapter);
            isShapeColorUpdated = true;
        }
        friendListAdapter = new FriendListAdapter(getActivity(), medFriends, selectFriendClick);
        getMedFriendList().setAdapter(friendListAdapter);
        getTextReminderTap1().setOnClickListener(this);
        getTextReminderTap2().setOnClickListener(this);
        getRadioRefillDays().setOnClickListener(this);
        getRadioRefillPills().setOnClickListener(this);
        getCheckBoxRefillReminder().setOnClickListener(this);
        getRadioDaysEveryDay().setOnClickListener(this);
        getRadioDaysSpecificDays().setOnClickListener(this);
        getRadioDaysDaysInterval().setOnClickListener(this);
        getRadioDurationContinious().setOnClickListener(this);
        getRadioDurationNumberOfDays().setOnClickListener(this);
        getReminderList();
        getTextMedicineDoseTap().setOnTouchListener(this);
        getSelectedDate().setOnTouchListener(this);

        getPagerMedicine().setVisibility(View.GONE);
        getPagerMedicinePrimaryColor().setVisibility(View.GONE);
        getPagerMedicineSecondaryColor().setVisibility(View.GONE);
        getNameText().setOnTouchListener(this);
        getInstructionText().setOnTouchListener(this);
        getRefillreminderText().setOnTouchListener(this);
        getScheduleText().setOnTouchListener(this);
        getReminderTimeText().setOnTouchListener(this);
        getShapeColorText().setOnTouchListener(this);
        getTextDosage().setOnTouchListener(this);
//        getMedFriendList().setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                for (int j = 0; j < adapterView.getCount(); j++) {
//                    friendListAdapter.getItem(j).ischecked = false;
//                }
//                friendListAdapter.getItem(i).ischecked = !friendListAdapter.getItem(i).ischecked;
//                friendListAdapter.notifyDataSetChanged();
//            }
//        });

        getDetailDosageIcon().setOnTouchListener(this);
        getDetailInstructionsIcon().setOnTouchListener(this);
        getDetailNameIcon().setOnTouchListener(this);
        getDetailRefillReminderIcon().setOnTouchListener(this);
        getDetailReminderTimeIcon().setOnTouchListener(this);
        getDetailScheduleIcon().setOnTouchListener(this);
        getDetailShapeColorIcon().setOnTouchListener(this);

//        toggleShow(getDetailName());
//        toggleShow(getDetailReminderTime());
//        toggleShow(getDetailShapeColor());
//        toggleShow(getDetailSchedule());
//        toggleShow(getDetailDosage());
//        toggleShow(getDetailInstructions());
//        toggleShow(getDetailRefillReminder());
//        toggleShow(getDetailMedFriend());
//        toggleShow(getDetailPatientName());
        getMedicineName().setOnTouchListener(this);
        getAddTime().setOnTouchListener(this);
        getAddFriend().setOnTouchListener(this);
        getMedFriendText().setOnTouchListener(this);
        getPatientNameText().setOnTouchListener(this);
        getDetailMedFriendIcon().setOnTouchListener(this);
        getDetailPatientNameIcon().setOnTouchListener(this);

        if (initCalendar != null){
            mm = initCalendar.get(Calendar.MONTH);
            yy = initCalendar.get(Calendar.YEAR);
            dd = initCalendar.get(Calendar.DAY_OF_MONTH);

            selectedDate.setText(months[initCalendar.get(Calendar.MONTH)]+" "+initCalendar.get(Calendar.DATE)+", "+initCalendar.get(Calendar.YEAR));
//        ViewHelper.hideShowViewGroup((ViewGroup)getHideView1(), false);
//        ViewHelper.hideShowView((View)getDetailName(), false);
        }
        app = (App) getActivity().getApplicationContext();
        if (app.isMed){
            if (app.medName != null && !app.medName.equals("")){
                getMedicineName().setText("");
                getMedicineName().append(app.medName);
            }
            if (app.strengthName != null ){
                if ( app.strengthName.equals("")){
                    getTextMedicineDoseTap().setText("Tap to set");
                }
                else {
                    getTextMedicineDoseTap().setText(app.strengthName);
                }
            }else {
                getTextMedicineDoseTap().setText("Tap to set");
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (app.isMed){
            if (app.medName != null && !app.medName.equals("")){
                getMedicineName().setText("");
                getMedicineName().append(app.medName);
            }
            if (app.strengthName != null){
                if ( app.strengthName.equals("")){
                    getTextMedicineDoseTap().setText("Tap to set");
                }
                else {
                    getTextMedicineDoseTap().setText(app.strengthName);
                }
            }else {
                getTextMedicineDoseTap().setText("Tap to set");
            }

        }
        updateImage();
        ViewHelper.hideShowViewGroup(getHideView3(), false);
        getPagerImage().setVisibility(View.VISIBLE);
        getPagerImage().setImageDrawable(imageView.getDrawable());

    }



    //OnTouchListener methods
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(view, null);
            return true;
        }
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_MOVE){
            ViewHelper.fadeIn(view, null);
            return true;
        }
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
            ViewHelper.fadeIn(view, null);
            int scrollView_X = getScrollView().getScrollX();
            int scrollView_Y = getScrollView().getScrollY();
            switch (view.getId()){
                case R.id.addFriend:
                    AddMedFriendDialog.newInstance(this).show(getFragmentManager(), TAG);
                    break;
                case R.id.medFriendText:
                    if (getHideView10().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView10(), false);
                        getDetailMedFriendIcon().setImageResource(R.drawable.ic_downarrow);
                    }
                    else {
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView10(), true);
                        getDetailMedFriendIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    break;
                case R.id.patientNameText:
                    if (getHideView9().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView9(), false);
                        getDetailPatientNameIcon().setImageResource(R.drawable.ic_downarrow);
                    }
                    else {
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView9(), true);
                        getDetailPatientNameIcon().setImageResource(R.drawable.ic_detail_icon);
                        getScrollView().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getScrollView().fullScroll(ScrollView.FOCUS_DOWN);
                            }
                        },100);
                    }
                    break;
                case R.id.detailMedFriendIcon:
                    if (getDetailMedFriend().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowView((View)getDetailMedFriend(), false);
                        getDetailMedFriendIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    else {
                        ViewHelper.hideShowView((View)getDetailMedFriend(), true);
                        getDetailMedFriendIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    break;
                case R.id.detailPatientNameIcon:
                    if (getDetailPatientName().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowView((View)getDetailPatientName(), false);
                        getDetailPatientNameIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    else {
                        ViewHelper.hideShowView((View)getDetailPatientName(), true);
                        getDetailPatientNameIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    break;
                case R.id.textMedicineDoseTap:
                    DosageDialog.newInstance(this).show(getFragmentManager(), TAG);
                    break;
                case R.id.selectedDate:
                    //final Calendar calendar = Calendar.getInstance();
                    if (initCalendar != null){
                        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this,
                                initCalendar.get(Calendar.YEAR), initCalendar.get(Calendar.MONTH), initCalendar.get(Calendar.DAY_OF_MONTH));
                        datePickerDialog.setTitle("Set start Date");
                        datePickerDialog.getDatePicker().setMinDate(initCalendar.get(Calendar.DATE));
                        datePickerDialog.show();
                    }
                    else {
                        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this,
                                editCalendar1.get(Calendar.YEAR), editCalendar1.get(Calendar.MONTH), editCalendar1.get(Calendar.DAY_OF_MONTH));
                        datePickerDialog.setTitle("Set start Date");
                        datePickerDialog.getDatePicker().setMinDate(editCalendar1.get(Calendar.DATE));
                        datePickerDialog.show();
                    }
                    break;
                case R.id.addTime:
                    TimeQuantityDialog.newInstance(this).show(getFragmentManager(), TAG);
                    isReminderEdited    =   false;
                    editReminderIndex   =   -1;
                    break;
                case R.id.pagerImage:
                    getPagerImage().setVisibility(View.GONE);
                    getPagerMedicine().setVisibility(View.VISIBLE);
                    imageView.setVisibility(View.VISIBLE);
                    getHideView3().setVisibility(View.VISIBLE);
                    getPagerMedicinePrimaryColor().setVisibility(View.VISIBLE);
                    updateImage();
                    break;
                case R.id.nameText:
                    if (getHideView1().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView1(), false);
                        getDetailNameIcon().setImageResource(R.drawable.ic_downarrow);
                    }
                    else {
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView1(), true);
                        getDetailNameIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    break;
                case R.id.reminderTimeText:
                    if (getHideView2().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView2(), false);
                        getDetailReminderTimeIcon().setImageResource(R.drawable.ic_downarrow);
                    }
                    else {
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView2(), true);
                        getDetailReminderTimeIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    break;
                case R.id.shapeColorText:
                    if (getHideView3().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView3(), false);
                        getPagerImage().setVisibility(View.VISIBLE);
                        getPagerImage().setImageDrawable(imageView.getDrawable());
                        getDetailShapeColorIcon().setImageResource(R.drawable.ic_downarrow);
                    }
                    else {
                        getPagerImage().setVisibility(View.GONE);
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView3(), true);
                        getDetailShapeColorIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    break;
                case R.id.scheduleText:
                    if (getHideView4().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView4(), false);
                        getDetailScheduleIcon().setImageResource(R.drawable.ic_downarrow);
                    }
                    else {
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView4(), true);
                        getDetailScheduleIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    break;
                case R.id.textDosage:
                    if (getHideView5().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView5(), false);
                        getDetailDosageIcon().setImageResource(R.drawable.ic_downarrow);
                    }
                    else {
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView5(), true);
                        getDetailDosageIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    break;
                case R.id.instructionText:
                    if (getHideView6().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView6(), false);
                        getDetailInstructionsIcon().setImageResource(R.drawable.ic_downarrow);
                    }
                    else {
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView6(), true);
                        getDetailInstructionsIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    break;
                case R.id.refillreminderText:
                    if (getHideView7().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView7(), false);
                        getDetailRefillReminderIcon().setImageResource(R.drawable.ic_downarrow);
                    }
                    else {
                        ViewHelper.hideShowViewGroup((ViewGroup)getHideView7(), true);
                        getDetailRefillReminderIcon().setImageResource(R.drawable.ic_detail_icon);
                        getScrollView().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getScrollView().fullScroll(ScrollView.FOCUS_DOWN);
                            }
                        },100);
                    }
                    //lastViewAnimate();
                    break;
//                case R.id.hideView1:
//                    break;
                case R.id.detailNameIcon:
                    if (getDetailName().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowView((View)getDetailName(), false);
                        getDetailNameIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    else {
                        ViewHelper.hideShowView((View)getDetailName(), true);
                        getDetailNameIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    break;
                case R.id.detailReminderTimeIcon:
                    if (getDetailReminderTime().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowView((View)getDetailReminderTime(), false);
                        getDetailReminderTimeIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    else {
                        ViewHelper.hideShowView((View)getDetailReminderTime(), true);
                        getDetailReminderTimeIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    break;
                case R.id.detailShapeColorIcon:
                    if (getDetailShapeColor().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowView((View)getDetailShapeColor(), false);
                        getDetailShapeColorIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    else {
                        ViewHelper.hideShowView((View)getDetailShapeColor(), true);
                        getDetailShapeColorIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    break;
                case R.id.detailScheduleIcon:
                    if (getDetailSchedule().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowView((View) getDetailSchedule(), false);
                        getDetailScheduleIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    else {
                        ViewHelper.hideShowView((View)getDetailSchedule(), true);
                        getDetailScheduleIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    break;
                case R.id.detailDosageIcon:
                    if (getDetailDosage().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowView((View)getDetailDosage(), false);
                        getDetailDosageIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    else {
                        ViewHelper.hideShowView((View)getDetailDosage(), true);
                        getDetailDosageIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    break;
                case R.id.detailInstructionsIcon:
                    if (getDetailInstructions().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowView((View)getDetailInstructions(), false);
                        getDetailInstructionsIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    else {
                        ViewHelper.hideShowView((View)getDetailInstructions(), true);
                        getDetailInstructionsIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    break;
                case R.id.detailRefillReminderIcon:
                    if (getDetailRefillReminder().getVisibility() == View.VISIBLE){
                        ViewHelper.hideShowView((View)getDetailRefillReminder(), false);
                        getDetailRefillReminderIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    else {
                        ViewHelper.hideShowView((View)getDetailRefillReminder(), true);
                        getDetailRefillReminderIcon().setImageResource(R.drawable.ic_detail_icon);
                    }
                    break;
                case R.id.medicineName:
                    app.isMed = true;
                    startActivity(new Intent(getActivity(), MedicineSearchActivity.class)
                            .putExtra("searchName", getMedicineName().getText().toString().trim())
                    .putExtra("isform", true));
                    break;
//                case R.id.shapeColorText1:
//                    break;
//                case R.id.shapeColorText1:
//                    break;
//                case R.id.shapeColorText1:
//                    break;
//                case R.id.shapeColorText1:
//                    break;
//                case R.id.shapeColorText1:
//                    break;
//                case R.id.shapeColorText1:
//                    break;
            }
            getScrollView().scrollTo(scrollView_X, scrollView_Y);
            return true;
        }
        return false;
    }
    private View.OnClickListener deleteClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final int position = getReminderList().getPositionForView(v);
            TimeQuantityAdapter adapter = (TimeQuantityAdapter)getReminderList().getAdapter();
            if (position != ListView.INVALID_POSITION) {
                adapter.remove(adapter.getItem(position));
                adapter.notifyDataSetChanged();
            }
        }
    };
    private View.OnClickListener selectFriendClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final int position = getMedFriendList().getPositionForView(v);
//            friendListAdapter = new FriendListAdapter(getActivity(), medFriends);
            friendListAdapter = (FriendListAdapter)getMedFriendList().getAdapter();
            if (position != ListView.INVALID_POSITION) {
                for (int j = 0; j < friendListAdapter.getCount(); j++) {
                    friendListAdapter.getItem(j).ischecked = false;
                }
                //friendListAdapter.getItem(i).ischecked = !friendListAdapter.getItem(i).ischecked;
                friendListAdapter.getItem(position).ischecked = !friendListAdapter.getItem(position).ischecked;
                friendListAdapter.notifyDataSetChanged();
            }
        }
    };


    //TimeQuantityDialog.TimeSetlistener methods
    @Override
    public void onTimeChange(Calendar cal, String quantity, Integer currentHour, Integer currentMinute) {
        ReminderTime reminderTime = new ReminderTime(cal, quantity, currentHour, currentMinute);
        for (ReminderTime time : reminderTimes){
            if ((time.getHours()*60)+time.getMinutes() == (currentHour*60)+currentMinute && time.getQuantity().equals(quantity)){
                repeat = true;
            }
        }
        if (!repeat){
            reminderTimes.add(reminderTime);
            int m;
            for (int k = reminderTimes.size(); k > 0; k--) {

                for (int l = 0; l < reminderTimes.size() - 1; l++) {
                    m = l + 1;
                    ReminderTime reminderTime1 = reminderTimes.get(l);
                    ReminderTime reminderTime2 = reminderTimes.get(m);
                    Calendar calendarsort1 = new GregorianCalendar(yy, mm, dd, reminderTime1.getHours(), reminderTime1.getMinutes(),0);
                    Calendar calendarsort2 = new GregorianCalendar(yy, mm, dd, reminderTime2.getHours(), reminderTime2.getMinutes(),0);

                    int time1 = (calendarsort1.get(Calendar.HOUR_OF_DAY) *60) + calendarsort1.get(Calendar.MINUTE);
                    int time2 = (calendarsort2.get(Calendar.HOUR_OF_DAY) *60) + calendarsort2.get(Calendar.MINUTE);

                    if(time1 > time2)//Need to sort
                    {
                        reminderTimes.set(l, reminderTime2);
                        reminderTimes.set(m, reminderTime1);
                    }
                }
            }
        }

        repeat = false;
        TimeQuantityAdapter timeQuantityAdapter = new TimeQuantityAdapter(getActivity(), 0, reminderTimes, deleteClick);
        getReminderList().setAdapter(timeQuantityAdapter);
        //Log.d(TAG, "jnkjd");
    }

    @Override
    public void deletePreviousReminder() {
        if(isReminderEdited){
            isReminderEdited = false;

            TimeQuantityAdapter adapter = (TimeQuantityAdapter)getReminderList().getAdapter();
            if (editReminderIndex != ListView.INVALID_POSITION) {
                adapter.remove(adapter.getItem(editReminderIndex));
                adapter.notifyDataSetChanged();
            }
            editReminderIndex   =   -1;
        }
    }

    //OnClick implementation
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.radioDaysSpecificDays:
                DaysSpecificDialog.newInstance(this).show(getFragmentManager(), TAG);
                break;
            case R.id.radioDaysDaysInterval:
                DaysIntervalDialog.newInstance(this).show(getFragmentManager(), TAG);
                break;
            case R.id.radioDurationNumberOfDays:
                DurationScheduleDialog.newInstance(this).show(getFragmentManager(), TAG);
                break;
            case R.id.radioDurationContinious:
                getRadioDurationNumberOfDays().setText("Fixed number of days");
                break;
            case R.id.radioDaysEveryDay:
                getRadioDaysSpecificDays().setText("Specific days of week");
                getRadioDaysDaysInterval().setText("Days interval");
                break;
            case R.id.checkBoxRefillReminder:
                rxcheck();
                break;
            case R.id.radioRefillDays:
                getTextReminder1().setText(REMIND_ME_REFILL);
                if (day1 != -1 && day2 != -1){
                    getTextReminderTap1().setText(day1+" Days");
                    getTextReminderTap2().setText(day2+" Days");
                }
                else {
                    getTextReminderTap1().setText(REMIND_DAYS_1);
                    getTextReminderTap2().setText(REMIND_DAYS_2);
                }
                break;
            case R.id.radioRefillPills:
                getTextReminder1().setText(REMIND_ME_PILLS);
                if (pill1 != -1 && pill2 != -1){
                    getTextReminderTap1().setText(pill1+" Pills");
                    getTextReminderTap2().setText(pill2+" Pills");
                }
                else {
                    getTextReminderTap1().setText(REMIND_PILLS_1);
                    getTextReminderTap2().setText(REMIND_PILLS_2);
                }
                break;
            case R.id.textReminderTap1:
                if (getTextReminder1().getText().toString().equals(REMIND_ME_REFILL)){
                    RefillDialog.newInstance(this, RefillDialog.DAYS_1, textReminderTap1.getText().toString().replaceAll("[^0-9]", "")).show(getFragmentManager(), TAG);
                }
                else {
                    RefillDialog.newInstance(this, RefillDialog.PILLS_1, textReminderTap1.getText().toString().replaceAll("[^0-9]", "")).show(getFragmentManager(), TAG);
                }
                break;
            case R.id.textReminderTap2:
                if (getTextReminder1().getText().toString().equals(REMIND_ME_REFILL)){
                    RefillDialog.newInstance(this, RefillDialog.DAYS_2, textReminderTap2.getText().toString().replaceAll("[^0-9]", "")).show(getFragmentManager(), TAG);
                }
                else {
                    RefillDialog.newInstance(this, RefillDialog.PILLS_2, textReminderTap2.getText().toString().replaceAll("[^0-9]", "")).show(getFragmentManager(), TAG);
                }
                break;

        }
    }

    private void rxcheck() {
        if (getCheckBoxRefillReminder().isChecked()){
            getHideView8().setVisibility(View.VISIBLE);
            getScrollView().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getScrollView().fullScroll(ScrollView.FOCUS_DOWN);
                }
            },100);
        }
        else {
            getHideView8().setVisibility(View.GONE);
        }
    }
//    private void lastViewAnimate(){
//        if (getHideView7().getVisibility() == View.GONE){
//            getHideView7().setVisibility(View.VISIBLE);
//            getScrollView().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    getScrollView().fullScroll(ScrollView.FOCUS_DOWN);
//                }
//            },100);
//        }
//        else {
//            getHideView7().setVisibility(View.GONE);
//        }
//    }

    @Override
    public void onAddMedFriend(MedFriend medFriend) {
        if (medFriend != null){
            progressFragment =  new DialogProgressFragment();
            progressFragment.show(getFragmentManager(), TAG);
            progressFragment.setCancelable(false);
            AddMedFriendParser medFriendParser = new AddMedFriendParser(getActivity());
            medFriendParser.addMedFriend(medFriend);
            medFriendParser.setMedFriendParserListener(this);




        }
    }
    @Override
    public void onUpdateMedFriend(MedFriend medFriend) {
        if (medFriend != null){
            medFriendAfter = medFriend;
            progressFragment =  new DialogProgressFragment();
            progressFragment.show(getFragmentManager(), TAG);
            progressFragment.setCancelable(false);
            UpdateMedFriendParser updateMedFriendParser = new UpdateMedFriendParser(getActivity());
            updateMedFriendParser.updateMedFriend(medFriend);
            updateMedFriendParser.setMedFriendParserListener(this);
        }
    }
    @Override
    public void onDayCountChange(String quantity) {
        if (quantity != null){
            getRadioDurationNumberOfDays().setText("Fixed number of days: "+ quantity);
        }
        else {
            getRadioDurationNumberOfDays().setText("Fixed number of days");
            getRadioDurationContinious().setChecked(true);
        }
    }

    @Override
    public void onDaySpecificChange(ArrayList<String> days) {
        if (days == null || days.size() == 0) {
            getRadioDaysEveryDay().setChecked(true);
            getRadioDaysSpecificDays().setText("Specific days of week");
            getRadioDaysDaysInterval().setText("Days interval");
        } else {
            StringBuilder builder = new StringBuilder();
            for (String d: days){
                builder.append(",");
                builder.append(d);
            }
            getRadioDaysSpecificDays();
            getRadioDaysSpecificDays().setText("Specific days of week: " + builder.toString().replaceFirst(",", ""));
            getRadioDaysDaysInterval().setText("Days interval");
            //getRadioDaysSpecificDays().setChecked(true);
        }

    }

    @Override
    public void onDayIntervalChange(String quantity) {
        if (quantity != null){
            getRadioDaysDaysInterval().setText("Days interval: "+quantity);
            getRadioDaysSpecificDays().setText("Specific days of week");
            //getRadioDaysDaysInterval().setChecked(true);
        }
        else {
            getRadioDaysEveryDay().setChecked(true);
            getRadioDaysDaysInterval().setText("Days interval");
            getRadioDaysSpecificDays().setText("Specific days of week");
        }

    }

    @Override
    public void onDosageChange(String quantity) {
        if (quantity != null){
            getTextMedicineDoseTap().setText(quantity);
        }
        else {
            if (app.strengthName != null){
                if ( app.strengthName.equals("")){
                    getTextMedicineDoseTap().setText("Tap to set");
                }
                else {
                    getTextMedicineDoseTap().setText(app.strengthName);
                }
            }
            else {
                getTextMedicineDoseTap().setText("Tap to set");
            }
        }
    }

    @Override
    public void onRefillChange(String quantity, String type) {
        switch (type) {
            case RefillDialog.DAYS_1:
                getTextReminderTap1().setText(quantity + " Days");
                day1 = Integer.parseInt(quantity);
                break;
            case RefillDialog.DAYS_2:
                getTextReminderTap2().setText(quantity + " Days");
                day2 = Integer.parseInt(quantity);
                break;
            case RefillDialog.PILLS_1:
                getTextReminderTap1().setText(quantity + " Pills");
                pill1 = Integer.parseInt(quantity);
                break;
            default:
                getTextReminderTap2().setText(quantity + " Pills");
                pill2 = Integer.parseInt(quantity);
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int yy, int mm, int dd) {
        Calendar tempCal = new GregorianCalendar(yy, mm, dd);
        if (initCalendar != null){
            if (initCalendar.before(tempCal) || (initCalendar.getTimeInMillis() - tempCal.getTimeInMillis() < 100000000)){
                selectedDate.setText(months[mm]+" "+dd+", "+yy);
                this.mm = mm;
                this.yy = yy;
                this.dd = dd;
            }
            else {
                this.mm = initCalendar.get(Calendar.MONTH);
                this.yy = initCalendar.get(Calendar.YEAR);
                this.dd = initCalendar.get(Calendar.DAY_OF_MONTH);
                selectedDate.setText(months[this.mm]+" "+this.dd
                        +", "+this.yy);
                Toast.makeText(getActivity(), "Start Date cannot be before Calendar", Toast.LENGTH_SHORT).show();
            }
        }
        else if (editCalendar1 != null){
            if (editCalendar1.before(tempCal) || (editCalendar1.getTimeInMillis() - tempCal.getTimeInMillis() < 100000000)){
                selectedDate.setText(months[mm]+" "+dd+", "+yy);
                this.mm = mm;
                this.yy = yy;
                this.dd = dd;
            }
            else {
                this.mm = editCalendar1.get(Calendar.MONTH);
                this.yy = editCalendar1.get(Calendar.YEAR);
                this.dd = editCalendar1.get(Calendar.DAY_OF_MONTH);
                selectedDate.setText(months[this.mm]+" "+this.dd
                        +", "+this.yy);
                Toast.makeText(getActivity(), "Start Date cannot be before Calendar", Toast.LENGTH_SHORT).show();
            }
        }
    }
    public String stringDate(Calendar calendar1){

        int currentMonthTest = calendar1.get(Calendar.MONTH) + 1;
        int currentYearTest = calendar1.get(Calendar.YEAR);
        int currentDayOfWeekTest = calendar1.get(Calendar.DAY_OF_WEEK);
        int currentDayOfMonthTest = calendar1.get(Calendar.DAY_OF_MONTH);
        int currentHourTest = calendar1.get(Calendar.HOUR_OF_DAY);
        int currentMinTest  = calendar1.get(Calendar.MINUTE);


        String monthStringTest = "";
        String dayOfWeekStringTest = "";

        switch (currentMonthTest){
            case 1:
                monthStringTest = "JAN";
                break;
            case 2:
                monthStringTest = "FEB";
                break;
            case 3:
                monthStringTest = "MAR";
                break;
            case 4:
                monthStringTest = "APR";
                break;
            case 5:
                monthStringTest = "MAY";
                break;
            case 6:
                monthStringTest = "JUN";
                break;
            case 7:
                monthStringTest = "JUL";
                break;
            case 8:
                monthStringTest = "AUG";
                break;
            case 9:
                monthStringTest = "SEP";
                break;
            case 10:
                monthStringTest = "OCT";
                break;
            case 11:
                monthStringTest = "NOV";
                break;
            case 12:
                monthStringTest = "DEC";
                break;
        }

        switch (currentDayOfWeekTest) {
            case 1:
                dayOfWeekStringTest = "SUN";
                break;
            case 2:
                dayOfWeekStringTest = "MON";
                break;
            case 3:
                dayOfWeekStringTest = "TUE";
                break;
            case 4:
                dayOfWeekStringTest = "WED";
                break;
            case 5:
                dayOfWeekStringTest = "THU";
                break;
            case 6:
                dayOfWeekStringTest = "FRI";
                break;
            case 7:
                dayOfWeekStringTest = "SAT";
                break;
        }


        return  "date is "+monthStringTest + " " + currentDayOfMonthTest + ", " + dayOfWeekStringTest+" "+currentYearTest+" time is "+currentHourTest+":"+currentMinTest;
    }
    private void finalEntry() {

        Calendar mselectTime;

        /*
        Need to sort the time before making entry. If you first enter 8:15 AM and than enter 8:05 PM. You will find the 8:05 PM comes first
        than 8:10 AM. Thus wrote the sorting method.
         */
        int hour = reminderTimes.get(0).getHours();
        int minute = reminderTimes.get(0).getMinutes();
        if (yy!=-1 && dd!=-1 && mm!=-1){

            mselectTime = new GregorianCalendar(yy, mm, dd, hour, minute, 0);
        }
        else {

            mselectTime = new GregorianCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH), hour , minute, 0);
        }
        mselectTime.set(Calendar.MILLISECOND, 0);
        /*
        This while loop was causing the start date issue which was creating the issue. Commenting this.
         */

        String startDate = String.valueOf(mselectTime.getTimeInMillis());
        String medName = getMedicineName().getText().toString();
        String endDate;

        String patientName = getPatientName().getText().toString().trim(); // this is patient Name

        if (getRadioDurationNumberOfDays().isChecked()){

            String in = getRadioDurationNumberOfDays().getText().toString().replaceAll("[^0-9]", "");
            int str = Integer.parseInt(in);
            mselectTime.add(Calendar.DAY_OF_MONTH, str);
            endDate = String.valueOf(mselectTime.getTimeInMillis());

        }
        else {
            endDate = "N/A";
        }
        int daysType;
        String daysAction;//
        int intervalCount   =   0;
        int weekActionIndex = -1;
        String[] parts;
        ArrayList<String> partsActual = new ArrayList<>();

        int currentDayOfWeek = -1;

        ArrayList<Integer> reminderintervals = new ArrayList<>();
        ArrayList<Integer> reminderintervalsOriginal = new ArrayList<>();



        if (getRadioDaysEveryDay().isChecked()){
            daysType = 1;
            daysAction = "Everyday";
            reminderintervals.add(1);
        }
        else if (getRadioDaysSpecificDays().isChecked()){
            daysType = 2;
            daysAction = getRadioDaysSpecificDays().getText().toString().replace(SPECIFIC_DAYS_WEEK, "");

            parts = daysAction.split(",");

            ArrayList<String> afterDate = new ArrayList<>();
            ArrayList<String> beforDate = new ArrayList<>();


            Calendar calendarInterval = new GregorianCalendar();
            calendarInterval.setTimeInMillis(Long.parseLong(startDate));
            currentDayOfWeek = calendarInterval.get(Calendar.DAY_OF_WEEK);

            while (intervalCount < parts.length){

                String actionWeekDay = parts[intervalCount];

                switch (actionWeekDay) {
                    case "Sun":
                        weekActionIndex = 1;
                        break;
                    case "Mon":
                        weekActionIndex = 2;
                        break;
                    case "Tue":
                        weekActionIndex = 3;
                        break;
                    case "Wed":
                        weekActionIndex = 4;
                        break;
                    case "Thu":
                        weekActionIndex = 5;
                        break;
                    case "Fri":
                        weekActionIndex = 6;
                        break;
                    case "Sat":
                        weekActionIndex = 7;
                        break;
                }

                if(weekActionIndex >= currentDayOfWeek)
                {
                    afterDate.add(actionWeekDay);
                }
                else {
                    beforDate.add(actionWeekDay);

                }
                intervalCount++;
            }
            partsActual.addAll(afterDate);
            partsActual.addAll(beforDate);

            intervalCount = 0;

            while (intervalCount < partsActual.size()){

                String actionWeekDay = partsActual.get(intervalCount);

                switch (actionWeekDay) {
                    case "Sun":
                        weekActionIndex = 1;
                        break;
                    case "Mon":
                        weekActionIndex = 2;
                        break;
                    case "Tue":
                        weekActionIndex = 3;
                        break;
                    case "Wed":
                        weekActionIndex = 4;
                        break;
                    case "Thu":
                        weekActionIndex = 5;
                        break;
                    case "Fri":
                        weekActionIndex = 6;
                        break;
                    case "Sat":
                        weekActionIndex = 7;
                        break;
                }

                if(weekActionIndex >= currentDayOfWeek)
                {
                    reminderintervals.add(weekActionIndex-currentDayOfWeek);
                }
                else {

                    int differenceIndex =   Math.abs((currentDayOfWeek - weekActionIndex) - 7);
                    reminderintervals.add(differenceIndex);
                }

                currentDayOfWeek    =   weekActionIndex;
                intervalCount++;

            }

        }
        else {
            daysType = 3;
            daysAction = getRadioDaysDaysInterval().getText().toString().replaceAll("[^0-9]", "");
            reminderintervals.add(Integer.parseInt(daysAction));

        }

//        Log.d("daysAction","daysAction : "+daysAction);
//
//        for (Integer value:reminderintervals){
//
//            Log.d("Intervals are","value is "+value);
//        }
        reminderintervalsOriginal.addAll(reminderintervals);
        //String

        String dosage = getTextMedicineDoseTap().getText().toString();
        if (dosage.equals("Tap to set")){
            dosage = "100.00 mg";
        }
        String[] doseArray = dosage.split(" ");
        double dose = Double.parseDouble(TextUtils.isDigitsOnly(doseArray[0])?doseArray[0]:"0");
        String doseUnit = (doseArray.length<2)?"MG":doseArray[1];
        int status = 1;
        String instruct = "";
        int insCheckedID = getRadioInstruction().getCheckedRadioButtonId();
        if (getInstructionName().getText().toString().equals("")){
            switch (insCheckedID){
                case R.id.radioInstructionbefore:
                    instruct  = getRadioInstructionbefore().getText().toString();
                    break;
                case R.id.radioInstructionWith:
                    instruct  = getRadioInstructionWith().getText().toString();
                    break;
                case R.id.radioInstructionAfter:
                    instruct  = getRadioInstructionAfter().getText().toString();
                    break;
                case R.id.radioInstructionNeverMind:
                    instruct  = "No Food Instructions";
                    break;
            }
        }
        else {
            switch (insCheckedID){
                case R.id.radioInstructionbefore:
                    instruct  = getRadioInstructionbefore().getText().toString() +":"+ getInstructionName().getText().toString();
                    break;
                case R.id.radioInstructionWith:
                    instruct  = getRadioInstructionWith().getText().toString() +":"+ getInstructionName().getText().toString();
                    break;
                case R.id.radioInstructionAfter:
                    instruct  = getRadioInstructionAfter().getText().toString() +":"+ getInstructionName().getText().toString();
                    break;
                case R.id.radioInstructionNeverMind:
                    instruct  = getInstructionName().getText().toString();
                    break;
            }
        }

        float pills;
        float threshold = -1;
        int qtyType;

        int repeatCurrentDayIntervalRX = 0;//need to set the reminder for the RX on Days
        int repeatDayIntervalRX  =   0;//need to set the reminder for the RX on Days

        if (getCheckBoxRefillReminder().isChecked()){
            if (getRadioRefillDays().isChecked()){
                pills = -1;
                qtyType = 1;

                repeatCurrentDayIntervalRX  =   Integer.parseInt(getTextReminderTap1().getText().toString().replaceAll("[^0-9]",""));
                repeatDayIntervalRX =   Integer.parseInt(getTextReminderTap2().getText().toString().replaceAll("[^0-9]",""));
            }
            else {
                pills = Float.parseFloat(getTextReminderTap1().getText().toString().replaceAll("[^0-9]", ""));
                threshold = Float.parseFloat(getTextReminderTap2().getText().toString().replaceAll("[^0-9]", ""));
                qtyType = 2;

            }
        }
        else {
            pills = -1;
            qtyType = 0;
        }



        MedicineDBHandler medicineDBHandler = new MedicineDBHandler(getActivity());

        int medShapeIndex = -1;
        int medColor1   =   -1;
        int medColor2   =   -1;

        if(isShapeColorUpdated)
        {
            medShapeIndex   =   pagerMedicine.getCurrentItem()+2;
            medColor1       =   pagerPrimaryColor.getCurrentItem()+2;
            medColor2       =   pagerSecondaryColor.getCurrentItem()+2;
        }

        isShapeColorUpdated =   false;

        //Log.d("finalEntry","shape is "+medShapeIndex+" color1 is "+medColor1+" color2 is "+medColor2);

        Medicine medicine2 = new Medicine(medName, patientName, medShapeIndex, medColor1, medColor2, startDate, endDate, dose, doseUnit, status, instruct, pills);
        medicine2._threshold = threshold;
        medicine2.set_medID((int) medicineDBHandler.addMedicine(medicine2));

        int finalintervalCount = reminderintervals.size();
        int intervalIndex;

        boolean ignoreFirstTime;

        int currentDayOfWeekActual = currentDayOfWeek;
        for (ReminderTime reminderModal:reminderTimes){
            Calendar calendar1 = new GregorianCalendar(yy, mm, dd, reminderModal.getHours(), reminderModal.getMinutes(),0);
             /*
            Need to reset the reminder Interval for each reminder. Along with the currentDayIndex
             */

            if(reminderintervals != null)
            {
                reminderintervals.clear();
            }

            reminderintervals.addAll(reminderintervalsOriginal);

            currentDayOfWeek = currentDayOfWeekActual;
            intervalIndex = 0;

            ignoreFirstTime = daysType == 2;

            for (int x=0; x<COUNT;x++) {
                if(ignoreFirstTime)
                {
                    //this is needed as for specific days interval pattern has been define which needs to be added to the calender.
                    ignoreFirstTime =   false;
                }
                else
                {
                    ReminderModal modal = new ReminderModal(medicine2.get_medID(), reminderModal.getQuantity(),
                            String.valueOf(calendar1.getTimeInMillis()), 0);
                    modal._taken = -1;
                    modal._taken_time = "";

                    String strtime = modal.get_time();

                    Calendar temp =  new GregorianCalendar();
                    temp.setTimeInMillis(Long.parseLong(strtime));

                    if(!endDate.equals("N/A")){

                        //this means that end date is set thus we need to make sure we are within the end date range if we
                        //go beyond we will break and stop adding the reminder.

                        Calendar endTemp    =   new GregorianCalendar();
                        endTemp.setTimeInMillis(Long.parseLong(endDate));
                        endTemp.set(Calendar.SECOND, 0);
                        endTemp.set(Calendar.MILLISECOND, 0);
                        int resultToStop = endTemp.compareTo(temp);

                        //Log.d("finalEntry","final resultToStop: "+resultToStop);

                        if(resultToStop < 1){
                            break;
                        }
                        else
                        {
                            modal.set_remindID(medicineDBHandler.addReminder(modal));
                            Log.d("finalEntry",stringDate(calendar1));
                        }
                    }
                    else
                    {
                        modal.set_remindID(medicineDBHandler.addReminder(modal));
                        Log.d("finalEntry",stringDate(calendar1));
                    }

                }


                //Adding reminders to future dates as per the scheduling
                calendar1.add(Calendar.DATE, reminderintervals.get(intervalIndex));


                intervalIndex ++;
                if(intervalIndex >= finalintervalCount)
                {
                    intervalCount   =   0;

                    if(daysType == 2)// need to maintain the specific day sequence
                    {
                        reminderintervals.clear();
                        while (intervalCount <  partsActual.size()){

                            String actionWeekDay = partsActual.get(intervalCount);

                            switch (actionWeekDay) {
                                case "Sun":
                                    weekActionIndex = 1;
                                    break;
                                case "Mon":
                                    weekActionIndex = 2;
                                    break;
                                case "Tue":
                                    weekActionIndex = 3;
                                    break;
                                case "Wed":
                                    weekActionIndex = 4;
                                    break;
                                case "Thu":
                                    weekActionIndex = 5;
                                    break;
                                case "Fri":
                                    weekActionIndex = 6;
                                    break;
                                case "Sat":
                                    weekActionIndex = 7;
                                    break;
                            }

                            if(weekActionIndex > currentDayOfWeek)
                            {
                                reminderintervals.add(weekActionIndex-currentDayOfWeek);
                            }
                            else {

                                int differenceIndex =   Math.abs((currentDayOfWeek - weekActionIndex) - 7);
                                reminderintervals.add(differenceIndex);
                            }

                            currentDayOfWeek    =   weekActionIndex;
                            intervalCount++;

                        }
                    }

                    finalintervalCount = reminderintervals.size();
                    intervalIndex   =   0;

                }
            }
        }

        //Calendar calendar2 = null;


//        if (quantity != null){
//            medicineDBHandler.addReminder(new ReminderModal(addedMedicineID, quantity, "N/A", qtyType)); //fourth reminder
//        }


        //if user have requested for RX reminder depending upon the days we need to add two reminders and

        if(qtyType == 1)
        {
            Log.d("finalEntry","quantity "+repeatCurrentDayIntervalRX +" repeatDayIntervalRX "+repeatDayIntervalRX+" type is "+qtyType);
            Calendar temp;

            int hourRX   =   reminderTimes.get(0).getHours();
            int minuteRX =   reminderTimes.get(0).getMinutes();
            if (yy!=-1 && dd!=-1 && mm!=-1){
                temp = new GregorianCalendar(yy, mm, dd, hourRX, minuteRX, 0);
            }
            else {
                temp = new GregorianCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH), hourRX , minuteRX, 0);
            }

            temp.add(Calendar.DATE,repeatCurrentDayIntervalRX);

            //we will always add repeatDayIntervalRX in quantity as we need reminder after those many days continuously
            ReminderModal modal = new ReminderModal(medicine2.get_medID(),String.valueOf(repeatDayIntervalRX),
                    String.valueOf(temp.getTimeInMillis()), 1);
            modal._taken = -1;
            modal._taken_time = "";

            //need to add current and repeat count to the current date
            Calendar temp1;
            if (yy!=-1 && dd!=-1 && mm!=-1){

                temp1 = new GregorianCalendar(yy, mm, dd, hourRX, minuteRX, 0);
            }
            else {

                temp1 = new GregorianCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH), hourRX , minuteRX, 0);
            }

            temp1.add(Calendar.DATE,(repeatCurrentDayIntervalRX+repeatDayIntervalRX));

            ReminderModal modal1 = new ReminderModal(medicine2.get_medID(),String.valueOf(repeatDayIntervalRX),
                    String.valueOf(temp1.getTimeInMillis()), 1);
            modal1._taken = -1;
            modal1._taken_time = "";
        }


        ArrayList<ReminderModal> reminderModals = medicineDBHandler.findReminder(medicine2.get_medID());
        if (reminderModals != null && reminderModals.size() > 0){
            medicine2.set_reminderModals(reminderModals);
        }

        medicineDBHandler.addSchedule(new Schedule(medicine2.get_medID(), daysAction, daysType));
        Schedule schedule = medicineDBHandler.findSchedule();

        if (schedule != null){
            medicine2.set_schedule(schedule);
        }

        ArrayList<Long> count = new ArrayList<>();
        for (MedFriend friend : medFriends){
            if (friend.ischecked){
                count.add(medicineDBHandler.addMapMedFriend(new MapMedFriend(medicine2.get_medID(), friend._medFriendID)));
            }
        }
        Log.d(TAG, "Total Friend for this medicine is "+count.size());
        Log.d(TAG, "and the id's are "+count.toString());

        setAlarm(medicine2);

        Log.e(TAG, "Successfully Inserted");
    }
    private void finalEntryUpdate() {

        Calendar mselectTime;

        /*
        Need to sort the time before making entry. If you first enter 8:15 AM and than enter 8:05 PM. You will find the 8:05 PM comes first
        than 8:10 AM. Thus wrote the sorting method.
         */
        int hour = reminderTimes.get(0).getHours();
        int minute = reminderTimes.get(0).getMinutes();
        if (yy!=-1 && dd!=-1 && mm!=-1){

            mselectTime = new GregorianCalendar(yy, mm, dd, hour, minute, 0);
        }
        else {

            mselectTime = new GregorianCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH), hour , minute, 0);
        }
        mselectTime.set(Calendar.MILLISECOND, 0);
        /*
        This while loop was causing the start date issue which was creating the issue. Commenting this.
         */

        String startDate = String.valueOf(mselectTime.getTimeInMillis());
        String medName = getMedicineName().getText().toString();
        String endDate;

        String patientName = getPatientName().getText().toString().trim(); // this is patient Name

        if (getRadioDurationNumberOfDays().isChecked()){

            String in = getRadioDurationNumberOfDays().getText().toString().replaceAll("[^0-9]", "");
            int str = Integer.parseInt(in);
            mselectTime.add(Calendar.DAY_OF_MONTH, str);
            endDate = String.valueOf(mselectTime.getTimeInMillis());

        }
        else {
            endDate = "N/A";
        }
        int daysType;
        String daysAction;//
        int intervalCount   =   0;
        int weekActionIndex = -1;
        String[] parts;
        ArrayList<String> partsActual = new ArrayList<>();

        int currentDayOfWeek = -1;

        ArrayList<Integer> reminderintervals = new ArrayList<>();
        ArrayList<Integer> reminderintervalsOriginal = new ArrayList<>();



        if (getRadioDaysEveryDay().isChecked()){
            daysType = 1;
            daysAction = "Everyday";
            reminderintervals.add(1);
        }
        else if (getRadioDaysSpecificDays().isChecked()){
            daysType = 2;
            daysAction = getRadioDaysSpecificDays().getText().toString().replace(SPECIFIC_DAYS_WEEK, "");

            parts = daysAction.split(",");

            ArrayList<String> afterDate = new ArrayList<>();
            ArrayList<String> beforDate = new ArrayList<>();


            Calendar calendarInterval = new GregorianCalendar();
            calendarInterval.setTimeInMillis(Long.parseLong(startDate));
            currentDayOfWeek = calendarInterval.get(Calendar.DAY_OF_WEEK);

            while (intervalCount < parts.length){

                String actionWeekDay = parts[intervalCount];

                switch (actionWeekDay) {
                    case "Sun":
                        weekActionIndex = 1;
                        break;
                    case "Mon":
                        weekActionIndex = 2;
                        break;
                    case "Tue":
                        weekActionIndex = 3;
                        break;
                    case "Wed":
                        weekActionIndex = 4;
                        break;
                    case "Thu":
                        weekActionIndex = 5;
                        break;
                    case "Fri":
                        weekActionIndex = 6;
                        break;
                    case "Sat":
                        weekActionIndex = 7;
                        break;
                }

                if(weekActionIndex >= currentDayOfWeek)
                {
                    afterDate.add(actionWeekDay);
                }
                else {
                    beforDate.add(actionWeekDay);

                }
                intervalCount++;
            }
            partsActual.addAll(afterDate);
            partsActual.addAll(beforDate);

            intervalCount = 0;

            while (intervalCount < partsActual.size()){

                String actionWeekDay = partsActual.get(intervalCount);

                switch (actionWeekDay) {
                    case "Sun":
                        weekActionIndex = 1;
                        break;
                    case "Mon":
                        weekActionIndex = 2;
                        break;
                    case "Tue":
                        weekActionIndex = 3;
                        break;
                    case "Wed":
                        weekActionIndex = 4;
                        break;
                    case "Thu":
                        weekActionIndex = 5;
                        break;
                    case "Fri":
                        weekActionIndex = 6;
                        break;
                    case "Sat":
                        weekActionIndex = 7;
                        break;
                }

                if(weekActionIndex >= currentDayOfWeek)
                {
                    reminderintervals.add(weekActionIndex-currentDayOfWeek);
                }
                else {

                    int differenceIndex =   Math.abs((currentDayOfWeek - weekActionIndex) - 7);
                    reminderintervals.add(differenceIndex);
                }

                currentDayOfWeek    =   weekActionIndex;
                intervalCount++;

            }

        }
        else {
            daysType = 3;
            daysAction = getRadioDaysDaysInterval().getText().toString().replaceAll("[^0-9]", "");
            reminderintervals.add(Integer.parseInt(daysAction));

        }

//        Log.d("daysAction","daysAction : "+daysAction);
//
//        for (Integer value:reminderintervals){
//
//            Log.d("Intervals are","value is "+value);
//        }
        reminderintervalsOriginal.addAll(reminderintervals);
        //String

        String dosage = getTextMedicineDoseTap().getText().toString();
        if (dosage.equals("Tap to set")){
            dosage = "100.00 mg";
        }
        String[] doseArray = dosage.split(" ");
        double dose = Double.parseDouble(doseArray[0]);
        String doseUnit = doseArray[1];
        int status = 1;
        String instruct = "";
        int insCheckedID = getRadioInstruction().getCheckedRadioButtonId();
        if (getInstructionName().getText().toString().equals("")){
            switch (insCheckedID){
                case R.id.radioInstructionbefore:
                    instruct  = getRadioInstructionbefore().getText().toString();
                    break;
                case R.id.radioInstructionWith:
                    instruct  = getRadioInstructionWith().getText().toString();
                    break;
                case R.id.radioInstructionAfter:
                    instruct  = getRadioInstructionAfter().getText().toString();
                    break;
                case R.id.radioInstructionNeverMind:
                    instruct  = "No Food Instructions";
                    break;
            }
        }
        else {
            switch (insCheckedID){
                case R.id.radioInstructionbefore:
                    instruct  = getRadioInstructionbefore().getText().toString() +":"+ getInstructionName().getText().toString();
                    break;
                case R.id.radioInstructionWith:
                    instruct  = getRadioInstructionWith().getText().toString() +":"+ getInstructionName().getText().toString();
                    break;
                case R.id.radioInstructionAfter:
                    instruct  = getRadioInstructionAfter().getText().toString() +":"+ getInstructionName().getText().toString();
                    break;
                case R.id.radioInstructionNeverMind:
                    instruct  = getInstructionName().getText().toString();
                    break;
            }
        }

        float pills;
        float threshold = -1;
        int qtyType;

        int repeatCurrentDayIntervalRX = 0;//need to set the reminder for the RX on Days
        int repeatDayIntervalRX  =   0;//need to set the reminder for the RX on Days

        if (getCheckBoxRefillReminder().isChecked()){
            if (getRadioRefillDays().isChecked()){
                pills = -1;
                qtyType = 1;

                repeatCurrentDayIntervalRX  =   Integer.parseInt(getTextReminderTap1().getText().toString().replaceAll("[^0-9]",""));
                repeatDayIntervalRX =   Integer.parseInt(getTextReminderTap2().getText().toString().replaceAll("[^0-9]",""));
            }
            else {
                pills = Float.parseFloat(getTextReminderTap1().getText().toString().replaceAll("[^0-9]", ""));
                threshold = Float.parseFloat(getTextReminderTap2().getText().toString().replaceAll("[^0-9]", ""));
                qtyType = 2;

            }
        }
        else {
            pills = -1;
            qtyType = 0;
        }



        MedicineDBHandler medicineDBHandler = new MedicineDBHandler(getActivity());

        int medShapeIndex = -1;
        int medColor1   =   -1;
        int medColor2   =   -1;

        if(isShapeColorUpdated)
        {
            medShapeIndex   =   pagerMedicine.getCurrentItem()+2;
            medColor1       =   pagerPrimaryColor.getCurrentItem()+2;
            medColor2       =   pagerSecondaryColor.getCurrentItem()+2;
        }

        isShapeColorUpdated =   false;

        //Log.d("finalEntry","shape is "+medShapeIndex+" color1 is "+medColor1+" color2 is "+medColor2);

        Medicine medicine2 = new Medicine(medName, patientName, medShapeIndex, medColor1, medColor2, startDate, endDate, dose, doseUnit, status, instruct, pills);
        medicine2._threshold = threshold;
        medicine2.set_medID((int) medicineDBHandler.addMedicine(medicine2));

        int finalintervalCount = reminderintervals.size();
        int intervalIndex;

        boolean ignoreFirstTime;

        int currentDayOfWeekActual = currentDayOfWeek;

        for (ReminderTime reminderModal:reminderTimes){
            Calendar calendar1 = new GregorianCalendar(yy, mm, dd, reminderModal.getHours(), reminderModal.getMinutes(),0);
            /* this boolean here is to check that in update mode, if reminder time is before current time
                then that reminder will not be added for that day and time.*/
            boolean isPast;

             /*
            Need to reset the reminder Interval for each reminder. Along with the currentDayIndex
             */
            if(reminderintervals != null)
            {
                reminderintervals.clear();
            }

            reminderintervals.addAll(reminderintervalsOriginal);

            currentDayOfWeek = currentDayOfWeekActual;
            intervalIndex = 0;

            ignoreFirstTime = daysType == 2;

            for (int x=0; x<COUNT;x++) {
                if(ignoreFirstTime)
                {
                    //this is needed as for specific days interval pattern has been define which needs to be added to the calender.
                    ignoreFirstTime =   false;
                }
                else
                {

                    ReminderModal modal = new ReminderModal(medicine2.get_medID(), reminderModal.getQuantity(),
                            String.valueOf(calendar1.getTimeInMillis()), 0);
                    modal._taken = -1;
                    modal._taken_time = "";

                    String strtime = modal.get_time();

                    Calendar temp =  new GregorianCalendar();
                    temp.setTimeInMillis(Long.parseLong(strtime));

                    if(!endDate.equals("N/A")){

                        //this means that end date is set thus we need to make sure we are within the end date range if we
                        //go beyond we will break and stop adding the reminder.

                        Calendar endTemp    =   new GregorianCalendar();
                        endTemp.setTimeInMillis(Long.parseLong(endDate));
                        endTemp.set(Calendar.SECOND, 0);
                        endTemp.set(Calendar.MILLISECOND, 0);
                        int resultToStop = endTemp.compareTo(temp);

                        //Log.d("finalEntry","final resultToStop: "+resultToStop);

                        if(resultToStop < 1){
                            break;
                        }
                        else
                        {
                            isPast = false;
                            if (calendar1.before(AppConstants.getCurrentCalendar())){
                                isPast = true;
                            }
                            if (!isPast) {
                                modal.set_remindID(medicineDBHandler.addReminder(modal));
                                Log.d("finalEntry", stringDate(calendar1));
                            }
                        }
                    }
                    else
                    {
                        isPast = false;
                        if (calendar1.before(AppConstants.getCurrentCalendar())){
                            isPast = true;
                        }
                        if (!isPast) {
                            modal.set_remindID(medicineDBHandler.addReminder(modal));
                            Log.d("finalEntry", stringDate(calendar1));
                        }
                    }

                }


                //Adding reminders to future dates as per the scheduling
                calendar1.add(Calendar.DATE, reminderintervals.get(intervalIndex));


                intervalIndex ++;
                if(intervalIndex >= finalintervalCount)
                {
                    intervalCount   =   0;

                    if(daysType == 2)// need to maintain the specific day sequence
                    {
                        reminderintervals.clear();
                        while (intervalCount <  partsActual.size()){

                            String actionWeekDay = partsActual.get(intervalCount);

                            switch (actionWeekDay) {
                                case "Sun":
                                    weekActionIndex = 1;
                                    break;
                                case "Mon":
                                    weekActionIndex = 2;
                                    break;
                                case "Tue":
                                    weekActionIndex = 3;
                                    break;
                                case "Wed":
                                    weekActionIndex = 4;
                                    break;
                                case "Thu":
                                    weekActionIndex = 5;
                                    break;
                                case "Fri":
                                    weekActionIndex = 6;
                                    break;
                                case "Sat":
                                    weekActionIndex = 7;
                                    break;
                            }

                            if(weekActionIndex > currentDayOfWeek)
                            {
                                reminderintervals.add(weekActionIndex-currentDayOfWeek);
                            }
                            else {

                                int differenceIndex =   Math.abs((currentDayOfWeek - weekActionIndex) - 7);
                                reminderintervals.add(differenceIndex);
                            }

                            currentDayOfWeek    =   weekActionIndex;
                            intervalCount++;

                        }
                    }

                    finalintervalCount = reminderintervals.size();
                    intervalIndex   =   0;

                }
            }
        }

        //Calendar calendar2 = null;


//        if (quantity != null){
//            medicineDBHandler.addReminder(new ReminderModal(addedMedicineID, quantity, "N/A", qtyType)); //fourth reminder
//        }


        //if user have requested for RX reminder depending upon the days we need to add two reminders and

        if(qtyType == 1)
        {
            Log.d("finalEntry","quantity "+repeatCurrentDayIntervalRX +" repeatDayIntervalRX "+repeatDayIntervalRX+" type is "+qtyType);
            Calendar temp;

            int hourRX   =   reminderTimes.get(0).getHours();
            int minuteRX =   reminderTimes.get(0).getMinutes();
            if (yy!=-1 && dd!=-1 && mm!=-1){
                temp = new GregorianCalendar(yy, mm, dd, hourRX, minuteRX, 0);
            }
            else {
                temp = new GregorianCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH), hourRX , minuteRX, 0);
            }

            temp.add(Calendar.DATE,repeatCurrentDayIntervalRX);

            //we will always add repeatDayIntervalRX in quantity as we need reminder after those many days continuously
            ReminderModal modal = new ReminderModal(medicine2.get_medID(),String.valueOf(repeatDayIntervalRX),
                    String.valueOf(temp.getTimeInMillis()), 1);
            modal._taken = -1;
            modal._taken_time = "";

            //need to add current and repeat count to the current date
            Calendar temp1;
            if (yy!=-1 && dd!=-1 && mm!=-1){

                temp1 = new GregorianCalendar(yy, mm, dd, hourRX, minuteRX, 0);
            }
            else {

                temp1 = new GregorianCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH), hourRX , minuteRX, 0);
            }

            temp1.add(Calendar.DATE,(repeatCurrentDayIntervalRX+repeatDayIntervalRX));

            ReminderModal modal1 = new ReminderModal(medicine2.get_medID(),String.valueOf(repeatDayIntervalRX),
                    String.valueOf(temp1.getTimeInMillis()), 1);
            modal1._taken = -1;
            modal1._taken_time = "";
        }


        ArrayList<ReminderModal> reminderModals = medicineDBHandler.findReminder(medicine2.get_medID());
        if (reminderModals != null && reminderModals.size() > 0){
            medicine2.set_reminderModals(reminderModals);
        }

        medicineDBHandler.addSchedule(new Schedule(medicine2.get_medID(), daysAction, daysType));
        Schedule schedule = medicineDBHandler.findSchedule();

        if (schedule != null){
            medicine2.set_schedule(schedule);
        }

        ArrayList<Long> count = new ArrayList<>();
        for (MedFriend friend : medFriends){
            if (friend.ischecked){
                count.add(medicineDBHandler.addMapMedFriend(new MapMedFriend(medicine2.get_medID(), friend._medFriendID)));
            }
        }
        Log.d(TAG, "Total Friend for this medicine is "+count.size());
        Log.d(TAG, "and the id's are "+count.toString());

        setAlarm(medicine2);

        Log.e(TAG, "Successfully Inserted");
    }

    private void setAlarm(Medicine medicine1) {

        //this values is needed as we need to know whether while adding the reminder
        //if a particular medicine dose time has already passed than we need to by pass that entry

        Calendar calendar = AppConstants.getCurrentCalendar();
        int hour1 = calendar.get(Calendar.HOUR_OF_DAY);
        int minute1 = calendar.get(Calendar.MINUTE);

        int date1   =   calendar.get(Calendar.DAY_OF_YEAR);

        //Log.d("SetAlram","date1 is "+date1);

        for (int i = 0; i< medicine1.get_reminderModals().size(); i++){
            ReminderModal reminderModal = medicine1.get_reminderModals().get(i);
            if (!reminderModal.get_time().equals("N/A")) {

                Calendar calendar2 = new GregorianCalendar();
                calendar2.setTimeInMillis(Long.parseLong(reminderModal.get_time()));

                int hour2 = calendar2.get(Calendar.HOUR_OF_DAY);
                int minute2 = calendar2.get(Calendar.MINUTE);
                int date2   =   calendar2.get(Calendar.DAY_OF_YEAR);

                Log.d("SetAlram","date2 is "+date2);

                boolean addReminder = false;

                if(date1 == date2){
                    if (((hour2*60)+minute2) > ((hour1*60)+minute1)){
                        addReminder = true;
                    }
                }
                else
                {
                    addReminder = true;
                }

                if (addReminder){

                    /*
                    Adding the pills and RX reminder to the OS
                     */
                    Intent intent = new Intent(getActivity(), AlarmReceiver.class);
                    intent.putExtra("medicine_id",reminderModal.get_medID());
                    intent.putExtra("reminder_id",reminderModal.get_remindID());
                    if(reminderModal.get_qty_type() == 0)
                    {
                        intent.putExtra("notificationtype",1);// 1 mean it is for pills reminder.
                    }
                    else if(reminderModal.get_qty_type() == 1)
                    {
                        intent.putExtra("notificationtype",3);// 3 mean it is for RX reminder days.
                    }

                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity().getApplicationContext(), reminderModal.get_remindID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    AlarmManager alarmManager = (AlarmManager) getActivity().getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                    alarmManager.cancel(pendingIntent);
                    if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                        alarmManager.set(AlarmManager.RTC_WAKEUP, Long.parseLong(reminderModal.get_time()),pendingIntent);
                    } else {
                        alarmManager.setExact(AlarmManager.RTC_WAKEUP, Long.parseLong(reminderModal.get_time()),pendingIntent);
                    }

                    Log.d("SetAlarm","reminder is set on "+stringDate(calendar2)+"with reminder id "+reminderModal.get_remindID()+" med id "+reminderModal.get_medID()+" for type "+reminderModal.get_qty_type());

                    /*
                    Adding background Scheduler only for pills reminder
                     */
                    if(reminderModal.get_qty_type() == 0)
                    {
                        AppConstants.createNotificationScheduler(getActivity().getApplicationContext(), reminderModal, 10);
                    }
                }
            }
        }
        AddMedicineParser addMedicineParser = new AddMedicineParser(getActivity());
        addMedicineParser.addMedicine(medicine1);
    }

    @Override
    public void onPageScrolled(int i, float v, int i2) {
        imageView.setVisibility(View.GONE);
    }

    @Override
    public void onPageSelected(int pos) {
        for (int mCard : mCards) {
            if (mCard != 0)
                getResources().getDrawable(mCard).clearColorFilter();
        }
        //imageView.setVisibility(View.GONE);

    }

    private void updateImage() {
        if (mCards[pagerMedicine.getCurrentItem()+2] != 0)
        {
            if (pagerMedicine.getCurrentItem() == 3)
            {
                Log.d("UpdateImage","Setting Capsule color1 is "+(pagerPrimaryColor.getCurrentItem()+2)+" and color 2 "+(pagerSecondaryColor.getCurrentItem()+2));

                pagerSecondaryColor.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.VISIBLE);
                imageView.setColorFilter(null);
                imageView.setImageBitmap(mergeBitmap((pagerPrimaryColor.getCurrentItem()+2),(pagerSecondaryColor.getCurrentItem()+2)));
            }
            else
            {
                pagerSecondaryColor.setVisibility(View.GONE);
                if (mColors[pagerPrimaryColor.getCurrentItem()+2] != 0)
                {
                    if (pagerMedicine.getCurrentItem() == 0)
                    {
                        imageView.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.badge1copy));
                        imageView.setColorFilter(null);
                        imageView.setColorFilter(mColors[pagerPrimaryColor.getCurrentItem()+2], PorterDuff.Mode.MULTIPLY);
                    }
                    else
                    {
                        imageView.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(), mCards[pagerMedicine.getCurrentItem()+2]));
                        imageView.setColorFilter(null);
                        imageView.setColorFilter(mColors[pagerPrimaryColor.getCurrentItem()+2], PorterDuff.Mode.MULTIPLY);
                    }
                }
            }
        }
    }

    private void showMedicine() {
        imageView.setVisibility(View.VISIBLE);

        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(imageView, "alpha", .3f, 1f);
        fadeIn.setDuration(100);
        final AnimatorSet mAnimationSet = new AnimatorSet();

        mAnimationSet.play(fadeIn);

        mAnimationSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
            }
        });
        mAnimationSet.start();
    }

    public static int convertToDPUnit(Context context, float dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    @Override
    public void onPageScrollStateChanged(int i) {
        switch (i) {
            case ViewPager.SCROLL_STATE_IDLE:
                showMedicine();
                updateImage();
            break;
            case ViewPager.SCROLL_STATE_DRAGGING:
                imageView.setVisibility(View.VISIBLE);
                isShapeColorUpdated =   true;//this is check shape and color has been updated
            break;
            case ViewPager.SCROLL_STATE_SETTLING:
                imageView.setVisibility(View.VISIBLE);
                isShapeColorUpdated =   true;//this is check shape and color has been updated
            break;
            default:
                break;
        }
    }
    @Override
    public void updateFriendParserDidReceiveData(Object message) {
        progressFragment.dismiss();
        progressFragment = null;
        if (message != null){
            //MedFriend medFriend = (MedFriend)message;
            if (!dbHandler.findMedFriendFromEmailPhone(medFriend._friendMobile) && message.equals("100")) { //medFriend._friendEmail, medFriend._friendMobile
                dbHandler.updateMedFriend(medFriendAfter);
                medFriends = dbHandler.getAllMedFriends();
                friendListAdapter = new FriendListAdapter(getActivity(), medFriends, selectFriendClick);
                getMedFriendList().setAdapter(friendListAdapter);
                friendListAdapter.getItem(friendListAdapter.getCount()-1).ischecked = true;
            }else if (message.equals("101")){
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                // Setting Dialog Title
                alertDialog.setTitle("Validation Error");
                // Setting Dialog Message
                alertDialog.setMessage(message.toString());

                // Setting Positive "OK" Btn
                alertDialog.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to execute after dialog
                                dialog.cancel();
                            }
                        });
                // Showing Alert Dialog
                alertDialog.show();
            }
            else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                // Setting Dialog Title
                alertDialog.setTitle("Connection Error");
                // Setting Dialog Message
                alertDialog.setMessage("Check your Internet Connection");

                // Setting Positive "OK" Btn
                alertDialog.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to execute after dialog
                                dialog.cancel();
                            }
                        });
                // Showing Alert Dialog
                alertDialog.show();
            }
        }
    }
    @Override
    public void medFriendParserDidReceiveData(Object message) {
        progressFragment.dismiss();
        progressFragment = null;
        if (message != null){
            MedFriend medFriend = (MedFriend)message;
            if (!dbHandler.findMedFriendFromEmailPhone(medFriend._friendMobile)) { //medFriend._friendEmail, medFriend._friendMobile
                dbHandler.addMedFriend(medFriend);
                medFriends = dbHandler.getAllMedFriends();
                friendListAdapter = new FriendListAdapter(getActivity(), medFriends, selectFriendClick);
                getMedFriendList().setAdapter(friendListAdapter);
                friendListAdapter.getItem(friendListAdapter.getCount()-1).ischecked = true;
            }else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                // Setting Dialog Title
                alertDialog.setTitle("Validation Error");
                // Setting Dialog Message
                alertDialog.setMessage("Buddy Already Exists");

                // Setting Positive "OK" Btn
                alertDialog.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to execute after dialog
                                dialog.cancel();
                            }
                        });
                // Showing Alert Dialog
                alertDialog.show();
            }
        }
    }

    @Override
    public void medFriendParserDidReceivedConnectionError(AsyncLoaderNew.Status status) {
        progressFragment.dismiss();
        progressFragment = null;


        //Log.d("registerUserParserDidReceivedConnectionError"," "+status);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Error");
        alertDialog.setMessage(AppConstants.ConnectionError(status));
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }

    @Override
    public void medFriendParserDidReceivedProcessingError(String message) {
        progressFragment.dismiss();
        progressFragment = null;

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Error");
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }

    @Override
    public void didReceivedError(AsyncLoader.Status errorCode) {
        if (parser != null){
            parser.cancel(true);
            parser = null;
        }
        //progressDialog.dismiss();
        if (app.isMed){
            app.isMed = false;
            UseCases.startActivityByClearingStack(getActivity(), MainScreenActivity.class);
        }
        else {
            getActivity().onBackPressed();
        }
    }

    @Override
    public void didReceivedData(Object result) {
        if (parser != null){
            parser.cancel(true);
            parser = null;
        }
        //progressDialog.dismiss();
        if (app.isMed){
            app.isMed = false;
            UseCases.startActivityByClearingStack(getActivity(), MainScreenActivity.class);
        }
        else {
            getActivity().onBackPressed();
        }
    }

    @Override
    public void didReceivedProgress(double progress) {

    }
    @Override
    public void deleteFriendParserDidReceiveData(Object message) {
        if (emptyProgress != null) {
            emptyProgress.dismiss();
        }
        if (message.equals("100")){
            Toast.makeText(getActivity(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
            dbHandler.deleteMedFriend(medFriend._medFriendID);
            medFriends = dbHandler.getAllMedFriends();
            friendListAdapter = new FriendListAdapter(getActivity(), medFriends, selectFriendClick);
            getMedFriendList().setAdapter(friendListAdapter);

        }
        else if (message.equals("101")){
            Toast.makeText(getActivity(), "Error in Deletion", Toast.LENGTH_SHORT).show();
        }

        else {
            Toast.makeText(getActivity(), "Check your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
    private class CardsPagerAdapter extends PagerAdapter {
        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
                if (mCards[position] != 0) {
                    ImageView imageView = new ImageView(getActivity());
                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(convertToDPUnit(getActivity(), 24), convertToDPUnit(getActivity(), 24));
                    imageView.setLayoutParams(layoutParams);
                    int padding = convertToDPUnit(getActivity(), 8);
                    imageView.setPadding(padding, padding, padding, padding);
                    imageView.setImageDrawable(getResources().getDrawable(mCards[position]));
                    imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    //imageView.setLayerPaint(null);
                    container.addView(imageView);
                    return imageView;
                } else {
                    View view = new View(getActivity());
                    container.addView(view);
                    return view;
                }
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return mCards.length;
        }

        @Override
        public float getPageWidth(int position) {
            return 0.20f;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }

    private class PrimaryColorPagerAdapter extends PagerAdapter {


        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            if (mColors[position] != 0) {
                View view = View.inflate(container.getContext(), R.layout.cell_color, null);
                View color = view.findViewById(R.id.color);
                color.getBackground().setColorFilter(mColors[position], PorterDuff.Mode.MULTIPLY);
                container.addView(view);
                return view;
            } else {
                View view = new View(getActivity());
                container.addView(view);
                return view;
            }
        }

        @Override
        public float getPageWidth(int position) {
            return 0.20f;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return mColors.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }
    private class SecondaryColorPagerAdapter extends PagerAdapter {

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = View.inflate(container.getContext(), R.layout.cell_color, null);
            View color = view.findViewById(R.id.color);
            color.getBackground().setColorFilter(mColors[position], PorterDuff.Mode.MULTIPLY);

            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public float getPageWidth(int position) {
            return 0.20f;
        }

        @Override
        public int getCount() {
            return mColors.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }
    private class CreateMedicine extends AsyncTask<Integer, Void, Object> {

        @Override
        protected Object doInBackground(Integer... params) {
            if (medID != -1){
                MedicineDBHandler dbHandler = new MedicineDBHandler(getActivity());
                // we have to update the flags for update and delete where 1 is for update and -1 is for delete
                dbHandler.updateDeleteMedicineFlag("1", medID);
                //Once schedule is deleted we need to remove all the alaram and than delete the future reminders for medicine
                //Once schedule is deleted we need to remove all the alaram
                ArrayList<ReminderModal> reminderModals = dbHandler.findReminder(medID);
                for (ReminderModal modal: reminderModals) {
                                /*
                                We need to delete all the reminders which are scheduled on the AlarmManager along with the scheduler
                                 */
                    Intent intent = new Intent(getActivity(), AlarmReceiver.class);
                    intent.putExtra("medicine_id",modal.get_medID());
                    intent.putExtra("reminder_id",modal.get_remindID());
                    if(modal.get_qty_type() == 0)
                        intent.putExtra("notificationtype",1);// 1 mean it is for pill reminder.
                    else
                        intent.putExtra("notificationtype",3);// 3 mean it is for RX  reminder.

                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity().getApplicationContext(), modal.get_remindID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    AlarmManager alarmManager = (AlarmManager) getActivity().getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                    alarmManager.cancel(pendingIntent);
                    pendingIntent.cancel();

                    AppConstants.cancelNotificationScheduler(getActivity().getApplicationContext(), modal, 10);
                }
                //finally delete the medicine from the medicine table for future dates
                //dbHandler.deleteReminderWithMedicineID(medID);
                dbHandler.deleteReminderWithMedicineIDForFutureDate(medID);
                    //dbHandler.deleteMedicine(medID);
                finalEntryUpdate();
            }
            else {
                finalEntry();
            }
            return null;
        }

        protected void onPostExecute(Object result) {
            // Pass the result data back to the main activity
            if (emptyProgress != null) {
                emptyProgress.dismiss();
            }
            if (app.isMed){
                app.isMed = false;
                UseCases.startActivityByClearingStack(getActivity(), MainScreenActivity.class);
            }
            else
                getActivity().onBackPressed();
            if (medID == -1)
                Toast.makeText(getActivity(), "Successfully Created", Toast.LENGTH_SHORT).show();
            else {
                CalendarActivity.addButtonClicked = true;
                Toast.makeText(getActivity(), "Successfully Updated", Toast.LENGTH_SHORT).show();

            }
        }
    }
}
