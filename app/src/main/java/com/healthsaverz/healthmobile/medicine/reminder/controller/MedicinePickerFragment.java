package com.healthsaverz.healthmobile.medicine.reminder.controller;


import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.healthsaverz.healthmobile.medicine.R;

/**
 * A simple {@link android.app.Fragment} subclass.
 */
public class MedicinePickerFragment extends Fragment implements ViewPager.OnPageChangeListener {

    public static final String TAG = "MedicinePickerFragment";
    private ViewPager pagerMedicine;
    private ViewPager pagerPrimaryColor;
    private ViewPager pagerSecondaryColor;


    private int[] mCards = {
            0,
            0,
            R.drawable.badge1,
            R.drawable.badge2,
            R.drawable.badge3,
            R.drawable.capsule,
            R.drawable.badge5,
            R.drawable.badge6,
            R.drawable.badge7,
            R.drawable.badge8,
            R.drawable.badge9,
            R.drawable.badge10,
            R.drawable.badge11,
            R.drawable.badge12,
            R.drawable.badge14,
            R.drawable.badge15,
            R.drawable.badge16,
            R.drawable.badge17,
            R.drawable.badge18,
            R.drawable.badge19,
            R.drawable.badge20,
            R.drawable.badge21,
            R.drawable.badge22,
            R.drawable.badge23,
            R.drawable.badge24,
            R.drawable.badge25,
            R.drawable.badge26,
            R.drawable.badge27,
            R.drawable.badge28,
            0,
            0};

    private int[] mColors = {
            0,
            0,
            Color.parseColor("#3cbaff"),
            Color.parseColor("#2797d0"),
            Color.parseColor("#005c94"),
            Color.parseColor("#d4ff5f"),
            Color.parseColor("#a9e327"),
            Color.parseColor("#27a227"),
            Color.parseColor("#ffff5c"),
            Color.parseColor("#ff9900"),
            Color.parseColor("#ff7d27"),
            Color.parseColor("#efd597"),
            Color.parseColor("#dba747"),
            Color.parseColor("#936827"),
            Color.parseColor("#ff4141"),
            Color.parseColor("#e12727"),
            Color.parseColor("#c02727"),
            Color.parseColor("#ffae8e"),
            Color.parseColor("#ffa2b2"),
            Color.parseColor("#e9967a"),
            Color.parseColor("#fb9388"),
            Color.parseColor("#f3d2ff"),
            Color.parseColor("#dd82ff"),
            Color.parseColor("#c527ff"),
            Color.parseColor("#2d2d2d"),
            Color.parseColor("#7d7d7d"),
            Color.parseColor("#a9a9a9"),
            Color.parseColor("#d4d4d4"),
            Color.parseColor("#ffffff"),
            Color.parseColor("#000000"),
            0,0};
    private ImageView imageView;

    public MedicinePickerFragment() {
        // Required empty public constructor
    }

    public static MedicinePickerFragment newInstance() {
        MedicinePickerFragment fragment = new MedicinePickerFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_medicine_picker, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pagerMedicine = (ViewPager) view.findViewById(R.id.pagerPills);
        pagerMedicine.setAdapter(new CardsPagerAdapter());
        pagerMedicine.setOffscreenPageLimit(5);
        pagerMedicine.setCurrentItem(0);
        pagerMedicine.setOnPageChangeListener(this);

        pagerPrimaryColor = (ViewPager) view.findViewById(R.id.pagerPrimaryColor);
        pagerPrimaryColor.setAdapter(new PrimaryColorPagerAdapter());
        pagerPrimaryColor.setCurrentItem(0);
        pagerPrimaryColor.setOffscreenPageLimit(5);

        pagerPrimaryColor.setOnPageChangeListener(this);
        pagerSecondaryColor = (ViewPager) view.findViewById(R.id.pagerSecondaryColor);
        pagerSecondaryColor.setAdapter(new SecondaryColorPagerAdapter());
        pagerSecondaryColor.setCurrentItem(16);
        pagerSecondaryColor.setOffscreenPageLimit(5);

        pagerSecondaryColor.setVisibility(View.GONE);


        pagerSecondaryColor.setOnPageChangeListener(this);

    }

    public static int convertToDPUnit(Context context, float dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    @Override
    public void onResume() {
        super.onResume();

        updateImage();


    }

    @Override
    public void onPageScrolled(int i, float v, int i2) {

    }

    @Override
    public void onPageSelected(int pos) {
        for (int i=0; i<mCards.length; i++) {
            if (mCards[i] != 0)
                getResources().getDrawable(mCards[i]).clearColorFilter();
        }
        updateImage();

    }

    private void updateImage() {

        if (mCards[pagerMedicine.getCurrentItem()+2] != 0) {
            if (pagerMedicine.getCurrentItem() == 3) {
                pagerSecondaryColor.setVisibility(View.VISIBLE);
                Resources r = getResources();
                Drawable[] layers = new Drawable[2];
                layers[0] = r.getDrawable(R.drawable.badge3left);
                if (mColors[pagerPrimaryColor.getCurrentItem()+2] != 0)
                    layers[0].setColorFilter(mColors[pagerPrimaryColor.getCurrentItem()+2], PorterDuff.Mode.MULTIPLY);

                layers[1] = r.getDrawable(R.drawable.badge3right);
                if (mColors[pagerSecondaryColor.getCurrentItem()+2] != 0)
                    layers[1].setColorFilter(mColors[pagerSecondaryColor.getCurrentItem() + 2], PorterDuff.Mode.MULTIPLY);
                LayerDrawable layerDrawable = new LayerDrawable(layers);
                imageView.setImageDrawable(layerDrawable);
            } else {
                pagerSecondaryColor.setVisibility(View.GONE);
                if (mColors[pagerPrimaryColor.getCurrentItem()+2] != 0) {
                    if (pagerMedicine.getCurrentItem() == 0) {
                        imageView.setImageDrawable(getResources().getDrawable(R.drawable.badge1copy));
                        imageView.getDrawable().setColorFilter(mColors[pagerPrimaryColor.getCurrentItem()+2], PorterDuff.Mode.MULTIPLY);
                    } else {
                        imageView.setImageDrawable(getResources().getDrawable(mCards[pagerMedicine.getCurrentItem()+2]));
                        imageView.getDrawable().setColorFilter(mColors[pagerPrimaryColor.getCurrentItem()+2], PorterDuff.Mode.MULTIPLY);
                    }
                }

            }

        }
    }

    @Override
    public void onPageScrollStateChanged(int i) {
        switch (i) {
            case ViewPager.SCROLL_STATE_IDLE: {
//                        for (int x=0; x<pagerMedicine.getChildCount(); x++) {
//                            ImageView imageView = (ImageView)pagerMedicine.getChildAt(x);
//                            imageView.getDrawable().clearColorFilter();
//                        }

                updateImage();

            }
            break;
            case ViewPager.SCROLL_STATE_DRAGGING: {

            }
            break;
            default:
                break;
        }
    }

    private class CardsPagerAdapter extends PagerAdapter {

        private boolean mIsDefaultItemSelected = false;




        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            if (mCards[position] != 0) {
                ImageView imageView = new ImageView(getActivity());
                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(convertToDPUnit(getActivity(), 24), convertToDPUnit(getActivity(), 24));
                imageView.setLayoutParams(layoutParams);
                int padding = convertToDPUnit(getActivity(), 8);
                imageView.setPadding(padding, padding, padding, padding);
                imageView.setImageDrawable(getResources().getDrawable(mCards[position]));
                imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                container.addView(imageView);
                return imageView;
            } else {
                View view = new View(getActivity());
                container.addView(view);
                return view;
            }


        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return mCards.length;
        }

        @Override
        public float getPageWidth(int position) {
            return 0.20f;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }

    private class PrimaryColorPagerAdapter extends PagerAdapter {

        private boolean mIsDefaultItemSelected = false;


        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            if (mColors[position] != 0) {
                View view = View.inflate(container.getContext(), R.layout.cell_color, null);
                View color = view.findViewById(R.id.color);
                color.getBackground().setColorFilter(mColors[position], PorterDuff.Mode.MULTIPLY);
                container.addView(view);
                return view;
            } else {
                View view = new View(getActivity());
                container.addView(view);
                return view;
            }
        }

        @Override
        public float getPageWidth(int position) {
            return 0.20f;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return mColors.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }

    private class SecondaryColorPagerAdapter extends PagerAdapter {

        private boolean mIsDefaultItemSelected = false;


        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = View.inflate(container.getContext(), R.layout.cell_color, null);
            View color = view.findViewById(R.id.color);
            color.getBackground().setColorFilter(mColors[position], PorterDuff.Mode.MULTIPLY);

            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public float getPageWidth(int position) {
            return 0.20f;
        }

        @Override
        public int getCount() {
            return mColors.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }

    public interface OnMedicineInteractionListerner {

        public void onMedicineSet();

    }


}
