package com.healthsaverz.healthmobile.medicine.reminder.modal;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Ibrahim on 16-10-2014.
 */
public class TimeQuantityDialog extends DialogFragment implements View.OnTouchListener {

    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String TAG = "TimeQuantityDialog";
    private Context context;

    TimeSetlistener timeSetlistener;
    private String code;
    private Calendar calendar;




    public interface TimeSetlistener{
        void onTimeChange(Calendar cal, String quantity, Integer currentHour, Integer currentMinute);
        void deletePreviousReminder();
    }


    private TimePicker timepicker;

    public TimePicker getTimepicker() {
        if (timepicker == null) {
            timepicker = (TimePicker) getView().findViewById(R.id.timepicker);

        }
        return timepicker;
    }

    private ImageView minus;
    public ImageView getMinus() {
        if (minus == null) {
            minus = (ImageView) getView().findViewById(R.id.minus);

        }
        return minus;
    }

    private ImageView plus;
    public ImageView getPlus() {
        if (plus == null) {
            plus = (ImageView) getView().findViewById(R.id.plus);

        }
        return plus;
    }

    private TextView quantityText;
    public TextView getQuantityText() {
        if (quantityText == null) {
            quantityText = (TextView) getView().findViewById(R.id.quantityText);

        }
        return quantityText;
    }

    private Button setButton;
    public Button getVerifyButton() {
        if (setButton == null) {
            setButton = (Button) getView().findViewById(R.id.setButton);
            setButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                        ViewHelper.fadeOut(view, null);
                        return true;
                    } else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                        final int viewID = view.getId();
                        ViewHelper.fadeIn(view, null);
                        //int i = timepicker.getCurrentHour();
                        Calendar calendar1 = new GregorianCalendar();
                        calendar1.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                                calendar.get(Calendar.DAY_OF_MONTH),timepicker.getCurrentHour(), timepicker.getCurrentMinute(), 0);
                        calendar1.set(Calendar.MILLISECOND, 0);
                        long i = calendar1.getTimeInMillis();
                        //calendar.set(Calendar.MINUTE, );

                        if (timeSetlistener != null){

                            timeSetlistener.deletePreviousReminder();

                            timeSetlistener.onTimeChange(calendar1, getQuantityText().getText().toString(),
                                    timepicker.getCurrentHour(), timepicker.getCurrentMinute());
                        }
                        dismiss();
                        return true;


                    }
                    return false;
                }
            });
        }
        return setButton;
    }

    private Button cancelButton;
    public Button getCancelButton() {
        if (cancelButton == null) {
            cancelButton = (Button) getView().findViewById(R.id.cancelButton);
            cancelButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    //UseCases.saveRegistrationStatus(getActivity(), false);
                    dismiss();
                    return true;
                }
            });
        }
        return cancelButton;
    }

    public static TimeQuantityDialog newInstance(TimeSetlistener timeSetlistener){
        TimeQuantityDialog timeQuantityDialog = new TimeQuantityDialog();
        timeQuantityDialog.timeSetlistener = timeSetlistener;
        return timeQuantityDialog;
    }
    public static TimeQuantityDialog newInstance(TimeSetlistener timeSetlistener, Calendar calendar) {
        TimeQuantityDialog timeQuantityDialog = new TimeQuantityDialog();
        timeQuantityDialog.timeSetlistener = timeSetlistener;
        timeQuantityDialog.calendar = calendar;
        return timeQuantityDialog;
    }

    public TimeQuantityDialog() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dialog_time_quantity, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getTimepicker();
        if (calendar == null){
            calendar = Calendar.getInstance();
            timepicker.setCurrentHour(8);
            timepicker.setCurrentMinute(0);
        }
        else {
            timepicker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
            timepicker.setCurrentMinute(calendar.get(Calendar.MINUTE));
        }
        timepicker.setIs24HourView(false);
        getVerifyButton();
        getCancelButton();
        code = getQuantityText().getText().toString().trim();
        getPlus().setOnTouchListener(this);
        getMinus().setOnTouchListener(this);
        getQuantityText();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_SmileWindow;
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return dialog;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(view, null);
            return true;
        } else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
            final int viewID = view.getId();
            ViewHelper.fadeIn(view, null);
            switch (view.getId()){
                case R.id.plus:
                    double add = Double.parseDouble((quantityText.getText().toString()));
                    add = add + 0.50;
                    quantityText.setText(String.format("%.2f", add));
                    break;
                case R.id.minus:
                    double sub = Double.parseDouble((quantityText.getText().toString()));
                    if (sub > 0.50){
                        sub = sub - 0.50;
                        quantityText.setText(String.format("%.2f", sub));
                    }
                    break;

            }

        }
        return false;
    }


}
