package com.healthsaverz.healthmobile.medicine.asyncloader;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by priyankranka on 07/11/14.
 */
public class AsyncLoaderNew extends AsyncTask <String,Double,byte[]> {

    private static final String TAG = "AsyncLoader";

    public interface Listener {
        public void didReceivedError(Status status);

        public void didReceivedData(byte[] data);

        public void didReceivedProgress(Double progress);
    }

    public enum RequestMethod {
        GET, POST,POST_HEADER,GET_HEADER, REST_GET, REST_POST, SOAP_GET, SOAP_POST;
    }

    public enum State {
        NOT_DOWNLOAD, DOWNLOADING, DOWNLOADED;
    }

    public enum Status {
        IDEAL, SUCCESS, ERROR, HTTP_404, MALFORMED_URL, URISYNTAX_ERROR, UNSUPPORTED_ENCODING,ABRUPT;
    }

    private RequestMethod requestMethod;
    private State state;
    private Status status;

    public Listener getListener() {
        return listener;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    private Listener listener;

    public String requestMessage;
    public String requestActionMethod;

    public AsyncLoaderNew(RequestMethod requestMethod) {

        //this.state          =   State.NOT_DOWNLOAD;
        //this.status         =   Status.IDEAL;
        this.requestMethod = requestMethod;
    }

    // If % character is not found that means we need to perform escape of
    // the percentage characters
    // so that url is properly formatted. if percentage character is found
    // that means url string already contains
    // escape character so we ignore injecting the percentage character

    public void executeRequest(String requestURL) {

        try {
            requestURL = requestURL.trim();
            if (requestURL.indexOf("%", 0) == -1) {
                URL url = new URL(requestURL);
                URI uri = null;

                uri = new URI(url.getProtocol(), url.getUserInfo(),url.getHost(), url.getPort(), url.getPath(),url.getQuery(), url.getRef());
                url = uri.toURL();
                requestURL = String.valueOf(url);
            }

            Log.d(TAG, "Input URL -> " + requestURL);

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        this.execute(requestURL);

    }

    @Override
    protected byte[] doInBackground(String... params) {

        HttpClient httpClient = new DefaultHttpClient();

        if (!this.isCancelled()) {
            try {
                HttpResponse httpResponse = null;
                if (this.requestMethod.equals(RequestMethod.GET) || this.requestMethod.equals(RequestMethod.REST_GET)) {
                    HttpGet httpGet = new HttpGet(params[0]);
                    httpResponse = httpClient.execute(httpGet);
                } else if (this.requestMethod.equals(RequestMethod.POST) || this.requestMethod.equals(RequestMethod.REST_POST)) {
                    HttpPost httpPost = new HttpPost(params[0]);
                    httpResponse = httpClient.execute(httpPost);
                }else if (this.requestMethod.equals(RequestMethod.POST_HEADER)) {
                    HttpPost httpPost = new HttpPost(params[0]);
                    httpPost.setHeader("Content-Type", "application/xml; charset=utf-8");

                    httpPost.setEntity(new StringEntity(this.requestMessage));
                    httpResponse = httpClient.execute(httpPost);
                }
                else if (this.requestMethod.equals(RequestMethod.GET_HEADER)) {
                    HttpGet httpGet = new HttpGet(params[0]);
                    httpGet.setHeader("Content-Type", "text/xml; charset=utf-8");
                    httpResponse = httpClient.execute(httpGet);
                }
                else if (this.requestMethod.equals(RequestMethod.SOAP_POST)) {
                    HttpPost httpPost = new HttpPost(params[0]);

                    httpPost.setHeader("SOAPAction", this.requestActionMethod);
                    httpPost.setHeader("Content-Type", "text/xml; charset=utf-8");
                    httpPost.setEntity(new StringEntity(this.requestMessage));

                    httpResponse = httpClient.execute(httpPost);

                }

                state = State.DOWNLOADED;
                status = Status.SUCCESS;

                return EntityUtils.toByteArray(httpResponse.getEntity());
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, e.toString());
                status = Status.UNSUPPORTED_ENCODING;

            }catch (ClientProtocolException e) {
                Log.e(TAG, e.toString());
                status = Status.HTTP_404;
            } catch (IOException e) {
                e.printStackTrace();
                state = State.NOT_DOWNLOAD;
                status = Status.ERROR;
                this.cancel(true);

            } catch (NullPointerException e) {
                e.printStackTrace();

                state = State.NOT_DOWNLOAD;
                status = Status.ERROR;
                this.cancel(true);
            }

        }
        else
        {
            state = State.NOT_DOWNLOAD;
            status = Status.ABRUPT;
        }

        return new byte[0];
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(byte[] bytes) {
        super.onPostExecute(bytes);

        Log.d(TAG, "onPostExecute" + state);

        if (this.listener != null) {
            if (this.status.equals(Status.SUCCESS)) {
                this.listener.didReceivedData(bytes);
            } else {
                this.listener.didReceivedError(this.status);
            }
        }

    }

    @Override
    protected void onProgressUpdate(Double... values) {
        super.onProgressUpdate(values);

        if (this.listener != null) {
            this.listener.didReceivedProgress(values[0]);
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();

        if (this.listener != null) {
            this.listener.didReceivedError(this.status);
        }
    }
}
