package com.healthsaverz.healthmobile.medicine.doctor.modal;

/**
 * Created by DG on 11/6/2014.
 */
public class EPrescription {

    public static final String SessionID = "sessionID";
    public static final String ClientID = "healthsaverzm";
    public static final String Message = "message";
    public static final String MemberId = "memberId";
    public static final String FileEncodedValue = "fileEncodedValue";


    public String clientID = "healthsaverzm";
    public String sessionID = "test";

    public String prescriptionImageName = "";
    public String fileEncodedValue ="";
    public String created_By ="";
    public String doctorId ="";   //UserID
    public String memberId = "7";


    public static String getSessionID() {
        return SessionID;
    }

    public static String getClientID() {
        return ClientID;
    }




    public String toAddEPresciptionXml() {
        String toAddEPresciptionXml = "<PrescriptionFile>" +
                "<sessionID>"+sessionID+"</sessionID>" +
                "<clientID>"+clientID+"</clientID>" +
                "<memberId>"+memberId+"</memberId>" +
                "<prescriptionImageName>"+prescriptionImageName+"</prescriptionImageName>" +
                "<prescriptionImageUrl> </prescriptionImageUrl>" +
                "<fileEncodedValue>"+fileEncodedValue+"</fileEncodedValue>" +
                "<created_By> </created_By>" +
                "<created_On></created_On>" +
                "<Extra_1> </Extra_1>" +
                "<Status> </Status>" +
                "<Extra_3> </Extra_3>" +
                "<Extra_4> </Extra_4>" +
                "<Extra_5> </Extra_5>" +
                "<doctorId>"+doctorId+"</doctorId>" +
                "<mobilePrescriptionId></mobilePrescriptionId>" +
                "</PrescriptionFile>";


        return toAddEPresciptionXml;
    }
}