package com.healthsaverz.healthmobile.medicine.medicine.controller;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.calendar.CalendarActivity;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.DialogProgressFragment;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.MedFriend;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.SmsParser;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.UpdateMedFriendParser;
import com.healthsaverz.healthmobile.medicine.medicine.modal.AlarmReceiver;
import com.healthsaverz.healthmobile.medicine.medicine.modal.Medicine;
import com.healthsaverz.healthmobile.medicine.medicine.modal.ReminderModal;
import com.healthsaverz.healthmobile.medicine.medicine.modal.UpdateTakeSkipCountParser;
import com.healthsaverz.healthmobile.medicine.reminder.controller.RefillActivity;
import com.healthsaverz.healthmobile.medicine.reminder.modal.SnoozeDialogFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

public class NotificationActivity extends Activity implements SnoozeDialogFragment.SnoozeDialogListener, SmsParser.SmsParserListener, UpdateMedFriendParser.UpdateFriendParserListener {

    /*
    Notificationtype = 1 normal pill reminder
    Notificationtype = 3 means reminder for RX days
    Notificationtype = 4 means reminder for snooze
    Notificationtype = 5 means reminder for RX pills RefillActivity
    Notificationtype = 10 means reminder for schedule after 30 minutes miss for message
     */
    int medicine_id;
    int reminder_id;
    private ImageView medImage;
    private MedicineDBHandler dbHandler;
    private Medicine medicine;
    private String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec" };
    private ReminderModal reminderModal1;

    int notificationtype;

    public static final String TAG = NotificationActivity.class.getSimpleName();
    private ArrayList<MedFriend> medFriends;
    private int start = 0;

    private DialogProgressFragment progressFragment;
    private SmsParser smsParser;
    private UpdateTakeSkipCountParser updateTakeSkipCountParser;
    //private boolean isprocessed;

    public SmsParser getSmsParser(){
        if (smsParser != null){
            smsParser.cancel(true);
            smsParser = null;
        }
        smsParser = new SmsParser(this);
        smsParser.setSmsParserListener(this);
        return smsParser;
    }

    public ImageView getMedImage() {
        if (medImage == null) {
            medImage = (ImageView) this.findViewById(R.id.medImage);

        }
        return medImage;
    }
    private TextView medicineName;
    public TextView getMedicineName() {
        if (medicineName == null) {
            medicineName = (TextView) this.findViewById(R.id.medicineName);

        }
        return medicineName;
    }
    private TextView patientName;
    public TextView getPatientName() {
        if (patientName == null) {
            patientName = (TextView) this.findViewById(R.id.patientName);

        }
        return patientName;
    }
    private TextView medDetail;
    public TextView getMedDetail() {
        if (medDetail == null) {
            medDetail = (TextView) this.findViewById(R.id.medDetail);

        }
        return medDetail;
    }
    private TextView medTime;
    public TextView getMedTime() {
        if (medTime == null) {
            medTime = (TextView) this.findViewById(R.id.medTime);

        }
        return medTime;
    }

    private TextView quantity;
    public TextView getQuantity() {
        if (quantity == null) {
            quantity = (TextView) this.findViewById(R.id.quantity);

        }
        return quantity;
    }
    private Button setButton;
    public Button getVerifyButton() { // Take Button
        if (setButton == null) {

            setButton = (Button) this.findViewById(R.id.setButton);
            setButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN)
                    {
                        ViewHelper.fadeOut(view, null);
                        return true;
                    }
                    else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP)
                    {
                        ViewHelper.fadeIn(view, null);
                        float finalPillCount;
                        if (medicine.get_pills() != -1.0f){

                            float v = (float) (medicine.get_pills() - ((reminderModal1.get_quantity() != null) ? Float.parseFloat(reminderModal1.get_quantity()) : 0.0));

                            finalPillCount = medicine.get_pills() - Float.parseFloat(reminderModal1.get_quantity());

                            Log.d("NotificationActivity","finalPillCount : "+finalPillCount);

                            Medicine medicine1 = new Medicine(medicine.get_medID(), medicine.get_medName(), medicine.get_patientName(),
                                    medicine.get_shape(), medicine.get_color1(), medicine.get_color2(), medicine.get_startDate(),
                                    medicine.get_endDate(), medicine.get_dose(), medicine.get_unit(), medicine.get_status(),
                                    medicine.get_description(),finalPillCount);
                            medicine1._threshold = medicine._threshold;
                            medicine1._tempEndDate  =   medicine._tempEndDate;

                            dbHandler.updateMedicine(medicine1);

                            reminderModal1._taken_time = AppConstants.getCurrentCalendar().getTimeInMillis()+"";
                            if (Long.parseLong(reminderModal1._taken_time) - Long.parseLong(reminderModal1.get_time()) < AppConstants.TIME_DIFFERENCE_MINUTES )
                                reminderModal1._taken = 1;
                            else
                                reminderModal1._taken = 2;
                            reminderModal1._snooze = -1;
                            dbHandler.updateReminder(reminderModal1);

                            if (v <= medicine._threshold) {
                                Intent intent1 = new Intent(NotificationActivity.this,RefillActivity.class);
                                intent1.putExtra("reminder_id",reminder_id);
                                intent1.putExtra("medicine_id",medicine_id);
                                intent1.putExtra("time",reminderModal1.get_time());
                                intent1.putExtra("notificationtype",notificationtype);

                                startActivity(intent1);
                            }

                        } else {
                            reminderModal1._taken_time = AppConstants.getCurrentCalendar().getTimeInMillis()+"";
                            if (Long.parseLong(reminderModal1._taken_time) - Long.parseLong(reminderModal1.get_time()) < AppConstants.TIME_DIFFERENCE_MINUTES )
                                reminderModal1._taken = 1;
                            else
                                reminderModal1._taken = 2;
                            reminderModal1._snooze = -1;
                            dbHandler.updateReminder(reminderModal1);
                            finalPillCount = medicine.get_pills();
                        }
                        updateTakeSkipCountParser.updateReminder(reminderModal1, medicine.get_medName(), medicine.get_dose(), finalPillCount);
                        /*
                         Once we have taken the pill we need to the remover the scheduler.
                        */
                        AppConstants.cancelNotificationScheduler(getApplicationContext(), reminderModal1, 10);
                        finish();
                        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.cancel(reminderModal1.get_remindID());
                        return true;
                    }
                    return false;
                }
            });
        }
        return setButton;
    }



    private Button cancelButton;
    public Button getCancelButton() {
        if (cancelButton == null) {
            cancelButton = (Button) this.findViewById(R.id.cancelButton);
            cancelButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    //UseCases.saveRegistrationStatus(getActivity(), false);
                    if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN)
                    {
                        ViewHelper.fadeOut(view, null);
                        return true;
                    }
                    else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP)
                    {
                        ViewHelper.fadeIn(view, null);
                        reminderModal1._taken = 0;
                        reminderModal1._taken_time = AppConstants.getCurrentCalendar().getTimeInMillis()+"";
                        reminderModal1._snooze = -1;
                        dbHandler.updateReminder(reminderModal1);
                        updateTakeSkipCountParser.updateReminder(reminderModal1, medicine.get_medName(), medicine.get_dose(), medicine.get_pills());

                        /*
                         Once we have choose skip the pill we need to the remover the shceduler as SMS will be send immediately to Med Friend
                        */

                        AppConstants.cancelNotificationScheduler(getApplicationContext(), reminderModal1, 10);
                        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.cancel(reminderModal1.get_remindID());

                        /*
                        Send SMS to the med Friend
                         */

                        medFriends = dbHandler.getAllMedFriendsForMedID(medicine.get_medID());
                        if (medFriends != null && medFriends.size() > start){
                            for (MedFriend medFriend : medFriends){
                                medFriend._skipCount = medFriend._skipCount+1;
                            }
                            getSmsParser().getsms(medFriends.get(start),reminderModal1.get_medID(), reminderModal1.get_remindID());
                            progressFragment =  new DialogProgressFragment();
                            progressFragment.show(getFragmentManager(), TAG);
                            progressFragment.setCancelable(false);
                        }
                        else {
                            finish();
                        }

                        return true;
                    }

                    return false;
                }
            });
        }
        return cancelButton;
    }

    private Button snoozeButton;
    public Button getSnoozeButton() {
        if (snoozeButton == null) {
            snoozeButton = (Button) this.findViewById(R.id.snoozeButton);
            snoozeButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    //UseCases.saveRegistrationStatus(getActivity(), false);

                    if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN)
                    {
                        ViewHelper.fadeOut(view, null);

                        return true;
                    }
                    else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP)
                    {
                        ViewHelper.fadeIn(view, null);
                        SnoozeDialogFragment.newInstance(NotificationActivity.this).show(getFragmentManager(), TAG);
                        return true;
                    }

                    return false;
                }
            });
        }
        return cancelButton;
    }

    private int[] mCards = {
            0,
            0,
            R.drawable.badge1,
            R.drawable.badge2,
            R.drawable.badge3,
            R.drawable.capsule,
            R.drawable.badge5,
            R.drawable.badge6,
            R.drawable.badge7,
            R.drawable.badge8,
            R.drawable.badge9,
            R.drawable.badge10,
            R.drawable.badge11,
            R.drawable.badge12,
            R.drawable.badge14,
            R.drawable.badge15,
            R.drawable.badge16,
            R.drawable.badge17,
            R.drawable.badge18,
            R.drawable.badge19,
            R.drawable.badge20,
            R.drawable.badge21,
            R.drawable.badge22,
            R.drawable.badge23,
            R.drawable.badge24,
            R.drawable.badge25,
            R.drawable.badge26,
            R.drawable.badge27,
            R.drawable.badge28,
            0,
            0};

    private int[] mColors = {
            0,
            0,
            Color.parseColor("#3cbaff"),
            Color.parseColor("#2797d0"),
            Color.parseColor("#005c94"),
            Color.parseColor("#d4ff5f"),
            Color.parseColor("#a9e327"),
            Color.parseColor("#27a227"),
            Color.parseColor("#ffff5c"),
            Color.parseColor("#ff9900"),
            Color.parseColor("#ff7d27"),
            Color.parseColor("#efd597"),
            Color.parseColor("#dba747"),
            Color.parseColor("#936827"),
            Color.parseColor("#ff4141"),
            Color.parseColor("#e12727"),
            Color.parseColor("#c02727"),
            Color.parseColor("#ffae8e"),
            Color.parseColor("#ffa2b2"),
            Color.parseColor("#e9967a"),
            Color.parseColor("#fb9388"),
            Color.parseColor("#f3d2ff"),
            Color.parseColor("#dd82ff"),
            Color.parseColor("#c527ff"),
            Color.parseColor("#2d2d2d"),
            Color.parseColor("#7d7d7d"),
            Color.parseColor("#a9a9a9"),
            Color.parseColor("#d4d4d4"),
            Color.parseColor("#ffffff"),
            Color.parseColor("#000000"),
            0,
            0};


    public Bitmap mergeBitmap(int colo1,int colo2)
    {

        Bitmap firstBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.badge3left);
        Bitmap secondBitmap = BitmapFactory.decodeResource(this.getResources(),R.drawable.badge3right);

        Bitmap comboBitmap;

        float width, height;

        width = firstBitmap.getWidth() + secondBitmap.getWidth();
        height = firstBitmap.getHeight();

        comboBitmap = Bitmap.createBitmap((int)(width/2), (int)height, Bitmap.Config.ARGB_4444);

        Paint firstPaint = new Paint();
        firstPaint.setColorFilter(new PorterDuffColorFilter(mColors[colo1], PorterDuff.Mode.MULTIPLY));

        Paint secondPaint = new Paint();
        secondPaint.setColorFilter(new PorterDuffColorFilter(mColors[colo2], PorterDuff.Mode.MULTIPLY));

        Canvas comboImage = new Canvas(comboBitmap);

        comboImage.drawBitmap(firstBitmap, 0f, 0f, firstPaint);
        comboImage.drawBitmap(secondBitmap,0f,0f, secondPaint);

        return comboBitmap;

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_notification);
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setFinishOnTouchOutside(false);
        medicine_id = this.getIntent().getIntExtra("medicine_id", -1);
        reminder_id =   this.getIntent().getIntExtra("reminder_id",-1);
        notificationtype = this.getIntent().getIntExtra("notificationtype",-1);
        Log.d("onCreate NotfcatonAcvty", "med_id " + medicine_id);
        Log.d("onCreate NotfcatonAcvty","rem_id "+reminder_id);
        //isprocessed = true;
        if (medicine_id != -1){
            dbHandler = new MedicineDBHandler(this);
            medicine = dbHandler.findMedicinefromID(medicine_id);
            if (medicine != null)
            {
                reminderModal1 = dbHandler.findSpecificReminder(reminder_id);

                Calendar calendar = new GregorianCalendar();
                calendar.setTimeInMillis(Long.parseLong(reminderModal1.get_time()));
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);

                if (medicine.get_shape() == 5) {

                    getMedImage().setColorFilter(null);
                    getMedImage().setImageBitmap(mergeBitmap(medicine.get_color1(),medicine.get_color2()));

                }else if(medicine.get_shape() == -1){
                    getMedImage().setImageBitmap(BitmapFactory.decodeResource(this.getResources(),mCards[2]));
                    getMedImage().setColorFilter(null);
                    getMedImage().setColorFilter(mColors[28], PorterDuff.Mode.MULTIPLY);
                }
                else {
                    getMedImage().setImageBitmap(BitmapFactory.decodeResource(this.getResources(),mCards[medicine.get_shape()]));
                    getMedImage().setColorFilter(null);
                    getMedImage().setColorFilter(mColors[medicine.get_color1()], PorterDuff.Mode.MULTIPLY);
                }
                getMedicineName().setText(medicine.get_medName());
                getPatientName().setText("For : "+medicine.get_patientName());

                getMedTime().setText("Scheduled for "+AppConstants.mFormat.format((double) calendar.get(Calendar.HOUR_OF_DAY))+":"+AppConstants.mFormat.format((double) calendar.get(Calendar.MINUTE))+", "
                        +AppConstants.mFormat.format((double) calendar.get(Calendar.DAY_OF_MONTH))+"-"+months[calendar.get(Calendar.MONTH)]);
                getMedDetail().setText("Take "+ reminderModal1.get_quantity()+" "+medicine.get_description());
                if (medicine.get_pills() != -1){
                    getQuantity().setText("You currently have: "+String.valueOf(medicine.get_pills())+" pills.");
                }
                else {
                    getQuantity().setVisibility(View.GONE);
                }
            }
            else {
                finish();
                Toast.makeText(this, "No medication found", Toast.LENGTH_SHORT).show();
            }
        }

        getVerifyButton();
        getCancelButton();
        getSnoozeButton();
//        ActionBar actionBar = getActionBar();
//        actionBar.setBackgroundDrawable(new ColorDrawable(0xFF160203));
        //AppConstants.reScheduleAllReminders(getApplicationContext());
        //reScheduleAllReminders();
        updateTakeSkipCountParser = new UpdateTakeSkipCountParser(NotificationActivity.this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CalendarActivity.addButtonClicked = true;
        //m.stop();
    }


    //this method is called when user selects any snooze time
    @Override
    public void didSelectSnoozeTime(int snoozeTime) {
        //if we get -1 in callback that means user has press cancel button
        if(snoozeTime != -1)
        {
            Calendar calendar = AppConstants.getCurrentCalendar();
            int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
            int currentMin  = calendar.get(Calendar.MINUTE);
            currentMin = (currentHour*60) + currentMin;
            int maxDayMinutes = (24*60);

            if(currentMin+snoozeTime > maxDayMinutes)//this indicate it is going to next date we need to set at 23:50
            {
                // We need to alert the user on this condition and asking to set snooze time less.
                getAlert("Please select a lesser Snooze time");
            }
            else
            {
                Calendar calendar3 = new GregorianCalendar();
                calendar3.setTimeInMillis(Long.parseLong(reminderModal1.get_time()));
                int timeDiff =(int) TimeUnit.MILLISECONDS.toMinutes( calendar.getTimeInMillis() - calendar3.getTimeInMillis()) ;
                if (reminderModal1._snooze == -1){
                    reminderModal1._snooze = snoozeTime + timeDiff;
                }
                else {
                    reminderModal1._snooze = snoozeTime + timeDiff;
                }
                calendar3.add(Calendar.MINUTE,reminderModal1._snooze);

                reminderModal1._taken = -1;
                reminderModal1._taken_time = "";
                //reminderModal1._snooze = 1; // snooze 1 is to indicate that this is a snooze time and not the original time set by the user (by default it is -1)
                dbHandler.updateReminder(reminderModal1);
                updateTakeSkipCountParser.updateReminder(reminderModal1, medicine.get_medName(), medicine.get_dose(), medicine.get_pills());
            /*
            We need to create the new alarm for the snooze time
             */
                Intent intent1 = new Intent(this, AlarmReceiver.class);
                //intent1.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                intent1.putExtra("medicine_id",reminderModal1.get_medID());
                intent1.putExtra("reminder_id",reminderModal1.get_remindID());
                if(reminderModal1.get_qty_type() == 0)// dose reminder
                {
                    intent1.putExtra("notificationtype",4);// 4 means it is for snooze medication reminder.
                }
                else if(reminderModal1.get_qty_type() == 1)// RX reminder based on Days
                {
                    intent1.putExtra("notificationtype",5);// 5 mean it is for snooze RX reminder days.
                }
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),reminderModal1.get_remindID(), intent1, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);

                alarmManager.cancel(pendingIntent);
                if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    alarmManager.set(AlarmManager.RTC_WAKEUP,calendar3.getTimeInMillis(),pendingIntent);
                } else {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP,calendar3.getTimeInMillis(),pendingIntent);
                }
            /*
              Once we have snooze the pill we need to the remove the scheduler and add a new scheduler with added snooze time.
            */

                AppConstants.cancelNotificationScheduler(getApplicationContext(), reminderModal1, 10);
                AppConstants.createNotificationScheduler(getApplicationContext(), reminderModal1, 10);

                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.cancel(reminderModal1.get_remindID());
                finish();
            }

        }
    }


    @Override
    public void smsParserDidReceiveData(String message) {
        if (message != null && message.equals("1701")){
            UpdateMedFriendParser medFriendParser = new UpdateMedFriendParser(NotificationActivity.this);
            medFriendParser.setMedFriendParserListener(NotificationActivity.this);
            medFriendParser.updateMedFriend(medFriends.get(start));
        }
        else  {
            start++;
            if (medFriends != null && medFriends.size() > start){
                getSmsParser().getsms(medFriends.get(start),reminderModal1.get_medID(), reminderModal1.get_remindID());
            }
            else {
                if (progressFragment != null){
                    progressFragment.dismiss();
                    progressFragment = null;
                }
                //getAlert("Something went wrong");
                finish();
            }
        }
    }

    @Override
    public void smsParserDidReceivedConnectionError(AsyncLoaderNew.Status status) {
        start++;
        if (medFriends != null && medFriends.size() > start){
            getSmsParser().getsms(medFriends.get(start),reminderModal1.get_medID(), reminderModal1.get_remindID());
        }
        else {
            if (progressFragment != null){
                progressFragment.dismiss();
                progressFragment = null;
            }
            //getAlert(status.toString());
            finish();
        }
    }

    @Override
    public void smsParserDidReceivedProcessingError(String message) {
        start++;
        if (medFriends != null && medFriends.size() > start){
            getSmsParser().getsms(medFriends.get(start),reminderModal1.get_medID(), reminderModal1.get_remindID());
        }
        else {
            if (progressFragment != null){
                progressFragment.dismiss();
                progressFragment = null;
            }
            //getAlert(message);
            finish();
        }
        Log.d("NotifictionActivity", message);
    }

    @Override
    public void updateFriendParserDidReceiveData(Object message) {
        dbHandler.updateMedFriend(medFriends.get(start));
        start++;
        if (medFriends != null && medFriends.size() > start){
            getSmsParser().getsms(medFriends.get(start),reminderModal1.get_medID(), reminderModal1.get_remindID());
        }
        else {
            if (progressFragment != null){
                progressFragment.dismiss();
                progressFragment = null;
            }
            Toast.makeText(this, "We've informed your friends", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void getAlert(String message){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle("Error");
        // Setting Dialog Message
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);

        // Setting Positive "OK" Btn
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                        finish();
                    }
                });
        // Showing Alert Dialog
        alertDialog.show();
    }
}
