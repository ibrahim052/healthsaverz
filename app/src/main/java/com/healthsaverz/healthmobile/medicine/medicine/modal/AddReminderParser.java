package com.healthsaverz.healthmobile.medicine.medicine.modal;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;

import java.text.SimpleDateFormat;

/**
 * Created by DELL PC on 2/14/2015.
 */
public class AddReminderParser extends AsyncTask<String,Void,String> implements AsyncLoaderNew.Listener {

    /*
    Start from addMedicine method
    Takes String as input and out is based on addMedicineParserListener
     */

    private AsyncLoaderNew asyncLoaderNew;
    private Context context;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public AddReminderParser(Context context){

        this.context = context;
    }

    private static final String METHOD_ADD_REMINDER = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/ReminderServices/addReminder2";

    public void addNewReminder(ReminderModal reminderModal, String previousTime, String medName, String dose){
        asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.POST_HEADER);
        String sessionID = PreferenceManager.getStringForKey(context, PreferenceManager.SESSION_ID, "");
        String clientID = PreferenceManager.getStringForKey(context, PreferenceManager.CLIENT_ID, "");
        String userId = PreferenceManager.getStringForKey(context, PreferenceManager.ID, "");


        String requestXML = "<reminder>" +
                "<sessionID>"+sessionID+"</sessionID>" +
                "<clientID>"+clientID+"</clientID>" +
                "<userId>"+userId+"</userId>" +
                "<medicineName>"+medName+"</medicineName>" +
                "<dose>"+dose+"</dose> " +
                "<quantity>"+reminderModal.get_quantity()+"</quantity>" +
                "<time>"+sdf.format(Long.parseLong(reminderModal.get_time()))+"</time>" +
                "<previousTime>"+(previousTime.equals("")?"":sdf.format(Long.parseLong(previousTime)))+"</previousTime>"+
                "<quantityType>"+reminderModal.get_qty_type()+"</quantityType>" +
                "<reminderStatus>"+reminderModal._taken+"</reminderStatus>" +
                "<takenTime>"+"</takenTime>" +
                "<snoozed>"+reminderModal._snooze+"</snoozed>" +
                "<snoozedTime></snoozedTime>" +
                "<status>A</status>" +
                "<createdBy>"+userId+"</createdBy>" +
                "<extra_1></extra_1>" +
                "<extra_2></extra_2>" +
                "<extra_3></extra_3>" +
                "<extra_4></extra_4>" +
                "<extra_5></extra_5>" +
                "</reminder>";
        Log.d("requestXML ", "requestXML " + requestXML);
        asyncLoaderNew.requestMessage   =   requestXML;
        asyncLoaderNew.executeRequest(METHOD_ADD_REMINDER);
        asyncLoaderNew.setListener(this);
    }
    //AsyncLoaderNew.Listener method implementation
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {


    }

    @Override
    public void didReceivedData(byte[] data) {

        String responseString = new String(data);
        Log.d("addMedicineParser","didReceivedData is "+responseString);
        execute(responseString);
    }

    @Override
    public void didReceivedProgress(Double progress) {
    }
    //Current Thread Stack
    @Override
    protected String doInBackground(String... params) {
        return null;
    }
}

