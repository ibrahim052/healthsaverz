package com.healthsaverz.healthmobile.medicine.doctor.view;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.doctor.modal.Doctor;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by healthsaverz5 on 3/20/2015.
 * @author Priyanka Kale
 */
public class DoctorParser extends AsyncTask<String, Void, Doctor> implements AsyncLoaderNew.Listener  {

    private AsyncLoaderNew asyncLoaderNew;
    public Context context;
    private static final String METHOD_GET_DOCTORDETAIL = AppConstants.WEB_DOMAIN+"/sHealthSaverz/jaxrs/DoctorServices/getDoctor/healthsaverz/test/DoctorId/";


    public DoctorParser(Context context) {
        this.context = context;
    }

    public interface DoctorDetailParserListener
    {
        void doctorDetailParserDidReceivedMenu(Doctor docdetails);
        void doctorDetailDidReceivedConnectionError(AsyncLoaderNew.Status status);// AsyncLoaderNew.Status
        void doctorDetailDidReceivedProcessingError(String message);//server message for unsuccessful
    }

    private DoctorDetailParserListener doctorDetailParserListener;

    public DoctorDetailParserListener getProductMenuParserListener() {
        return doctorDetailParserListener;
    }

    public void setProductMenuParserListener(DoctorDetailParserListener doctorDetailParserListener) {
        this.doctorDetailParserListener = doctorDetailParserListener;
    }


    public void getDoctorDetailsFromSever() {
        /**
         *  get doctor details from server with doctorId
         */
        Log.d("SERVER HIT ONE","getDoctorDetailsFromSever METHOD_GET_DOCTORDETAIL");
        AsyncLoaderNew asyncLoadergetDoc = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.GET);
        String doctorId = PreferenceManager.getStringForKey(context, PreferenceManager.DOCTORID, "");
        asyncLoadergetDoc.setListener(this);
        asyncLoadergetDoc.executeRequest(METHOD_GET_DOCTORDETAIL + doctorId );

   }
    @Override
    protected Doctor doInBackground(String ... params) {

        Doctor doctor = new Doctor();
        try {
            JSONObject json = new JSONObject(params[0]);

//            Log.d("DoctorParse"," doctor.doctorId"+ doctor.message);
            if(!json.getString("message").equalsIgnoreCase("Password is invalid")) {
//                Log.d("DoctorParse"," doctor.cellNumber-----------"+ doctor.cellNumber);
                doctor.doctorId = json.getString("doctorId");
                doctor.doctorName = json.getString("fullName");
                doctor.cellNumber = json.getString("cellNumber");
                doctor.fullAddress = json.getString("fullAddress");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return doctor;
    }

    @Override
    protected void onPostExecute(Doctor s) {
        super.onPostExecute(s);
        if(s == null){
            if(doctorDetailParserListener != null){
                doctorDetailParserListener.doctorDetailDidReceivedProcessingError("Invalid Data");
            }
        }
        else
        {
            if(doctorDetailParserListener != null){
                doctorDetailParserListener.doctorDetailParserDidReceivedMenu(s);

            }
        }
    }

    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {
    }

    @Override
    public void didReceivedData(byte[] data) {
        String responseString = new String(data);
        execute(responseString);
    }

    @Override
    public void didReceivedProgress(Double progress) {
    }

}
