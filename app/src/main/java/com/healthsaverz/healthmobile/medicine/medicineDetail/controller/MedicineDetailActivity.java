package com.healthsaverz.healthmobile.medicine.medicineDetail.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoader;
import com.healthsaverz.healthmobile.medicine.asyncloader.ParserListener;
import com.healthsaverz.healthmobile.medicine.global.App;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.Parser;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
import com.healthsaverz.healthmobile.medicine.medicineDetail.modal.SubstitueRequest;
import com.healthsaverz.healthmobile.medicine.medicineSearch.modal.MedicineRequest;
import com.healthsaverz.healthmobile.medicine.medicineSearch.modal.MedicineSearchResponse;
import com.healthsaverz.healthmobile.medicine.medicineSearch.modal.SubstituteSearchResponse;
import com.healthsaverz.healthmobile.medicine.reminder.controller.ReminderActivity;

import java.util.ArrayList;
import java.util.Calendar;

public class MedicineDetailActivity extends Activity implements ParserListener, View.OnTouchListener {


    private ListView listView;
    private ProgressBar locationProgress;
    private Parser parser;
    private TextView mediTitle;
    private TextView descriptionMedic;
    private TextView composition;
    private ImageView icon;
    private TextView mrpText;
    private Button addReminderButton;
    //private Button callUsButton;
    private TextView emptyText;

    private String url1 = "sHealthSaverz/jaxrs/ProductServices/getSubsitute/";
    private MedicineSearchResponse obj;
    private MedicineSearchResponse medicineSearchResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_detail);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        mediTitle = (TextView) findViewById(R.id.medi_title);
        descriptionMedic = (TextView) findViewById(R.id.descriptionMedic);
        composition = (TextView) findViewById(R.id.composition);
        //composition.setOnTouchListener(this);
        listView = (ListView) findViewById(R.id.medic_list);
        emptyText = (TextView)findViewById(R.id.emptyText);
        listView.setEmptyView(null);
        icon = (ImageView)findViewById(R.id.medi_icon);
        mrpText = (TextView)findViewById(R.id.mrpText);
        locationProgress = (ProgressBar) findViewById(R.id.progressbar);
        addReminderButton = (Button)findViewById(R.id.addReminderButton);
        //callUsButton = (Button)findViewById(R.id.callUsButton);
        addReminderButton.setOnTouchListener(this);
        //callUsButton.setOnTouchListener(this);

        getParser();
        medicineSearchResponse = (MedicineSearchResponse) getIntent().getParcelableExtra("medicine");
        MedicineRequest medicineRequest = (MedicineRequest) getIntent().getParcelableExtra("product");
        //medicineRequest.clientID = PreferenceManager.getStringForKey(this, "clientID", "null");
        medicineRequest.sessionID = PreferenceManager.getStringForKey(this, "sessionID", "null");

        if (medicineSearchResponse != null && medicineRequest != null){
            mediTitle.setText(medicineSearchResponse.brandsName + " "
                    + ((medicineSearchResponse.strengthName.equals("0"))?"":medicineSearchResponse.strengthName+" ")
                    //+ ((medicineSearchResponse.strengthUnit == 0)?"":medicineSearchResponse.strengthUnit+" ")

                    + ((medicineSearchResponse.strengthUnitName.equals("0"))?"":medicineSearchResponse.strengthUnitName+" ")
                    +" ("
                    + ((medicineSearchResponse.packagingSizeName.equals("0"))?"":medicineSearchResponse.packagingSizeName+" ")
                    + ((medicineSearchResponse.packagingUnitName.equals("0"))?"":medicineSearchResponse.packagingUnitName+" ")
                    + ((medicineSearchResponse.productPackagingName.equals("0"))?"":medicineSearchResponse.productPackagingName)
                    + ")");
            descriptionMedic.setText(medicineSearchResponse.manfacuterName);
            if (!medicineSearchResponse.genericName.equals("0")){
                composition.setText(medicineSearchResponse.genericName +((medicineSearchResponse.strengthName.equals("0"))?"":" ("+medicineSearchResponse.strengthName
                        + ((medicineSearchResponse.strengthUnitName.equals("0"))?"":medicineSearchResponse.strengthUnitName+")")));
            }
            if (medicineSearchResponse.mrp > 0){
                mrpText.setText("MRP: "+medicineSearchResponse.mrp
                        + ((medicineSearchResponse.packagingSizeName.equals("0"))?"":" for "+medicineSearchResponse.packagingSizeName+" ")
                        + ((medicineSearchResponse.packagingUnitName.equals("0"))?"":medicineSearchResponse.packagingUnitName+" in a pack"));
            }
            switch (medicineSearchResponse.iconId){
                case 0:
                    icon.setImageDrawable(null);
                    break;
                case 1:
                    icon.setImageResource(R.drawable.bottle);
                    break;
                case 2:
                    icon.setImageResource(R.drawable.eyedropper);
                    break;
                case 3:
                    icon.setImageResource(R.drawable.injection);
                    break;
                case 4:
                    icon.setImageResource(R.drawable.tablet);
                    break;
                case 5:
                    icon.setImageDrawable(null);
                    break;
            }
            //icon.setImageResource(R.drawable.ic_launcher);
            ////  Creating a request modal for searching substitutes of the medicine you searched
            SubstitueRequest substitueRequest = new SubstitueRequest(medicineRequest.getClientID(), medicineRequest.getSessionID(), medicineRequest.getSearchTitle(),
                    String.valueOf(medicineSearchResponse.strength), String.valueOf(medicineSearchResponse.strengthUnit),
                    String.valueOf(medicineRequest.getStartRow()), String.valueOf(medicineRequest.getEndRow()), medicineSearchResponse.productPackagingName,
                    medicineSearchResponse.packagingSizeName, String.valueOf(medicineSearchResponse.packagingUnit));
            ////    Calling parser
            getParser().substituteDetails(substitueRequest);
        }

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(view, null);
            return true;
        }
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
            ViewHelper.fadeIn(view, null);
            switch (view.getId()){
                case R.id.composition:
                    ///////// A dialog is shown on clicking the composition to show the full composition

                    new AlertDialog.Builder(this).setTitle("Composition")
                            .setMessage(medicineSearchResponse.genericName
                                    +((medicineSearchResponse.strengthName.equals("0"))?"":" ("+medicineSearchResponse.strengthName
                                    + ((medicineSearchResponse.strengthUnitName.equals("0"))?"":medicineSearchResponse.strengthUnitName+")")))
                            .show();
                    return true;
                case R.id.addReminderButton:
                    ////////Statrting reminder activity and passing medicine name and dose in intent
                    App app = (App) this.getApplicationContext();
                    app.isMed = true;
                    app.medName = medicineSearchResponse.brandsName;
                    app.strengthName = ((medicineSearchResponse.strengthName.equals("0"))?"":medicineSearchResponse.strengthName+" ")
                            + ((medicineSearchResponse.strengthUnitName.equals("0"))?"":medicineSearchResponse.strengthUnitName);
                    Intent intent = new Intent(this, ReminderActivity.class);
                    intent.putExtra("CurrCal", Calendar.getInstance().getTimeInMillis());
                    startActivity(intent);
                    return true;
                case R.id.callUsButton:
                    String uri = "tel:" + AppConstants.call_us.trim() ;
                    Intent intent1 = new Intent(Intent.ACTION_DIAL);
                    intent1.setData(Uri.parse(uri));
                    startActivity(intent1);
                    return true;
            }
        }
        return false;
    }
    public Parser getParser() {
        if (parser == null) {
            parser = new Parser(this);
            parser.setListener(this);
        }
        return parser;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.medicine_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void didReceivedError(AsyncLoader.Status errorCode) {
        if (parser != null){
            parser.cancel(true);
            parser = null;
        }
        Toast.makeText(this, "Error in connection ", Toast.LENGTH_SHORT).show();
        listView.setAdapter(null);
        listView.setEmptyView(emptyText);
        locationProgress.setVisibility(View.GONE);
    }

    @Override
    public void didReceivedData(Object result) {
        if (result != null) {
            ArrayList<SubstituteSearchResponse> substituteSearchResponses = (ArrayList<SubstituteSearchResponse>) result;
            if (substituteSearchResponses.size() > 0){
                SubstituteAdapter substituteAdapter = new SubstituteAdapter(substituteSearchResponses, this);
                listView.setAdapter(substituteAdapter);
                locationProgress.setVisibility(View.GONE);

            }
            else {
//                Toast.makeText(this, "No result Found ", Toast.LENGTH_SHORT).show();
                listView.setAdapter(null);
                listView.setEmptyView(emptyText);
                locationProgress.setVisibility(View.GONE);
            }
            //Toast.makeText(this, "Search Text : ", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, "No result Found ", Toast.LENGTH_SHORT).show();
            listView.setAdapter(null);
            listView.setEmptyView(emptyText);
            locationProgress.setVisibility(View.GONE);

        }
        if (parser != null){
            parser.cancel(true);
            parser = null;
        }
    }

    @Override
    public void didReceivedProgress(double progress) {

    }


}
