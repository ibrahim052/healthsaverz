package com.healthsaverz.healthmobile.medicine.filters.controller;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.filters.modal.Filter;
import com.healthsaverz.healthmobile.medicine.filters.modal.FilterMaster;
import com.healthsaverz.healthmobile.medicine.filters.modal.FilterParser;
import com.healthsaverz.healthmobile.medicine.filters.modal.FilterType;
import com.healthsaverz.healthmobile.medicine.global.DialogProgressFragment;

import java.util.ArrayList;

public class FilterActivity extends Activity implements FilterParser.FilterParserListener, AdapterView.OnItemClickListener, View.OnClickListener {

    FilterMaster filterMaster = null;

    DialogProgressFragment progressFragment;
    public static final String TAG = FilterActivity.class.getSimpleName();

    public static final int FILTER_SUBCATEGORY  =   1;
    public static final int FILTER              =   2;

    ListView listViewFilterSubcategories = null;
    ListView listViewFilters = null;

    int subCategorySelected = 0;

    FilterListViewAdapter   filterSubcategoryListViewAdapter = null;
    FilterListViewAdapter   filterListViewAdapter = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        int categoryId = getIntent().getIntExtra("categoryId",-1);

        progressFragment =  new DialogProgressFragment();
        progressFragment.show(getFragmentManager(), TAG);
        progressFragment.setCancelable(false);

        FilterParser filterParser = new FilterParser(this);
        filterParser.setFilterParserListener(this);
        filterParser.validateCategeroy(""+categoryId);

        listViewFilterSubcategories = (ListView)findViewById(R.id.filter_categories);
        listViewFilters = (ListView)findViewById(R.id.filters);

        findViewById(R.id.action_cancel).setOnClickListener(this);
        findViewById(R.id.action_apply).setOnClickListener(this);
    }

    //FilterParser.FilterParserListener implementation methods
    @Override
    public void filterParserDidReceivedFilter(FilterMaster filterMaster) {

        progressFragment.dismiss();
        progressFragment = null;

        if(this.filterMaster != null){
            this.filterMaster = null;
        }

        this.filterMaster = filterMaster;

        Log.d("filterParserDidRcvdFltr", "category id is " + filterMaster.categoryid);

        for (int i=0;i<filterMaster.filterTypes.size();i++)
        {
            FilterType filterType1 = filterMaster.filterTypes.get(i);

            Log.d("filterParserDidRcvdFltr","filterType1.attributeType  is "+filterType1.attributeType);
            Log.d("filterParserDidRcvdFltr","filterType1.attributeCategoryType  is "+filterType1.attributeCategoryType);

            Log.d("filterParserDidRcvdFltr","Filters  is ");

            for(int j=0;j<filterType1.filters.size();j++){
                Filter filter1 = filterType1.filters.get(j);

                Log.d("filterParserDidRcvdFltr","filter1.filterId  is "+filter1.filterId);
                Log.d("filterParserDidRcvdFltr","filter1.attributeValue  is "+filter1.attributeValue);
                Log.d("filterParserDidRcvdFltr","filter1.attributeDisplayType  is "+filter1.attributeDisplayType);
                Log.d("filterParserDidRcvdFltr","filter1.ischecked  is "+filter1.isChecked);
            }
        }

        if(filterSubcategoryListViewAdapter != null){
            listViewFilterSubcategories.removeAllViews();
            filterSubcategoryListViewAdapter = null;
        }

        filterSubcategoryListViewAdapter    = new FilterListViewAdapter(FILTER_SUBCATEGORY);
        listViewFilterSubcategories.setAdapter(filterSubcategoryListViewAdapter);
        listViewFilterSubcategories.setOnItemClickListener(this);

        if(filterListViewAdapter != null){
            listViewFilters.removeAllViews();
            filterListViewAdapter = null;
        }

        filterListViewAdapter   =   new FilterListViewAdapter(FILTER);
        listViewFilters.setAdapter(filterListViewAdapter);
        listViewFilters.setOnItemClickListener(this);
    }

    @Override
    public void filterParserDidReceivedConnectionError(AsyncLoaderNew.Status status) {

    }

    @Override
    public void filterParserDidReceivedProcessingError(String message) {

    }

    // View.OnClickListener implementation
    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.action_cancel){

            onBackPressed();
        }
        else if(v.getId() == R.id.action_apply){

            //Need to extract all the checked filters and send the ids of filter back to product list activity

            ArrayList<Integer> filterIds = new ArrayList<>();

            for (int i=0;i<filterMaster.filterTypes.size();i++)
            {
                FilterType filterType1 = filterMaster.filterTypes.get(i);

                for(int j=0;j<filterType1.filters.size();j++){
                    Filter filter1 = filterType1.filters.get(j);

                    if(filter1.isChecked){
                        filterIds.add(filter1.filterId);
                    }
                }
            }

            Intent resultIntent = new Intent();
            resultIntent.putIntegerArrayListExtra("filterids",filterIds);
            setResult(1,resultIntent);
            finish();

        }
    }

    //AdapterView.OnItemClickListener implementation
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if(parent.equals(listViewFilterSubcategories)){

            subCategorySelected = position;
            filterSubcategoryListViewAdapter.notifyDataSetChanged();
            filterListViewAdapter.notifyDataSetChanged();
        }
        else if(parent.equals(listViewFilters)){

            filterMaster.filterTypes.get(subCategorySelected).filters.get(position).isChecked = !filterMaster.filterTypes.get(subCategorySelected).filters.get(position).isChecked;
            filterSubcategoryListViewAdapter.notifyDataSetChanged();
            filterListViewAdapter.notifyDataSetChanged();
        }
    }



    public static class FilterViewHolder{
        TextView filterTitleTextView;
        ImageView filterCheckImageView;
    }

    public static class FilterSubcategoryViewHolder{
        TextView filterSubcategoryTitleTextView;

    }

    public class FilterListViewAdapter extends BaseAdapter{

        int listViewType = -1;


        LayoutInflater layoutInflater;

        public FilterListViewAdapter (int type){

            listViewType = type;
            layoutInflater = (LayoutInflater)FilterActivity.this.getSystemService(LAYOUT_INFLATER_SERVICE);
            subCategorySelected =   0;
        }

        @Override
        public int getCount() {
            if(listViewType == FilterActivity.FILTER_SUBCATEGORY)
                return filterMaster.filterTypes.size();
            else if(listViewType == FilterActivity.FILTER)
            {
                return filterMaster.filterTypes.get(subCategorySelected).filters.size();
            }

            return 0;
        }

        @Override
        public Object getItem(int position) {

            if(listViewType == FilterActivity.FILTER_SUBCATEGORY)
                return filterMaster.filterTypes.get(position);
            else if(listViewType == FilterActivity.FILTER)
            {
                return filterMaster.filterTypes.get(subCategorySelected).filters.get(position);
            }

            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(listViewType == FilterActivity.FILTER_SUBCATEGORY){

                if(convertView == null){

                    convertView = layoutInflater.inflate(android.R.layout.simple_list_item_1,null);

                    FilterSubcategoryViewHolder viewHolder = new FilterSubcategoryViewHolder();
                    viewHolder.filterSubcategoryTitleTextView = (TextView) convertView.findViewById(android.R.id.text1);

                    convertView.setTag(viewHolder);
                }

                FilterSubcategoryViewHolder viewHolder = (FilterSubcategoryViewHolder)convertView.getTag();

                viewHolder.filterSubcategoryTitleTextView.setText(filterMaster.filterTypes.get(position).attributeType);

                if(position == subCategorySelected){
                    /*
                    this indicate which cell is selected by the user
                     */
                    convertView.setBackgroundColor(FilterActivity.this.getResources().getColor(R.color.healthGreen));
                }
                else {
                    convertView.setBackgroundColor(FilterActivity.this.getResources().getColor(android.R.color.white));
                }
            }
            else if(listViewType == FilterActivity.FILTER){

                if(convertView == null){

                    convertView = layoutInflater.inflate(R.layout.cell_filter,null);

                    FilterViewHolder viewHolder = new FilterViewHolder();
                    viewHolder.filterTitleTextView = (TextView) convertView.findViewById(R.id.filter_title);
                    viewHolder.filterCheckImageView = (ImageView) convertView.findViewById(R.id.filter_check);

                    convertView.setTag(viewHolder);
                }

                FilterViewHolder viewHolder = (FilterViewHolder)convertView.getTag();

                viewHolder.filterTitleTextView.setText(filterMaster.filterTypes.get(subCategorySelected).filters.get(position).attributeValue);
                if(filterMaster.filterTypes.get(subCategorySelected).filters.get(position).isChecked){
                    viewHolder.filterCheckImageView.setImageBitmap(BitmapFactory.decodeResource(FilterActivity.this.getResources(),R.drawable.check));
                }
                else {
                    viewHolder.filterCheckImageView.setImageBitmap(BitmapFactory.decodeResource(FilterActivity.this.getResources(),R.drawable.uncheck));
                }
            }

            return convertView;
        }
    }
}
