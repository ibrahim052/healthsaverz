package com.healthsaverz.healthmobile.medicine.global;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoader;
import com.healthsaverz.healthmobile.medicine.asyncloader.ConnectionDetector;
import com.healthsaverz.healthmobile.medicine.asyncloader.ParserException;
import com.healthsaverz.healthmobile.medicine.asyncloader.ParserListener;
import com.healthsaverz.healthmobile.medicine.bookaservice.model.Deal;
import com.healthsaverz.healthmobile.medicine.doctor.modal.EPrescription;
import com.healthsaverz.healthmobile.medicine.ehr.model.EmrParcelable;
import com.healthsaverz.healthmobile.medicine.login.modal.ForgotPassword;
import com.healthsaverz.healthmobile.medicine.login.modal.User;
import com.healthsaverz.healthmobile.medicine.medicineDetail.modal.SubstitueRequest;
import com.healthsaverz.healthmobile.medicine.medicineSearch.modal.MedicineRequest;
import com.healthsaverz.healthmobile.medicine.medicineSearch.modal.MedicineSearchResponse;
import com.healthsaverz.healthmobile.medicine.medicineSearch.modal.SubstituteSearchResponse;
import com.healthsaverz.healthmobile.medicine.reminder.modal.MedicineInfo;
import com.healthsaverz.healthmobile.medicine.uploadPrescription.model.Prescription;
import com.healthsaverz.healthmobile.medicine.uploadPrescription.model.UploadPresciptionResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;

public class Parser extends AsyncTask<String, Void, Object> implements AsyncLoader.Listener {

    private final static String TAG = "Parser";

    //public static final String DOMAIN = "http://180.149.247.137:8080";
    private static final String METHOD_lOGIN = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/LoginServices/login/healthsaverzn/";
    private static final String METHOD_ADD_USER = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/UserServices/addUser/";
    private static final String METHOD_VALIDATE_USER = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/UserServices/addUser";
    private static final String METHOD_SEARCH_ITEM = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/ProductServices/getTitle/";
    private static final String METHOD_SEARCH_ITEM_MONGO = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/ProductServices/getTitleFromMongoDB/";
    private static final String METHOD_SEARCH_GENERIC_NAME = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/ProductServices/getBySalt/";
    private static final String METHOD_FORGOT_PASSWORD = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/UserServices/forgetPassword/healthsaverz/";
    private static final String METHOD_ADD_MEDICINE = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/MedicineInfoServices/addMedicineInfo";
    //{ClientId}/{to}/{subject}/{text}/{typeOfemail}
    private static final String METHOD_MEDICINE_DETAIL = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/ProductServices/getSubsitute/";
    public static final String METHOD_UPLOAD_PRESCIPTION = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/PrescriptionServices/addPrescriptionMobile";
    public static final String METHOD_GET_PRESCRIPTION = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/PrescriptionServices/getPrescriptionsOfaMember/";
                                                                //{sessionId}/healthsaverz/{memberId}
    public static final String METHOD_UPLOAD_E_PRESCIPTION = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/PrescriptionServices/addPrescriptionMobileByDoctor";
    public static final String METHOD_GET_ALL_DEALS = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/BookAServiceDealServices/getBookAServiceDeals/";
    public static final String METHOD_GET_DEAL_DETAIL = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/BookAServiceDealServices/getBookAServiceDealsDescription/";

    public static final String SUCCESS = "100";
    public static final String FAILURE = "101";
    public static final String ALREADY_EXIST = "102";
    private String planCategory;




    //Constructor

    private enum ParserType {
        Login_form, Not_Defined, ADD_USER, VALIDATE_USER, SEARCH_PRODUCT, FORGOT_PASSWORD, MEDICINE_DETAIL, SEARCH_GENERIC_NAME,
        UPLOAD_PRESCIPTION, GET_PRESCRIPTION, ADD_MEDICINE, UPLOAD_E_PRESCIPTION, GET_ALL_DEALS, GET_DEAL_DETAIL
    }



    private ParserType parserType = ParserType.Not_Defined;
    private AsyncLoader loader;
    private Context context;
    private AsyncLoader.Status status = AsyncLoader.Status.NOT_DEFINED;
    private ParserListener listener;

    //Login User
    public void getLogin(String user, String pass) {
        parserType = ParserType.Login_form;
        asyncLoaderExecute(METHOD_lOGIN + user + "/" + pass, AsyncLoader.RequestMethod.REST_GET);

    }
    public void addMedicine(MedicineInfo medicineInfo) {

        parserType = ParserType.ADD_MEDICINE;
        String builder = "<medicineInfo>"+
                "<clientID>"+medicineInfo.clientID+"</clientID>"+
                "<sessionID>"+medicineInfo.sessionID+"</sessionID>"+
                "<userId>"+medicineInfo.userID+"</userId>"+
                "<medicineName>"+medicineInfo.medicineName+"</medicineName>"+
                "<dose>"+medicineInfo.dose+"</dose>"+
                "<status>A</status>"+
                "<createdBy></createdBy>"+
                "<extra_1/>"+
                "<extra_2/>"+
                "<extra_3/>"+
                "<extra_4/>"+
                "<extra_5/>"+
                "</medicineInfo>";
        asyncLoaderExecute(METHOD_ADD_MEDICINE, builder, AsyncLoader.RequestMethod.SOAP_POST);
    }
    public void getEmrs(String sessionID, String memberID) {
        parserType = ParserType.GET_PRESCRIPTION;
        asyncLoaderExecute(METHOD_GET_PRESCRIPTION + sessionID + "/healthsaverzm/" + memberID, AsyncLoader.RequestMethod.REST_GET);
    }

    public void forgotPassword(ForgotPassword password) {
        parserType = ParserType.FORGOT_PASSWORD;
        StringBuilder url = new StringBuilder(METHOD_FORGOT_PASSWORD);

        url.append(password.emailID);
        url.append("/");
        url.append(password.subject);
        url.append("/");
        url.append(password.text);
        url.append("/");
        url.append(password.typeOfEmail);

        asyncLoaderExecute(url.toString(), AsyncLoader.RequestMethod.GET);

    }
// Search the item

    public void searchProduct(MedicineRequest medicineRequest) {
        parserType = ParserType.SEARCH_PRODUCT ;
        StringBuilder url = new StringBuilder(METHOD_SEARCH_ITEM_MONGO);

        url.append(medicineRequest.getSessionID());
        url.append("/");
        url.append(medicineRequest.getClientID());
        url.append("/");
        url.append(medicineRequest.getSearchTitle());
        url.append("/");
        url.append(medicineRequest.getStartRow());
        url.append("/");
        url.append(medicineRequest.getEndRow());

        // StringBuilder buffer = new StringBuilder((CharSequence) products);

        asyncLoaderExecute(url.toString(), AsyncLoader.RequestMethod.GET);
    }

//Search by Generic Name
    public void searchProductGenericName(MedicineRequest medicineRequest) {
        parserType = ParserType.SEARCH_GENERIC_NAME ;
        StringBuilder url = new StringBuilder(METHOD_SEARCH_GENERIC_NAME);

        url.append(medicineRequest.getSessionID());
        url.append("/");
        url.append(medicineRequest.getClientID());
        url.append("/");
        url.append(medicineRequest.getSearchTitle());
        url.append("/");
        url.append(medicineRequest.getStartRow());
        url.append("/");
        url.append(medicineRequest.getEndRow());

        // StringBuilder buffer = new StringBuilder((CharSequence) products);

        asyncLoaderExecute(url.toString(), AsyncLoader.RequestMethod.GET);
    }

    //Medicine Details

    public void substituteDetails(SubstitueRequest substitueRequest) {
        parserType = ParserType.MEDICINE_DETAIL;
        StringBuilder url = new StringBuilder(METHOD_MEDICINE_DETAIL);
        url.append(substitueRequest.sessionID);
        url.append("/");
        url.append(substitueRequest.clientID);
        url.append("/");
        url.append(substitueRequest.MedicineName);
        url.append("/");
        url.append(substitueRequest.Strength);
        url.append("/");
        url.append(substitueRequest.StrengthUnit);
        url.append("/");
        url.append(substitueRequest.productPackagingName);
        url.append("/");
        url.append(substitueRequest.packagingSize);
        url.append("/");
        url.append(substitueRequest.packagingUnit);
        url.append("/");
        url.append(substitueRequest.StartRow);
        url.append("/");
        url.append(substitueRequest.EndRow);

        // StringBuilder buffer = new StringBuilder((CharSequence) products);

        asyncLoaderExecute(url.toString(), AsyncLoader.RequestMethod.GET);
    }
    public void addUser(User user) {
        parserType = ParserType.ADD_USER;
        asyncLoaderExecute(METHOD_ADD_USER, user.toAddUserXml(), AsyncLoader.RequestMethod.SOAP_POST);
    }

    public void getAllDeals() {
        String clientID = PreferenceManager.getStringForKey(context, PreferenceManager.CLIENT_ID, "healthsaverz");
        String sessionID = PreferenceManager.getStringForKey(context, PreferenceManager.SESSION_ID, "test");
        parserType = ParserType.GET_ALL_DEALS;
        //asyncLoaderExecute(METHOD_GET_ALL_DEALS+clientID+"/"+sessionID,  AsyncLoader.RequestMethod.GET);
        asyncLoaderExecute("http://192.168.2.126:8080/sHealthSaverz/jaxrs/BookAServiceDealServices/getBookAServiceDeals/healthsaverz/test",  AsyncLoader.RequestMethod.GET);
    }
    public void getDealDetail(int dealID) {
        String clientID = PreferenceManager.getStringForKey(context, PreferenceManager.CLIENT_ID, "healthsaverz");
        String sessionID = PreferenceManager.getStringForKey(context, PreferenceManager.SESSION_ID, "test");
        parserType = ParserType.GET_DEAL_DETAIL;
        //asyncLoaderExecute(METHOD_GET_DEAL_DETAIL+clientID+"/"+sessionID+"/"+dealID,  AsyncLoader.RequestMethod.GET);
        asyncLoaderExecute("http://192.168.2.126:8080/sHealthSaverz/jaxrs/BookAServiceDealServices/getBookAServiceDealsDescription/healthsaverz/test/"+dealID,  AsyncLoader.RequestMethod.GET);
    }


    //VALIDATE_USER Request
    public void validateUsers(User users)
    {
        users.toAddUserXml();
        parserType = ParserType.VALIDATE_USER;
        asyncLoaderExecute(METHOD_VALIDATE_USER, users.toAddUserXml(), AsyncLoader.RequestMethod.SOAP_POST);
    }

    public void getUploadPrescription(Prescription prescription)
    {
        parserType = ParserType.UPLOAD_PRESCIPTION;
        asyncLoaderExecute(METHOD_UPLOAD_PRESCIPTION, prescription.toAddPresciptionXml(), AsyncLoader.RequestMethod.SOAP_POST);
    }

    public void getUploadEPrescription(EPrescription ePrescription)
    {
        parserType = ParserType.UPLOAD_E_PRESCIPTION;
        asyncLoaderExecute(METHOD_UPLOAD_E_PRESCIPTION, ePrescription.toAddEPresciptionXml(), AsyncLoader.RequestMethod.SOAP_POST);
    }


    public Parser(Context context) {
        this.context = context;
    }

    @Override
    public void didReceivedError(AsyncLoader.Status errorCode) {
        if (this.listener != null) {
            this.listener.didReceivedError(errorCode);
        }
    }

    @Override
    public void didReceivedData(byte[] data) {
        final String output = new String(data);
        Log.i(TAG, output);
        if (!this.isCancelled()) {
            if (this.getStatus() == Status.PENDING || this.getStatus() == Status.RUNNING){
                this.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, output);
            }
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (!ConnectionDetector.isConnectingToInternet(context)) {
            if (this.listener != null) {
                this.listener.didReceivedError(AsyncLoader.Status.CONNECTIVITY_ERROR);
            }
            this.cancel(true);
        }
    }

    @Override
    public void didReceivedProgress(double progress) {

    }

    public ParserListener getListener() {
        return listener;
    }

    //Setters
    public void setListener(ParserListener listener) {
        this.listener = listener;
    }



    private void asyncLoaderExecute(String url, String requestMessage, AsyncLoader.RequestMethod requestMethod) {
        loader = new AsyncLoader(requestMethod);
        loader.context = context;
        if (requestMethod == AsyncLoader.RequestMethod.SOAP_POST || requestMethod == AsyncLoader.RequestMethod.PUT) {
            loader.getHeaders().put("Content-Type", "application/xml");
            loader.requestMessage = requestMessage;
        }
        loader.setListener(this);
        loader.executeRequest(url);
    }

    private void asyncLoaderExecute(String url, AsyncLoader.RequestMethod requestMethod) {
        loader = new AsyncLoader(requestMethod);
        loader.context = context;
        loader.setListener(this);
        loader.executeRequest(url);
    }



    @Override
    protected Object doInBackground(String... strings) {
        if(!isCancelled()){
            XmlPullParser parser = null;
            try {
                switch (parserType) {
                    case Not_Defined:
                        status = AsyncLoader.Status.NOT_DEFINED;
                        return null;

                    case SEARCH_PRODUCT:
                        ArrayList<MedicineSearchResponse> jsonArrayList = parseValidateMedicine(strings[0]);
                        status = AsyncLoader.Status.SUCCESS;
                        return jsonArrayList;

                    case SEARCH_GENERIC_NAME:
                        jsonArrayList = parseValidateMedicine(strings[0]);
                        status = AsyncLoader.Status.SUCCESS;
                        return jsonArrayList;

                    case MEDICINE_DETAIL:
                        final ArrayList<SubstituteSearchResponse> jsonArrayListMedicine = parseValidateMedicineSubstitute(strings[0]);
                        status = AsyncLoader.Status.SUCCESS;
                        return jsonArrayListMedicine;

                    case UPLOAD_PRESCIPTION:
                        //UploadPresciptionResponse uploadPresciptionResponse = parseUploadPresciptionResponse(strings[0]);
                        status = AsyncLoader.Status.SUCCESS;
                        return strings[0];
                    case UPLOAD_E_PRESCIPTION:
                        //UploadPresciptionResponse uploadPresciptionResponse = parseUploadPresciptionResponse(strings[0]);
                        status = AsyncLoader.Status.SUCCESS;
                        return strings[0];
                    case GET_PRESCRIPTION:
                        parser = XmlPullParserFactory.newInstance().newPullParser();
                        parser.setInput(new StringReader(strings[0]));
                        final ArrayList<EmrParcelable> emrParcelables = parseGetAllEMRs(parser);
                        status = AsyncLoader.Status.SUCCESS;
                        return emrParcelables;
                    case ADD_MEDICINE:
                        Object forgotString = strings[0];
                        status = AsyncLoader.Status.SUCCESS;
                        return forgotString;
                    case GET_ALL_DEALS:
                        final ArrayList<Deal> jsonArrayListDeals = parseValidateDeals(strings[0]);
                        status = AsyncLoader.Status.SUCCESS;
                        return jsonArrayListDeals;
                    case GET_DEAL_DETAIL:
                        final String description = parseValidateDealDetail(strings[0]);
                        status = AsyncLoader.Status.SUCCESS;
                        return description;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            cancel(true);
        }

        return null;
    }

    private ArrayList<EmrParcelable> parseGetAllEMRs(XmlPullParser parser)
            throws XmlPullParserException, ParserException, IOException {
        if (!isCancelled()) {
            ArrayList<EmrParcelable> emrParcelables = null;
            EmrParcelable emr = null;


            int event = parser.getEventType();
            String elementName = "";
            HashMap<String, String> hashMap = new HashMap<String, String>();
            boolean proceed = false;
            while (event != XmlPullParser.END_DOCUMENT) {
                if (!this.isCancelled()) {
                    switch (event) {
                        case XmlPullParser.START_TAG:
                            elementName = parser.getName();

                            if (elementName.equals(EmrParcelable.PRESCRIPTION_FILES)) {
                                emrParcelables = new ArrayList<EmrParcelable>();
                            } else if (elementName.equals(EmrParcelable.PRESCRIPTION_FILE)) {
                                hashMap = new HashMap<String, String>();

                            } else if (elementName.equals(EmrParcelable.CLIENT_ID) || elementName.equals(EmrParcelable.MESSAGE)
                                    || elementName.equals(EmrParcelable.SESSION_ID) || elementName.equals(EmrParcelable.CREATED_BY)
                                    || elementName.equals(EmrParcelable.CREATED_ON) || elementName.equals(EmrParcelable.MEMBER_ID)
                                    || elementName.equals(EmrParcelable.PRESCRIPTION_ID) || elementName.equals(EmrParcelable.PRESCRIPTION_IMAGE_URL)
                                    || elementName.equals(EmrParcelable.STATUS)) {
                                String value = parser.nextText();
                                if (value != null) {
                                    hashMap.put(elementName, value.trim());
                                } else {
                                    throw new ParserException(
                                            "parseGetAllFriendsForUserIDResponse : null value for tag " +
                                                    elementName);
                                }
                            }

                            break;
                        case XmlPullParser.END_TAG:
                            elementName = parser.getName();
                            if (elementName.equals(EmrParcelable.PRESCRIPTION_FILE)) {
                                emrParcelables.add(new EmrParcelable(hashMap));
                            }
                            break;
                        default:
                            break;
                    }
                    event = parser.next();
                } else {
                    throw new ParserException("parseGetAllFriendsForUserIDResponse cancelled abruptly");
                }
            }
            if (emrParcelables != null) {
                return emrParcelables;
            }

        }
        return null;
    }


    private UploadPresciptionResponse parseUploadPresciptionResponse(String string) {
        if(!isCancelled()){
            if (string != null){
                UploadPresciptionResponse uploadPresciptionResponse = new UploadPresciptionResponse();
                uploadPresciptionResponse.status = string;

                return uploadPresciptionResponse;
            }
            else {
                return null;
            }
        }
        else {
            cancel(true);
            return null;
        }
    }
    private ArrayList<Deal> parseValidateDeals(String deals){
        if (!isCancelled()){
            ArrayList<Deal> dealArrayList = new ArrayList<>();
            if (deals != null){
                try {
                    JSONArray jsonArray = new JSONArray(deals);
                    for (int i = 0; i<jsonArray.length();i++){
                    Deal deal;
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        deal = new Deal();
                        if (!jsonObject.isNull("sessionID")){
                            deal.sessionID = jsonObject.getString("sessionID");
                        }
                        if (!jsonObject.isNull("clientID")){
                            deal.clientID = jsonObject.getString("clientID");
                        }
                        if (!jsonObject.isNull("message")){
                            deal.message = jsonObject.getString("message");
                        }
                        if (!jsonObject.isNull("description")){
                            deal.description = jsonObject.getString("description");
                        }
                        if (!jsonObject.isNull("startDate")){
                            deal.startDate = jsonObject.getString("startDate");
                        }
                        if (!jsonObject.isNull("endDate")){
                            deal.endDate = jsonObject.getString("endDate");
                        }
                        if (!jsonObject.isNull("status")){
                            deal.status = jsonObject.getString("status");
                        }
                        if (!jsonObject.isNull("created_By")){
                            deal.created_By = jsonObject.getInt("created_By");
                        }
                        if (!jsonObject.isNull("bookAserviceDealId")){
                            deal.bookAserviceDealId = jsonObject.getInt("bookAserviceDealId");
                        }
                        if (!jsonObject.isNull("name")){
                            deal.name = jsonObject.getString("name");
                        }
                        if (!jsonObject.isNull("mrp")){
                            deal.mrp = jsonObject.getString("mrp");
                        }
                        if (!jsonObject.isNull("amount")){
                            deal.amount = jsonObject.getString("amount");
                        }
                        if (!jsonObject.isNull("title")){
                            deal.title = jsonObject.getString("title");
                        }
                        dealArrayList.add(deal);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return dealArrayList;
            }
            return null;
        }
        else {
            cancel(true);
            return null;
        }
    }
    private String parseValidateDealDetail(String deals){
        if (!isCancelled()){
            String description = null;
            if (deals != null){
                try {
                    JSONObject jsonObject = new JSONObject(deals);
                    if (!jsonObject.isNull("description")){
                        description = jsonObject.getString("description");
                        return description;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        else {
            cancel(true);
            return null;
        }
    }
    private ArrayList<MedicineSearchResponse> parseValidateMedicine(String medicine) {
        if(!isCancelled()){
            ArrayList<MedicineSearchResponse> jsonArrayListMedicine = new ArrayList<MedicineSearchResponse>();
            MedicineSearchResponse medicineSearchResponse;

            if (medicine != null) {
                try {
                    JSONArray jsonArray = new JSONArray(medicine);


                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                        medicineSearchResponse = new MedicineSearchResponse();
                        if (!jsonObject1.isNull(MedicineSearchResponse.SessionID)){
                            medicineSearchResponse.sessionID = jsonObject1.getString(MedicineSearchResponse.SessionID);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ClientID)){
                            medicineSearchResponse.clientID = jsonObject1.getString(MedicineSearchResponse.ClientID);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Message)){
                            medicineSearchResponse.message = jsonObject1.getString(MedicineSearchResponse.Message);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.MoleculeTypeName)){
                            medicineSearchResponse.moleculeTypeName = jsonObject1.getString(MedicineSearchResponse.MoleculeTypeName);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ProductCategoryName)){
                            medicineSearchResponse.productCategoryName = jsonObject1.getString(MedicineSearchResponse.ProductCategoryName);
                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.WarehouseName)){
                            medicineSearchResponse.warehouseName = jsonObject1.getString(MedicineSearchResponse.WarehouseName);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.GroupName)){
                            medicineSearchResponse.groupName = jsonObject1.getString(MedicineSearchResponse.GroupName);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.SubGroupName)){
                            medicineSearchResponse.subGroupName = jsonObject1.getString(MedicineSearchResponse.SubGroupName);
                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.DiseaseTypeName)){
                            medicineSearchResponse.diseaseTypeName = jsonObject1.getString(MedicineSearchResponse.DiseaseTypeName);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ProductTypeName)){
                            medicineSearchResponse.productTypeName = jsonObject1.getString(MedicineSearchResponse.ProductTypeName);

                        }
                        // handleJson.ProductPackagingId(Integer.parseInt(jsonObject1.getString(HandleJSON.ProductPackagingId)));
                        if (!jsonObject1.isNull(MedicineSearchResponse.Description)){
                            medicineSearchResponse.description = jsonObject1.getString(MedicineSearchResponse.Description);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Icon)){
                            medicineSearchResponse.icon = jsonObject1.getString(MedicineSearchResponse.Icon);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Status)){
                            medicineSearchResponse.status = jsonObject1.getString(MedicineSearchResponse.Status);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ItemType)){
                            medicineSearchResponse.itemType = jsonObject1.getString(MedicineSearchResponse.ItemType);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ManfacuterName)){
                            medicineSearchResponse.manfacuterName = jsonObject1.getString(MedicineSearchResponse.ManfacuterName);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ImageURL)){
                            medicineSearchResponse.imageURL = jsonObject1.getString(MedicineSearchResponse.ImageURL);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Created_By)){
                            medicineSearchResponse.created_By = jsonObject1.getInt(MedicineSearchResponse.Created_By);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.BrandsId)){
                            medicineSearchResponse.brandsId = jsonObject1.getInt(MedicineSearchResponse.BrandsId);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ProductPackagingId)){
                            medicineSearchResponse.productPackagingId = jsonObject1.getInt(MedicineSearchResponse.ProductPackagingId);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ManufacturerTagName)){
                            medicineSearchResponse.manufacturerTagName = jsonObject1.getString(MedicineSearchResponse.ManufacturerTagName);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.MoleculeTypeId)){
                            medicineSearchResponse.moleculeTypeId = jsonObject1.getInt(MedicineSearchResponse.MoleculeTypeId);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ManufacturerTag)){
                            medicineSearchResponse.manufacturerTag = jsonObject1.getInt(MedicineSearchResponse.ManufacturerTag);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ManufacturingDate)){
                            medicineSearchResponse.manufacturingDate = jsonObject1.getString(MedicineSearchResponse.ManufacturingDate);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.PackagingSizeName)){
                            medicineSearchResponse.packagingSizeName = jsonObject1.getString(MedicineSearchResponse.PackagingSizeName);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.StrengthUnitName)){
                            medicineSearchResponse.strengthUnitName = jsonObject1.getString(MedicineSearchResponse.StrengthUnitName);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ProductPackagingName)){
                            medicineSearchResponse.productPackagingName = jsonObject1.getString(MedicineSearchResponse.ProductPackagingName);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.PackagingUnitName)){
                            medicineSearchResponse.packagingUnitName = jsonObject1.getString(MedicineSearchResponse.PackagingUnitName);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ThumbnailImageURL)){
                            medicineSearchResponse.thumbnailImageURL = jsonObject1.getString(MedicineSearchResponse.ThumbnailImageURL);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.MetaDescription)){
                            medicineSearchResponse.metaDescription = jsonObject1.getString(MedicineSearchResponse.MetaDescription);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ProductCategoryId)){
                            medicineSearchResponse.productCategoryId = jsonObject1.getInt(MedicineSearchResponse.ProductCategoryId);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.SellingPricePercentage)){
                            medicineSearchResponse.sellingPricePercentage = jsonObject1.getInt(MedicineSearchResponse.SellingPricePercentage);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.DiscountPricePercentage)){
                            medicineSearchResponse.discountPricePercentage = jsonObject1.getInt(MedicineSearchResponse.DiscountPricePercentage);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Strength)){
                            medicineSearchResponse.strength = jsonObject1.getInt(MedicineSearchResponse.Strength);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.StrengthUnit)){
                            medicineSearchResponse.strengthUnit = jsonObject1.getInt(MedicineSearchResponse.StrengthUnit);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ProductId)){
                            medicineSearchResponse.productId = jsonObject1.getInt(MedicineSearchResponse.ProductId);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.TotalQuantity)){
                            medicineSearchResponse.totalQuantity = jsonObject1.getInt(MedicineSearchResponse.TotalQuantity);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Mrp)){
                            medicineSearchResponse.mrp = jsonObject1.getDouble(MedicineSearchResponse.Mrp);

                        }
                        // handleJson.TotalQuantity(Integer.parseInt(jsonObject1.getString("SessionID")));
                        if (!jsonObject1.isNull(MedicineSearchResponse.NelmtagName)){
                            medicineSearchResponse.nelmtagName = jsonObject1.getString(MedicineSearchResponse.NelmtagName);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.India_Or_MNC)){
                            medicineSearchResponse.india_Or_MNC = jsonObject1.getString(MedicineSearchResponse.India_Or_MNC);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Nelmtag)){
                            medicineSearchResponse.nelmtag = jsonObject1.getInt(MedicineSearchResponse.Nelmtag);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.India_MNC)){
                            medicineSearchResponse.india_MNC = jsonObject1.getInt(MedicineSearchResponse.India_MNC);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ExpiryDate)){
                            medicineSearchResponse.expiryDate = jsonObject1.getString(MedicineSearchResponse.ExpiryDate);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.BatchNumber)){
                            medicineSearchResponse.batchNumber = jsonObject1.getString(MedicineSearchResponse.BatchNumber);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.PackagingSize)){
                            medicineSearchResponse.packagingSize = jsonObject1.getInt(MedicineSearchResponse.PackagingSize);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.PackagingUnit)){
                            medicineSearchResponse.packagingUnit = jsonObject1.getInt(MedicineSearchResponse.PackagingUnit);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ManfactureId)){
                            medicineSearchResponse.manfactureId = jsonObject1.getInt(MedicineSearchResponse.ManfactureId);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.GroupId)){
                            medicineSearchResponse.groupId = jsonObject1.getInt(MedicineSearchResponse.GroupId);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.SubGroupId)){
                            medicineSearchResponse.subGroupId = jsonObject1.getInt(MedicineSearchResponse.SubGroupId);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.DiseaseTypeId)){
                            medicineSearchResponse.diseaseTypeId = jsonObject1.getInt(MedicineSearchResponse.DiseaseTypeId);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.BrandsName)){
                            medicineSearchResponse.brandsName = jsonObject1.getString(MedicineSearchResponse.BrandsName);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.StrengthName)){
                            medicineSearchResponse.strengthName = jsonObject1.getString(MedicineSearchResponse.StrengthName);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Video)){
                            medicineSearchResponse.video = jsonObject1.getString(MedicineSearchResponse.Video);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.MetaKeywords)){
                            medicineSearchResponse.metaKeywords = jsonObject1.getString(MedicineSearchResponse.MetaKeywords);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.BarcodeId)){
                            medicineSearchResponse.barcodeId = jsonObject1.getInt(MedicineSearchResponse.BarcodeId);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ProductTypeId)){
                            medicineSearchResponse.productTypeId = jsonObject1.getInt(MedicineSearchResponse.ProductTypeId);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.WarehouseId)){
                            medicineSearchResponse.warehouseId = jsonObject1.getInt(MedicineSearchResponse.WarehouseId);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Keywords)){
                            medicineSearchResponse.keywords = jsonObject1.getString(MedicineSearchResponse.Keywords);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.SellingPrice)){
                            medicineSearchResponse.sellingPrice = jsonObject1.getDouble(MedicineSearchResponse.SellingPrice);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.DiscountPrice)){
                            medicineSearchResponse.discountPrice = jsonObject1.getInt(MedicineSearchResponse.DiscountPrice);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.SalesTax)){
                            medicineSearchResponse.salesTax = jsonObject1.getInt(MedicineSearchResponse.SalesTax);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.PurchaseTax)){
                            medicineSearchResponse.purchaseTax = jsonObject1.getInt(MedicineSearchResponse.PurchaseTax);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.GenericName)){
                            medicineSearchResponse.genericName = jsonObject1.getString(MedicineSearchResponse.GenericName);
                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Created_On)){
                            medicineSearchResponse.created_On = jsonObject1.getString(MedicineSearchResponse.Created_On);
                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.IconUrl)){
                            medicineSearchResponse.iconUrl = jsonObject1.getString(MedicineSearchResponse.IconUrl);
                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.IconId)){
                            medicineSearchResponse.iconId = jsonObject1.getInt(MedicineSearchResponse.IconId);
                        }
                        jsonArrayListMedicine.add(medicineSearchResponse);

                    }
                    return jsonArrayListMedicine;

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else{
                return null;
            }
        }
        else {
            cancel(true);
        }
        return null;
    }
    private ArrayList<SubstituteSearchResponse> parseValidateMedicineSubstitute(String substitute) {
        if(!isCancelled()){


            ArrayList<SubstituteSearchResponse> jsonArrayListMedicine = new ArrayList<SubstituteSearchResponse>();
            SubstituteSearchResponse medicineSearchResponse;

            if (substitute != null) {
                try {
                    JSONArray jsonArray = new JSONArray(substitute);


                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                        medicineSearchResponse = new SubstituteSearchResponse();
                        if (!jsonObject1.isNull(MedicineSearchResponse.SessionID)){
                            medicineSearchResponse.setSessionID(jsonObject1.getString(MedicineSearchResponse.SessionID));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ClientID)){
                            medicineSearchResponse.setClientID(jsonObject1.getString(MedicineSearchResponse.ClientID));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Message)){
                            medicineSearchResponse.setMessage(jsonObject1.getString(MedicineSearchResponse.Message));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.MoleculeTypeName)){
                            medicineSearchResponse.setMoleculeTypeName(jsonObject1.getString(MedicineSearchResponse.MoleculeTypeName));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ProductTypeName)){
                            medicineSearchResponse.setProductTypeName(jsonObject1.getString(MedicineSearchResponse.ProductTypeName));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Description)){
                            medicineSearchResponse.setDescription(jsonObject1.getString(MedicineSearchResponse.Description));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Status)){
                            medicineSearchResponse.setStatus(jsonObject1.getString(MedicineSearchResponse.Status));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Strength)){
                            medicineSearchResponse.setStrength(jsonObject1.getInt(MedicineSearchResponse.Strength));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.StrengthUnit)){
                            medicineSearchResponse.setStrengthUnit(jsonObject1.getInt(MedicineSearchResponse.StrengthUnit));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ProductPackagingId)){
                            medicineSearchResponse.setProductPackagingId(jsonObject1.getInt(MedicineSearchResponse.ProductPackagingId));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ManufacturerTagName)){
                            medicineSearchResponse.setManufacturerTagName(jsonObject1.getString(MedicineSearchResponse.ManufacturerTagName));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.MoleculeTypeId)){
                            medicineSearchResponse.setMoleculeTypeId(jsonObject1.getInt(MedicineSearchResponse.MoleculeTypeId));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ManufacturerTag)){
                            medicineSearchResponse.setManufacturerTag(jsonObject1.getInt(MedicineSearchResponse.ManufacturerTag));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ManufacturingDate)){
                            medicineSearchResponse.setManufacturingDate(jsonObject1.getString(MedicineSearchResponse.ManufacturingDate));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.PackagingSizeName)){
                            medicineSearchResponse.setPackagingSizeName(jsonObject1.getString(MedicineSearchResponse.PackagingSizeName));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ManfacuterName)){
                            medicineSearchResponse.setManfacuterName(jsonObject1.getString(MedicineSearchResponse.ManfacuterName));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.StrengthUnitName)){
                            medicineSearchResponse.setStrengthUnitName(jsonObject1.getString(MedicineSearchResponse.StrengthUnitName));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ProductPackagingName)){
                            medicineSearchResponse.setProductPackagingName(jsonObject1.getString(MedicineSearchResponse.ProductPackagingName));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.PackagingUnitName)){
                            medicineSearchResponse.setPackagingUnitName(jsonObject1.getString(MedicineSearchResponse.PackagingUnitName));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ThumbnailImageURL)){
                            medicineSearchResponse.setThumbnailImageURL(jsonObject1.getString(MedicineSearchResponse.ThumbnailImageURL));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.MetaDescription)){
                            medicineSearchResponse.setMetaDescription(jsonObject1.getString(MedicineSearchResponse.MetaDescription));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ProductCategoryId)){
                            medicineSearchResponse.setProductCategoryId(jsonObject1.getInt(MedicineSearchResponse.ProductCategoryId));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.SellingPricePercentage)){
                            medicineSearchResponse.sellingPricePercentage = jsonObject1.getDouble(MedicineSearchResponse.SellingPricePercentage);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.DiscountPricePercentage)){
                            medicineSearchResponse.discountPricePercentage = jsonObject1.getDouble(MedicineSearchResponse.DiscountPricePercentage);

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.BrandsId)){
                            medicineSearchResponse.setBrandsId(jsonObject1.getInt(MedicineSearchResponse.BrandsId));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.GroupId)){
                            medicineSearchResponse.setGroupId(jsonObject1.getInt(MedicineSearchResponse.GroupId));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Mrp)){
                            medicineSearchResponse.setMrp(jsonObject1.getDouble(MedicineSearchResponse.Mrp));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.NelmtagName)){
                            medicineSearchResponse.setNelmtagName(jsonObject1.getString(MedicineSearchResponse.NelmtagName));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.India_Or_MNC)){
                            medicineSearchResponse.setIndia_Or_MNC(jsonObject1.getString(MedicineSearchResponse.India_Or_MNC));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Nelmtag)){
                            medicineSearchResponse.setNelmtag(jsonObject1.getInt(MedicineSearchResponse.Nelmtag));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.India_MNC)){
                            medicineSearchResponse.setIndia_MNC(jsonObject1.getInt(MedicineSearchResponse.India_MNC));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ExpiryDate)){
                            medicineSearchResponse.setExpiryDate(jsonObject1.getString(MedicineSearchResponse.ExpiryDate));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.BatchNumber)){
                            medicineSearchResponse.setBatchNumber(jsonObject1.getString(MedicineSearchResponse.BatchNumber));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.PackagingSize)){
                            medicineSearchResponse.setPackagingSize(jsonObject1.getInt(MedicineSearchResponse.PackagingSize));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.PackagingUnit)){
                            medicineSearchResponse.setPackagingUnit(jsonObject1.getInt(MedicineSearchResponse.PackagingUnit));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Icon)){
                            medicineSearchResponse.setIcon(jsonObject1.getString(MedicineSearchResponse.Icon));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ManfactureId)){
                            medicineSearchResponse.setManfactureId(jsonObject1.getInt(MedicineSearchResponse.ManfactureId));

                        }

                        if (!jsonObject1.isNull(MedicineSearchResponse.SubGroupId)){
                            medicineSearchResponse.setSubGroupId(jsonObject1.getInt(MedicineSearchResponse.SubGroupId));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.DiseaseTypeId)){
                            medicineSearchResponse.setDiseaseTypeId(jsonObject1.getInt(MedicineSearchResponse.DiseaseTypeId));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.BrandsName)){
                            medicineSearchResponse.setBrandsName(jsonObject1.getString(MedicineSearchResponse.BrandsName));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.StrengthName)){
                            medicineSearchResponse.setStrengthName(jsonObject1.getString(MedicineSearchResponse.StrengthName));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Video)){
                            medicineSearchResponse.setVideo(jsonObject1.getString(MedicineSearchResponse.Video));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.MetaKeywords)){
                            medicineSearchResponse.setMetaKeywords(jsonObject1.getString(MedicineSearchResponse.MetaKeywords));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.BarcodeId)){
                            medicineSearchResponse.setBarcodeId(jsonObject1.getInt(MedicineSearchResponse.BarcodeId));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ProductTypeId)){
                            medicineSearchResponse.setProductTypeId(jsonObject1.getInt(MedicineSearchResponse.ProductTypeId));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.WarehouseId)){
                            medicineSearchResponse.setWarehouseId(jsonObject1.getInt(MedicineSearchResponse.WarehouseId));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Keywords)){
                            medicineSearchResponse.setKeywords(jsonObject1.getString(MedicineSearchResponse.Keywords));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.TotalQuantity)){
                            medicineSearchResponse.setTotalQuantity(jsonObject1.getInt(MedicineSearchResponse.TotalQuantity));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.SellingPrice)){
                            medicineSearchResponse.setSellingPrice(jsonObject1.getDouble(MedicineSearchResponse.SellingPrice));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.DiscountPrice)){
                            medicineSearchResponse.setDiscountPrice(jsonObject1.getInt(MedicineSearchResponse.DiscountPrice));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ItemType)){
                            medicineSearchResponse.setItemType(jsonObject1.getString(MedicineSearchResponse.ItemType));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.SalesTax)){
                            medicineSearchResponse.setSalesTax(jsonObject1.getInt(MedicineSearchResponse.SalesTax));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.PurchaseTax)){
                            medicineSearchResponse.setPurchaseTax(jsonObject1.getInt(MedicineSearchResponse.PurchaseTax));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.GenericName)){
                            medicineSearchResponse.setGenericName(jsonObject1.getString(MedicineSearchResponse.GenericName));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Created_On)){
                            medicineSearchResponse.setCreated_On(jsonObject1.getString(MedicineSearchResponse.Created_On));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ImageURL)){
                            medicineSearchResponse.setImageURL(jsonObject1.getString(MedicineSearchResponse.ImageURL));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.Created_By)){
                            medicineSearchResponse.setCreated_By(jsonObject1.getInt(MedicineSearchResponse.Created_By));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.WarehouseName)){
                            medicineSearchResponse.setWarehouseName(jsonObject1.getString(MedicineSearchResponse.WarehouseName));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.GroupName)){
                            medicineSearchResponse.setGroupName(jsonObject1.getString(MedicineSearchResponse.GroupName));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.SubGroupName)){
                            medicineSearchResponse.setSubGroupName(jsonObject1.getString(MedicineSearchResponse.SubGroupName));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.DiseaseTypeName)){
                            medicineSearchResponse.setDiseaseTypeName(jsonObject1.getString(MedicineSearchResponse.DiseaseTypeName));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.ProductId)){
                            medicineSearchResponse.setProductId(jsonObject1.getInt(MedicineSearchResponse.ProductId));

                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.IconUrl)){
                            medicineSearchResponse.iconUrl = jsonObject1.getString(MedicineSearchResponse.IconUrl);
                        }
                        if (!jsonObject1.isNull(MedicineSearchResponse.IconId)){
                            medicineSearchResponse.iconId = jsonObject1.getInt(MedicineSearchResponse.IconId);
                        }

                        jsonArrayListMedicine.add(medicineSearchResponse);

                    }
                    return jsonArrayListMedicine;

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else {
                cancel(true);
            }

        }
        else{
            return null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);
        if (this.listener != null) {
            if (result != null) {
                if (status == AsyncLoader.Status.SUCCESS) {
                    this.listener.didReceivedData(result);
                }
            } else {
                this.listener.didReceivedError(status);
            }
        }
    }

}



