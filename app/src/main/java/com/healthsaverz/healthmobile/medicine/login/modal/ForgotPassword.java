package com.healthsaverz.healthmobile.medicine.login.modal;


/**
 * Created by DG on 10/16/2014.
 */
public class ForgotPassword {

    public static final String SUCCESS ="Please check mail, password has been sent";
    public static final String FAILURE ="Please enter valid username and email address";

    public String clientID = "healthsaverzn";
    public String subject ="ForgotPassword";
    public String text="Password";
    public String typeOfEmail ="Mail";
    public String emailID ;


    public ForgotPassword() {

    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static String getSuccess() {
        return SUCCESS;
    }

}
