package com.healthsaverz.healthmobile.medicine.uploadPrescription.model;

/**
 * Created by DG on 11/6/2014.
 */
public class Prescription {

    public static final String SessionID = "sessionID";
    public static final String ClientID = "healthsaverzm";
    public static final String Message = "message";
    public static final String MemberId = "memberId";
    public static final String FileEncodedValue = "fileEncodedValue";


    public String clientID = "healthsaverzm";
    public String sessionID = "test";

    public String memberId = "";
    public String fileEncodedValue ="";



    public static String getSessionID() {
        return SessionID;
    }

    public static String getClientID() {
        return ClientID;
    }

    public static String getMemberId() {
        return MemberId;
    }

    public static String getFileEncodedValue() {


        return FileEncodedValue;
    }



    public String toAddPresciptionXml() {
        String toAddPresciptionXml = "<prescriptionFile>" +
                "<sessionID>"+sessionID+"</sessionID>" +
                "<clientID>"+clientID+"</clientID>" +
                "<memberId>"+memberId+"</memberId>" +
                "<fileEncodedValue>"+fileEncodedValue+"</fileEncodedValue>" +
                "</prescriptionFile>";


        return toAddPresciptionXml;
    }
}