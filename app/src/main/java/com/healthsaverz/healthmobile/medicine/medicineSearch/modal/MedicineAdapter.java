package com.healthsaverz.healthmobile.medicine.medicineSearch.modal;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;

import java.util.ArrayList;

/**
 * Created by DG on 10/19/2014.
 */
public class MedicineAdapter extends BaseAdapter {
    private ArrayList<MedicineSearchResponse> medicineSearchResponses;
    private Context context;
    LayoutInflater layoutInflater;


    public MedicineAdapter(ArrayList<MedicineSearchResponse> medicineSearchResponses, Context context) {
        this.medicineSearchResponses = medicineSearchResponses;
        this.context = context;
        this.layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return medicineSearchResponses.size();
    }

    @Override
    public Object getItem(int i) {
        return medicineSearchResponses.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View productView, ViewGroup viewGroup) {

        if (productView == null){

            productView = layoutInflater.inflate(R.layout.cellpattern, null);
            ViewHolder holder = new ViewHolder();
            holder.ManfactureName= (TextView)productView.findViewById(R.id.medi_title);
            holder.GenericName = (TextView)productView.findViewById(R.id.descriptionMedic);
            //holder.tabletQuantity = (TextView)productView.findViewById(R.id.tabletofMedic);
            holder.logo = (ImageView) productView.findViewById(R.id.medi_icon);
            productView.setTag(holder);
        }
//brand name, strength name, strenght unit, strngth unit name, packaging size name, packaging unit name, packaging name
        ViewHolder holder = (ViewHolder)productView.getTag();
        MedicineSearchResponse medicineSearchResponse = medicineSearchResponses.get(i);
        holder.ManfactureName.setText(medicineSearchResponse.brandsName + " "
                + ((medicineSearchResponse.strengthName.equals("0"))?"":medicineSearchResponse.strengthName+" ")
                //+ ((medicineSearchResponse.strengthUnit == 0)?"":medicineSearchResponse.strengthUnit+" ")

                + ((medicineSearchResponse.strengthUnitName.equals("0"))?"":medicineSearchResponse.strengthUnitName+" ")
                +" ("
                + ((medicineSearchResponse.packagingSizeName.equals("0"))?"":medicineSearchResponse.packagingSizeName+" ")
                + ((medicineSearchResponse.packagingUnitName.equals("0"))?"":medicineSearchResponse.packagingUnitName+" ")
                + ((medicineSearchResponse.productPackagingName.equals("0"))?"":medicineSearchResponse.productPackagingName)
                + ")");
        holder.GenericName.setText(medicineSearchResponse.manfacuterName);
//        if (medicineSearchResponse.packagingSizeName.equals("0")){
//            holder.tabletQuantity.setVisibility(View.GONE);
//        }
//        else {
//            holder.tabletQuantity.setVisibility(View.VISIBLE);
//            holder.tabletQuantity.setText("("+ medicineSearchResponse.packagingSizeName +" Tablets in a Strip )");
//        }
        switch (medicineSearchResponse.iconId){
            case 0:
                holder.logo.setImageDrawable(null);
                break;
            case 1:
                holder.logo.setImageResource(R.drawable.bottle);
                break;
            case 2:
                holder.logo.setImageResource(R.drawable.eyedropper);
                break;
            case 3:
                holder.logo.setImageResource(R.drawable.injection);
                break;
            case 4:
                holder.logo.setImageResource(R.drawable.tablet);
                break;
            case 5:
                holder.logo.setImageDrawable(null);
                break;
        }

        return productView;
    }

    private class ViewHolder {
        public TextView ManfactureName;
        public TextView GenericName;
        //public TextView tabletQuantity;
        public ImageView logo;

    }
}
