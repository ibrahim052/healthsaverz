package com.healthsaverz.healthmobile.medicine.reminder.controller;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
import com.healthsaverz.healthmobile.medicine.medicine.controller.MedicineDBHandler;
import com.healthsaverz.healthmobile.medicine.medicine.modal.Medicine;
import com.healthsaverz.healthmobile.medicine.medicine.modal.ReminderModal;

public class RefillDaysActivity extends Activity {

    int medicine_id;
    int reminder_id;
    int notificationtype;
    private Button cancelButton;
    private MedicineDBHandler dbHandler;
    private Medicine medicine;
    private ReminderModal reminderModal1;
    private String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec" };
    private int[] mCards = {
            0,
            0,
            R.drawable.badge1,
            R.drawable.badge2,
            R.drawable.badge3,
            R.drawable.capsule,
            R.drawable.badge5,
            R.drawable.badge6,
            R.drawable.badge7,
            R.drawable.badge8,
            R.drawable.badge9,
            R.drawable.badge10,
            R.drawable.badge11,
            R.drawable.badge12,
            R.drawable.badge14,
            R.drawable.badge15,
            R.drawable.badge16,
            R.drawable.badge17,
            R.drawable.badge18,
            R.drawable.badge19,
            R.drawable.badge20,
            R.drawable.badge21,
            R.drawable.badge22,
            R.drawable.badge23,
            R.drawable.badge24,
            R.drawable.badge25,
            R.drawable.badge26,
            R.drawable.badge27,
            R.drawable.badge28,
            0,
            0};

    private int[] mColors = {
            0,
            0,
            Color.parseColor("#3cbaff"),
            Color.parseColor("#2797d0"),
            Color.parseColor("#005c94"),
            Color.parseColor("#d4ff5f"),
            Color.parseColor("#a9e327"),
            Color.parseColor("#27a227"),
            Color.parseColor("#ffff5c"),
            Color.parseColor("#ff9900"),
            Color.parseColor("#ff7d27"),
            Color.parseColor("#efd597"),
            Color.parseColor("#dba747"),
            Color.parseColor("#936827"),
            Color.parseColor("#ff4141"),
            Color.parseColor("#e12727"),
            Color.parseColor("#c02727"),
            Color.parseColor("#ffae8e"),
            Color.parseColor("#ffa2b2"),
            Color.parseColor("#e9967a"),
            Color.parseColor("#fb9388"),
            Color.parseColor("#f3d2ff"),
            Color.parseColor("#dd82ff"),
            Color.parseColor("#c527ff"),
            Color.parseColor("#2d2d2d"),
            Color.parseColor("#7d7d7d"),
            Color.parseColor("#a9a9a9"),
            Color.parseColor("#d4d4d4"),
            Color.parseColor("#ffffff"),
            Color.parseColor("#000000"),
            0,
            0};


    public Bitmap mergeBitmap(int colo1,int colo2)
    {

        Bitmap firstBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.badge3left);
        Bitmap secondBitmap = BitmapFactory.decodeResource(this.getResources(),R.drawable.badge3right);

        Bitmap comboBitmap;

        float width, height;

        width = firstBitmap.getWidth() + secondBitmap.getWidth();
        height = firstBitmap.getHeight();

        comboBitmap = Bitmap.createBitmap((int)(width/2), (int)height, Bitmap.Config.ARGB_4444);

        Paint firstPaint = new Paint();
        firstPaint.setColorFilter(new PorterDuffColorFilter(mColors[colo1], PorterDuff.Mode.MULTIPLY));

        Paint secondPaint = new Paint();
        secondPaint.setColorFilter(new PorterDuffColorFilter(mColors[colo2], PorterDuff.Mode.MULTIPLY));

        Canvas comboImage = new Canvas(comboBitmap);

        comboImage.drawBitmap(firstBitmap, 0f, 0f, firstPaint);
        comboImage.drawBitmap(secondBitmap,0f,0f, secondPaint);

        return comboBitmap;

    }
    private ImageView medImage;
    public ImageView getMedImage() {
        if (medImage == null) {
            medImage = (ImageView) this.findViewById(R.id.medImage);

        }
        return medImage;
    }
    private TextView medicineName;
    public TextView getMedicineName() {
        if (medicineName == null) {
            medicineName = (TextView) this.findViewById(R.id.medicineName);

        }
        return medicineName;
    }
    public Button getCancelButton() {
        if (cancelButton == null) {
            cancelButton = (Button) this.findViewById(R.id.cancelButton);
            cancelButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    //UseCases.saveRegistrationStatus(getActivity(), false);
                    finish();
                    return true;
                }
            });
        }
        return cancelButton;
    }
    private Button callUsButton;
    public Button getCallUsButton(){
        if (callUsButton == null){
            callUsButton = (Button)findViewById(R.id.callUsButton);
        }
        return callUsButton;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refill_days);
        medicine_id = this.getIntent().getIntExtra("medicine_id", -1);
        reminder_id =   this.getIntent().getIntExtra("reminder_id",-1);
        notificationtype = this.getIntent().getIntExtra("notificationtype",-1);

        getCancelButton().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                //UseCases.saveRegistrationStatus(getActivity(), false);
                finish();
                return true;
            }
        });
        getMedicineName();
        getMedImage();
        getCallUsButton().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    ViewHelper.fadeOut(v, null);
                    return true;
                } else if (event.getActionMasked() == MotionEvent.ACTION_UP) {
                    final int viewID = v.getId();
                    ViewHelper.fadeIn(v, null);
                    String uri = "tel:" + AppConstants.call_us.trim() ;
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(uri));
                    startActivity(intent);
                    return true;
                }
                return false;
            }
        });
        if (medicine_id != -1) {
            dbHandler = new MedicineDBHandler(this);
            medicine = dbHandler.findMedicinefromID(medicine_id);
            if (medicine.get_shape() == 5) {

                getMedImage().setColorFilter(null);
                getMedImage().setImageBitmap(mergeBitmap(medicine.get_color1(),medicine.get_color2()));

            }else if(medicine.get_shape() == -1){
                getMedImage().setImageBitmap(BitmapFactory.decodeResource(this.getResources(),mCards[2]));
                getMedImage().setColorFilter(null);
                getMedImage().setColorFilter(mColors[28], PorterDuff.Mode.MULTIPLY);
            }
            else {
                getMedImage().setImageBitmap(BitmapFactory.decodeResource(this.getResources(),mCards[medicine.get_shape()]));
                getMedImage().setColorFilter(null);
                getMedImage().setColorFilter(mColors[medicine.get_color1()], PorterDuff.Mode.MULTIPLY);
            }
            getMedicineName().setText(medicine.get_medName());
        }
        else {
            finish();
            //Toast.makeText(this, "No medication found", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_refill_days, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
