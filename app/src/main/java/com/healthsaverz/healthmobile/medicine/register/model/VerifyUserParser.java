package com.healthsaverz.healthmobile.medicine.register.model;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;

/**
 * Created by priyankranka on 08/11/14.
 */
public class VerifyUserParser extends AsyncTask<String,Void,String> implements AsyncLoaderNew.Listener {


     /*
    Start from verifyUser method
    Takes String as input and out is based on VerifyUserParserListener
     */

    private AsyncLoaderNew asyncLoaderNew;
    private Context context;

    public VerifyUserParser(Context context){

        this.context = context;
    }

    public interface VerifyUserParserListener{

        void verifyUserParserDidUserVerified(String message); //message as 100 for success
        void verifyUserParserDidReceivedConnectionError(AsyncLoaderNew.Status status);// AsyncLoaderNew.Status
        void verifyUserParserDidReceivedProcessingError(String message);//server message for unsuccessful
    }

    private VerifyUserParserListener verifyUserListener;

    public VerifyUserParserListener getVerifyUserListener() {
        return verifyUserListener;
    }

    public void setVerifyUserListener(VerifyUserParserListener verifyUserListener) {
        this.verifyUserListener = verifyUserListener;
    }

    private static final String METHOD_VALIDATE_USER = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/UserServices/verifyUserForMobile/";

    //local method

    public void verifyUser(String code){

        asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.GET);

        String userClientID = PreferenceManager.getStringForKey(context,PreferenceManager.CLIENT_ID,"-1");

        if(userClientID.equals("") || userClientID.equals("-1")){
            userClientID = "healthsaverz";
        }

        asyncLoaderNew.setListener(this);
        asyncLoaderNew.executeRequest(METHOD_VALIDATE_USER+userClientID+"/"+PreferenceManager.getStringForKey(context,PreferenceManager.ID,"-1")+"/"+code);

    }



    //Thread stack
    @Override
    protected String doInBackground(String... params) {

        if(isCancelled()){
            if(params[0].equals("{100}"))
                return "100";
            else
                return params[0];
        }
        else {
            return params[0];
        }

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(this.verifyUserListener != null){

            if(s.equals("100")){
                verifyUserListener.verifyUserParserDidUserVerified(s);
            }
            else
            {
                verifyUserListener.verifyUserParserDidReceivedProcessingError(s);
            }
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();

        if (this.verifyUserListener != null) {
            this.verifyUserListener.verifyUserParserDidReceivedProcessingError("Verification Abrupted");
        }
    }

    //AsyncLoaderNew.Listener method implementation
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {
        if(verifyUserListener != null){
            verifyUserListener.verifyUserParserDidReceivedConnectionError(status);
        }
    }

    @Override
    public void didReceivedData(byte[] data) {

        String responseString = new String(data);
        Log.d("VerifyUserParser", "didReceivedData is " + responseString);
        execute(responseString);
    }

    @Override
    public void didReceivedProgress(Double progress) {

    }
}
