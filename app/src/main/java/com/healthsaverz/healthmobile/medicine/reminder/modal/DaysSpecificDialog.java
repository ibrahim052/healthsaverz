package com.healthsaverz.healthmobile.medicine.reminder.modal;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;

import java.util.ArrayList;

/**
 * Created by Ibrahim on 17-10-2014.
 */
public class DaysSpecificDialog extends DialogFragment{

    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String TAG = "ScheduleDaysDialog";
    public String[] daysOfWeek = {"Sun","Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    public ArrayList<String> selectedDays;
    private Context context;

    private ArrayList<DaysOfWeek> daysOfWeeks = null;

    DaySpecificListener daySpecificListener;
    private String code;
    private ListView daysList;

    public ListView getDaysList() {
        if (daysList == null){
            daysList = (ListView)getView().findViewById(R.id.daysList);
        }
        return daysList;
    }

    public interface DaySpecificListener {
        void onDaySpecificChange(ArrayList<String> quantity);
    }


    private Button setButton;
        public Button getVerifyButton() {
            if (setButton == null) {
                setButton = (Button) getView().findViewById(R.id.setButton);
                setButton.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                            ViewHelper.fadeOut(view, null);
                            return true;
                        } else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                            final int viewID = view.getId();
                            ViewHelper.fadeIn(view, null);

                            for(int i=0;i<daysOfWeeks.size();i++){

                                DaysOfWeek daysOfWeek1 = daysOfWeeks.get(i);
                                if(daysOfWeek1.ischecked)
                                {
                                    selectedDays.add(daysOfWeek[i]);
                                }
                            }

                            if (daySpecificListener != null){
                                daySpecificListener.onDaySpecificChange(selectedDays);
                            }
                            dismiss();
                            return true;


                        }
                        return false;
                    }
                });
            }
            return setButton;
        }

    private Button cancelButton;
    public Button getCancelButton() {
        if (cancelButton == null) {
            cancelButton = (Button) getView().findViewById(R.id.cancelButton);
            cancelButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                        ViewHelper.fadeOut(view, null);
                        return true;
                    } else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                        final int viewID = view.getId();
                        ViewHelper.fadeIn(view, null);

                        if (daySpecificListener != null){
                            daySpecificListener.onDaySpecificChange(null);
                        }
                        dismiss();
                        return true;


                    }
                    return false;
                }
            });
        }
        return cancelButton;
    }

    public static DaysSpecificDialog newInstance(DaySpecificListener timeSetlistener){
        DaysSpecificDialog daysSpecificDialog = new DaysSpecificDialog();
        daysSpecificDialog.daySpecificListener = timeSetlistener;
        return daysSpecificDialog;
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dialog_days_specific, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        daysOfWeeks = new ArrayList<DaysOfWeek>();
        daysOfWeeks.add(new DaysOfWeek(daysOfWeek[0], false));
        daysOfWeeks.add(new DaysOfWeek(daysOfWeek[1], false));
        daysOfWeeks.add(new DaysOfWeek(daysOfWeek[2], false));
        daysOfWeeks.add(new DaysOfWeek(daysOfWeek[3], false));
        daysOfWeeks.add(new DaysOfWeek(daysOfWeek[4], false));
        daysOfWeeks.add(new DaysOfWeek(daysOfWeek[5], false));
        daysOfWeeks.add(new DaysOfWeek(daysOfWeek[6], false));
        selectedDays = new ArrayList<String>();
        getVerifyButton();
        getCancelButton();
        //code = getQuantityText().getText().toString().trim();
        getDaysList().setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return daysOfWeeks.size();
            }

            @Override
            public Object getItem(int i) {
                return daysOfWeeks.get(i);
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(final int i, View convertView, ViewGroup viewGroup) {
                LayoutInflater inflater = LayoutInflater.from(getActivity());

                MyViewHolder mViewHolder;
                if(convertView == null) {
                    convertView = inflater.inflate(R.layout.day_cell, null);
                    mViewHolder = new MyViewHolder();
                    convertView.setTag(mViewHolder);
                } else {
                    mViewHolder = (MyViewHolder) convertView.getTag();
                }
                mViewHolder.daysText = (TextView) convertView.findViewById(R.id.daysText);
                mViewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.checkboxDays);
                DaysOfWeek ofWeek = daysOfWeeks.get(i);
                mViewHolder.daysText.setText(ofWeek.getDay());
                mViewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                        daysOfWeeks.get(i).ischecked = b;

//                        if (b){
//                            selectedDays.add(daysOfWeek[i]);
//
//                        }else {
//                            selectedDays.remove(daysOfWeek[i]);
//                        }
                    }
                });
                return convertView;
            }
        });
//        getDaysList().setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                DaysOfWeek daysOfWeek1 = (DaysOfWeek) adapterView.getItemAtPosition(i);
//                daysOfWeeks.get(i).ischecked = true;
//
//                daysList.deferNotifyDataSetChanged();
//
//
//            }
//        });
    }
    private class MyViewHolder {
        TextView daysText;
        CheckBox checkBox;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_SmileWindow;
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return dialog;
    }



}
