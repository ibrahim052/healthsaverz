package com.healthsaverz.healthmobile.medicine.medicine.modal;


import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.DialogProgressFragment;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.MedFriend;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.SmsParser;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.UpdateMedFriendParser;
import com.healthsaverz.healthmobile.medicine.medicine.controller.MedicineDBHandler;
import com.healthsaverz.healthmobile.medicine.medicine.controller.NotificationActivity;
import com.healthsaverz.healthmobile.medicine.reminder.controller.RefillDaysActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class AlarmReceiver extends BroadcastReceiver implements SmsParser.SmsParserListener, UpdateMedFriendParser.UpdateFriendParserListener {

    private static final String TAG = "AlarmReceiver";

    int medicine_id;
	private Context context;
    private int reminder_id;
    private int notificationtype;
    private MedicineDBHandler dbHandler;
    private ArrayList<MedFriend> medFriends;
    private int start = 0;
    private SmsParser smsParser;
    private DialogProgressFragment progressFragment;
    private ReminderModal reminderModal1;

    public SmsParser getSmsParser(){
        if (smsParser == null){
            smsParser = new SmsParser(context);
            smsParser.setSmsParserListener(this);
        }
        return smsParser;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
//        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
//        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "YOUR TAG");
//        wl.acquire();
        notificationtype =   intent.getIntExtra("notificationtype",-1);
        dbHandler = new MedicineDBHandler(context);
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            reScheduleAllReminders();
        }
        /*
        Here notificationtype  NOTIFICATION_TYPE_MIDNIGHT  says it is the midnight scheduler to recreate all the future reminders
         */
        else if (notificationtype == AppConstants.NOTIFICATION_TYPE_MIDNIGHT){
            reScheduleAllReminders();

        }
        else {

            medicine_id          =    intent.getIntExtra("medicine_id", -1);
            reminder_id    =    intent.getIntExtra("reminder_id",-1);


            reminderModal1 = dbHandler.findSpecificReminder(reminder_id);

            Log.d("onReceive","medicine_id "+medicine_id);
            Log.d("onReceive","reminder_id "+reminder_id);
            Log.d("onReceive","notificationtype "+notificationtype);

            if (notificationtype == 10){
                ///------   miss count code as skip count-------------////////////
                medFriends = dbHandler.getAllMedFriendsForMedID(medicine_id);
                if (medFriends != null && medFriends.size() > start && reminderModal1 != null){
                    for (MedFriend medFriend : medFriends){
                        medFriend._missCount = medFriend._missCount+1;
                    }
                    getSmsParser().getsms(medFriends.get(start),reminderModal1.get_medID(), reminderModal1.get_remindID());
                }
            }
            else {
                if(reminderModal1 != null)
                {
                    startNotification();
                    //we need to add extra reminder ahead only when notification is for dose and RX reminder.
                    if(notificationtype == 1 || notificationtype == 3)
                    {
               /*
            We need to add one day further as reminder if new date is still with the end date of the medication
            1. We need to get the medicine object and get it's end date
            2. We need to get the last object from reminder table having medicine id.
            3. We need to check end date with reminder object time if end date is not arrived we need to add new reminder in
            the following schedule data
             */

                        Medicine medicine = dbHandler.findMedicinefromID(medicine_id);
                        ReminderModal lastReminder = dbHandler.findLastReminderForDate(medicine_id,reminderModal1.get_time(),reminderModal1.get_qty_type());

                        if(lastReminder != null){
                            //need to first get schedule to know after how many days we need to add the reminder.

                            ArrayList<Integer> reminderintervals = new ArrayList<Integer>();

                            if(reminderModal1.get_qty_type() == 0)//indicate dose reminder
                            {
                                Schedule schedule = dbHandler.findScheduleWithMedicinID(medicine_id);
                                if(schedule.get_type() == 1)//Every Day
                                {
                                    reminderintervals.add(1);
                                }
                                else if(schedule.get_type() == 3)//Specific interval of Day
                                {
                                    reminderintervals.add(Integer.parseInt(schedule.get_action()));
                                }
                                else if(schedule.get_type() == 2)//Specific days of the week
                                {
                    /*
                    currently we are keeping it simple. If interval of wednesday comes than we will set next reminder on next wednesday
                    so simply adding 7 days directly
                     */

                                    reminderintervals.add(7);
                                }
                            }
                            else if(reminderModal1.get_qty_type() == 1)//indicate RX days reminder
                            {
                                reminderintervals.add(Integer.parseInt(reminderModal1.get_quantity()));
                            }

                            String endDate = medicine.get_endDate();
                            Calendar temp = new GregorianCalendar();
                            temp.setTimeInMillis(Long.parseLong(lastReminder.get_time()));
                            AddReminderParser addReminderParser = new AddReminderParser(context);
                            if(!endDate.equals("N/A")){

                                //this means that end date is set thus we need to make sure we are within the end date range if we
                                //go beyond we will break and stop adding the reminder.

                                Calendar endTemp    =   new GregorianCalendar();
                                endTemp.setTimeInMillis(Long.parseLong(endDate));

                                int resultToStop = endTemp.compareTo(temp);
                                if (resultToStop >= 1) {
                                    temp.add(Calendar.DATE,reminderintervals.get(0));//adding interval to last register reminder
                                    ReminderModal modal = new ReminderModal(medicine_id, lastReminder.get_quantity(),
                                            String.valueOf(temp.getTimeInMillis()),reminderModal1.get_qty_type());
                                    modal._taken = -1;
                                    modal._taken_time = "";

                                    modal.set_remindID(dbHandler.addReminder(modal));
                                    addReminderParser.addNewReminder(modal, reminderModal1.get_time(), medicine.get_medName(), medicine.get_dose()+medicine.get_unit());

                                    Intent intent1 = new Intent(context, AlarmReceiver.class);
                                    //intent1.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                                    intent1.putExtra("medicine_id",modal.get_medID());
                                    intent1.putExtra("reminder_id",modal.get_remindID());
                                    if(modal.get_qty_type() == 0)// dose reminder
                                    {
                                        intent1.putExtra("notificationtype",1);// 1 mean it is for medication reminder.
                                    }
                                    else if(modal.get_qty_type() == 1)// RX reminder based on Days
                                    {
                                        intent1.putExtra("notificationtype",3);// 3 mean it is for RX reminder.
                                    }

                                    PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(),modal.get_remindID(), intent1, PendingIntent.FLAG_UPDATE_CURRENT);
                                    AlarmManager alarmManager = (AlarmManager) context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);

                                    alarmManager.cancel(pendingIntent);
                                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                                        alarmManager.set(AlarmManager.RTC_WAKEUP, Long.parseLong(modal.get_time()),pendingIntent);
                                    } else {
                                        alarmManager.setExact(AlarmManager.RTC_WAKEUP, Long.parseLong(modal.get_time()), pendingIntent);
                                    }

                                /*
                                Adding background Scheduler only for pills reminder
                                 */

                                    if(modal.get_qty_type() == 0)
                                    {
                                        AppConstants.createNotificationScheduler(context.getApplicationContext(), modal, 10);
                                    }
                                }
                            }
                            else
                            {
                                temp.add(Calendar.DATE,reminderintervals.get(0));//adding interval to last register reminder
                                ReminderModal modal = new ReminderModal(medicine_id, lastReminder.get_quantity(),
                                        String.valueOf(temp.getTimeInMillis()),reminderModal1.get_qty_type());
                                modal._taken = -1;
                                modal._taken_time = "";

                                modal.set_remindID(dbHandler.addReminder(modal));

                                addReminderParser.addNewReminder(modal, reminderModal1.get_time(), medicine.get_medName(), medicine.get_dose()+medicine.get_unit());
                                Intent intent1 = new Intent(context, AlarmReceiver.class);
                                //intent1.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                                intent1.putExtra("medicine_id",modal.get_medID());
                                intent1.putExtra("reminder_id",modal.get_remindID());
                                if(modal.get_qty_type() == 0)// dose reminder
                                {
                                    intent1.putExtra("notificationtype",1);// 1 mean it is for medication reminder.
                                }
                                else if(modal.get_qty_type() == 1)// RX reminder based on Days
                                {
                                    intent1.putExtra("notificationtype",3);// 3 mean it is for RX reminder.
                                }

                                PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(),modal.get_remindID(), intent1, PendingIntent.FLAG_UPDATE_CURRENT);
                                AlarmManager alarmManager = (AlarmManager) context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                                alarmManager.cancel(pendingIntent);
                                if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                                    alarmManager.set(AlarmManager.RTC_WAKEUP, Long.parseLong(modal.get_time()),pendingIntent);
                                } else {
                                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, Long.parseLong(modal.get_time()), pendingIntent);
                                }
                            /*
                                Adding background Scheduler only for pills reminder
                                 */

                                if(modal.get_qty_type() == 0)
                                {
                                    AppConstants.createNotificationScheduler(context.getApplicationContext(), modal, 10);
                                }
                            }
                            //ReminderModal lastReminder1 = dbHandler.findLastReminderForDate(medicine_id,reminderModal1.get_time());
                            //Log.d("onReceive","onReceive lastReminder added "+lastReminder1.get_remindID()+" "+lastReminder1.get_medID());
                        }
                    }
                }
                else
                {
                    Log.d("onReceive", "reminder deleted with medicine_id " + medicine_id);
                }
            }
        }
        //wl.release();
    }

    private void reScheduleAllReminders() {

        ArrayList<ReminderModal> reminderModals = dbHandler.findReminders();
        /*
        removing all reminders
         */
        for (ReminderModal reminderModal : reminderModals){
            Intent intent = new Intent(context, AlarmReceiver.class);
            //intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            intent.putExtra("medicine_id",reminderModal.get_medID());
            intent.putExtra("reminder_id",reminderModal.get_remindID());
            if(reminderModal.get_qty_type() == 0)
                intent.putExtra("notificationtype",1);// 1 mean it is for medication reminder.
            else
                intent.putExtra("notificationtype",3);// 3 mean it is for RX reminder.

            PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), reminderModal.get_remindID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pendingIntent);
            pendingIntent.cancel();
                                /*
                                 Need to remove the scheduler as well.
                                 */
            Intent intent4 = new Intent(context.getApplicationContext(), AlarmReceiver.class);
            //intent4.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            intent4.putExtra("medicine_id",reminderModal.get_medID());
            intent4.putExtra("reminder_id",reminderModal.get_remindID());
            intent4.putExtra("notificationtype",10);

            PendingIntent pendingIntent4 = PendingIntent.getBroadcast(context.getApplicationContext(), reminderModal.get_remindID()+ AppConstants.SCHEDULER_OFFSET, intent4, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager4 = (AlarmManager) context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
            alarmManager4.cancel(pendingIntent4);
            pendingIntent4.cancel();
        }
        /*
        creating upcoming reminders
         */
        Calendar calendar = AppConstants.getCurrentCalendar();
        calendar.add(Calendar.MINUTE, -AppConstants.SCHEDULER_TYPE_30);
        int hour1 = calendar.get(Calendar.HOUR_OF_DAY);
        int minute1 = calendar.get(Calendar.MINUTE);
        int date1   =   calendar.get(Calendar.DAY_OF_YEAR);
        int year1 = calendar.get(Calendar.YEAR);

        for (ReminderModal reminderModal : reminderModals){
            if (!reminderModal.get_time().equals("N/A")) {

                Calendar calendar2 = new GregorianCalendar();
                calendar2.setTimeInMillis(Long.parseLong(reminderModal.get_time()));

                int hour2 = calendar2.get(Calendar.HOUR_OF_DAY);
                int minute2 = calendar2.get(Calendar.MINUTE);
                int date2   =   calendar2.get(Calendar.DAY_OF_YEAR);
                int year2 = calendar2.get(Calendar.YEAR);
                Log.d("SetAlram","date2 is "+date2);

                //lets check whether reminder time is beyond the current time or not. Only reminders with beyond time are need to be
                //added. This condition is only for current date and not for future dates.

                boolean addReminder;


                addReminder = (date1 <= date2 || year1 < year2) && (date1 != date2 || ((hour2 * 60) + minute2) > ((hour1 * 60) + minute1));
//                if(date1 <= date2 || year1 < year2){
//                    if (date1 == date2){
//                        if (((hour2*60)+minute2) > ((hour1*60)+minute1)){
//                            addReminder = true;
//                        }
//                        else {
//                            addReminder = false;
//                        }
//                    }
//                    else {
//                        addReminder = true;
//                    }
//
//                }
//                else
//                {
//                    addReminder = false;
//                }

                if (addReminder){

                    /*
                    Adding the pills and RX reminder to the OS whose taken = -1
                     */

                    if (reminderModal._taken == -1){
                        Intent intent = new Intent(context, AlarmReceiver.class);
                        //intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                        intent.putExtra("medicine_id",reminderModal.get_medID());
                        intent.putExtra("reminder_id",reminderModal.get_remindID());
                        if(reminderModal.get_qty_type() == 0)
                        {
                            intent.putExtra("notificationtype",1);// 1 mean it is for pills reminder.
                        }
                        else if(reminderModal.get_qty_type() == 1)
                        {
                            intent.putExtra("notificationtype",3);// 3 mean it is for RX reminder.
                        }
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), reminderModal.get_remindID(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        AlarmManager alarmManager = (AlarmManager) context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);

                        //alarmManager.cancel(pendingIntent);
                        Calendar calendar3 = new GregorianCalendar();
                        calendar3.setTimeInMillis(Long.parseLong(reminderModal.get_time()));

                        if (reminderModal._snooze != -1){
                            calendar3.add(Calendar.MINUTE, reminderModal._snooze);
                        }
                        if (calendar3.after(Calendar.getInstance())){
                            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                                alarmManager.set(AlarmManager.RTC_WAKEUP, calendar3.getTimeInMillis(),pendingIntent);
                            } else {
                                alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar3.getTimeInMillis(),pendingIntent);
                            }
                            //Log.d("SetAlarm","reminder is set on "+stringDate(calendar2)+"with reminder id "+reminderModal.get_remindID()+" med id "+reminderModal.get_medID()+" for type "+reminderModal.get_qty_type());
                        }
                     /*
                    Adding background Scheduler only for pills reminder

                     */
                        if(reminderModal.get_qty_type() == 0)
                        {
                            AppConstants.createNotificationScheduler(context.getApplicationContext(), reminderModal, 10);
                        }
                    }
                }
            }
        }
    }

    void startNotification() {

        Log.d("Start", "notification");
        NotificationManager notificationManager = (NotificationManager)context.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        Intent resultIntent;
        resultIntent = new Intent(context,NotificationActivity.class);
        //resultIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        resultIntent.putExtra("medicine_id", medicine_id);
        resultIntent.putExtra("reminder_id",reminder_id);
        //resultIntent.putExtra("threshold",threshold);
        resultIntent.putExtra("time", AppConstants.getCurrentCalendar().getTimeInMillis());
        resultIntent.putExtra("notificationtype",notificationtype);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        Intent mainIntent = new Intent(context, MainScreenActivity.class);
//
//        mainIntent.putExtra("resultintent", resultIntent);
//        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);


//use the flag FLAG_UPDATE_CURRENT to override any notification already there
        PendingIntent contentIntent;

        NotificationCompat.Builder mBuilder;
        if(notificationtype == 1 || notificationtype == 4){
            contentIntent = PendingIntent.getActivity(context.getApplicationContext(),reminder_id, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder = new NotificationCompat.Builder(context.getApplicationContext())
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentTitle("It’s Medicine Time")
//                            .setStyle(new NotificationCompat.BigTextStyle()
//                                    .bigText("Health Saverz"))
                            .setContentText("It’s Medicine Time")
                            .setTicker("It’s Medicine Time")
                    .setSound(Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.notifi))
                    .setVibrate(new long[] { 100, 500, 100, 500 })
                    .setAutoCancel(true);

            mBuilder.setContentIntent(contentIntent);
            context.getApplicationContext().startActivity(resultIntent);
        }
//        else if(notificationtype == 3 || notificationtype == 5){
//            Intent intentRefill = new Intent(context,RefillActivity.class);
//            intentRefill.putExtra("reminder_id",reminder_id);
//            intentRefill.putExtra("medicine_id",medicine_id);
//            intentRefill.putExtra("time",reminderModal1.get_time());
//            intentRefill.putExtra("notificationtype",notificationtype);
//            mBuilder = new NotificationCompat.Builder(context)
//                    .setSmallIcon(R.drawable.ic_launcher)
//                    .setContentTitle("It’s medicine RX Refill time")
//                    .setStyle(new NotificationCompat.BigTextStyle()
//                            .bigText("Health Saverz"))
//                    .setContentText("It’s medicine RX Refill time")
//                    .setTicker("It’s medicine RX Refill time")
//                    .setSound(Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.notification))
//                    .setVibrate(new long[] { 100, 500, 100, 1000 })
//                    .setAutoCancel(true);
//            //mBuilder.setContentIntent(intentRefill)
//        }
        else{
            /*
             if(notificationtype == 3 || notificationtype == 5)
             */
            Intent intentRemind = new Intent(context,RefillDaysActivity.class);
            intentRemind.putExtra("reminder_id",reminder_id);
            intentRemind.putExtra("medicine_id",medicine_id);
            intentRemind.putExtra("time",reminderModal1.get_time());
            intentRemind.putExtra("notificationtype",notificationtype);
            intentRemind.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            //intentRemind.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            contentIntent = PendingIntent.getActivity(context.getApplicationContext(),reminder_id, intentRemind, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder = new NotificationCompat.Builder(context.getApplicationContext())
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle("It’s Medicine Refill Time")
//                    .setStyle(new NotificationCompat.BigTextStyle()
//                            .bigText("Health Saverz"))
                    .setContentText("It’s Medicine Refill Time")
                    .setTicker("It’s Medicine Refill Time")
                    .setSound(Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.notifi))
                    .setVibrate(new long[]{100, 500, 100, 500})
                    .setAutoCancel(true);
            mBuilder.setContentIntent(contentIntent);
        }
        Notification notification = mBuilder.build();
        //notification.flags = Notification.FLAG_INSISTENT;
        notificationManager.notify(reminder_id, notification);

        //reScheduleAllReminders();

    }

    @Override
    public void smsParserDidReceiveData(String message) {
        if (smsParser != null){
            smsParser.cancel(true);
            smsParser = null;
        }
        if (message != null && message.equals("1701")){
            UpdateMedFriendParser medFriendParser = new UpdateMedFriendParser(context);
            medFriendParser.setMedFriendParserListener(this);
            medFriendParser.updateMedFriend(medFriends.get(start));

        }
        else  {
            start++;
            if (medFriends != null && medFriends.size() > start){
                getSmsParser().getsms(medFriends.get(start),reminderModal1.get_medID(), reminderModal1.get_remindID());
            }
            else {
                if (progressFragment != null){
                    progressFragment.dismiss();
                    progressFragment = null;
                }
                //getAlert("Something went wrong");
            }
        }
    }

    @Override
    public void smsParserDidReceivedConnectionError(AsyncLoaderNew.Status status) {
        start++;
        if (smsParser != null){
            smsParser.cancel(true);
            smsParser = null;
        }
        if (medFriends != null && medFriends.size() > start){
            getSmsParser().getsms(medFriends.get(start),reminderModal1.get_medID(), reminderModal1.get_remindID());
        }
        else {
            if (progressFragment != null){
                progressFragment.dismiss();
                progressFragment = null;
            }
            //getAlert(status.toString());

        }
    }

    @Override
    public void smsParserDidReceivedProcessingError(String message) {
        start++;
        if (smsParser != null){
            smsParser.cancel(true);
            smsParser = null;
        }
        if (medFriends != null && medFriends.size() > start){
            getSmsParser().getsms(medFriends.get(start),reminderModal1.get_medID(), reminderModal1.get_remindID());
        }
        else {
            if (progressFragment != null){
                progressFragment.dismiss();
                progressFragment = null;
            }
            //getAlert(message);
        }
        Log.d("NotifictionActivity", message);
    }

    @Override
    public void updateFriendParserDidReceiveData(Object message) {
        dbHandler.updateMedFriend(medFriends.get(start));
        start++;
        if (medFriends != null && medFriends.size() > start){
            getSmsParser().getsms(medFriends.get(start),reminderModal1.get_medID(), reminderModal1.get_remindID());
        }
        else {
            if (progressFragment != null){
                progressFragment.dismiss();
                progressFragment = null;
            }
            Toast.makeText(context, "We've informed your friends", Toast.LENGTH_SHORT).show();
        }
    }

}
