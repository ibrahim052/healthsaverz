package com.healthsaverz.healthmobile.medicine.api;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;


import com.healthsaverz.healthmobile.medicine.BuildConfig;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by priyankranka on 23/02/14.
 */
public class AsyncLoaderEmr extends AsyncTask<String, Double, byte[]> {

    private static final String TAG = "AsyncLoader";
    private int dataIndex = -1;

    private Context context;

    /*
    this is needed as we are using url to make fileName if we process the url for escape symbols than it will result into mismatch of filename
    and every time file will be downloaded.
     */
    private String actualURL = null;


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public interface Listener {
        public void didReceivedError(Status status);

        public void didReceivedData(byte[] data, int dataIndex);

        public void didReceivedProgress(Double progress);
    }

    public enum RequestMethod {
        GET, POST, REST_GET, REST_POST, SOAP_GET, SOAP_POST;
    }

    public enum State {
        NOT_DOWNLOAD, DOWNLOADING, DOWNLOADED;
    }

    public enum Status {
        IDEAL, SUCCESS, ERROR, HTTP_404, MALFORMED_URL, URISYNTAX_ERROR, UNSUPPORTED_ENCODING;
    }

    private RequestMethod requestMethod;
    private State state;
    private Status loaderStatus;

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Status getLoaderStatus() {
        return loaderStatus;
    }

    public void setLoaderStatus(Status loaderStatus) {
        this.loaderStatus = loaderStatus;
    }

    public Listener getListener() {
        return listener;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    private Listener listener;

    public String requestMessage;
    public String requestActionMethod;

    public AsyncLoaderEmr() {

        this.state = State.NOT_DOWNLOAD;
        this.loaderStatus = Status.IDEAL;

    }

    public void getData(RequestMethod requestMethod, int dataIndex) {
        this.requestMethod = requestMethod;
        this.dataIndex = dataIndex;
    }

    // If % character is not found that means we need to perform escape of
    // the percentage characters
    // so that url is properly formatted. if percentage character is found
    // that means url string already contains
    // escape character so we ignore injecting the percentage character

    public void executeRequest(String requestURL) {

        try {
            actualURL   =   requestURL;
            requestURL = requestURL.trim();
            if (requestURL.indexOf("%", 0) == -1) {
                URL url = new URL(requestURL);
                URI uri = null;

                uri = new URI(url.getProtocol(), url.getUserInfo(),
                        url.getHost(), url.getPort(), url.getPath(),
                        url.getQuery(), url.getRef());

                url = uri.toURL();
                requestURL = String.valueOf(url);
            }
            if (BuildConfig.DEBUG)
                Log.d(TAG, "Input URL -> " + requestURL);

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        state = State.DOWNLOADING;
        loaderStatus = Status.SUCCESS;

        this.execute(requestURL);

    }

    @Override
    protected byte[] doInBackground(String... params) {


        OfflineManager offlineManager = OfflineManager.getInstance();

        //Log.d(TAG,"doInBackground context " + context);
        offlineManager.setContext(context);

        if(offlineManager.isFileExistAtPathFromURL(actualURL))
        {
            byte[] imageData = offlineManager.readFileAtPathFromURL(actualURL);

            return imageData;
        }
        else
        {
            HttpClient httpClient = new DefaultHttpClient();

            if (!this.isCancelled()) {
                try {
                    HttpResponse httpResponse = null;
                    if (this.requestMethod.equals(RequestMethod.GET) || this.requestMethod.equals(RequestMethod.REST_GET)) {
                        HttpGet httpGet = new HttpGet(params[0]);
                        httpResponse = httpClient.execute(httpGet);
                    } else if (this.requestMethod.equals(RequestMethod.POST) || this.requestMethod.equals(RequestMethod.REST_POST)) {
                        HttpPost httpPost = new HttpPost(params[0]);
                        httpResponse = httpClient.execute(httpPost);
                    } else if (this.requestMethod.equals(RequestMethod.SOAP_POST)) {
                        HttpPost httpPost = new HttpPost(params[0]);

                        httpPost.setHeader("SOAPAction", this.requestActionMethod);
                        httpPost.setHeader("Content-Type", "text/xml; charset=utf-8");
                        httpPost.setEntity(new StringEntity(this.requestMessage));

                        httpResponse = httpClient.execute(httpPost);

                    }


                    state = State.DOWNLOADED;
                    loaderStatus = Status.SUCCESS;
                    return EntityUtils.toByteArray(httpResponse.getEntity());
                } catch (IOException e) {
                    e.printStackTrace();
                    state = State.NOT_DOWNLOAD;
                    loaderStatus = Status.ERROR;


                } catch (NullPointerException e) {
                    e.printStackTrace();

                    state = State.NOT_DOWNLOAD;
                    loaderStatus = Status.ERROR;

                }

            }

            return new byte[0];
        }

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(byte[] bytes) {
        super.onPostExecute(bytes);

        if (this.listener != null) {
            if (this.loaderStatus.equals(Status.SUCCESS)) {
                this.listener.didReceivedData(bytes, dataIndex);
            } else {
                this.listener.didReceivedError(this.loaderStatus);
            }
        }

    }

    @Override
    protected void onProgressUpdate(Double... values) {
        super.onProgressUpdate(values);

        if (this.listener != null) {
            this.listener.didReceivedProgress(values[0]);
        }
    }

    //local methods
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
