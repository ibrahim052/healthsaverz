package com.healthsaverz.healthmobile.medicine.doctor.controller;

import android.annotation.TargetApi;
 import android.app.Activity;
 import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
 import android.content.DialogInterface;
 import android.content.Intent;
 import android.database.Cursor;
 import android.graphics.Bitmap;
 import android.graphics.BitmapFactory;
 import android.graphics.BlurMaskFilter;
 import android.graphics.Canvas;
 import android.graphics.Color;
 import android.graphics.EmbossMaskFilter;
 import android.graphics.MaskFilter;
 import android.graphics.Matrix;
 import android.graphics.Paint;
 import android.graphics.Path;
 import android.graphics.Point;
 import android.graphics.PorterDuff;
 import android.graphics.PorterDuffXfermode;
 import android.graphics.drawable.BitmapDrawable;
 import android.graphics.drawable.Drawable;
 import android.media.ExifInterface;
 import android.media.Image;
 import android.net.Uri;
 import android.os.Build;
 import android.os.Environment;
 import android.provider.MediaStore;
 import android.support.v7.app.ActionBarActivity;
 import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
 import android.util.Log;
 import android.view.Menu;
 import android.view.MenuItem;
 import android.view.MotionEvent;
 import android.view.View;
 import android.widget.Button;
 import android.widget.EditText;
 import android.widget.ImageView;
 import android.widget.LinearLayout;
 import android.widget.ListView;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoader;
import com.healthsaverz.healthmobile.medicine.asyncloader.ParserListener;
import com.healthsaverz.healthmobile.medicine.bookaservice.controller.BookAServiceActivity;
 import com.healthsaverz.healthmobile.medicine.doctor.modal.DrawingView;
import com.healthsaverz.healthmobile.medicine.doctor.modal.EPrescription;
import com.healthsaverz.healthmobile.medicine.doctor.view.ColorPickerDialog;
 import com.healthsaverz.healthmobile.medicine.global.App;
 import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.Parser;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;
 import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
 import com.healthsaverz.healthmobile.medicine.help.controller.WalkthroughActivity;
 import com.healthsaverz.healthmobile.medicine.uploadPrescription.model.GridImageAdapter;
 import com.healthsaverz.healthmobile.medicine.uploadPrescription.model.MyGridView;
import com.healthsaverz.healthmobile.medicine.uploadPrescription.model.Prescription;

import java.io.ByteArrayOutputStream;
import java.io.File;
 import java.io.FileOutputStream;
 import java.util.ArrayList;

public class SignaturePaintActivity extends Activity implements ColorPickerDialog.OnColorChangedListener, View.OnTouchListener, ParserListener {

    public  static final String SUCCESS = "100";
     private static final int REQUEST_IMAGE_CAPTURE = 5;
     DrawingView paintView;
     ImageView dummyView;
     MyGridView gridImages;
     private GridImageAdapter adapter;
     private String mLastPhotoPath = "";
     private int imageCount = 1;
     private int totalCount;
     ArrayList<Bitmap> bitmaps;
     private App app;
    String memberid = "7";
    String sessionID = "test";
    String clientID = "healthsaverzm";
     //MyView mv;

     AlertDialog dialog;
     private String doctorName;
     private Bitmap bitmapBottom;
    private ProgressDialog progressDialog;
    private Parser parser;
    private int i;
    private EPrescription prescription;

    public Parser getParser() {
        if (parser == null) {
            parser = new Parser(this);
            parser.setListener(this);
        }
        return parser;
    }

     @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);

         //mv= new MyView(this);
         //mv.setBackgroundResource(R.drawable.afor);//set the back ground if you wish to
         setContentView(R.layout.activity_signature_paint);
         paintView= (DrawingView) findViewById(R.id.paintView);
         gridImages = (MyGridView)findViewById(R.id.gridImages);
         dummyView  = (ImageView)findViewById(R.id.dummyView);
         //        Button clearButton= (Button) findViewById(R.id.clearButton);
         //        Button saveButton= (Button) findViewById(R.id.saveButton);
         //        clearButton.setOnTouchListener(this);
         //        saveButton.setOnTouchListener(this);
         findViewById(R.id.addNew).setOnTouchListener(this);
         findViewById(R.id.addNewCamera).setOnTouchListener(this);
         //setContentView(paintView);
         app = (App) getApplicationContext();
         doctorName = PreferenceManager.getStringForKey(this, PreferenceManager.DOCTOR_SIGNATURE, "");
         if (! doctorName.equals("")){
             final AlertDialog.Builder editalert = new AlertDialog.Builder(SignaturePaintActivity.this);
             editalert.setTitle("You have a saved Signature. Do you Want to use it?");
             editalert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                 public void onClick(DialogInterface dialog, int whichButton) {
                     Drawable drawable = Drawable.createFromPath(doctorName);
                     if (drawable != null) {
                         bitmapBottom = ((BitmapDrawable) drawable).getBitmap();
                         if (android.os.Build.VERSION.SDK_INT >= 16)
                             paintView.setBackground(drawable);
                         else
                             paintView.setBackgroundDrawable(drawable);
                     } else {
                         Toast.makeText(SignaturePaintActivity.this, "No signature found. Please create a new one", Toast.LENGTH_SHORT).show();
                         PreferenceManager.saveStringForKey(SignaturePaintActivity.this, PreferenceManager.DOCTOR_SIGNATURE, "");
                         doctorName = "";
                         paintView.invalidate();
                     }
                 }
             });
             editalert.setNegativeButton("No", new DialogInterface.OnClickListener() {

                 @Override
                 public void onClick(DialogInterface dialog, int which) {
//                     if (android.os.Build.VERSION.SDK_INT >= 16)
//                         paintView.setBackground(null);
//                     else
//                         paintView.setBackgroundDrawable(null);

                     doctorName = "";
                 }
             });
             editalert.show();

         }
         bitmaps = new ArrayList<>();
         mPaint = new Paint();
         mPaint.setAntiAlias(true);
         mPaint.setDither(true);
         mPaint.setColor(Color.BLACK);
         mPaint.setStyle(Paint.Style.STROKE);
         mPaint.setStrokeJoin(Paint.Join.ROUND);
         mPaint.setStrokeCap(Paint.Cap.ROUND);
         mPaint.setStrokeWidth(10);
         mEmboss = new EmbossMaskFilter(new float[] { 1, 1, 1 },
                 0.4f, 6, 3.5f);
         mBlur = new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL);
     }

     private Paint mPaint;
     private MaskFilter mEmboss;
     private MaskFilter  mBlur;

     public void colorChanged(int color) {
         mPaint.setColor(color);
     }

     @Override
     public boolean onTouch(View view, MotionEvent motionEvent) {
         if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
             ViewHelper.fadeOut(view, null);
             return true;
         }
         if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
             ViewHelper.fadeIn(view, null);
             switch (view.getId()) {
//                case R.id.appWork:
//                    return true;
//                case R.id.iconsColors:
//                    return true;
//                case R.id.addingReminder:
//                    return true;
                 case R.id.addNew:
                     Intent galleryIntent = new Intent(Intent.ACTION_PICK);
                     galleryIntent.setType("image/*");
                     galleryIntent.putExtra("return-data", true);
                     startActivityForResult(galleryIntent, 3);
                     return true;
                 case R.id.addNewCamera:
                     //ImagePickerFragment.newInstance().show(getSupportFragmentManager(), ImagePickerFragment.TAG);
                     dispatchTakePictureIntent();
                     return true;
//                case R.id.walkthrough:
//                    startActivity(new Intent(this, WalkthroughActivity.class));
//                    return true;
//
//                case R.id.talkToUs:
//                    String uri = "tel:" + AppConstants.call_us.trim() ;
//                    Intent intent1 = new Intent(Intent.ACTION_DIAL);
//                    intent1.setData(Uri.parse(uri));
//                    startActivity(intent1);
//                    return true;
//                case R.id.bookAService:
//                    startActivity(new Intent(this, BookAServiceActivity.class));
//                    return true;
                 case R.id.saveButton:
//                    AlertDialog.Builder editalert = new AlertDialog.Builder(SignaturePaintActivity.this);
//                    editalert.setTitle("Please Enter the name with which you want to Save");
//                    final EditText input = new EditText(SignaturePaintActivity.this);
//                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
//                            LinearLayout.LayoutParams.MATCH_PARENT,
//                            LinearLayout.LayoutParams.MATCH_PARENT);
//                    input.setLayoutParams(lp);
//                    editalert.setView(input);
//                    editalert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int whichButton) {
//
//                            String name= input.getText().toString();
//                            bitmapBottom = paintView.getDrawingCache();
//
//                            String path = Environment.getExternalStorageDirectory().getAbsolutePath();
//                            File file = new File("/sdcard/"+name+".png");
//                            try
//                            {
//                                if(!file.exists())
//                                {
//                                    file.createNewFile();
//                                }
//                                FileOutputStream ostream = new FileOutputStream(file);
//                                bitmapBottom.compress(Bitmap.CompressFormat.PNG, 10, ostream);
//                                ostream.close();
//                                //paintView.invalidate();
//                            }
//                            catch (Exception e)
//                            {
//                                e.printStackTrace();
//                            }finally
//                            {
//
//                                paintView.setDrawingCacheEnabled(false);
//                            }
//                        }
//                    });
//                    editalert.show();
                     return true;
             }
         }
         return false;
     }

     private static final int COLOR_MENU_ID = Menu.FIRST;
     private static final int EMBOSS_MENU_ID = Menu.FIRST + 1;
     private static final int BLUR_MENU_ID = Menu.FIRST + 2;
     private static final int ERASE_MENU_ID = Menu.FIRST + 3;
     private static final int SRCATOP_MENU_ID = Menu.FIRST + 4;
     private static final int Save = Menu.FIRST + 5;

     @Override
     public boolean onCreateOptionsMenu(Menu menu) {
         super.onCreateOptionsMenu(menu);
         //        menu.add(0, COLOR_MENU_ID, 0, "Color").setShortcut('3', 'c');
         //        menu.add(0, EMBOSS_MENU_ID, 0, "Emboss").setShortcut('4', 's');
         //        menu.add(0, BLUR_MENU_ID, 0, "Blur").setShortcut('5', 'z');
         //        menu.add(0, ERASE_MENU_ID, 0, "Erase").setShortcut('5', 'z');
         //        menu.add(0, SRCATOP_MENU_ID, 0, "SrcATop").setShortcut('5', 'z');
         //        menu.add(0, Save, 0, "Save").setShortcut('5', 'z');
         getMenuInflater().inflate(R.menu.menu_signature_paint, menu);
         return true;
     }

     @Override
     public boolean onPrepareOptionsMenu(Menu menu) {
         super.onPrepareOptionsMenu(menu);
         return true;
     }
     public boolean isExternalStorageWritable() {
         String state = Environment.getExternalStorageState();
         return Environment.MEDIA_MOUNTED.equals(state);
     }

     @TargetApi(Build.VERSION_CODES.KITKAT)
     public Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
         bmp1 = bmp1.copy(Bitmap.Config.ARGB_8888 ,true);
         bmp2 = bmp2.copy(Bitmap.Config.ARGB_8888 ,true);
         int screenWidth;
         int screenHeight;
         //        if (Build.VERSION.SDK_INT >= 11) {
         //            Point size = new Point();
         //            try {
         //
         //                getWindowManager().getDefaultDisplay().getSize(size);
         //                screenWidth = size.x;
         //                screenHeight = size.y;
         //            } catch (NoSuchMethodError e) {
         //                screenHeight = getWindowManager().getDefaultDisplay().getHeight();
         //                screenWidth = getWindowManager().getDefaultDisplay().getWidth();
         //            }
         //
         //        } else {
         //            DisplayMetrics metrics = new DisplayMetrics();
         //            getWindowManager().getDefaultDisplay().getMetrics(metrics);
         //            screenWidth = metrics.widthPixels;
         //            screenHeight = metrics.heightPixels;
         //        }
         screenHeight = bmp1.getHeight()+bmp2.getHeight();
         if (bmp1.getWidth()>bmp2.getWidth())
             screenWidth = bmp1.getWidth();
         else
             screenWidth = bmp2.getWidth();

         Bitmap bmOverlay = Bitmap.createBitmap(screenWidth, screenHeight, Bitmap.Config.ARGB_8888);
         Canvas canvas = new Canvas(bmOverlay);
         Paint paint = new Paint();
         //paint.setColor(Color.GREEN);
         int h = bmp1.getHeight();
         //        bmp2.setHeight(screenHeight/2);
         //        bmp1.setHeight(screenHeight/2);
         //        bmp2.setHeight(screenHeight/2);
         //        bmp1.setHeight(screenHeight/2);
         canvas.drawBitmap(bmp1, 0, 0 ,paint );
         canvas.drawBitmap(bmp2, 0, bmp1.getHeight(), paint);

         return bmOverlay;

     }
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File file;
            if (isExternalStorageWritable()) {
                /*
                if you use Environment.getExternalStorageDirectory() then your folder will remain in the sd card even if you delete the app

                if you use getFilesDir() then your folder and data inside internal memory will delete automatically on deleting the app
                if you use getExternalFilesDir() then your folder and data inside sd card will delete automatically on deleting the app.
                 */
                file=new File(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES),"/HealthSaverz/");
            } else {
                file = new File(getApplicationContext().getFilesDir(),"/HealthSaverz/"); //getDir("Health", Context.MODE_PRIVATE)
            }
            if (!file.exists()) {
                file.mkdirs();
            }
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Uri mImageCaptureUri = Uri.fromFile(new File(file.getAbsolutePath(), "prescription_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            app.mCurrentPhotoPath = mImageCaptureUri.getEncodedPath();
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
     @SuppressWarnings("deprecation")
     @Override
     public boolean onOptionsItemSelected(MenuItem item) {
         mPaint.setXfermode(null);
         mPaint.setAlpha(0xFF);

         switch (item.getItemId()) {
             case COLOR_MENU_ID:
                 new ColorPickerDialog(this, this, mPaint.getColor()).show();
                 return true;
             case EMBOSS_MENU_ID:
                 if (mPaint.getMaskFilter() != mEmboss) {
                     mPaint.setMaskFilter(mEmboss);
                 } else {
                     mPaint.setMaskFilter(null);
                 }
                 return true;
             case BLUR_MENU_ID:
                 if (mPaint.getMaskFilter() != mBlur) {
                     mPaint.setMaskFilter(mBlur);
                 } else {
                     mPaint.setMaskFilter(null);
                 }
                 return true;
             case R.id.clearButton:
                 paintView.reset();
//                 if (android.os.Build.VERSION.SDK_INT >= 16)
//                     paintView.setBackground(null);
//                 else
//                     paintView.setBackgroundDrawable(null);
                 return true;
//             case R.id.cameraButton:
//                 //ImagePickerFragment.newInstance().show(getSupportFragmentManager(), ImagePickerFragment.TAG);
//                 dispatchTakePictureIntent();
//                 return true;
//             case R.id.galleryButton:
//                 Intent galleryIntent = new Intent(Intent.ACTION_PICK);
//                 galleryIntent.setType("image/*");
//                 galleryIntent.putExtra("return-data", true);
//                 startActivityForResult(galleryIntent, 3);
//                 return true;
             case SRCATOP_MENU_ID:
                 mPaint.setXfermode(new PorterDuffXfermode(
                         PorterDuff.Mode.SRC_ATOP));
                 mPaint.setAlpha(0x80);
                 return true;
             case R.id.changeButton:
                 paintView.reset();
//                 if (android.os.Build.VERSION.SDK_INT >= 16)
//                     paintView.setBackground(null);
//                 else
//                     paintView.setBackgroundDrawable(null);
                 doctorName = PreferenceManager.getStringForKey(this, PreferenceManager.DOCTOR_SIGNATURE, "");
                 if (! doctorName.equals("")){
                     Drawable drawable = Drawable.createFromPath(doctorName);
                     if (android.os.Build.VERSION.SDK_INT >= 16)
                         paintView.setBackground(drawable);
                     else
                         paintView.setBackgroundDrawable(drawable);
                 }
                 return true;
             case R.id.action_dummy:
                 if (bitmapBottom==null){
                    saveSignature();
                 }
                 if (bitmaps!= null && bitmaps.size()>0){
                     //bitmapBottom = bitmapBottom.copy(Bitmap.Config.ARGB_8888 ,true);
                     Drawable drawable = Drawable.createFromPath(doctorName);
                     if (drawable != null) {
                         bitmapBottom = ((BitmapDrawable) drawable).getBitmap();
                         Bitmap fin = overlay(bitmaps.get(0), bitmapBottom);
                         dummyView.setVisibility(View.VISIBLE);
                         dummyView.setImageBitmap(fin);
                     }
                 }
                 else
                 Toast.makeText(SignaturePaintActivity.this, "Prescription cannot be empty", Toast.LENGTH_SHORT).show();

                 return true;
             case R.id.saveButton:
                saveSignature();
                 i = bitmaps.size()-1;
                 totalCount = bitmaps.size();
                 if (i>= 0){
                     progressDialog = new ProgressDialog(SignaturePaintActivity.this);
                     progressDialog.setCanceledOnTouchOutside(false);
                     progressDialog.setMessage("uploading " + imageCount + " of " + totalCount);
                     progressDialog.setButton(ProgressDialog.BUTTON_NEUTRAL, "Cancel", new DialogInterface.OnClickListener() {
                         @Override
                         public void onClick(DialogInterface dialogInterface, int i) {
                             progressDialog.dismiss();
                             if (parser != null) {
                                 parser.cancel(true);
                                 parser = null;
                             }
                         }
                     });
                     progressDialog.show();
                     String ic = encodeToBase64(bitmaps.get(i));
                     prescription = new EPrescription();
                     prescription.fileEncodedValue = ic;
                     prescription.memberId = memberid;
                     prescription.sessionID = sessionID;
                     prescription.clientID = clientID;
                     getParser().getUploadEPrescription(prescription);
                 }
                 return true;
         }
         return super.onOptionsItemSelected(item);
     }
    public void saveSignature(){
        AlertDialog.Builder editalert = new AlertDialog.Builder(SignaturePaintActivity.this);
        editalert.setTitle("Please Enter the name with which you want to Save");
        final EditText input = new EditText(SignaturePaintActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        String[] docs = doctorName.split("/");
        String[] docExtens = docs[docs.length-1].split("\\.");
        input.append(docExtens[0]);
        input.setLayoutParams(lp);
        editalert.setView(input);
        editalert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String name= input.getText().toString();
                if (name.contains("/") || name.contains("\\.")){
                    Toast.makeText(SignaturePaintActivity.this, "Invalid Name", Toast.LENGTH_SHORT).show();
                }
                paintView.setDrawingCacheEnabled(true);
                bitmapBottom = paintView.getDrawingCache();

                String path = Environment.getExternalStorageDirectory().getAbsolutePath();
                File file;
                if (isExternalStorageWritable()) {
                 /*
                  if you use Environment.getExternalStorageDirectory() then your folder will remain in the sd card even if you delete the app

                  if you use getFilesDir() then your folder and data inside internal memory will delete automatically on deleting the app
                  if you use getExternalFilesDir() then your folder and data inside sd card will delete automatically on deleting the app.
                 */
                    file=new File(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES),"/HealthSaverz/");
                } else {
                    file = new File(getApplicationContext().getFilesDir(),"/HealthSaverz/"); //getDir("Health", Context.MODE_PRIVATE)
                }
                if (!file.exists()) {
                    file.mkdirs();
                }
                File mImageCaptureUri = new File(file.getAbsolutePath(),name+".png");
                try
                {
                    if(!mImageCaptureUri.exists())
                    {
                        mImageCaptureUri.createNewFile();
                    }
                    FileOutputStream ostream = new FileOutputStream(mImageCaptureUri);
                    bitmapBottom.compress(Bitmap.CompressFormat.JPEG, 60, ostream);
                    ostream.close();
                    paintView.invalidate();
                    PreferenceManager.saveStringForKey(SignaturePaintActivity.this, PreferenceManager.DOCTOR_SIGNATURE, mImageCaptureUri.getPath());
                    //app.doctorSignatureName = name+".png";
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }finally
                {
                    paintView.setDrawingCacheEnabled(false);
                }

            }
        });

        editalert.show();
    }
    public static String encodeToBase64(Bitmap image)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }
     public void onActivityResult(int requestCode, int resultCode, Intent data) {
         super.onActivityResult(requestCode, resultCode, data);
         if (resultCode == Activity.RESULT_OK) {
             switch (requestCode) {
                 case REQUEST_IMAGE_CAPTURE:
                     if(mLastPhotoPath != null && !mLastPhotoPath.equals("")){
                         File file = new File(mLastPhotoPath);
                         //file.delete();
                     }
                     mLastPhotoPath = app.mCurrentPhotoPath;
                     setPic();
                     break;
                 case 3:
                     Uri selectedImage = data.getData();
                     String[] filePathColumn = {MediaStore.Images.Media.DATA};

                     Cursor cursor = getContentResolver().query(
                             selectedImage, filePathColumn, null, null, null);
                     cursor.moveToFirst();

                     int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                     app.mCurrentPhotoPath = "";
                     app.mCurrentPhotoPath = cursor.getString(columnIndex);
                     cursor.close();
                     mLastPhotoPath = app.mCurrentPhotoPath;
                     setPic();
                     break;
             }
         }
     }
     private void setPic() {
         int targetW = 450;
         int targetH = 450;
         BitmapFactory.Options bmOptions = new BitmapFactory.Options();
         bmOptions.inJustDecodeBounds = true;
         BitmapFactory.decodeFile(app.mCurrentPhotoPath, bmOptions);
         int photoW = bmOptions.outWidth;
         int photoH = bmOptions.outHeight;

         int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

         bmOptions.inJustDecodeBounds = false;
         bmOptions.inSampleSize = scaleFactor;
         bmOptions.inPurgeable = true;


         Bitmap bitmap = BitmapFactory.decodeFile(app.mCurrentPhotoPath, bmOptions);
         ////////////////////////////////////
         int rotate = 0;
         try {
             ExifInterface exif = new ExifInterface(app.mCurrentPhotoPath);
             int orientation = exif.getAttributeInt(
                     ExifInterface.TAG_ORIENTATION,
                     ExifInterface.ORIENTATION_NORMAL);
             switch (orientation) {
                 case ExifInterface.ORIENTATION_ROTATE_270:
                     rotate = 270;
                     break;
                 case ExifInterface.ORIENTATION_ROTATE_180:
                     rotate = 180;
                     break;
                 case ExifInterface.ORIENTATION_ROTATE_90:
                     rotate = 90;
                     break;
             }

             Log.v("", "Exif orientation: " + orientation);
         } catch (Exception e) {
             e.printStackTrace();
         }
         Matrix matrix = new Matrix();
         matrix.postRotate(rotate);
         bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
         bitmaps.add(bitmap);
         adapter = new GridImageAdapter(this, 0, bitmaps, deleteClick);
         gridImages.setAdapter(adapter);
         //        if (bitmaps.size() > 0) {
         //            prescriptionText.setVisibility(View.INVISIBLE);
         //        }
         //        else {
         //            prescriptionText.setVisibility(View.VISIBLE);
         //        }
     }
     private View.OnClickListener deleteClick = new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             final int position = gridImages.getPositionForView(v);
             if (position != ListView.INVALID_POSITION) {
                 adapter.remove(adapter.getItem(position));
                 adapter.notifyDataSetChanged();
                 //                if (adapter.getCount() > 0) {
                 //                    prescriptionText.setVisibility(View.INVISIBLE);
                 //                }
                 //                else {
                 //                    prescriptionText.setVisibility(View.VISIBLE);
                 //                }
             }
         }
     };

    @Override
    public void didReceivedError(AsyncLoader.Status errorCode) {
        progressDialog.dismiss();
        if (parser != null){
            parser.cancel(true);
            parser = null;
        }
    }

    @Override
    public void didReceivedData(Object result) {

        if (result != null && result.equals(SUCCESS)){
            adapter.remove(bitmaps.get(i));
            adapter.notifyDataSetChanged();
            imageCount ++;
            i--;
            if (i>= 0){
                String ic = encodeToBase64(bitmaps.get(i));
                prescription = new EPrescription();
                prescription.fileEncodedValue = ic;
                prescription.sessionID = sessionID;
                prescription.clientID = clientID;
                if (parser != null){
                    parser.cancel(true);
                    parser = null;
                }
                getParser().getUploadEPrescription(prescription);
                if (progressDialog != null){
                    progressDialog.setMessage("uploading "+imageCount+" of "+ totalCount);
                }
            }
            else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setTitle("Success");
                alertDialog.setMessage("Uploaded Successfully!\n" +
                        "You can check your prescription in EHR");

                alertDialog.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                finish();
                            }
                        });
                alertDialog.show();
                progressDialog.dismiss();
                if (parser != null){
                    parser.cancel(true);
                    parser = null;
                }
                totalCount = 0;
                imageCount = 1;
                i = 0;

            }
        }
        else {
            progressDialog.dismiss();
            if (parser != null){
                parser.cancel(true);
                parser = null;
            }
            Toast.makeText(this, "Error in Uploading", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void didReceivedProgress(double progress) {
        progressDialog.dismiss();
        if (parser != null){
            parser.cancel(true);
            parser = null;
        }
    }
}