package com.healthsaverz.healthmobile.medicine.login.modal;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.login.controller.LoginActivity;

/**
 * Created by priyankranka on 08/11/14.
 */
public class ForgotPasswordParser extends AsyncTask <String,Void,Object> implements AsyncLoaderNew.Listener {
    public ForgotPasswordParser(Context context) {
    }
     /*
    Start from loginUser method
    Takes String as input and out is based on VerifyUserParserListener
     */

    public interface MedFriendParserListener{

        void medFriendParserDidReceiveData(Object message); //message as 100 for success
        void medFriendParserDidReceivedConnectionError(AsyncLoaderNew.Status status);// AsyncLoaderNew.Status
        void medFriendParserDidReceivedProcessingError(String message);//server message for unsuccessful
    }

    private MedFriendParserListener medFriendParserListener;

    public MedFriendParserListener getMedFriendParserListener() {
        return medFriendParserListener;
    }

    public void setMedFriendParserListener(MedFriendParserListener medFriendParserListener) {
        this.medFriendParserListener = medFriendParserListener;
    }

    private static final String METHOD_FORGOT_PASSWORD = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/UserServices/forgetPassword/healthsaverz/";

    //local method
    public void forgotPassword(ForgotPassword password) {
        AsyncLoaderNew asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.GET);

        StringBuilder url = new StringBuilder(METHOD_FORGOT_PASSWORD);

        url.append(password.emailID);
        url.append("/");
        url.append(password.subject);
        url.append("/");
        url.append(password.text);
        url.append("/");
        url.append(password.typeOfEmail);

        asyncLoaderNew.executeRequest(url.toString());
        asyncLoaderNew.setListener(this);
        Log.d("requestXML ","requestXML "+url.toString());
    }

    @Override
    protected Object doInBackground(String... params) {
        if(!isCancelled())
        {
            String k = params[0];
            return k;
        }
        else {
            cancel(true);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object s) {
        super.onPostExecute(s);
        if(this.medFriendParserListener != null){
            if(s != null){
                medFriendParserListener.medFriendParserDidReceiveData(s);
            }
            else {
                medFriendParserListener.medFriendParserDidReceivedProcessingError("Error occurred while adding MedFriend");
            }
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();

        if (this.medFriendParserListener != null) {
            this.medFriendParserListener.medFriendParserDidReceivedProcessingError("Process Abrupt");
        }
    }


    //AsyncLoaderNew.Listener method implementation
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {
        if(medFriendParserListener != null){
            medFriendParserListener.medFriendParserDidReceivedConnectionError(status);
        }
    }

    @Override
    public void didReceivedData(byte[] data) {
        String responseString = new String(data);
        Log.d("LoginUserParser", "didReceivedData is " + responseString);
        execute(responseString);
    }

    @Override
    public void didReceivedProgress(Double progress) {

    }
}
