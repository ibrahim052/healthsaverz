package com.healthsaverz.healthmobile.medicine.reminder.modal;

/**
 * Created by Ibrahim on 10-12-2014.
 */
public class MedicineInfo {

    public String clientID = "healthsaverz";
    public String sessionID = "test";
    public String userID = "";
    public String medicineName = "";
    public String dose = "";


    public String medXml = "<medicineInfo>"+
            "<clientID>"+clientID+"</clientID>"+
            "<sessionID>"+sessionID+"</sessionID>"+
            "<userId>"+userID+"</userId>"+
            "<medicineName>"+medicineName+"</medicineName>"+
            "<dose>"+dose+"</dose>"+
            "<status>A</status>"+
            "<createdBy></createdBy>"+
            "<extra_1/>"+
            "<extra_2/>"+
            "<extra_3/>"+
            "<extra_4/>"+
            "<extra_5/>"+
            "</medicineInfo>";

    public String getMedXml() {
        return medXml;
    }
}
