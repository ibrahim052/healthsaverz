package com.healthsaverz.healthmobile.medicine.bookaservice.model;

/**
 * Created by DELL PC on 4/13/2015.
 */
public class Deal  {
    public String clientID = "healthsaverzm";
    public String sessionID = "test";

    public String message = "";
    public String description ="";
    public String startDate ="";
    public String endDate ="";
    public String status ="";
    public int created_By;
    public int bookAserviceDealId;
    public String name ="";
    public String mrp ="";
    public String amount ="";
    public String title ="";
    public boolean isChecked;

    public Deal(String clientID, String sessionID, String message, String description, String startDate, String endDate, String status, int created_By, int bookAserviceDealId, String name, String mrp, String amount, String title) {
        this.clientID = clientID;
        this.sessionID = sessionID;
        this.message = message;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.created_By = created_By;
        this.bookAserviceDealId = bookAserviceDealId;
        this.name = name;
        this.mrp = mrp;
        this.amount = amount;
        this.title = title;
    }

    public Deal() {

    }
}
