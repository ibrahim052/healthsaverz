package com.healthsaverz.healthmobile.medicine.reminder.modal;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;

/**
 * Created by Ibrahim on 18-10-2014.
 */
public class RefillDialog extends DialogFragment implements View.OnTouchListener{

//    public static final String TRUE = "true";
//    public static final String FALSE = "false";
    public static final String TAG = "RefillDialog";
    public static final String DAYS_1 = "d1";
    public static final String DAYS_2 = "d2";
    public static final String PILLS_1 = "l1";
    public static final String PILLS_2 = "l2";


    private Context context;

    RefillListener refillListener;
    private String code;
    private String text;

    public static RefillDialog newInstance(RefillListener refillListener, String type, String text) {
        RefillDialog refillDialog = new RefillDialog();
        refillDialog.refillListener = refillListener;
        refillDialog.code = type;
        refillDialog.text = text;
        return refillDialog;
    }

    public static RefillDialog newInstance(RefillListener timeSetlistener, String type){
        RefillDialog refillDialog = new RefillDialog();
        refillDialog.refillListener = timeSetlistener;
        refillDialog.code = type;
        return refillDialog;
    }
    public interface RefillListener {
        void onRefillChange(String quantity, String type);
    }

    private ImageView minus;
    public ImageView getMinus() {
        if (minus == null) {
            minus = (ImageView) getView().findViewById(R.id.minus);

        }
        return minus;
    }

    private ImageView plus;
    public ImageView getPlus() {
        if (plus == null) {
            plus = (ImageView) getView().findViewById(R.id.plus);

        }
        return plus;
    }

    private TextView headText;

    public TextView getHeadText() {
        if (headText == null) {
            headText = (TextView) getView().findViewById(R.id.headText);

        }
        return headText;
    }

    private EditText quantityText;
    public EditText getQuantityText() {
        if (quantityText == null) {
            quantityText = (EditText) getView().findViewById(R.id.quantityText);
            if (text != null){
                quantityText.setText("");
                quantityText.append(text);
            }
        }
        return quantityText;
    }

    private Button setButton;
    public Button getVerifyButton() {
        if (setButton == null) {
            setButton = (Button) getView().findViewById(R.id.setButton);
            setButton.setOnTouchListener(this);
        }
        return setButton;
    }

    private Button cancelButton;
    public Button getCancelButton() {
        if (cancelButton == null) {
            cancelButton = (Button) getView().findViewById(R.id.cancelButton);
            cancelButton.setOnTouchListener(this);
        }
        return cancelButton;
    }






    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialogue_schedule_days, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (code.equals(DAYS_1)){
            getHeadText().setText("Set number of days for first RX reminder");
        }
        else if (code.equals(DAYS_2)){
            getHeadText().setText("Set number of days for first recurring RX reminder");
        }
        else if (code.equals(PILLS_1)){
            getHeadText().setText("Number of pills I currently have");
        }
        else {
            getHeadText().setText("Default number of pills");
        }
        getVerifyButton();
        getCancelButton();
        getPlus().setOnTouchListener(this);
        getMinus().setOnTouchListener(this);
        getQuantityText().requestFocus();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_SmileWindow;
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return dialog;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(view, null);
            return true;
        } else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
            final int viewID = view.getId();
            ViewHelper.fadeIn(view, null);
            if (quantityText.getText().toString().equals("") || quantityText.getText().toString().equals("0"))
            {
                quantityText.setText("1");
            }
            switch (view.getId()){
                case R.id.plus:
                    int add = Integer.parseInt((quantityText.getText().toString()));
                    add = add + 1;
                    quantityText.setText(String.valueOf(add));
                    return true;
                case R.id.minus:
                    int sub = Integer.parseInt((quantityText.getText().toString()));
                    if (sub > 1){
                        sub = sub - 1;
                        quantityText.setText(String.valueOf(sub));
                    }
                    return true;
                case R.id.setButton:
                    if (refillListener != null){
                        refillListener.onRefillChange(quantityText.getText().toString(), code);
                    }
                    dismiss();
                    return true;
                case R.id.cancelButton:
                    dismiss();
                    return true;

            }
//            if (validate()){
//                final String code = verifyCode.getText().toString().trim();
//                //final String systemCode = PreferenceManager.getStringForKey(getActivity(), PreferenceManager.VERIFICATION_CODE, "");
//                if (!code.equals(systemCode)) {
//                    //Toast.makeText(getActivity(), "Invalid Verification Code", Toast.LENGTH_SHORT).show();
//                    invalidVerificationCode();
//                } else {
//                    PreferenceManager.saveStringForKey(getActivity(), PreferenceManager.IS_VERIFICATION_STATUS, VerifyFragment.TRUE);
//                    //Toast.makeText(getActivity(), "Successfully Registered", Toast.LENGTH_SHORT).show();
//                    callbacks.didPressView(viewID);
//                    dismiss();
//                    return true;
//                }
//            }


        }
        return false;
    }

}

