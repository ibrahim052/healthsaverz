package com.healthsaverz.healthmobile.medicine.medicine.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.calendar.CalenderReminder;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.MapMedFriend;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.MedFriend;
import com.healthsaverz.healthmobile.medicine.medicine.modal.Medicine;
import com.healthsaverz.healthmobile.medicine.medicine.modal.ReminderModal;
import com.healthsaverz.healthmobile.medicine.medicine.modal.Schedule;
import com.healthsaverz.healthmobile.medicine.pieChart.modal.MedicineReportModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Ibrahim on 09-10-2014.
 */
public class MedicineDBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "demoDB.db";

    public static final String TABLE_MEDICINES = "medicines";
    public static final String TABLE_REMINDERS = "reminders";
    public static final String TABLE_MED_FRIEND = "med_friend";
    public static final String TABLE_SCHEDULES = "schedules";
    public static final String TABLE_MED_FRIEND_MAP = "med_friend_map";

    public static final String COLUMN_MEDID = "med_id";
    public static final String COLUMN_MED_FRIEND_ID = "med_friend_id";
    ////////////////----------------MEDICINES---------------////////////
    public static final String COLUMN_MEDNAME = "medName";
    public static final String COLUMN_PATIENT_NAME = "patient_name";
    public static final String COLUMN_SHAPE = "shape";
    public static final String COLUMN_COLOR1 = "color1";
    public static final String COLUMN_COLOR2 = "color2";
    public static final String COLUMN_STARTDATE = "startDate";
    public static final String COLUMN_ENDDATE = "endDate";
    public static final String COLUMN_DOSE = "dose";
    public static final String COLUMN_UNIT = "unit";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_PILLS = "pills";
    public static final String COLUMN_PILLS_THRESHOLD = "pills_threshold";
    public static final String COLUMN_UPDATE_DELETE_FLAG = "update_delete_flag";
    public static final String COLUMN_UPDATE_DELETE_DATE = "update_delete_date";
    ////////////////////-------------REMINDERS---------///////////////
    public static final String COLUMN_REMINDID = "remind_id";
    public static final String COLUMN_QUANTITY = "quantity";
    public static final String COLUMN_TIME = "time";
    public static final String COLUMN_QTY_TYPE = "qty_type";
     public static final String COLUMN_SNOOZE = "snooze";

    /////////////---------------MED_FRIEND-----------/////////////////
//    public static final String COLUMN_MED_FRIEND_ID = "med_friend_id";
    public static final String COLUMN_FRIEND_LOCAL_ID = "local_id";
    private static final String COLUMN_USERID = "user_id";
    public static final String COLUMN_FRIEND_NAME = "name";
    public static final String COLUMN_FRIEND_EMAIL = "email_id";
    public static final String COLUMN_FRIEND_PHONE = "phone_number";
    public static final String COLUMN_FRIEND_UPDATED_DATE = "updated_date";
    public static final String COLUMN_MED_MISS_COUNT = "med_miss_count";
    public static final String COLUMN_MED_SKIP_COUNT = "med_skip_count";
    public static final String COLUMN_EXTRA_1 = "extra_1";
    public static final String COLUMN_EXTRA_2 = "extra_2";
    public static final String COLUMN_EXTRA_3 = "extra_3";
    public static final String COLUMN_EXTRA_4 = "extra_4";
    public static final String COLUMN_EXTRA_5 = "extra_5";

/////////////////---------------SCHEDULE----------/////////////////////////
    public static final String COLUMN_SCHEDID = "sched_id";
    public static final String COLUMN_ACTION = "action";
    public static final String COLUMN_TYPE = "type";
    private static final String COLUMN_TEMPENDDATE = "temp_end_date";
    private static final String COLUMN_TAKEN = "taken";
    private static final String COLUMN_TAKEN_TIME = "taken_time";
    ///////////////--------------MAPP_MED_FRIEND-----------------////////////////
    public static final String COLUMN_MAP_ID = "map_id";
//    public static final String COLUMN_MEDID = "med_id";
//    public static final String COLUMN_MED_FRIEND_ID = "med_friend_id";

    String CREATE_MEDICINES_TABLE = "CREATE TABLE " +
            TABLE_MEDICINES + "("
            + COLUMN_MEDID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_MEDNAME + " TEXT,"
            + COLUMN_PATIENT_NAME + " TEXT,"
            + COLUMN_SHAPE + " INTEGER,"
            + COLUMN_COLOR1 + " INTEGER,"
            + COLUMN_COLOR2 + " INTEGER,"
            + COLUMN_STARTDATE + " TEXT,"
            + COLUMN_ENDDATE + " TEXT,"
            + COLUMN_DOSE + " INTEGER,"
            + COLUMN_UNIT + " TEXT,"
            + COLUMN_STATUS + " INTEGER,"
            + COLUMN_DESCRIPTION + " TEXT,"
            + COLUMN_PILLS + " REAL,"
            + COLUMN_PILLS_THRESHOLD + " REAL,"
            + COLUMN_TEMPENDDATE + " TEXT,"
            + COLUMN_UPDATE_DELETE_FLAG + " TEXT,"
            + COLUMN_UPDATE_DELETE_DATE + " TEXT,"
            + COLUMN_EXTRA_1 + " TEXT,"
            + COLUMN_EXTRA_2 + " TEXT,"
            + COLUMN_EXTRA_3 + " TEXT,"
            + COLUMN_EXTRA_4 + " TEXT,"
            + COLUMN_EXTRA_5 + " TEXT" + ")";

    String CREATE_REMINDER_TABLE = "CREATE TABLE " +
            TABLE_REMINDERS + "("
            + COLUMN_REMINDID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_MEDID + " INTEGER,"
            + COLUMN_QUANTITY + " TEXT,"
            + COLUMN_TIME + " TEXT,"
            + COLUMN_QTY_TYPE + " INTEGER,"
            + COLUMN_TAKEN + " INTEGER,"
            + COLUMN_TAKEN_TIME + " TEXT,"
            + COLUMN_SNOOZE+ " INTEGER,"
            + COLUMN_EXTRA_1 + " TEXT,"
            + COLUMN_EXTRA_2 + " TEXT,"
            + COLUMN_EXTRA_3 + " TEXT,"
            + COLUMN_EXTRA_4 + " TEXT,"
            + COLUMN_EXTRA_5 + " TEXT"+ ")";

    String CREATE_MED_FRIEND_TABLE = "CREATE TABLE " +
            TABLE_MED_FRIEND + "("
            + COLUMN_FRIEND_LOCAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_MED_FRIEND_ID + " INTEGER,"
            + COLUMN_USERID + " TEXT,"
            + COLUMN_FRIEND_NAME + " TEXT,"
            + COLUMN_FRIEND_EMAIL + " TEXT,"
            + COLUMN_FRIEND_PHONE + " TEXT,"
            + COLUMN_FRIEND_UPDATED_DATE + " DATETIME,"
            + COLUMN_MED_MISS_COUNT + " INTEGER,"
            + COLUMN_MED_SKIP_COUNT + " INTEGER,"
            + COLUMN_EXTRA_1 + " TEXT,"
            + COLUMN_EXTRA_2 + " TEXT,"
            + COLUMN_EXTRA_3 + " TEXT,"
            + COLUMN_EXTRA_4 + " TEXT,"
            + COLUMN_EXTRA_5 + " TEXT" + ")";

    String CREATE_SCHEDULE_TABLE = "CREATE TABLE " +
            TABLE_SCHEDULES + "("
            + COLUMN_SCHEDID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_MEDID + " INTEGER,"
            + COLUMN_ACTION + " TEXT,"
            + COLUMN_TYPE + " INTEGER,"
            + COLUMN_EXTRA_1 + " TEXT,"
            + COLUMN_EXTRA_2 + " TEXT,"
            + COLUMN_EXTRA_3 + " TEXT,"
            + COLUMN_EXTRA_4 + " TEXT,"
            + COLUMN_EXTRA_5 + " TEXT"+ ")";

    String CREATE_MED_FRIEND_MAP = "CREATE TABLE " +
            TABLE_MED_FRIEND_MAP + "("
            + COLUMN_MAP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_MEDID + " INTEGER,"
            + COLUMN_MED_FRIEND_ID + " INTEGER,"
            + COLUMN_EXTRA_1 + " TEXT,"
            + COLUMN_EXTRA_2 + " TEXT,"
            + COLUMN_EXTRA_3 + " TEXT,"
            + COLUMN_EXTRA_4 + " TEXT,"
            + COLUMN_EXTRA_5 + " TEXT"+ ")";

    public MedicineDBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_MEDICINES_TABLE);
        db.execSQL(CREATE_REMINDER_TABLE);
        db.execSQL(CREATE_MED_FRIEND_TABLE);
        db.execSQL(CREATE_SCHEDULE_TABLE);
        db.execSQL(CREATE_MED_FRIEND_MAP);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEDICINES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REMINDERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MED_FRIEND);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCHEDULES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MED_FRIEND_MAP);
        onCreate(db);
    }

    ////////////--------------MEDICINE ADD, GET, DELETE ---------------////////////
    public long addMedicine(Medicine medicine) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_MEDNAME, medicine.get_medName());
        values.put(COLUMN_PATIENT_NAME, medicine.get_patientName());
        values.put(COLUMN_SHAPE, medicine.get_shape());
        values.put(COLUMN_COLOR1, medicine.get_color1());
        values.put(COLUMN_COLOR2, medicine.get_color2());
        values.put(COLUMN_STARTDATE, medicine.get_startDate());
        values.put(COLUMN_ENDDATE, medicine.get_endDate());
        values.put(COLUMN_DOSE, medicine.get_dose());
        values.put(COLUMN_UNIT, medicine.get_unit());
        values.put(COLUMN_STATUS, medicine.get_status());
        values.put(COLUMN_DESCRIPTION, medicine.get_description());
        values.put(COLUMN_PILLS, medicine.get_pills());
        values.put(COLUMN_PILLS_THRESHOLD, medicine._threshold);
        values.put(COLUMN_UPDATE_DELETE_FLAG, medicine.updateDeleteFlag);
        values.put(COLUMN_UPDATE_DELETE_DATE, medicine.updateDeleteDate);
        values.put(COLUMN_TEMPENDDATE, medicine._tempEndDate);
        values.put(COLUMN_EXTRA_1, medicine.extra_1);
        values.put(COLUMN_EXTRA_2, medicine.extra_2);
        values.put(COLUMN_EXTRA_3, medicine.extra_3);
        values.put(COLUMN_EXTRA_4, medicine.extra_4);
        values.put(COLUMN_EXTRA_5, medicine.extra_5);


        SQLiteDatabase db = this.getWritableDatabase();

        long medid = db.insert(TABLE_MEDICINES, null, values);

        Log.d("DBHANDLER"," medicine id : "+medid);

        db.close();
        Log.e("DBHANDLER", values.toString());

        return medid;
    }

    public void updateMedicine(Medicine medicine) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_MEDNAME, medicine.get_medName());
        values.put(COLUMN_PATIENT_NAME, medicine.get_patientName());
        values.put(COLUMN_SHAPE, medicine.get_shape());
        values.put(COLUMN_COLOR1, medicine.get_color1());
        values.put(COLUMN_COLOR2, medicine.get_color2());
        values.put(COLUMN_STARTDATE, medicine.get_startDate());
        values.put(COLUMN_ENDDATE, medicine.get_endDate());
        values.put(COLUMN_DOSE, medicine.get_dose());
        values.put(COLUMN_UNIT, medicine.get_unit());
        values.put(COLUMN_STATUS, medicine.get_status());
        values.put(COLUMN_DESCRIPTION, medicine.get_description());
        values.put(COLUMN_PILLS, medicine.get_pills());
        values.put(COLUMN_PILLS_THRESHOLD, medicine._threshold);
        values.put(COLUMN_TEMPENDDATE, medicine._tempEndDate);
        values.put(COLUMN_EXTRA_1, medicine.extra_1);
        values.put(COLUMN_EXTRA_2, medicine.extra_2);
        values.put(COLUMN_EXTRA_3, medicine.extra_3);
        values.put(COLUMN_EXTRA_4, medicine.extra_4);
        values.put(COLUMN_EXTRA_5, medicine.extra_5);
        SQLiteDatabase db = this.getWritableDatabase();

        db.update(TABLE_MEDICINES, values, COLUMN_MEDID + "=?", new String[]{String.valueOf(medicine.get_medID())});
        db.close();
    }
    public void updateDeleteMedicineFlag(String flag, int medID) {   //flag = 1 for update, flag = -1 for delete
        ContentValues values = new ContentValues();
        values.put(COLUMN_UPDATE_DELETE_FLAG, flag);
        values.put(COLUMN_UPDATE_DELETE_DATE, AppConstants.getCurrentCalendar().getTimeInMillis());
        SQLiteDatabase db = this.getWritableDatabase();

        db.update(TABLE_MEDICINES, values, COLUMN_MEDID + "=?", new String[]{String.valueOf(medID)});
        db.close();
    }

    public boolean deleteMedicine(int medicineID) {
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = COLUMN_MEDID + "=?";
        String[] whereArgs = new String[] { String.valueOf(medicineID) };
        int result = db.delete(TABLE_MEDICINES, whereClause, whereArgs);
        if(result >= 0)
            return true;
        else
            return false;
    }
    private static final int START_INDEX = 31;

    public ArrayList<CalenderReminder> findMedicinesWithDate(int dayIndex) {
        ArrayList<ReminderModal> reminderModals = findReminders();
        Calendar calendar2 = Calendar.getInstance();
        calendar2.add(Calendar.DAY_OF_YEAR, dayIndex - START_INDEX);
        Date currentTime = calendar2.getTime();
        int currentDay = calendar2.get(Calendar.DATE);
        int currentMonth = calendar2.get(Calendar.MONTH) + 1;
        int currentYear = calendar2.get(Calendar.YEAR);
        int currentDayOfWeek = calendar2.get(Calendar.DAY_OF_WEEK);
        int currentDayOfMonth = calendar2.get(Calendar.DAY_OF_MONTH);
        int CurrentDayOfYear = calendar2.get(Calendar.DAY_OF_YEAR);
        int currentHour = calendar2.get(Calendar.HOUR_OF_DAY);
        int currentMin  = calendar2.get(Calendar.MINUTE);

        ArrayList<CalenderReminder> calenderReminderArrayList = new ArrayList<CalenderReminder>();
        //WHERE Price BETWEEN 10 AND 20

        ArrayList<ReminderModal> list = new ArrayList<ReminderModal>();

        for (ReminderModal modal: reminderModals) {

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(Long.parseLong(modal.get_time()));

            Date currentTime1 = calendar.getTime();
            int currentDay1 = calendar.get(Calendar.DATE);
            int currentMonth1 = calendar.get(Calendar.MONTH) + 1;
            int currentYear1 = calendar.get(Calendar.YEAR);
            int currentDayOfWeek1 = calendar.get(Calendar.DAY_OF_WEEK);
            int currentDayOfMonth1 = calendar.get(Calendar.DAY_OF_MONTH);
            int CurrentDayOfYear1 = calendar.get(Calendar.DAY_OF_YEAR);
            int currentHour1 = calendar.get(Calendar.HOUR_OF_DAY);
            int currentMin1  = calendar.get(Calendar.MINUTE);

            int dayDifference = Math.abs(calendar2.get(Calendar.DAY_OF_YEAR)-calendar.get(Calendar.DAY_OF_YEAR));

            if(dayDifference == 0 && modal.get_qty_type() == 0){

                Medicine medicine = findMedicinefromID(modal.get_medID());

                CalenderReminder calenderReminder = new CalenderReminder();
                calenderReminder.isHeader = false;

                calenderReminder.medicineID = medicine.get_medID();
                calenderReminder.reminderID =   modal.get_remindID();
                calenderReminder.medicineName = medicine.get_medName();
                calenderReminder.medicineDose = medicine.get_dose() + medicine.get_unit();
                calenderReminder.medicineShape = medicine.get_shape();
                calenderReminder.medicineColor1 = medicine.get_color1();
                calenderReminder.medicineColor2 = medicine.get_color2();
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTimeInMillis(Long.parseLong(modal.get_time()));
                calenderReminder.medicineScheduleDose = modal.get_quantity();//calendar1.get(Calendar.YEAR) + "-" + calendar1.get(Calendar.MONTH) + "-" + calendar1.get(Calendar.DATE);
                calenderReminder.medicineTime = calendar1.get(Calendar.HOUR_OF_DAY) + ":" + calendar1.get(Calendar.MINUTE);
                calenderReminder.medicineStatus = modal._taken;

                if(!modal._taken_time.equals(""))
                {
                    calendar1.setTimeInMillis(Long.parseLong(modal._taken_time));
                    calenderReminder.medicineTakenTime  =   calendar1.get(Calendar.HOUR_OF_DAY) + ":" + calendar1.get(Calendar.MINUTE);
                }
                else
                {
                    calenderReminder.medicineTakenTime  =   modal._taken_time;
                }


                calenderReminderArrayList.add(calenderReminder);

//                Log.d("findMedicinesWithDate","ReminderModal reminder id : "+calenderReminder.reminderID);
//                Log.d("findMedicinesWithDate","ReminderModal medid : "+calenderReminder.medicineID);
//                Log.d("findMedicinesWithDate","medicineName : "+calenderReminder.medicineName);
//                Log.d("findMedicinesWithDate","time: "+calenderReminder.medicineTime);
//                Log.d("findMedicinesWithDate","quantity_type : "+calenderReminder. medicineScheduleDose);
//                Log.d("findMedicinesWithDate","taken "+calenderReminder.medicineStatus);
//                Log.d("findMedicinesWithDate","medicine dose "+calenderReminder.medicineDose);
//                Log.d("findMedicinesWithDate","medicine taken time "+calenderReminder.medicineTakenTime);
            }

//            if (calendar.compareTo(calendar2) == 0) {
//
//            }
        }
//        Log.d("findMedicinesWithDate","filtered total count from DB "+calenderReminderArrayList.size());

        return calenderReminderArrayList;
    }

    public Medicine findMedicinefromID(int medID) {

        //String query = "Select * FROM " + TABLE_MEDICINES + " WHERE " + COLUMN_MEDID + " =  \"" + medID + "\"";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_MEDICINES, new String[]{COLUMN_MEDID, COLUMN_MEDNAME, COLUMN_SHAPE,
                        COLUMN_COLOR1, COLUMN_COLOR2, COLUMN_PATIENT_NAME, COLUMN_STARTDATE, COLUMN_ENDDATE, COLUMN_PILLS,
                        COLUMN_DOSE, COLUMN_UNIT, COLUMN_PILLS_THRESHOLD, COLUMN_TEMPENDDATE, COLUMN_UPDATE_DELETE_FLAG, COLUMN_UPDATE_DELETE_DATE},
                COLUMN_MEDID + " =?", new String[]{String.valueOf(medID)}, null, null, null);
        Medicine medicine = new Medicine();
        //Cursor cursor1 = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            medicine.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            medicine.set_medName(cursor.getString(cursor.getColumnIndex(COLUMN_MEDNAME)));
            medicine.set_shape(cursor.getInt(cursor.getColumnIndex(COLUMN_SHAPE)));
            medicine.set_color1(cursor.getInt(cursor.getColumnIndex(COLUMN_COLOR1)));
            medicine.set_color2(cursor.getInt(cursor.getColumnIndex(COLUMN_COLOR2)));
            medicine.set_patientName(cursor.getString(cursor.getColumnIndex(COLUMN_PATIENT_NAME)));
            medicine.set_startDate(cursor.getString(cursor.getColumnIndex(COLUMN_STARTDATE)));
            medicine.set_endDate(cursor.getString(cursor.getColumnIndex(COLUMN_ENDDATE)));
            medicine.set_dose(cursor.getDouble(cursor.getColumnIndex(COLUMN_DOSE)));
            medicine.set_unit(cursor.getString(cursor.getColumnIndex(COLUMN_UNIT)));
            medicine.set_pills(cursor.getFloat(cursor.getColumnIndex(COLUMN_PILLS)));
            medicine.updateDeleteFlag = cursor.getString(cursor.getColumnIndex(COLUMN_UPDATE_DELETE_FLAG));
            medicine.updateDeleteDate = cursor.getString(cursor.getColumnIndex(COLUMN_UPDATE_DELETE_DATE));
            medicine._threshold = cursor.getFloat(cursor.getColumnIndex(COLUMN_PILLS_THRESHOLD));
            medicine._tempEndDate = cursor.getString(cursor.getColumnIndex(COLUMN_TEMPENDDATE));

            cursor.close();
        } else {
            medicine = null;
        }
        db.close();
        return medicine;
    }

    public Medicine findMedicine() {

        String query = "Select MAX(" + COLUMN_MEDID + ") FROM " + TABLE_MEDICINES;  // + " WHERE " + COLUMN_MEDNAME + " =  \"" + medicineName +"\""
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);
        int medicineId;
        Medicine medicine = new Medicine();
        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            medicineId = Integer.parseInt(cursor.getString(0));
            cursor.close();
        } else {
            medicineId = -1;
        }
        if (medicineId != -1) {
            String query2 = "Select * FROM " + TABLE_MEDICINES + " WHERE " + COLUMN_MEDID + " =  \"" + medicineId + "\"";
            Cursor cursor1 = db.rawQuery(query2, null);


            if (cursor1.moveToFirst()) {
                cursor1.moveToFirst();
                medicine.set_medID(Integer.parseInt(cursor1.getString(0)));
                medicine.set_medName(cursor1.getString(1));
                medicine.set_patientName(cursor1.getString(2));
                medicine.set_shape(Integer.parseInt(cursor1.getString(3)));
                medicine.set_color1(Integer.parseInt(cursor1.getString(4)));
                medicine.set_color2(Integer.parseInt(cursor1.getString(5)));
                medicine.set_startDate(cursor1.getString(6));
                medicine.set_endDate(cursor1.getString(7));
                medicine.set_dose(Double.parseDouble(cursor1.getString(8)));
                medicine.set_unit(cursor1.getString(9));
                medicine.set_status(Integer.parseInt(cursor1.getString(10)));
                medicine.set_description(cursor1.getString(11));
                medicine.set_pills(cursor1.getFloat(12));
                medicine._threshold = cursor1.getFloat(13);
                medicine._tempEndDate = cursor1.getString(14);
                cursor1.close();
            } else {
                medicine = null;
            }
        } else {
            medicine = null;
        }

        db.close();
        return medicine;
    }

    public ArrayList<Integer> getallMedicineIDs(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_REMINDERS, new String[]{COLUMN_MEDID},
                null, null, null, null, null);

        //= db.rawQuery(query, null);
        //int reminID;
        ArrayList<Integer> medicineIDs = new ArrayList<Integer>();
        int id = -1;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            id = cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID));
            medicineIDs.add(id);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return medicineIDs;
    }

    ////////////////-------------------REMINDER ADD,GET,DELETE  ---------------//////////////
    public int addReminder(ReminderModal reminderModal) {

        ContentValues values = new ContentValues();

        values.put(COLUMN_MEDID, reminderModal.get_medID());
        values.put(COLUMN_QUANTITY, reminderModal.get_quantity());
        values.put(COLUMN_TIME, reminderModal.get_time());
        values.put(COLUMN_QTY_TYPE, reminderModal.get_qty_type());
        values.put(COLUMN_TAKEN, reminderModal._taken);
        values.put(COLUMN_TAKEN_TIME, reminderModal._taken_time);
        values.put(COLUMN_SNOOZE, reminderModal._snooze);

        SQLiteDatabase db = this.getWritableDatabase();

        int result = (int) db.insert(TABLE_REMINDERS, null, values);
        db.close();
        Log.e("DBHANDLER", values.toString());

        return result;
    }

    public void updateReminder(ReminderModal reminderModal) {

        ContentValues values = new ContentValues();
        values.put(COLUMN_MEDID, reminderModal.get_medID());
        values.put(COLUMN_QUANTITY, reminderModal.get_quantity());
        values.put(COLUMN_TIME, reminderModal.get_time());
        values.put(COLUMN_QTY_TYPE, reminderModal.get_qty_type());
        values.put(COLUMN_TAKEN, reminderModal._taken);
        values.put(COLUMN_TAKEN_TIME, reminderModal._taken_time);
        values.put(COLUMN_SNOOZE, reminderModal._snooze);

        String where = COLUMN_REMINDID + "=" + reminderModal.get_remindID();

        SQLiteDatabase db = this.getWritableDatabase();

        db.update(TABLE_REMINDERS, values, where, null);
        db.close();
        Log.e("DBHANDLER", values.toString());
    }

    public ReminderModal findreminderifExist(ReminderModal modal) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_REMINDERS, new String[]{COLUMN_REMINDID, COLUMN_MEDID, COLUMN_QUANTITY, COLUMN_TIME, COLUMN_QTY_TYPE,COLUMN_TAKEN,COLUMN_TAKEN_TIME,COLUMN_SNOOZE},
                COLUMN_MEDID + " =?"+" AND "+COLUMN_TIME+ " =?", new String[]{String.valueOf(modal.get_medID()), modal.get_time()}, null, null, null);
        ReminderModal reminder = null;
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            reminder = new ReminderModal();
            reminder.set_remindID(cursor.getInt(cursor.getColumnIndex(COLUMN_REMINDID)));
            reminder.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            reminder.set_time(cursor.getString(cursor.getColumnIndex(COLUMN_TIME)));
            reminder.set_quantity(cursor.getString(cursor.getColumnIndex(COLUMN_QUANTITY)));
            reminder.set_qty_type(cursor.getInt(cursor.getColumnIndex(COLUMN_QTY_TYPE)));
            reminder._taken = cursor.getInt(cursor.getColumnIndex(COLUMN_TAKEN));
            reminder._taken_time = cursor.getString(cursor.getColumnIndex(COLUMN_TAKEN_TIME));
            reminder._snooze = cursor.getInt(cursor.getColumnIndex(COLUMN_SNOOZE));
        }
        cursor.close();
        db.close();
        return reminder;
    }
    public ReminderModal findSpecificReminder(int reminderID) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_REMINDERS, new String[]{COLUMN_REMINDID, COLUMN_MEDID, COLUMN_QUANTITY, COLUMN_TIME, COLUMN_QTY_TYPE,COLUMN_TAKEN,COLUMN_TAKEN_TIME,COLUMN_SNOOZE},
                COLUMN_REMINDID + " =?", new String[]{String.valueOf(reminderID)}, null, null, null);
        ReminderModal reminder = null;
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            reminder = new ReminderModal();
            reminder.set_remindID(cursor.getInt(cursor.getColumnIndex(COLUMN_REMINDID)));
            reminder.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            reminder.set_time(cursor.getString(cursor.getColumnIndex(COLUMN_TIME)));
            reminder.set_quantity(cursor.getString(cursor.getColumnIndex(COLUMN_QUANTITY)));
            reminder.set_qty_type(cursor.getInt(cursor.getColumnIndex(COLUMN_QTY_TYPE)));
            reminder._taken = cursor.getInt(cursor.getColumnIndex(COLUMN_TAKEN));
            reminder._taken_time = cursor.getString(cursor.getColumnIndex(COLUMN_TAKEN_TIME));
            reminder._snooze = cursor.getInt(cursor.getColumnIndex(COLUMN_SNOOZE));
        }
        cursor.close();
        db.close();
        return reminder;
    }

    public ArrayList<ReminderModal> findReminder(int medID) {
        // String query = "Select MAX("+COLUMN_REMINDID+") FROM " + TABLE_REMINDERS ;
        //String query = "Select * FROM " + TABLE_REMINDERS  + " WHERE " + COLUMN_MEDID + " =  \"" + medID +"\"";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_REMINDERS, new String[]{COLUMN_REMINDID, COLUMN_MEDID, COLUMN_QUANTITY, COLUMN_TIME, COLUMN_QTY_TYPE,COLUMN_TAKEN,COLUMN_TAKEN_TIME,COLUMN_SNOOZE},
                COLUMN_MEDID + " =?", new String[]{String.valueOf(medID)}, null, null, null);

        //= db.rawQuery(query, null);
        //int reminID;
        ArrayList<ReminderModal> reminderModals = new ArrayList<>();
        ReminderModal reminder;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            reminder = new ReminderModal();
            reminder.set_remindID(cursor.getInt(cursor.getColumnIndex(COLUMN_REMINDID)));
            reminder.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            reminder.set_time(cursor.getString(cursor.getColumnIndex(COLUMN_TIME)));
            reminder.set_quantity(cursor.getString(cursor.getColumnIndex(COLUMN_QUANTITY)));
            reminder.set_qty_type(cursor.getInt(cursor.getColumnIndex(COLUMN_QTY_TYPE)));
            reminder._taken = cursor.getInt(cursor.getColumnIndex(COLUMN_TAKEN));
            reminder._taken_time = cursor.getString(cursor.getColumnIndex(COLUMN_TAKEN_TIME));
            reminder._snooze = cursor.getInt(cursor.getColumnIndex(COLUMN_SNOOZE));
            reminderModals.add(reminder);
            cursor.moveToNext();
        }
        cursor.close();

        db.close();
        return reminderModals;
    }
    public ArrayList<Medicine> findAllMedicines() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_MEDICINES, new String[]{COLUMN_MEDID, COLUMN_MEDNAME, COLUMN_SHAPE,
                        COLUMN_COLOR1, COLUMN_COLOR2, COLUMN_PATIENT_NAME, COLUMN_STARTDATE, COLUMN_ENDDATE,
                        COLUMN_DOSE, COLUMN_UNIT, COLUMN_UPDATE_DELETE_FLAG, COLUMN_UPDATE_DELETE_DATE},
                null, null, null, null, null);

        //= db.rawQuery(query, null);
        //int reminID;
        ArrayList<Medicine> medicines = new ArrayList<Medicine>();
        Medicine medicine;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            medicine = new Medicine();
            medicine.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            medicine.set_medName(cursor.getString(cursor.getColumnIndex(COLUMN_MEDNAME)));
            medicine.set_shape(cursor.getInt(cursor.getColumnIndex(COLUMN_SHAPE)));
            medicine.set_color1(cursor.getInt(cursor.getColumnIndex(COLUMN_COLOR1)));
            medicine.set_color2(cursor.getInt(cursor.getColumnIndex(COLUMN_COLOR2)));
            medicine.set_patientName(cursor.getString(cursor.getColumnIndex(COLUMN_PATIENT_NAME)));
            medicine.set_startDate(cursor.getString(cursor.getColumnIndex(COLUMN_STARTDATE)));
            medicine.set_endDate(cursor.getString(cursor.getColumnIndex(COLUMN_ENDDATE)));
            medicine.set_dose(cursor.getDouble(cursor.getColumnIndex(COLUMN_DOSE)));
            medicine.set_unit(cursor.getString(cursor.getColumnIndex(COLUMN_UNIT)));
            medicine.updateDeleteFlag = cursor.getString(cursor.getColumnIndex(COLUMN_UPDATE_DELETE_FLAG));
            medicine.updateDeleteDate = cursor.getString(cursor.getColumnIndex(COLUMN_UPDATE_DELETE_DATE));
            medicines.add(medicine);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return medicines;
    }
    public ArrayList<Medicine> findAllActiveMedicines() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_MEDICINES, new String[]{COLUMN_MEDID, COLUMN_MEDNAME, COLUMN_SHAPE,
                        COLUMN_COLOR1, COLUMN_COLOR2, COLUMN_PATIENT_NAME, COLUMN_STARTDATE, COLUMN_ENDDATE,
                        COLUMN_DOSE, COLUMN_UNIT, COLUMN_UPDATE_DELETE_FLAG, COLUMN_UPDATE_DELETE_DATE},
                COLUMN_UPDATE_DELETE_FLAG + " =?", new String[]{"0"}, null, null, null);

        //= db.rawQuery(query, null);
        ArrayList<Medicine> medicines = new ArrayList<>();
        Medicine medicine;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            medicine = new Medicine();
            medicine.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            medicine.set_medName(cursor.getString(cursor.getColumnIndex(COLUMN_MEDNAME)));
            medicine.set_shape(cursor.getInt(cursor.getColumnIndex(COLUMN_SHAPE)));
            medicine.set_color1(cursor.getInt(cursor.getColumnIndex(COLUMN_COLOR1)));
            medicine.set_color2(cursor.getInt(cursor.getColumnIndex(COLUMN_COLOR2)));
            medicine.set_patientName(cursor.getString(cursor.getColumnIndex(COLUMN_PATIENT_NAME)));
            medicine.set_startDate(cursor.getString(cursor.getColumnIndex(COLUMN_STARTDATE)));
            medicine.set_endDate(cursor.getString(cursor.getColumnIndex(COLUMN_ENDDATE)));
            medicine.set_dose(cursor.getDouble(cursor.getColumnIndex(COLUMN_DOSE)));
            medicine.set_unit(cursor.getString(cursor.getColumnIndex(COLUMN_UNIT)));
            medicine.updateDeleteFlag = cursor.getString(cursor.getColumnIndex(COLUMN_UPDATE_DELETE_FLAG));
            medicine.updateDeleteDate = cursor.getString(cursor.getColumnIndex(COLUMN_UPDATE_DELETE_DATE));
            medicines.add(medicine);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return medicines;
    }
    public ArrayList<ReminderModal> findRemindersForTime(int medID, int quantity_type) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_REMINDERS, new String[]{COLUMN_REMINDID, COLUMN_MEDID, COLUMN_QUANTITY, COLUMN_TIME, COLUMN_QTY_TYPE,COLUMN_TAKEN,COLUMN_TAKEN_TIME,COLUMN_SNOOZE},
                COLUMN_MEDID + " =?" +" AND "+ COLUMN_QTY_TYPE +" =?", new String[]{String.valueOf(medID), String.valueOf(quantity_type)}, null, null, null);
        ArrayList<ReminderModal> reminderModals = new ArrayList<>();
        ReminderModal reminder;
        Calendar calendar2 = null;
        boolean isreapeat = false;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            reminder = new ReminderModal();
            reminder.set_remindID(cursor.getInt(cursor.getColumnIndex(COLUMN_REMINDID)));
            reminder.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            reminder.set_time(cursor.getString(cursor.getColumnIndex(COLUMN_TIME)));
            reminder.set_quantity(cursor.getString(cursor.getColumnIndex(COLUMN_QUANTITY)));
            reminder.set_qty_type(cursor.getInt(cursor.getColumnIndex(COLUMN_QTY_TYPE)));
            reminder._taken = cursor.getInt(cursor.getColumnIndex(COLUMN_TAKEN));
            reminder._taken_time = cursor.getString(cursor.getColumnIndex(COLUMN_TAKEN_TIME));
            reminder._snooze = cursor.getInt(cursor.getColumnIndex(COLUMN_SNOOZE));
            Calendar calendar1 = new GregorianCalendar();
            calendar1.setTimeInMillis(Long.parseLong(reminder.get_time()));
//            int a = calendar1.get(Calendar.DAY_OF_YEAR);
//            int b = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
            if (calendar2 != null && (calendar1.get(Calendar.HOUR_OF_DAY) + calendar1.get(Calendar.MINUTE)
                    != calendar2.get(Calendar.HOUR_OF_DAY)+calendar2.get(Calendar.MINUTE))){
                isreapeat = false;
            }

            if (!isreapeat){   //calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR)
                reminderModals.add(reminder);
                isreapeat = true;
            }
            calendar2 = calendar1;

            cursor.moveToNext();
        }
        cursor.close();
        db.close();

        return reminderModals;
    }

    public ArrayList<ReminderModal> findRemindersForRX(int medID, int quantity_type) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_REMINDERS, new String[]{COLUMN_REMINDID, COLUMN_MEDID, COLUMN_QUANTITY, COLUMN_TIME, COLUMN_QTY_TYPE,COLUMN_TAKEN,COLUMN_TAKEN_TIME,COLUMN_SNOOZE},
                COLUMN_MEDID + " =?" +" AND "+ COLUMN_QTY_TYPE +" =?", new String[]{String.valueOf(medID), String.valueOf(quantity_type)}, null, null, null);
        ArrayList<ReminderModal> reminderModals = new ArrayList<>();
        ReminderModal reminder;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            reminder = new ReminderModal();
            reminder.set_remindID(cursor.getInt(cursor.getColumnIndex(COLUMN_REMINDID)));
            reminder.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            reminder.set_time(cursor.getString(cursor.getColumnIndex(COLUMN_TIME)));
            reminder.set_quantity(cursor.getString(cursor.getColumnIndex(COLUMN_QUANTITY)));
            reminder.set_qty_type(cursor.getInt(cursor.getColumnIndex(COLUMN_QTY_TYPE)));
            reminder._taken = cursor.getInt(cursor.getColumnIndex(COLUMN_TAKEN));
            reminder._taken_time = cursor.getString(cursor.getColumnIndex(COLUMN_TAKEN_TIME));
            reminder._snooze = cursor.getInt(cursor.getColumnIndex(COLUMN_SNOOZE));
            reminderModals.add(reminder);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();

        return reminderModals;
    }

    public ArrayList<ReminderModal> findReminders() {

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_REMINDERS, new String[]{COLUMN_REMINDID, COLUMN_MEDID, COLUMN_QUANTITY, COLUMN_TIME, COLUMN_QTY_TYPE,COLUMN_TAKEN,COLUMN_TAKEN_TIME,COLUMN_SNOOZE},
                null, null, null, null, null);

        ArrayList<ReminderModal> reminderModals = new ArrayList<>();
        ReminderModal reminder;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            reminder = new ReminderModal();
            reminder.set_remindID(cursor.getInt(cursor.getColumnIndex(COLUMN_REMINDID)));
            reminder.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            reminder.set_time(cursor.getString(cursor.getColumnIndex(COLUMN_TIME)));
            reminder.set_quantity(cursor.getString(cursor.getColumnIndex(COLUMN_QUANTITY)));
            reminder.set_qty_type(cursor.getInt(cursor.getColumnIndex(COLUMN_QTY_TYPE)));
            reminder._taken = cursor.getInt(cursor.getColumnIndex(COLUMN_TAKEN));
            reminder._taken_time = cursor.getString(cursor.getColumnIndex(COLUMN_TAKEN_TIME));
            reminder._snooze = cursor.getInt(cursor.getColumnIndex(COLUMN_SNOOZE));
            reminderModals.add(reminder);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return reminderModals;
    }

    public ReminderModal findLastReminderForDate (int medicineid, String time,int quantity_type){

        ArrayList<ReminderModal> reminderModals = findReminder(medicineid);

        ReminderModal finalReminder = null;

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(time));

        int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        int currentMin  = calendar.get(Calendar.MINUTE);

        currentMin  =   (currentHour*60) + currentMin;

        //Log.d("findLastReminderForDate","currentMin is "+currentMin + " Formated is "+calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE));

        for(ReminderModal modal:reminderModals){

            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTimeInMillis(Long.parseLong(modal.get_time()));

            int currentHour1 = calendar1.get(Calendar.HOUR_OF_DAY);
            int currentMin1  = calendar1.get(Calendar.MINUTE);
            currentMin1 =   (currentHour1*60) + currentMin1;

            //Log.d("findLastReminderForDate for","modal id "+ modal.get_remindID() +" and time is "+currentMin1 + " Formated is "+calendar1.get(Calendar.HOUR_OF_DAY) + ":" + calendar1.get(Calendar.MINUTE));

            if(currentMin1 == currentMin && quantity_type == modal.get_qty_type()){
                //we need to extract reminder modal for the particular time and min only.

                Log.d("findLstReminderFrD","modal id "+ modal.get_remindID());
                if(finalReminder != null)
                {
                    finalReminder   =   null;
                }
                finalReminder = modal;
            }
        }

        return finalReminder;
    }

    public boolean deleteReminder(int reminderID) {

        SQLiteDatabase db = this.getWritableDatabase();

        String whereClause = COLUMN_REMINDID + "=?";
        String[] whereArgs = new String[] { String.valueOf(reminderID) };
        int result = db.delete(TABLE_REMINDERS, whereClause, whereArgs);

        if(result >= 0)
            return true;
        else
            return false;
    }
    //delete from reminder where reminder.time >= datetime('now','localtime') and med_id=?
    public boolean deleteReminderWithMedicineID(int medicineid) {
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = COLUMN_MEDID + "=?";
        String[] whereArgs = new String[] { String.valueOf(medicineid) };
        int result = db.delete(TABLE_REMINDERS, whereClause, whereArgs);
        return result >= 0;
    }
    public boolean deleteReminderWithMedicineIDForFutureDate(int medicineid) {
        SQLiteDatabase db = this.getWritableDatabase();
        //"datetime(1423561920000/1000, 'unixepoch','localtime')"

        //med_id=? AND datetime(1423561920000/1000, 'unixepoch','localtime') >= datetime('now','localtime')

        String whereClause = COLUMN_MEDID + "=? AND datetime("+COLUMN_TIME+"/1000, 'unixepoch','localtime') >= datetime('now','localtime')";   //+COLUMN_TIME +" >= datetime('now','localtime')"
        String[] whereArgs = new String[] { String.valueOf(medicineid) };
        int result = db.delete(TABLE_REMINDERS, whereClause, whereArgs);
        return result >= 0;
    }

    public ArrayList<ReminderModal> getRemindersForPastDates(int daysInterval, int medID) {
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = COLUMN_MEDID + "=? AND datetime("+COLUMN_TIME+"/1000, 'unixepoch','localtime') " +
                "BETWEEN datetime('now','localtime','-"+daysInterval+" day') AND datetime('now','localtime')";
        //"AND datetime("+COLUMN_TIME+"/1000, 'unixepoch','localtime') > datetime('now','localtime')-15";   //+COLUMN_TIME +" >= datetime('now','localtime')"
        String[] whereArgs = new String[] { String.valueOf(medID)};
        Cursor cursor = db.query(TABLE_REMINDERS, new String[]{COLUMN_REMINDID, COLUMN_MEDID, COLUMN_QUANTITY, COLUMN_TIME, COLUMN_QTY_TYPE,COLUMN_TAKEN,COLUMN_TAKEN_TIME,COLUMN_SNOOZE},
                whereClause, whereArgs, null, null, null);
        ArrayList<ReminderModal> reminderModals = new ArrayList<>();
        ReminderModal reminder;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            reminder = new ReminderModal();
            reminder.set_remindID(cursor.getInt(cursor.getColumnIndex(COLUMN_REMINDID)));
            reminder.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            reminder.set_time(cursor.getString(cursor.getColumnIndex(COLUMN_TIME)));
            reminder.set_quantity(cursor.getString(cursor.getColumnIndex(COLUMN_QUANTITY)));
            reminder.set_qty_type(cursor.getInt(cursor.getColumnIndex(COLUMN_QTY_TYPE)));
            reminder._taken = cursor.getInt(cursor.getColumnIndex(COLUMN_TAKEN));
            reminder._taken_time = cursor.getString(cursor.getColumnIndex(COLUMN_TAKEN_TIME));
            reminder._snooze = cursor.getInt(cursor.getColumnIndex(COLUMN_SNOOZE));
            reminderModals.add(reminder);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return reminderModals;
    }
    public ArrayList<ReminderModal> getAllRemindersForPastDates(int daysInterval) {
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = "datetime("+COLUMN_TIME+"/1000, 'unixepoch','localtime') " +
                "BETWEEN datetime('now','localtime','-"+daysInterval+" day') AND datetime('now','localtime')";
                //"AND datetime("+COLUMN_TIME+"/1000, 'unixepoch','localtime') > datetime('now','localtime')-15";   //+COLUMN_TIME +" >= datetime('now','localtime')"
        //String[] whereArgs = new String[] { String.valueOf(0), String.valueOf(-1)};
        Cursor cursor = db.query(TABLE_REMINDERS, new String[]{COLUMN_REMINDID, COLUMN_MEDID, COLUMN_QUANTITY, COLUMN_TIME, COLUMN_QTY_TYPE,COLUMN_TAKEN,COLUMN_TAKEN_TIME,COLUMN_SNOOZE},
                whereClause, null, null, null, null);
        ArrayList<ReminderModal> reminderModals = new ArrayList<>();
        ReminderModal reminder;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            reminder = new ReminderModal();
            reminder.set_remindID(cursor.getInt(cursor.getColumnIndex(COLUMN_REMINDID)));
            reminder.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            reminder.set_time(cursor.getString(cursor.getColumnIndex(COLUMN_TIME)));
            reminder.set_quantity(cursor.getString(cursor.getColumnIndex(COLUMN_QUANTITY)));
            reminder.set_qty_type(cursor.getInt(cursor.getColumnIndex(COLUMN_QTY_TYPE)));
            reminder._taken = cursor.getInt(cursor.getColumnIndex(COLUMN_TAKEN));
            reminder._taken_time = cursor.getString(cursor.getColumnIndex(COLUMN_TAKEN_TIME));
            reminder._snooze = cursor.getInt(cursor.getColumnIndex(COLUMN_SNOOZE));
            reminderModals.add(reminder);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return reminderModals;
    }
    public ArrayList<ReminderModal> getAllRemindersForPastDatesMISSED(int daysInterval) {
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = "("+COLUMN_TAKEN+"=? OR "+COLUMN_TAKEN+"=?) AND datetime("+COLUMN_TIME+"/1000, 'unixepoch','localtime') " +
                "BETWEEN datetime('now','localtime','-"+daysInterval+" day') AND datetime('now','localtime')";   //+COLUMN_TIME +" >= datetime('now','localtime')"
        String[] whereArgs = new String[] { String.valueOf(0), String.valueOf(-1)};

        Cursor cursor = db.query(TABLE_REMINDERS, new String[]{COLUMN_REMINDID, COLUMN_MEDID, COLUMN_QUANTITY, COLUMN_TIME, COLUMN_QTY_TYPE,COLUMN_TAKEN,COLUMN_TAKEN_TIME,COLUMN_SNOOZE},
                whereClause, whereArgs, null, null, null);
        ArrayList<ReminderModal> reminderModals = new ArrayList<>();
        ReminderModal reminder;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            reminder = new ReminderModal();
            reminder.set_remindID(cursor.getInt(cursor.getColumnIndex(COLUMN_REMINDID)));
            reminder.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            reminder.set_time(cursor.getString(cursor.getColumnIndex(COLUMN_TIME)));
            reminder.set_quantity(cursor.getString(cursor.getColumnIndex(COLUMN_QUANTITY)));
            reminder.set_qty_type(cursor.getInt(cursor.getColumnIndex(COLUMN_QTY_TYPE)));
            reminder._taken = cursor.getInt(cursor.getColumnIndex(COLUMN_TAKEN));
            reminder._taken_time = cursor.getString(cursor.getColumnIndex(COLUMN_TAKEN_TIME));
            reminder._snooze = cursor.getInt(cursor.getColumnIndex(COLUMN_SNOOZE));
            reminderModals.add(reminder);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return reminderModals;
    }
    public ArrayList<ReminderModal> getAllRemindersForPastDatesONTIME(int daysInterval) {
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = COLUMN_TAKEN+"=? AND datetime("+COLUMN_TIME+"/1000, 'unixepoch','localtime') " +
                "BETWEEN datetime('now','localtime','-"+daysInterval+" day') AND datetime('now','localtime')";   //+COLUMN_TIME +" >= datetime('now','localtime')"
        String[] whereArgs = new String[] { String.valueOf(1) };

        Cursor cursor = db.query(TABLE_REMINDERS, new String[]{COLUMN_REMINDID, COLUMN_MEDID, COLUMN_QUANTITY, COLUMN_TIME, COLUMN_QTY_TYPE,COLUMN_TAKEN,COLUMN_TAKEN_TIME,COLUMN_SNOOZE},
                whereClause, whereArgs, null, null, null);
        ArrayList<ReminderModal> reminderModals = new ArrayList<>();
        ReminderModal reminder;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            reminder = new ReminderModal();
            reminder.set_remindID(cursor.getInt(cursor.getColumnIndex(COLUMN_REMINDID)));
            reminder.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            reminder.set_time(cursor.getString(cursor.getColumnIndex(COLUMN_TIME)));
            reminder.set_quantity(cursor.getString(cursor.getColumnIndex(COLUMN_QUANTITY)));
            reminder.set_qty_type(cursor.getInt(cursor.getColumnIndex(COLUMN_QTY_TYPE)));
            reminder._taken = cursor.getInt(cursor.getColumnIndex(COLUMN_TAKEN));
            reminder._taken_time = cursor.getString(cursor.getColumnIndex(COLUMN_TAKEN_TIME));
            reminder._snooze = cursor.getInt(cursor.getColumnIndex(COLUMN_SNOOZE));
            reminderModals.add(reminder);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return reminderModals;
    }
    public ArrayList<ReminderModal> getAllRemindersForPastDatesTAKEN(int daysInterval) {
        SQLiteDatabase db = this.getWritableDatabase();

        String whereClause = COLUMN_TAKEN +"=? AND datetime("+COLUMN_TIME+"/1000, 'unixepoch','localtime') " +
                "BETWEEN datetime('now','localtime','-"+daysInterval+" day') AND datetime('now','localtime')";   //+COLUMN_TIME +" >= datetime('now','localtime')"
        String[] whereArgs = new String[] { String.valueOf(2) };

        Cursor cursor = db.query(TABLE_REMINDERS, new String[]{COLUMN_REMINDID, COLUMN_MEDID, COLUMN_QUANTITY, COLUMN_TIME, COLUMN_QTY_TYPE,COLUMN_TAKEN,COLUMN_TAKEN_TIME,COLUMN_SNOOZE},
                whereClause, whereArgs, null, null, null);
        ArrayList<ReminderModal> reminderModals = new ArrayList<>();
        ReminderModal reminder;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            reminder = new ReminderModal();
            reminder.set_remindID(cursor.getInt(cursor.getColumnIndex(COLUMN_REMINDID)));
            reminder.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            reminder.set_time(cursor.getString(cursor.getColumnIndex(COLUMN_TIME)));
            reminder.set_quantity(cursor.getString(cursor.getColumnIndex(COLUMN_QUANTITY)));
            reminder.set_qty_type(cursor.getInt(cursor.getColumnIndex(COLUMN_QTY_TYPE)));
            reminder._taken = cursor.getInt(cursor.getColumnIndex(COLUMN_TAKEN));
            reminder._taken_time = cursor.getString(cursor.getColumnIndex(COLUMN_TAKEN_TIME));
            reminder._snooze = cursor.getInt(cursor.getColumnIndex(COLUMN_SNOOZE));
            reminderModals.add(reminder);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return reminderModals;
    }
    public ArrayList<ReminderModal> getAllRemindersForDatesPatiMed(int daysInterval, String medName, String patientName) {
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = "datetime("+COLUMN_TIME+"/1000, 'unixepoch','localtime') " +
                "BETWEEN datetime('now','localtime','-"+daysInterval+" day') AND datetime('now','localtime')";
        //"AND datetime("+COLUMN_TIME+"/1000, 'unixepoch','localtime') > datetime('now','localtime')-15";   //+COLUMN_TIME +" >= datetime('now','localtime')"
        //String[] whereArgs = new String[] { String.valueOf(0), String.valueOf(-1)};
        Cursor cursor;
        String query1 = "SELECT remind_id, reminders.med_id, quantity, time, qty_type, taken, taken_time, snooze FROM reminders  " +
                "JOIN medicines on medicines.med_id=reminders.med_id WHERE datetime(reminders.time/1000, 'unixepoch','localtime') " +
                "BETWEEN datetime('now','localtime','-"+daysInterval+" day') AND datetime('now','localtime')";

        String query2 = "SELECT remind_id, reminders.med_id, quantity, time, qty_type, taken, taken_time, snooze FROM reminders  " +
                "JOIN medicines on medicines.med_id=reminders.med_id WHERE medicines.medName=\""+medName+"\" " +
                " AND datetime(reminders.time/1000, 'unixepoch','localtime') " +
                "BETWEEN datetime('now','localtime','-"+daysInterval+" day') AND datetime('now','localtime')";

        String query3 = "SELECT remind_id, reminders.med_id, quantity, time, qty_type, taken, taken_time, snooze FROM reminders  " +
                "JOIN medicines on medicines.med_id=reminders.med_id WHERE medicines.patient_name=\""+patientName+"\" " +
                " AND datetime(reminders.time/1000, 'unixepoch','localtime') " +
                "BETWEEN datetime('now','localtime','-"+daysInterval+" day') AND datetime('now','localtime')";

        String query4 = "SELECT remind_id, reminders.med_id, quantity, time, qty_type, taken, taken_time, snooze FROM reminders  " +
                "JOIN medicines on medicines.med_id=reminders.med_id WHERE medicines.patient_name=\""+patientName+"\" " +
                "AND medicines.medName=\""+medName+"\" AND datetime(reminders.time/1000, 'unixepoch','localtime') " +
                "BETWEEN datetime('now','localtime','-"+daysInterval+" day') AND datetime('now','localtime')";
        if (patientName.equals("") && medName.equals("")) {
            cursor = db.rawQuery(query1, null);
        } else if (!patientName.equals("") && !medName.equals("")) {
            cursor = db.rawQuery(query4, null);
        } else if (patientName.equals("")) {
            cursor = db.rawQuery(query2, null);
        } else {
            cursor = db.rawQuery(query3, null);
        }
        ArrayList<ReminderModal> reminderModals = new ArrayList<>();
        ReminderModal reminder;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            reminder = new ReminderModal();
            reminder.set_remindID(cursor.getInt(cursor.getColumnIndex(COLUMN_REMINDID)));
            reminder.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            reminder.set_time(cursor.getString(cursor.getColumnIndex(COLUMN_TIME)));
            reminder.set_quantity(cursor.getString(cursor.getColumnIndex(COLUMN_QUANTITY)));
            reminder.set_qty_type(cursor.getInt(cursor.getColumnIndex(COLUMN_QTY_TYPE)));
            reminder._taken = cursor.getInt(cursor.getColumnIndex(COLUMN_TAKEN));
            reminder._taken_time = cursor.getString(cursor.getColumnIndex(COLUMN_TAKEN_TIME));
            reminder._snooze = cursor.getInt(cursor.getColumnIndex(COLUMN_SNOOZE));
            reminderModals.add(reminder);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return reminderModals;
    }
    public ArrayList<ReminderModal> getAllRemindersForFilterDatesPatiMed(int daysInterval, int taken, String medName, String patientName) {
        SQLiteDatabase db = this.getWritableDatabase();
//        String whereClause = "("+COLUMN_TAKEN+"=? OR "+COLUMN_TAKEN+"=?) AND datetime("+COLUMN_TIME+"/1000, 'unixepoch','localtime') " +
//                "BETWEEN datetime('now','localtime','-"+daysInterval+" day') AND datetime('now','localtime')";   //+COLUMN_TIME +" >= datetime('now','localtime')"
//        String[] whereArgs = new String[] { String.valueOf(0), String.valueOf(-1)};
        String query1 = "SELECT remind_id, reminders.med_id, quantity, time, qty_type, taken, taken_time, snooze FROM reminders  " +
                "JOIN medicines on medicines.med_id=reminders.med_id WHERE (reminders.taken=0 OR reminders.taken=-1) " +
                "AND datetime(reminders.time/1000, 'unixepoch','localtime') BETWEEN datetime('now','localtime','-"+daysInterval+" day') " +
                "AND datetime('now','localtime')";

        String query2 = "SELECT remind_id, reminders.med_id, quantity, time, qty_type, taken, taken_time, snooze FROM reminders  " +
                "JOIN medicines on medicines.med_id=reminders.med_id WHERE medicines.medName=\""+medName+"\" " +
                "AND (reminders.taken=0 OR reminders.taken=-1) AND datetime(reminders.time/1000, 'unixepoch','localtime') " +
                "BETWEEN datetime('now','localtime','-"+daysInterval+" day') AND datetime('now','localtime')";

        String query3 = "SELECT remind_id, reminders.med_id, quantity, time, qty_type, taken, taken_time, snooze FROM reminders  " +
                "JOIN medicines on medicines.med_id=reminders.med_id WHERE medicines.patient_name=\""+patientName+"\" " +
                "AND (reminders.taken=0 OR reminders.taken=-1) AND datetime(reminders.time/1000, 'unixepoch','localtime') " +
                "BETWEEN datetime('now','localtime','-"+daysInterval+" day') AND datetime('now','localtime')";

        String query4 = "SELECT remind_id, reminders.med_id, quantity, time, qty_type, taken, taken_time, snooze FROM reminders  " +
                "JOIN medicines on medicines.med_id=reminders.med_id WHERE medicines.patient_name=\""+patientName+"\" " +
                "AND medicines.medName=\""+medName+"\" AND (reminders.taken=0 OR reminders.taken=-1) " +
                "AND datetime(reminders.time/1000, 'unixepoch','localtime') BETWEEN datetime('now','localtime','-"+daysInterval+" day') " +
                "AND datetime('now','localtime')";
        String query5 = "SELECT remind_id, reminders.med_id, quantity, time, qty_type, taken, taken_time, snooze FROM reminders  " +
                "JOIN medicines on medicines.med_id=reminders.med_id WHERE reminders.taken="+taken+" " +
                "AND datetime(reminders.time/1000, 'unixepoch','localtime') BETWEEN datetime('now','localtime','-"+daysInterval+" day') " +
                "AND datetime('now','localtime')";

        String query6 = "SELECT remind_id, reminders.med_id, quantity, time, qty_type, taken, taken_time, snooze FROM reminders  " +
                "JOIN medicines on medicines.med_id=reminders.med_id WHERE medicines.medName=\""+medName+"\" " +
                "AND reminders.taken="+taken+" AND datetime(reminders.time/1000, 'unixepoch','localtime') " +
                "BETWEEN datetime('now','localtime','-"+daysInterval+" day') AND datetime('now','localtime')";

        String query7 = "SELECT remind_id, reminders.med_id, quantity, time, qty_type, taken, taken_time, snooze FROM reminders  " +
                "JOIN medicines on medicines.med_id=reminders.med_id WHERE medicines.patient_name=\""+patientName+"\" " +
                "AND reminders.taken="+taken+" AND datetime(reminders.time/1000, 'unixepoch','localtime') " +
                "BETWEEN datetime('now','localtime','-"+daysInterval+" day') AND datetime('now','localtime')";

        String query8 = "SELECT remind_id, reminders.med_id, quantity, time, qty_type, taken, taken_time, snooze FROM reminders  " +
                "JOIN medicines on medicines.med_id=reminders.med_id WHERE medicines.patient_name=\""+patientName+"\" " +
                "AND medicines.medName=\""+medName+"\" AND reminders.taken="+taken+" " +
                "AND datetime(reminders.time/1000, 'unixepoch','localtime') BETWEEN datetime('now','localtime','-"+daysInterval+" day') " +
                "AND datetime('now','localtime')";
        Cursor cursor;
        if (taken == 0 || taken == -1) {
            if (patientName.equals("") && medName.equals("")) {
                cursor = db.rawQuery(query1, null);
            } else if (!patientName.equals("") && !medName.equals("")) {
                cursor = db.rawQuery(query4, null);
            } else if (patientName.equals("")) {
                cursor = db.rawQuery(query2, null);
            } else {
                cursor = db.rawQuery(query3, null);
            }
        }else {
            if (patientName.equals("") && medName.equals("")) {
                cursor = db.rawQuery(query5, null);
            } else if (!patientName.equals("") && !medName.equals("")) {
                cursor = db.rawQuery(query8, null);
            } else if (patientName.equals("")) {
                cursor = db.rawQuery(query6, null);
            } else {
                cursor = db.rawQuery(query7, null);
            }
        }
        ArrayList<ReminderModal> reminderModals = new ArrayList<>();
        ReminderModal reminder;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            reminder = new ReminderModal();
            reminder.set_remindID(cursor.getInt(cursor.getColumnIndex(COLUMN_REMINDID)));
            reminder.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            reminder.set_time(cursor.getString(cursor.getColumnIndex(COLUMN_TIME)));
            reminder.set_quantity(cursor.getString(cursor.getColumnIndex(COLUMN_QUANTITY)));
            reminder.set_qty_type(cursor.getInt(cursor.getColumnIndex(COLUMN_QTY_TYPE)));
            reminder._taken = cursor.getInt(cursor.getColumnIndex(COLUMN_TAKEN));
            reminder._taken_time = cursor.getString(cursor.getColumnIndex(COLUMN_TAKEN_TIME));
            reminder._snooze = cursor.getInt(cursor.getColumnIndex(COLUMN_SNOOZE));
            reminderModals.add(reminder);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return reminderModals;
    }
/*
select  Distinct medicine.medicine_name , medicine.med_id, (Select count(reminder_id) from reminder where reminder.status = 2
and reminder.med_id = medicine.med_id and reminder.time  BETWEEN DATETIME('now', '-15 day','localtime') AND
DATETIME('now','localtime')) as reminder_count, medicine.shape , medicine.color1 , medicine.color2 ,
(select  count(reminder_id) from reminder where reminder.time  BETWEEN DATETIME('now', '-15 day','localtime') AND
DATETIME('now','localtime') and reminder.med_id = medicine.med_id) as total_reminders from medicine join reminder on
reminder.med_id = medicine.med_id where reminder.status = 2 and reminder.time  BETWEEN DATETIME('now', '-15 day','localtime')
AND DATETIME('now','localtime')
 */
public ArrayList<MedicineReportModel> getAllMedicinesWITHReminderCountForFixedDays(int daysInterval, int type) {
    SQLiteDatabase db = this.getWritableDatabase();
    String query;
    if (type==0 || type==-1) {
        query = "select Distinct "+TABLE_MEDICINES+"."+COLUMN_MEDNAME+" , "+TABLE_MEDICINES+"."+COLUMN_MEDID+", " +
                TABLE_MEDICINES+"."+COLUMN_SHAPE+" , "+TABLE_MEDICINES+"."+COLUMN_COLOR1+", "+TABLE_MEDICINES+"."+COLUMN_COLOR2+", "+
                "(Select count("+COLUMN_REMINDID+") from "+TABLE_REMINDERS+" where ("+TABLE_REMINDERS+"."+COLUMN_TAKEN+"= 0 OR "
                +TABLE_REMINDERS+"."+COLUMN_TAKEN +"= -1)and "+TABLE_REMINDERS+"."+COLUMN_MEDID+" = "+TABLE_MEDICINES+"."+COLUMN_MEDID+" " +
                " AND  datetime(" +TABLE_REMINDERS+"."+COLUMN_TIME+ "/1000, 'unixepoch','localtime') BETWEEN datetime('now','localtime','-" +
                daysInterval + " day') AND datetime('now','localtime')) as reminder_count, (select  count("+COLUMN_REMINDID+") from "
                +TABLE_REMINDERS+" where datetime(" +TABLE_REMINDERS+"."+COLUMN_TIME+ "/1000, 'unixepoch','localtime') " +
                " BETWEEN datetime('now','localtime','-" + daysInterval + " day') AND datetime('now','localtime') and "+TABLE_REMINDERS+"."+COLUMN_MEDID+" = "+TABLE_MEDICINES+"."+COLUMN_MEDID+")" +
                " as total_reminders from "+ TABLE_MEDICINES+" JOIN "+TABLE_REMINDERS+" on "+TABLE_REMINDERS+"."+COLUMN_MEDID+" = "+TABLE_MEDICINES+"."+COLUMN_MEDID+
                " where ("+TABLE_REMINDERS+"."+COLUMN_TAKEN+" = -1 OR " +TABLE_REMINDERS+"."+ COLUMN_TAKEN + "=0) AND  datetime(" +TABLE_REMINDERS+"."+COLUMN_TIME+ "/1000, 'unixepoch','localtime') " +
                " BETWEEN datetime('now','localtime','-" + daysInterval + " day') AND datetime('now','localtime')";
//        whereClause = "(" + COLUMN_TAKEN + "=? OR " + COLUMN_TAKEN + "=?) AND datetime(" + COLUMN_TIME + "/1000, 'unixepoch','localtime') " +
//                "BETWEEN datetime('now','localtime','-" + daysInterval + " day') AND datetime('now','localtime')";   //+COLUMN_TIME +" >= datetime('now','localtime')"
    }
    else {
        query = "select Distinct "+TABLE_MEDICINES+"."+COLUMN_MEDNAME+" , "+TABLE_MEDICINES+"."+COLUMN_MEDID+" , " +
                TABLE_MEDICINES+"."+COLUMN_SHAPE+" , "+TABLE_MEDICINES+"."+COLUMN_COLOR1+" , "+TABLE_MEDICINES+"."+COLUMN_COLOR2+" , "+
                "(Select count("+COLUMN_REMINDID+") from "+TABLE_REMINDERS+" where "+TABLE_REMINDERS+"."+COLUMN_TAKEN+"= "+type+
                " AND "+TABLE_REMINDERS+"."+COLUMN_MEDID+" = "+TABLE_MEDICINES+"."+COLUMN_MEDID+" " +
                " AND  datetime(" +TABLE_REMINDERS+"."+COLUMN_TIME+ "/1000, 'unixepoch','localtime') BETWEEN datetime('now','localtime','-" +
                daysInterval + " day') AND datetime('now','localtime')) as reminder_count , (select  count("+COLUMN_REMINDID+") from "
                +TABLE_REMINDERS+" where datetime(" +TABLE_REMINDERS+"."+COLUMN_TIME+ "/1000, 'unixepoch','localtime') " +
                "BETWEEN datetime('now','localtime','-" + daysInterval + " day') AND datetime('now','localtime') and "+TABLE_REMINDERS+"."+COLUMN_MEDID+" = "+TABLE_MEDICINES+"."+COLUMN_MEDID+")" +
                " as total_reminders from "+ TABLE_MEDICINES+" JOIN "+TABLE_REMINDERS+" on "+TABLE_REMINDERS+"."+COLUMN_MEDID+" = "+TABLE_MEDICINES+"."+COLUMN_MEDID+
                " where "+TABLE_REMINDERS+"."+COLUMN_TAKEN+" = "+type+" AND  datetime(" +TABLE_REMINDERS+"."+COLUMN_TIME+ "/1000, 'unixepoch','localtime') " +
                "BETWEEN datetime('now','localtime','-" + daysInterval + " day') AND datetime('now','localtime')";
//        whereClause = COLUMN_TAKEN + "="+type+" AND datetime(" + COLUMN_TIME + "/1000, 'unixepoch','localtime') " +
//                "BETWEEN datetime('now','localtime','-" + daysInterval + " day') AND datetime('now','localtime')";
    }
    Cursor cursor = db.rawQuery(query, null);

    ArrayList<MedicineReportModel> reportModels = new ArrayList<>();
    MedicineReportModel medicineReportModel;
    int i = cursor.getCount();
    cursor.moveToFirst();
    while (!cursor.isAfterLast()) {
        medicineReportModel = new MedicineReportModel();
        medicineReportModel.medID = cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID));
        medicineReportModel.medicineName = cursor.getString(cursor.getColumnIndex(COLUMN_MEDNAME));
        medicineReportModel.shape = cursor.getInt(cursor.getColumnIndex(COLUMN_SHAPE));
        medicineReportModel.color1 = cursor.getInt(cursor.getColumnIndex(COLUMN_COLOR1));
        medicineReportModel.color2 = cursor.getInt(cursor.getColumnIndex(COLUMN_COLOR2));
        medicineReportModel.valueCount = cursor.getInt(cursor.getColumnIndex("reminder_count"));
        medicineReportModel.totalCount = cursor.getInt(cursor.getColumnIndex("total_reminders"));
        reportModels.add(medicineReportModel);
        cursor.moveToNext();
    }
    cursor.close();
    db.close();
    return reportModels;
}
    ////////////////-------------------SCHEDULE ADD,GET,DELETE  ---------------//////////////
    public void addSchedule(Schedule schedule) {

        /*
        String CREATE_SCHEDULE_TABLE = "CREATE TABLE " +
            TABLE_SCHEDULES + "("
            + COLUMN_SCHEDID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_MEDID + " INTEGER,"
            + COLUMN_ACTION + " TEXT,"
            + COLUMN_TYPE + " INTEGER" + ")";
         */

        ContentValues values = new ContentValues();
        values.put(COLUMN_MEDID, schedule.get_medID());
        values.put(COLUMN_ACTION, schedule.get_action());
        values.put(COLUMN_TYPE, schedule.get_type());

        SQLiteDatabase db = this.getWritableDatabase();

        db.insert(TABLE_SCHEDULES, null, values);
        db.close();
        Log.e("DBHANDLER", values.toString());
    }

    public Schedule findScheduleWithMedicinID (int medicinid){

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.query(TABLE_SCHEDULES, new String[]{COLUMN_SCHEDID, COLUMN_MEDID, COLUMN_ACTION, COLUMN_TYPE},
                COLUMN_MEDID + " =?", new String[]{String.valueOf(medicinid)}, null, null, null);
        Schedule schedule = null;
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            schedule = new Schedule();

            schedule.set_scheduleID(cursor.getInt(cursor.getColumnIndex(COLUMN_SCHEDID)));
            schedule.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            schedule.set_action(cursor.getString(cursor.getColumnIndex(COLUMN_ACTION)));
            schedule.set_type(cursor.getInt(cursor.getColumnIndex(COLUMN_TYPE)));
        }
        cursor.close();
        db.close();
        return schedule;
    }

    public Schedule findSchedule() {
        String query = "Select MAX(" + COLUMN_SCHEDID + ") FROM " + TABLE_SCHEDULES;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        Schedule schedule = new Schedule();
        int scheduleID;
        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            scheduleID = Integer.parseInt(cursor.getString(0));


            cursor.close();
        } else {
            scheduleID = -1;
        }
        if (scheduleID != -1) {
            String query2 = "Select * FROM " + TABLE_SCHEDULES + " WHERE " + COLUMN_SCHEDID + " =  \"" + scheduleID + "\"";
            Cursor cursor1 = db.rawQuery(query2, null);


            if (cursor1.moveToFirst()) {
                cursor1.moveToFirst();
                schedule.set_scheduleID(Integer.parseInt(cursor1.getString(0)));
                schedule.set_medID(Integer.parseInt(cursor1.getString(1)));
                schedule.set_action(cursor1.getString(2));
                schedule.set_type(Integer.parseInt(cursor1.getString(3)));
                cursor1.close();
            } else {
                schedule = null;
            }
        } else {
            schedule = null;
        }
        db.close();
        return schedule;
    }

    public boolean deleteSchedule(int medicineid) {

        SQLiteDatabase db = this.getWritableDatabase();

        String whereClause = COLUMN_MEDID + "=?";
        String[] whereArgs = new String[] { String.valueOf(medicineid) };
        int result = db.delete(TABLE_SCHEDULES, whereClause, whereArgs);

        return result >= 0;
    }



    ////////////--------------MEDI FRIEND MAPPING ADD, UPDATE, DELETE ---------------////////////
    public long addMapMedFriend(MapMedFriend mapMedFriend) {

        ContentValues values = new ContentValues();
        values.put(COLUMN_MEDID, mapMedFriend._medID);
        values.put(COLUMN_MED_FRIEND_ID, mapMedFriend._medFriendID);

        SQLiteDatabase db = this.getWritableDatabase();

        long mapFriendid = db.insert(TABLE_MED_FRIEND_MAP, null, values);

        Log.d("DBHANDLER"," medFriendMap id : "+mapFriendid);

        db.close();
        Log.e("DBHANDLER", values.toString());

        return mapFriendid;
    }

    public void updateMapMedFriend(MapMedFriend mapMedFriend) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_MEDID, mapMedFriend._medID);
        values.put(COLUMN_MED_FRIEND_ID, mapMedFriend._medFriendID);
        SQLiteDatabase db = this.getWritableDatabase();

        db.update(TABLE_MED_FRIEND, values, COLUMN_MAP_ID + "=?", new String[]{String.valueOf(mapMedFriend._map_id)});
        db.close();
    }

    public boolean deleteMapMedFriend(int mapID) {

        SQLiteDatabase db = this.getWritableDatabase();

        String whereClause = COLUMN_MED_FRIEND_ID + "=?";
        String[] whereArgs = new String[] { String.valueOf(mapID) };
        int result = db.delete(COLUMN_MAP_ID, whereClause, whereArgs);
        return result >= 0;
    }




    ////////////--------------MEDI FRIEND ADD, UPDATE, DELETE ---------------////////////
    public long addMedFriend(MedFriend medFriend) {

        ContentValues values = new ContentValues();
        values.put(COLUMN_MED_FRIEND_ID, medFriend._medFriendID);
        values.put(COLUMN_USERID, medFriend._userID);
        values.put(COLUMN_FRIEND_NAME, medFriend._friendName);
        values.put(COLUMN_FRIEND_EMAIL, medFriend._friendEmail);
        values.put(COLUMN_FRIEND_PHONE, medFriend._friendMobile);
        values.put(COLUMN_MED_MISS_COUNT, medFriend._missCount);
        values.put(COLUMN_MED_SKIP_COUNT, medFriend._skipCount);
        values.put(COLUMN_EXTRA_1, medFriend.extra_1);
        values.put(COLUMN_EXTRA_2, medFriend.extra_2);
        values.put(COLUMN_EXTRA_3, medFriend.extra_3);
        values.put(COLUMN_EXTRA_4, medFriend.extra_4);
        values.put(COLUMN_EXTRA_5, medFriend.extra_5);


        SQLiteDatabase db = this.getWritableDatabase();

        long frienddid = db.insert(TABLE_MED_FRIEND, null, values);

        Log.d("DBHANDLER"," medFriend id : "+frienddid);

        db.close();
        Log.e("DBHANDLER", values.toString());

        return frienddid;
    }

    public void updateMedFriend(MedFriend medFriend) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_MED_FRIEND_ID, medFriend._medFriendID);
        values.put(COLUMN_USERID, medFriend._userID);
        values.put(COLUMN_FRIEND_NAME, medFriend._friendName);
        values.put(COLUMN_FRIEND_EMAIL, medFriend._friendEmail);
        values.put(COLUMN_FRIEND_PHONE, medFriend._friendMobile);
        values.put(COLUMN_MED_MISS_COUNT, medFriend._missCount);
        values.put(COLUMN_MED_SKIP_COUNT, medFriend._skipCount);
        values.put(COLUMN_EXTRA_1, medFriend.extra_1);
        values.put(COLUMN_EXTRA_2, medFriend.extra_2);
        values.put(COLUMN_EXTRA_3, medFriend.extra_3);
        values.put(COLUMN_EXTRA_4, medFriend.extra_4);
        values.put(COLUMN_EXTRA_5, medFriend.extra_5);
        SQLiteDatabase db = this.getWritableDatabase();

        db.update(TABLE_MED_FRIEND, values, COLUMN_FRIEND_LOCAL_ID + "=?", new String[]{String.valueOf(medFriend._local_id)});
        db.close();
    }

    public boolean deleteMedFriend(int medFriendID) {

        SQLiteDatabase db = this.getWritableDatabase();

        String whereClause = COLUMN_MED_FRIEND_ID + "=?";
        String[] whereArgs = new String[] { String.valueOf(medFriendID) };
        int result = db.delete(TABLE_MED_FRIEND, whereClause, whereArgs);

        return result >= 0;
    }
    public boolean findMedFriendFromEmailPhone(String phone) {   //String email, String phone

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_MED_FRIEND, new String[]{COLUMN_FRIEND_LOCAL_ID, COLUMN_MED_FRIEND_ID, COLUMN_USERID,
                        COLUMN_FRIEND_NAME, COLUMN_FRIEND_EMAIL, COLUMN_FRIEND_PHONE, COLUMN_FRIEND_UPDATED_DATE,
                        COLUMN_MED_MISS_COUNT, COLUMN_MED_SKIP_COUNT, COLUMN_EXTRA_1, COLUMN_EXTRA_2, COLUMN_EXTRA_3,
                        COLUMN_EXTRA_4, COLUMN_EXTRA_5},
                COLUMN_FRIEND_EMAIL + " =?", new String[]{phone}, null, null, null);   // +" OR "+ COLUMN_FRIEND_PHONE +" =?"
        MedFriend medFriend;
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                medFriend = new MedFriend();
                medFriend._local_id = cursor.getInt(cursor.getColumnIndex(COLUMN_FRIEND_LOCAL_ID));
                medFriend._medFriendID = cursor.getInt(cursor.getColumnIndex(COLUMN_MED_FRIEND_ID));
                medFriend._userID = cursor.getInt(cursor.getColumnIndex(COLUMN_USERID));
                medFriend._friendName = cursor.getString(cursor.getColumnIndex(COLUMN_FRIEND_NAME));
                medFriend._friendEmail = cursor.getString(cursor.getColumnIndex(COLUMN_FRIEND_EMAIL));
                medFriend._friendMobile = cursor.getString(cursor.getColumnIndex(COLUMN_FRIEND_PHONE));
                medFriend._missCount = cursor.getInt(cursor.getColumnIndex(COLUMN_MED_MISS_COUNT));
                medFriend._skipCount = cursor.getInt(cursor.getColumnIndex(COLUMN_MED_SKIP_COUNT));
                medFriend.extra_1 = cursor.getString(cursor.getColumnIndex(COLUMN_EXTRA_1));
                medFriend.extra_2 = cursor.getString(cursor.getColumnIndex(COLUMN_EXTRA_2));
                medFriend.extra_3 = cursor.getString(cursor.getColumnIndex(COLUMN_EXTRA_3));
                medFriend.extra_4 = cursor.getString(cursor.getColumnIndex(COLUMN_EXTRA_4));
                medFriend.extra_5 = cursor.getString(cursor.getColumnIndex(COLUMN_EXTRA_5));
                cursor.close();

            } else {
                medFriend = null;
            }
        } else {
            medFriend = null;
        }

        db.close();
        return medFriend != null;
    }
    public ArrayList<MedFriend> getAllMedFriends(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_MED_FRIEND, new String[]{COLUMN_FRIEND_LOCAL_ID, COLUMN_MED_FRIEND_ID, COLUMN_USERID,
                        COLUMN_FRIEND_NAME, COLUMN_FRIEND_EMAIL, COLUMN_FRIEND_PHONE, COLUMN_FRIEND_UPDATED_DATE,
                        COLUMN_MED_MISS_COUNT, COLUMN_MED_SKIP_COUNT, COLUMN_EXTRA_1, COLUMN_EXTRA_2, COLUMN_EXTRA_3,
                        COLUMN_EXTRA_4, COLUMN_EXTRA_5},
                null, null, null, null, null);
        //= db.rawQuery(query, null);
        //int reminID;
        ArrayList<MedFriend> friends = new ArrayList<>();
        MedFriend medFriend;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            medFriend = new MedFriend();
            medFriend._local_id = cursor.getInt(cursor.getColumnIndex(COLUMN_FRIEND_LOCAL_ID));
            medFriend._medFriendID = cursor.getInt(cursor.getColumnIndex(COLUMN_MED_FRIEND_ID));
            medFriend._userID = cursor.getInt(cursor.getColumnIndex(COLUMN_USERID));
            medFriend._friendName = cursor.getString(cursor.getColumnIndex(COLUMN_FRIEND_NAME));
            medFriend._friendEmail = cursor.getString(cursor.getColumnIndex(COLUMN_FRIEND_EMAIL));
            medFriend._friendMobile = cursor.getString(cursor.getColumnIndex(COLUMN_FRIEND_PHONE));
            medFriend._missCount = cursor.getInt(cursor.getColumnIndex(COLUMN_MED_MISS_COUNT));
            medFriend._skipCount = cursor.getInt(cursor.getColumnIndex(COLUMN_MED_SKIP_COUNT));
            medFriend.extra_1 = cursor.getString(cursor.getColumnIndex(COLUMN_EXTRA_1));
            medFriend.extra_2 = cursor.getString(cursor.getColumnIndex(COLUMN_EXTRA_2));
            medFriend.extra_3 = cursor.getString(cursor.getColumnIndex(COLUMN_EXTRA_3));
            medFriend.extra_4 = cursor.getString(cursor.getColumnIndex(COLUMN_EXTRA_4));
            medFriend.extra_5 = cursor.getString(cursor.getColumnIndex(COLUMN_EXTRA_5));
            friends.add(medFriend);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return friends;
    }


    public ArrayList<MedFriend> getAllMedFriendsForMedID(int medID) {
        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.query(TABLE_MED_FRIEND+" JOIN "+TABLE_MED_FRIEND_MAP, new String[]{COLUMN_FRIEND_LOCAL_ID, COLUMN_MED_FRIEND_ID, COLUMN_USERID,
//                        COLUMN_FRIEND_NAME, COLUMN_FRIEND_EMAIL, COLUMN_FRIEND_PHONE, COLUMN_FRIEND_UPDATED_DATE,
//                        COLUMN_MED_MISS_COUNT, COLUMN_MED_SKIP_COUNT, COLUMN_EXTRA_1, COLUMN_EXTRA_2, COLUMN_EXTRA_3,
//                        COLUMN_EXTRA_4, COLUMN_EXTRA_5},
//                COLUMN_MED_FRIEND_ID + " =?", new String[]{String.valueOf(medID)}, null, null, null);
        String query2 = "Select "+TABLE_MED_FRIEND+".* FROM " + TABLE_MED_FRIEND +" JOIN "+TABLE_MED_FRIEND_MAP+" ON "
                +TABLE_MED_FRIEND_MAP+"."+COLUMN_MED_FRIEND_ID+"="+TABLE_MED_FRIEND+"."+COLUMN_MED_FRIEND_ID
                +" WHERE " + COLUMN_MEDID + " =  \"" + medID + "\"";
        Cursor cursor = db.rawQuery(query2, null);
        ArrayList<MedFriend> friends = new ArrayList<>();
        MedFriend medFriend;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            medFriend = new MedFriend();
            medFriend._local_id = cursor.getInt(cursor.getColumnIndex(COLUMN_FRIEND_LOCAL_ID));
            medFriend._medFriendID = cursor.getInt(cursor.getColumnIndex(COLUMN_MED_FRIEND_ID));
            medFriend._userID = cursor.getInt(cursor.getColumnIndex(COLUMN_USERID));
            medFriend._friendName = cursor.getString(cursor.getColumnIndex(COLUMN_FRIEND_NAME));
            medFriend._friendEmail = cursor.getString(cursor.getColumnIndex(COLUMN_FRIEND_EMAIL));
            medFriend._friendMobile = cursor.getString(cursor.getColumnIndex(COLUMN_FRIEND_PHONE));
            medFriend._missCount = cursor.getInt(cursor.getColumnIndex(COLUMN_MED_MISS_COUNT));
            medFriend._skipCount = cursor.getInt(cursor.getColumnIndex(COLUMN_MED_SKIP_COUNT));
            medFriend.extra_1 = cursor.getString(cursor.getColumnIndex(COLUMN_EXTRA_1));
            medFriend.extra_2 = cursor.getString(cursor.getColumnIndex(COLUMN_EXTRA_2));
            medFriend.extra_3 = cursor.getString(cursor.getColumnIndex(COLUMN_EXTRA_3));
            medFriend.extra_4 = cursor.getString(cursor.getColumnIndex(COLUMN_EXTRA_4));
            medFriend.extra_5 = cursor.getString(cursor.getColumnIndex(COLUMN_EXTRA_5));
            friends.add(medFriend);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return friends;
    }

    public ArrayList<Medicine> getAllPatientList(String filter, String filterName) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor;
        if (filter.equals("p")) {
            if (filterName.equals(""))
//                cursor = db.query(true, TABLE_MEDICINES, new String[]{COLUMN_MEDID, COLUMN_MEDNAME,
//                            COLUMN_PATIENT_NAME,
//                            COLUMN_DOSE, COLUMN_UNIT},
//                    null, null, COLUMN_PATIENT_NAME, null, null, null);
            cursor = db.rawQuery("Select medicines.med_id," + COLUMN_MEDNAME + "," + COLUMN_DOSE + "," + COLUMN_UNIT + "," + COLUMN_PATIENT_NAME +
                    " FROM " +TABLE_REMINDERS+" JOIN "+ TABLE_MEDICINES+" ON medicines.med_id=reminders.med_id" + " GROUP BY " + COLUMN_PATIENT_NAME, null);
            else
            cursor = db.rawQuery("Select medicines.med_id," + COLUMN_MEDNAME + "," + COLUMN_DOSE + "," + COLUMN_UNIT + "," + COLUMN_PATIENT_NAME +
                    " FROM " +TABLE_REMINDERS+" JOIN "+ TABLE_MEDICINES+" ON medicines.med_id=reminders.med_id" + " WHERE "+COLUMN_MEDNAME+"=\""+filterName+"\" GROUP BY " + COLUMN_PATIENT_NAME, null);
//            else
//                cursor = db.query(true, TABLE_MEDICINES, new String[]{COLUMN_MEDID, COLUMN_MEDNAME,
//                                COLUMN_PATIENT_NAME,
//                                COLUMN_DOSE, COLUMN_UNIT},
//                        COLUMN_MEDNAME+"=?", new String[]{filterName}, COLUMN_PATIENT_NAME, null, null, null);
        }
        else {
            if (filterName.equals(""))
                cursor = db.rawQuery("Select medicines.med_id," + COLUMN_MEDNAME + "," + COLUMN_DOSE + "," + COLUMN_UNIT + "," + COLUMN_PATIENT_NAME +
                    " FROM " +TABLE_REMINDERS+" JOIN "+ TABLE_MEDICINES+" ON medicines.med_id=reminders.med_id" + " GROUP BY " + COLUMN_MEDNAME + "," + COLUMN_DOSE, null);
            else
                cursor = db.rawQuery("Select medicines.med_id," + COLUMN_MEDNAME + "," + COLUMN_DOSE + "," + COLUMN_UNIT + "," + COLUMN_PATIENT_NAME +
                    " FROM " +TABLE_REMINDERS+" JOIN "+ TABLE_MEDICINES+" ON medicines.med_id=reminders.med_id" + " WHERE "+COLUMN_PATIENT_NAME+"=\""+filterName+"\" GROUP BY " + COLUMN_MEDNAME + "," + COLUMN_DOSE, null);
        }
        ArrayList<Medicine> patients = new ArrayList<>();
        Medicine medicine;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            medicine = new Medicine();
            medicine.set_medID(cursor.getInt(cursor.getColumnIndex(COLUMN_MEDID)));
            medicine.set_medName(cursor.getString(cursor.getColumnIndex(COLUMN_MEDNAME)));
            //medicine.set_shape(cursor.getInt(cursor.getColumnIndex(COLUMN_SHAPE)));
            //medicine.set_color1(cursor.getInt(cursor.getColumnIndex(COLUMN_COLOR1)));
            //medicine.set_color2(cursor.getInt(cursor.getColumnIndex(COLUMN_COLOR2)));
            medicine.set_patientName(cursor.getString(cursor.getColumnIndex(COLUMN_PATIENT_NAME)));
           // medicine.set_startDate(cursor.getString(cursor.getColumnIndex(COLUMN_STARTDATE)));
           // medicine.set_endDate(cursor.getString(cursor.getColumnIndex(COLUMN_ENDDATE)));
            medicine.set_dose(cursor.getDouble(cursor.getColumnIndex(COLUMN_DOSE)));
            medicine.set_unit(cursor.getString(cursor.getColumnIndex(COLUMN_UNIT)));
            //medicine.updateDeleteFlag = cursor.getString(cursor.getColumnIndex(COLUMN_UPDATE_DELETE_FLAG));
            //medicine.updateDeleteDate = cursor.getString(cursor.getColumnIndex(COLUMN_UPDATE_DELETE_DATE));
            patients.add(medicine);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return patients;
    }
}
