package com.healthsaverz.healthmobile.medicine.global;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.medicine.modal.AlarmReceiver;
import com.healthsaverz.healthmobile.medicine.medicine.modal.ReminderModal;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by priyankranka on 07/11/14.
 */
public class AppConstants {
    //http://192.168.2.107:8080/sHealthSaverz/jaxrs/LoginServices/login/healthsaverz/RanjeetGaikwad/12345678
    public static final String WEB_DOMAIN = "http://180.179.227.12:8080";  //http://180.149.247.137:8080  http://180.179.227.12:8080
    public static final int SCHEDULER_OFFSET = -10000;
    public static final int SCHEDULER_TYPE_30 = 30;
    public static final int SCHEDULER_TYPE_MIDNIGHT = -200;
    public static final int NOTIFICATION_TYPE_MIDNIGHT = 24;
    public static final int TIME_DIFFERENCE_MINUTES = 1800000; //for 30 minutes
    public static final DecimalFormat mFormat= new DecimalFormat("00");
    public static final String call_us = "022-6169-9494";
    public static final String email_us = "app@healthsaverz.com ";
    public static final String f_a_q = "http://www.healthsaverz.com/FAQ/index.asp";
    public static final String website = "http://www.healthsaverz.com";
    public static final String facebook = "https://www.facebook.com/healthsaverz";
    public static final String CrittercismAppID = "55067ea4e0697fa44963772d";
    public static final String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec" };

    public static String getDeviceToken(){
        return "DEVICE_TOKEN";
    }
    public static String osVersion(){
        return android.os.Build.VERSION.RELEASE + "("+android.os.Build.VERSION.SDK_INT+")";
    }
    public static String deviceName(){
        return Build.MANUFACTURER +" "+ android.os.Build.MODEL;
    }
    public static String ConnectionError(AsyncLoaderNew.Status status){


        if(status ==  AsyncLoaderNew.Status.IDEAL)
        {
            return "No Internet Connection.";
        }
        else if(status ==  AsyncLoaderNew.Status.ERROR)
        {
            return "No Internet Connection.";
        }
        else if(status ==  AsyncLoaderNew.Status.HTTP_404)
        {
            return "Page not found, 404 Error.";
        }
        else if(status ==  AsyncLoaderNew.Status.MALFORMED_URL)
        {
            return "Unsupported request";
        }
        else if(status ==  AsyncLoaderNew.Status.URISYNTAX_ERROR)
        {
            return "Unsupported request";
        }
        else if(status ==  AsyncLoaderNew.Status.UNSUPPORTED_ENCODING)
        {
            return "Unsupported device";
        }
        else if(status ==  AsyncLoaderNew.Status.ABRUPT)
        {
            return "Connection Interrupted.";
        }

        return "Connection Error: General Error.";
    }

    public static boolean cancelNotificationScheduler(Context context, ReminderModal reminderModal1, int notificationType) {
        if (reminderModal1 != null){
            Intent intent = new Intent(context, AlarmReceiver.class);
            //intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            intent.putExtra("medicine_id",reminderModal1.get_medID());
            intent.putExtra("reminder_id",reminderModal1.get_remindID());
            intent.putExtra("notificationtype",notificationType);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, reminderModal1.get_remindID()+ SCHEDULER_OFFSET,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pendingIntent);
            pendingIntent.cancel();
            return true;
        }
        return false;
    }
    public static boolean createNotificationScheduler(Context context, ReminderModal reminderModal, int notificationType) {
        if (reminderModal != null){
            Intent intent1 = new Intent(context, AlarmReceiver.class);
            //intent1.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            intent1.putExtra("medicine_id",reminderModal.get_medID());
            intent1.putExtra("reminder_id",reminderModal.get_remindID());
            intent1.putExtra("notificationtype",notificationType);
            PendingIntent pendingIntentScheduler = PendingIntent.getBroadcast(context, reminderModal.get_remindID()+SCHEDULER_OFFSET,
                    intent1, PendingIntent.FLAG_UPDATE_CURRENT);

            Calendar calendar3 = new GregorianCalendar();
            calendar3.setTimeInMillis(Long.parseLong(reminderModal.get_time()));
            if (reminderModal._snooze != -1){
                calendar3.add(Calendar.MINUTE, reminderModal._snooze + SCHEDULER_TYPE_30);
            }
            else {
                calendar3.add(Calendar.MINUTE, SCHEDULER_TYPE_30);
            }

            //min = calendar3.get(Calendar.MINUTE);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pendingIntentScheduler);
            if (calendar3.after(Calendar.getInstance())){
                if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    alarmManager.set(AlarmManager.RTC_WAKEUP, calendar3.getTimeInMillis(),pendingIntentScheduler);
                } else {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar3.getTimeInMillis(),pendingIntentScheduler);
                }
            }
            return true;
        }
        return false;
    }

    public static Calendar getCurrentCalendar() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    public static void setScheduleMidNightNotification(Context context, Calendar midNightCal) {

        Intent intent12 = new Intent(context, AlarmReceiver.class);
        intent12.putExtra("notificationtype",AppConstants.NOTIFICATION_TYPE_MIDNIGHT);
        intent12.putExtra("MidCalendar", midNightCal);
        /*
        Here notificationtype  NOTIFICATION_TYPE_MIDNIGHT  says it is the modnight scheduler to recreate all the future reminder.
         */
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), AppConstants.SCHEDULER_TYPE_MIDNIGHT, intent12, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        Long time = midNightCal.getTimeInMillis();
        Long interval = (long) 86400000; //24 hours in milliseconds
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, midNightCal.getTimeInMillis(), interval, pendingIntent);
//        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
//
//        } else {
//            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, midNightCal.getTimeInMillis(), pendingIntent);
//        }
    }
}
