package com.healthsaverz.healthmobile.medicine.reminder.modal;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;

/**
 * Created by Ibrahim on 17-10-2014.
 */
public class DurationScheduleDialog extends DialogFragment implements View.OnTouchListener {

    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String TAG = "ScheduleDaysDialog";
    private Context context;

    DaysListener daysListener;
    private String code;



    public interface DaysListener {
        void onDayCountChange(String quantity);
    }

    private ImageView minus;
    public ImageView getMinus() {
        if (minus == null) {
            minus = (ImageView) getView().findViewById(R.id.minus);

        }
        return minus;
    }

    private ImageView plus;
    public ImageView getPlus() {
        if (plus == null) {
            plus = (ImageView) getView().findViewById(R.id.plus);

        }
        return plus;
    }

    private EditText quantityText;
    public EditText getQuantityText() {
        if (quantityText == null) {
            quantityText = (EditText) getView().findViewById(R.id.quantityText);

        }
        return quantityText;
    }

    private Button setButton;
    public Button getVerifyButton() {
        if (setButton == null) {
            setButton = (Button) getView().findViewById(R.id.setButton);
            setButton.setOnTouchListener(this);
        }
        return setButton;
    }

    private Button cancelButton;
    public Button getCancelButton() {
        if (cancelButton == null) {
            cancelButton = (Button) getView().findViewById(R.id.cancelButton);
            cancelButton.setOnTouchListener(this);
        }
        return cancelButton;
    }

    public static DurationScheduleDialog newInstance(DaysListener timeSetlistener){
        DurationScheduleDialog scheduleDaysDialog = new DurationScheduleDialog();
        scheduleDaysDialog.daysListener = timeSetlistener;
        return scheduleDaysDialog;
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dialogue_schedule_days, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getVerifyButton();
        getCancelButton();
        code = getQuantityText().getText().toString().trim();
        getPlus().setOnTouchListener(this);
        getMinus().setOnTouchListener(this);
        getQuantityText().setText("");
        getQuantityText().append("30");
        getQuantityText().requestFocus();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_SmileWindow;
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return dialog;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(view, null);
            return true;
        } else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
            final int viewID = view.getId();
            ViewHelper.fadeIn(view, null);
            if (quantityText.getText().toString().equals("") || quantityText.getText().toString().equals("0"))
            {
                quantityText.setText("1");
            }
            switch (view.getId()){
                case R.id.plus:
                    int add = Integer.parseInt((quantityText.getText().toString()));
                    add = add + 1;
                    quantityText.setText("");
                    quantityText.append(String.valueOf(add));
                    break;
                case R.id.minus:
                    int sub = Integer.parseInt((quantityText.getText().toString()));
                    if (sub > 1){
                        sub = sub - 1;
                        quantityText.setText("");
                        quantityText.append(String.valueOf(sub));
                    }
                    break;
                case R.id.setButton:
                    if (daysListener != null){
                        daysListener.onDayCountChange(quantityText.getText().toString());
                    }
                    dismiss();
                    return true;
                case R.id.cancelButton:
                    if (daysListener != null){
                        daysListener.onDayCountChange(null);
                    }
                    dismiss();
                    return true;

            }

        }
        return false;
    }


}
