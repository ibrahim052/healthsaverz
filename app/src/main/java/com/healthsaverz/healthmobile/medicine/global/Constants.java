package com.healthsaverz.healthmobile.medicine.global;

/**
 * Created by priyankranka on 24/03/14.
 */
public class Constants {

    //Constants need to configure various fragments depending upon the screeen type
    public static final String SCREEN_TYPE = "screentype";

    public static final int SCREEN_NOT_DEFINED = 0;
    public static final int COLLECTION_CATEGORY_PAGE = 0;
    public static final int ABOUT_V_WALK = 1;
    public static final int CONTACT_V_WALK = 2;
    public static final int ABOUT_DEVELOPER = 3;
    public static final int CATEGORY_PAGE = 4;
    public static final int CATEGORY_DETAIL_PAGE = 5;

    //Constants related for inflating the grid view depending upon the grid type
    public static final String GRID_TYPE = "gridtype";

    public static final int GRID_CATEGORY = 0;
    public static final int GRID_PRODUCT_THUMBNAILS = 1;

    //Constants needs to pass the values from one Activity to another
    public static final String CATEGORY_ID = "categoryid";
    public static final String CATEGORY_NAME = "categoryname";
    public static final String CATEGORY_PRODUCT_LIST = "categoryproductlist";
    public static final String PRODUCT_NAME = "productname";
    public static final String PRODUCT_INDEX = "productindex";

}
