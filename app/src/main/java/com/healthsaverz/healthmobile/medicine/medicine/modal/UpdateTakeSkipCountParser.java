package com.healthsaverz.healthmobile.medicine.medicine.modal;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by DELL PC on 2/14/2015.
 */
public class UpdateTakeSkipCountParser extends AsyncTask<String,Void,String> implements AsyncLoaderNew.Listener {

    /*
    Start from addMedicine method
    Takes String as input and out is based on addMedicineParserListener
     */

    private AsyncLoaderNew asyncLoaderNew;
    private Context context;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public UpdateTakeSkipCountParser(Context context){

        this.context = context;
    }

    private static final String METHOD_UPDATE_TASK = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/ReminderServices/updateReminder2";

    public void updateReminder(ReminderModal reminderModal, String medName, double dose, float pills){
        asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.POST_HEADER);
        String sessionID = PreferenceManager.getStringForKey(context, PreferenceManager.SESSION_ID, "");
        String clientID = PreferenceManager.getStringForKey(context, PreferenceManager.CLIENT_ID, "");
        String userId = PreferenceManager.getStringForKey(context, PreferenceManager.ID, "");


        String requestXML = "<reminder>" +
                        "<sessionID>"+sessionID+"</sessionID>" +
                        "<clientID>"+clientID+"</clientID>" +
                        "<userId>"+userId+"</userId>" +
                        "<medicineName>"+medName+"</medicineName>" +
                        "<dose>"+dose+"</dose> " +
                        "<quantity>"+reminderModal.get_quantity()+"</quantity>" +
                        "<time>"+(reminderModal.get_time().equals("")?"":sdf.format(Long.parseLong(reminderModal.get_time())))+"</time>" +
                        "<quantityType>"+reminderModal.get_qty_type()+"</quantityType>" +
                        "<reminderStatus>"+reminderModal._taken+"</reminderStatus>" +
                        "<takenTime>"+(reminderModal._taken_time.equals("")?"":sdf.format(Long.parseLong(reminderModal._taken_time)))+"</takenTime>" +
                        "<snoozed>"+reminderModal._snooze+"</snoozed>" +
                        "<snoozedTime></snoozedTime>" +
                        "<pills>"+pills+"</pills>" +
                        "<status>A</status>" +
                        "<createdBy>"+userId+"</createdBy>" +
                        "<extra_1></extra_1>" +
                        "<extra_2></extra_2>" +
                        "<extra_3></extra_3>" +
                        "<extra_4></extra_4>" +
                        "<extra_5></extra_5>" +
                        "</reminder>";
        Log.d("requestXML ", "requestXML " + requestXML);
        asyncLoaderNew.requestMessage   =   requestXML;
        asyncLoaderNew.executeRequest(METHOD_UPDATE_TASK);
        asyncLoaderNew.setListener(this);

    }


    //AsyncLoaderNew.Listener method implementation
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {


    }

    @Override
    public void didReceivedData(byte[] data) {

        String responseString = new String(data);
        Log.d("addMedicineParser","didReceivedData is "+responseString);
        execute(responseString);
    }

    @Override
    public void didReceivedProgress(Double progress) {
    }


    //Current Thread Stack
    @Override
    protected String doInBackground(String... params) {
        return null;
    }
}
