package com.healthsaverz.healthmobile.medicine.medicineSearch.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DG on 10/11/2014.
 */
public class SubstituteSearchResponse implements Parcelable {

    public static final String SessionID = "sessionID";
    public static final String ClientID = "clientID";
    public static final String Message = "message";
    public static final String MoleculeTypeName = "moleculeTypeName";
    public static final String ProductTypeName        = "productTypeName";
    public static final String Description = "description";
    public static final String Status = "status";
    public static final String Strength = "strength";
    public static final String StrengthUnit = "strengthUnit";
    public static final String ProductPackagingId = "productPackagingId";
    public static final String ManufacturerTagName        = "manufacturerTagName";
    public static final String MoleculeTypeId = "moleculeTypeId";
    public static final String ManufacturerTag        = "manufacturerTag";
    public static final String ManufacturingDate        = "manufacturingDate";
    public static final String PackagingSizeName        = "packagingSizeName";
    public static final String ManfacuterName = "manfacuterName";
    public static final String StrengthUnitName        = "strengthUnitName";
    public static final String ProductPackagingName = "productPackagingName";
    public static final String PackagingUnitName        = "packagingUnitName";
    public static final String ThumbnailImageURL = "thumbnailImageURL";
    public static final String MetaDescription = "metaDescription";
    public static final String ProductCategoryId = "productCategoryId";
    public static final String SellingPricePercentage = "sellingPricePercentage";
    public static final String DiscountPricePercentage = "discountPricePercentage";
    public static final String BrandsId  = "brandsId" ;
    public static final String GroupId = "groupId";
    public static final String Mrp = "mrp";
    public static final String NelmtagName = "nelmtagName";
    public static final String India_Or_MNC = "india_Or_MNC";
    public static final String Nelmtag = "nelmtag";
    public static final String India_MNC = "india_MNC";
    public static final String ExpiryDate = "expiryDate";
    public static final String BatchNumber = "batchNumber";
    public static final String  PackagingSize	= "packagingSize";
    public static final String PackagingUnit = "packagingUnit";
    public static final String Icon = "icon";
    public static final String ManfactureId = "manfactureId";
    public static final String SubGroupId = "subGroupId";
    public static final String DiseaseTypeId = "diseaseTypeId";
    public static final String BrandsName = "brandsName";
    public static final String StrengthName        = "strengthName";
    public static final String Video   = "video";
    public static final String MetaKeywords    = "metaKeywords";
    public static final String BarcodeId = "barcodeId" ;
    public static final String ProductTypeId = "productTypeId";
    public static final String WarehouseId  = "warehouseId";
    public static final String Keywords      = "keywords";
    public static final String TotalQuantity = "totalQuantity";
    public static final String SellingPrice  ="sellingPrice";
    public static final String DiscountPrice = "discountPrice" ;
    public static final String ItemType = "itemType";
    public static final String SalesTax  = "salesTax" ;
    public static final String PurchaseTax        = "purchaseTax";
    public static final String GenericName        = "genericName";
    public static final String Created_On        = "created_On";
    public static final String ImageURL   = "imageURL";
    public static final String Created_By  = "created_By" ;

    public static final String WarehouseName = "warehouseName";
    public static final String GroupName = "groupName";
    public static final String SubGroupName = "subGroupName";
    public static final String DiseaseTypeName = "diseaseTypeName";
    public static final String ProductId = "productId";
    public static final String IconUrl        = "iconUrl";
    public static final String IconId        = "iconId";


    //public static final String ProductSizeId = "productSizeId" ;




//    public static final String ProductSize = "productSize";
//    public static final String Dimension = "dimension";
//    public static final String Title = "title";
//    public static final String ProductFeatureId = "productFeatureId";
//    public static final String DisplayQuantity = "displayQuantity";
//    public static final String OutOfStockFlag = "outOfStockFlag";
//    public static final String BackOrderMaxQty = "backOrderMaxQty";
//    public static final String PreOrderMaxQty = "preOrderMaxQty";
//    public static final String BackOrderDescription = "backOrderDescription";
//    public static final String PreOrderDescription = "preOrderDescription";
//    public static final String NotifyforQuantityBelow = "notifyforQuantityBelow";
//    public static final String Kind_Of_Medicine = "kind_Of_Medicine";
//    public static final String ProductColorId = "productColorId";
//   // public static final String DisplayQuantity ="displayQuantity"
//    public static final String Weight = "weight";
//    public static final String WeightType    = "weightType";
//    public static final String MinimumQty  = "minimumQty";
//    public static final String BackOrderFlag       = "backOrderFlag";
//    public static final String PreOrderFlag       = "preOrderFlag";
    public static final String Extra_1         = "extra_1";
    public static final String Extra_2         = "extra_2";
    public static final String Extra_3         = "extra_3";
    public static final String Extra_4         = "extra_4";
    public static final String Extra_5         = "extra_5";

    private String sessionID = "";
    private String clientID = "";
    private String message = "";
    private String moleculeTypeName	       = "";
    private String productTypeName	       = "";
    private String warehouseName	       = "";
    private String groupName	       = "";
    private String subGroupName	       = "";
    private String diseaseTypeName	       = "";
    private String description	       = "";
    private String icon	       = "";
    private String status = "";
    private String itemType  = "";
    private String manfacuterName	       = "";
    private String imageURL = "";
    private int created_By   ;
    private int brandsId	    ;
    private int productPackagingId	    ;
    private String manufacturerTagName	       = "";
    private int moleculeTypeId	       ;
    private int logo;
    private int manufacturerTag;
    private String manufacturingDate = "";
    private String packagingSizeName = "";
    private String strengthUnitName  = "";
    private String productPackagingName   = "";
    private String packagingUnitName    = "";
    private String thumbnailImageURL   = "";
    private String metaDescription   = "";
    private int productCategoryId   ;
    public double sellingPricePercentage  ;
    public double discountPricePercentage  ;
    private int strength    ;
    private int strengthUnit    ;
    private int productId  ;
    private int totalQuantity   ;
    private double mrp  ;
    private String nelmtagName     = "";
    private String india_Or_MNC    = "";
    private int nelmtag     ;
    private int india_MNC     ;
    private String expiryDate   = "";
    private String batchNumber   = "";
    private int packagingSize  ;
    private int packagingUnit  ;
    private int manfactureId  ;
    private int groupId  ;
    private int subGroupId  ;
    private int diseaseTypeId  ;
    private String brandsName      = "";
    private String strengthName       = "";
    private String video  = "" ;
    private String metaKeywords  = "" ;
    private int barcodeId  ;
    private int productTypeId  ;
    private int warehouseId  ;
    private String keywords  = "" ;
    private double sellingPrice   ;
    private int discountPrice  ;
    private int salesTax   ;
    private int purchaseTax   ;
    private String genericName        = "";
    private String created_On        = "";
    public String iconUrl = "";
    public int iconId ;
    private String extra_1         = "";
    private String extra_2         = "";
    private String extra_3         = "";
    private String extra_4         = "";
    private String extra_5         = "";

    public SubstituteSearchResponse() {

    }


    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMoleculeTypeName() {
        return moleculeTypeName;
    }

    public void setMoleculeTypeName(String moleculeTypeName) {
        this.moleculeTypeName = moleculeTypeName;
    }



    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getSubGroupName() {
        return subGroupName;
    }

    public void setSubGroupName(String subGroupName) {
        this.subGroupName = subGroupName;
    }

    public String getDiseaseTypeName() {
        return diseaseTypeName;
    }

    public void setDiseaseTypeName(String diseaseTypeName) {
        this.diseaseTypeName = diseaseTypeName;
    }

    public String getProductTypeName() {
        return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getManfacuterName() {
        return manfacuterName;
    }

    public void setManfacuterName(String manfacuterName) {
        this.manfacuterName = manfacuterName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public int getCreated_By() {
        return created_By;
    }

    public void setCreated_By(int created_By) {
        this.created_By = created_By;
    }

    public int getBrandsId() {
        return brandsId;
    }

    public void setBrandsId(int brandsId) {
        this.brandsId = brandsId;
    }

    public int getProductPackagingId() {
        return productPackagingId;
    }

    public void setProductPackagingId(int productPackagingId) {
        this.productPackagingId = productPackagingId;
    }

    public String getManufacturerTagName() {
        return manufacturerTagName;
    }

    public void setManufacturerTagName(String manufacturerTagName) {
        this.manufacturerTagName = manufacturerTagName;
    }

    public int getMoleculeTypeId() {
        return moleculeTypeId;
    }

    public void setMoleculeTypeId(int moleculeTypeId) {
        this.moleculeTypeId = moleculeTypeId;
    }

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    public int getManufacturerTag() {
        return manufacturerTag;
    }

    public void setManufacturerTag(int manufacturerTag) {
        this.manufacturerTag = manufacturerTag;
    }

    public String getManufacturingDate() {
        return manufacturingDate;
    }

    public void setManufacturingDate(String manufacturingDate) {
        this.manufacturingDate = manufacturingDate;
    }

    public String getPackagingSizeName() {
        return packagingSizeName;
    }

    public void setPackagingSizeName(String packagingSizeName) {
        this.packagingSizeName = packagingSizeName;
    }

    public String getStrengthUnitName() {
        return strengthUnitName;
    }

    public void setStrengthUnitName(String strengthUnitName) {
        this.strengthUnitName = strengthUnitName;
    }

    public String getProductPackagingName() {
        return productPackagingName;
    }

    public void setProductPackagingName(String productPackagingName) {
        this.productPackagingName = productPackagingName;
    }

    public String getPackagingUnitName() {
        return packagingUnitName;
    }

    public void setPackagingUnitName(String packagingUnitName) {
        this.packagingUnitName = packagingUnitName;
    }

    public String getThumbnailImageURL() {
        return thumbnailImageURL;
    }

    public void setThumbnailImageURL(String thumbnailImageURL) {
        this.thumbnailImageURL = thumbnailImageURL;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public int getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(int productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public double getSellingPricePercentage() {
        return sellingPricePercentage;
    }

    public void setSellingPricePercentage(double sellingPricePercentage) {
        this.sellingPricePercentage = sellingPricePercentage;
    }

    public double getDiscountPricePercentage() {
        return discountPricePercentage;
    }

    public void setDiscountPricePercentage(double discountPricePercentage) {
        this.discountPricePercentage = discountPricePercentage;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getStrengthUnit() {
        return strengthUnit;
    }

    public void setStrengthUnit(int strengthUnit) {
        this.strengthUnit = strengthUnit;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public double getMrp() {
        return mrp;
    }

    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    public String getNelmtagName() {
        return nelmtagName;
    }

    public void setNelmtagName(String nelmtagName) {
        this.nelmtagName = nelmtagName;
    }

    public String getIndia_Or_MNC() {
        return india_Or_MNC;
    }

    public void setIndia_Or_MNC(String india_Or_MNC) {
        this.india_Or_MNC = india_Or_MNC;
    }

    public int getNelmtag() {
        return nelmtag;
    }

    public void setNelmtag(int nelmtag) {
        this.nelmtag = nelmtag;
    }

    public int getIndia_MNC() {
        return india_MNC;
    }

    public void setIndia_MNC(int india_MNC) {
        this.india_MNC = india_MNC;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public int getPackagingSize() {
        return packagingSize;
    }

    public void setPackagingSize(int packagingSize) {
        this.packagingSize = packagingSize;
    }

    public int getPackagingUnit() {
        return packagingUnit;
    }

    public void setPackagingUnit(int packagingUnit) {
        this.packagingUnit = packagingUnit;
    }

    public int getManfactureId() {
        return manfactureId;
    }

    public void setManfactureId(int manfactureId) {
        this.manfactureId = manfactureId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getSubGroupId() {
        return subGroupId;
    }

    public void setSubGroupId(int subGroupId) {
        this.subGroupId = subGroupId;
    }

    public int getDiseaseTypeId() {
        return diseaseTypeId;
    }

    public void setDiseaseTypeId(int diseaseTypeId) {
        this.diseaseTypeId = diseaseTypeId;
    }

    public String getBrandsName() {
        return brandsName;
    }

    public void setBrandsName(String brandsName) {
        this.brandsName = brandsName;
    }

    public String getStrengthName() {
        return strengthName;
    }

    public void setStrengthName(String strengthName) {
        this.strengthName = strengthName;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public int getBarcodeId() {
        return barcodeId;
    }

    public void setBarcodeId(int barcodeId) {
        this.barcodeId = barcodeId;
    }

    public int getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(int productTypeId) {
        this.productTypeId = productTypeId;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public int getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(int discountPrice) {
        this.discountPrice = discountPrice;
    }

    public int getSalesTax() {
        return salesTax;
    }

    public void setSalesTax(int salesTax) {
        this.salesTax = salesTax;
    }

    public int getPurchaseTax() {
        return purchaseTax;
    }

    public void setPurchaseTax(int purchaseTax) {
        this.purchaseTax = purchaseTax;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getCreated_On() {
        return created_On;
    }

    public void setCreated_On(String created_On) {
        this.created_On = created_On;
    }

    public String getExtra_1() {
        return extra_1;
    }

    public void setExtra_1(String extra_1) {
        this.extra_1 = extra_1;
    }

    public String getExtra_2() {
        return extra_2;
    }

    public void setExtra_2(String extra_2) {
        this.extra_2 = extra_2;
    }

    public String getExtra_3() {
        return extra_3;
    }

    public void setExtra_3(String extra_3) {
        this.extra_3 = extra_3;
    }

    public String getExtra_4() {
        return extra_4;
    }

    public void setExtra_4(String extra_4) {
        this.extra_4 = extra_4;
    }

    public String getExtra_5() {
        return extra_5;
    }

    public void setExtra_5(String extra_5) {
        this.extra_5 = extra_5;
    }

    private SubstituteSearchResponse(Parcel in){
        sessionID = in.readString();
        clientID  = in.readString();
        message  = in.readString();
        moleculeTypeName = in.readString();
        productTypeName = in.readString();
        warehouseName = in.readString();

        groupName  = in.readString();
        subGroupName  = in.readString();
        diseaseTypeName = in.readString();
        description = in.readString();
        icon = in.readString();
        status   = in.readString();
        itemType    = in.readString();

        manfacuterName = in.readString();
        imageURL  = in.readString();
        created_By  = in.readInt();
        brandsId  = in.readInt();
        productPackagingId  = in.readInt();
        manufacturerTagName = in.readString();
        moleculeTypeId = in.readInt();
        logo = in.readInt();
        manufacturerTag  = in.readInt();
        manufacturingDate   = in.readString();
        packagingSizeName    = in.readString();
        strengthUnitName      = in.readString();
        productPackagingName = in.readString();

        packagingUnitName           = in.readString();
        metaDescription     = in.readString();
        productCategoryId      = in.readInt();
        sellingPricePercentage      = in.readInt();
        discountPricePercentage        = in.readDouble();
        strength         = in.readInt();
        strengthUnit     = in.readInt();
        productId   = in.readInt();
        totalQuantity    = in.readInt();
        mrp    = in.readDouble();
        nelmtagName       = in.readString();
        india_Or_MNC     = in.readString();
        nelmtag      = in.readInt();
        india_MNC      = in.readInt();
        expiryDate         = in.readString();
        batchNumber        = in.readString();
        packagingSize       = in.readInt();
        packagingUnit   = in.readInt();
        manfactureId   = in.readInt();
        groupId   = in.readInt();
        subGroupId    = in.readInt();
        diseaseTypeId    = in.readInt();
        brandsName       = in.readString();
        strengthName        = in.readString();
        video    = in.readString();
        metaKeywords    = in.readString();
        barcodeId   = in.readInt();
        productTypeId   = in.readInt();

        warehouseId      = in.readInt();
        keywords      = in.readString();
        sellingPrice          = in.readDouble();
        discountPrice          = in.readInt();
        salesTax       = in.readInt();
        purchaseTax       = in.readInt();
        genericName           = in.readString();
        created_On           = in.readString();
        iconUrl = in.readString();
        iconId = in.readInt();
        extra_1               = in.readString();
        extra_2               = in.readString();
        extra_3                   = in.readString();
        extra_4                   = in.readString();
        extra_5                = in.readString();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(sessionID);
        out.writeString(clientID);
        out.writeString(message);
        out.writeString(moleculeTypeName);
        out.writeString(productTypeName);
        out.writeString(description);
        out.writeString(warehouseName);
        out.writeString(groupName);
        out.writeString(subGroupName);
        out.writeString(diseaseTypeName);
        out.writeString(description);
        out.writeString(icon);
        out.writeString(status);
        out.writeString(itemType);
        out.writeString(manfacuterName);
        out.writeString(imageURL);
        out.writeInt(created_By);
        out.writeInt(manufacturerTag);
        out.writeInt(brandsId);
        out.writeInt(productPackagingId);
        out.writeString(manufacturerTagName);
        out.writeInt(moleculeTypeId);
        out.writeInt(logo);
        out.writeString(packagingUnitName);
        out.writeString(metaDescription);
        out.writeInt(productCategoryId);
        out.writeDouble(sellingPricePercentage);
        out.writeDouble(discountPricePercentage);
        out.writeInt(strength);
        out.writeInt(strengthUnit);
        out.writeInt(productId);
        out.writeInt(totalQuantity);
        out.writeDouble(mrp);
        out.writeString(nelmtagName);
        out.writeString(india_Or_MNC);
        out.writeInt(nelmtag);
        out.writeInt(india_MNC);
        out.writeString(expiryDate);
        out.writeString(batchNumber);
        out.writeInt(packagingSize);
        out.writeInt(packagingUnit);
        out.writeInt(manfactureId);
        out.writeInt(groupId);
        out.writeInt(subGroupId);
        out.writeInt(diseaseTypeId);
        out.writeString(brandsName);

        out.writeString(strengthName);
        out.writeString(video);
        out.writeString(metaKeywords);
        out.writeInt(productTypeId);
        out.writeInt(barcodeId);
        out.writeInt(warehouseId);
        out.writeString(keywords);
        out.writeDouble(sellingPrice);
        out.writeInt(discountPrice);
        out.writeInt(salesTax);

        out.writeInt(purchaseTax);
        out.writeString(genericName);
        out.writeString(created_On);
        out.writeString(created_On);
        out.writeString(keywords);
        out.writeString(iconUrl);
        out.writeInt(iconId);
        out.writeString(Extra_1);
        out.writeString(Extra_2);
        out.writeString(Extra_3);
        out.writeString(Extra_4);
        out.writeString(Extra_5);

    }

    public static final Creator<SubstituteSearchResponse> CREATOR = new Creator<SubstituteSearchResponse>() {
        public SubstituteSearchResponse createFromParcel(Parcel in) {
            return new SubstituteSearchResponse(in);
        }

        public SubstituteSearchResponse[] newArray(int size) {
            return new SubstituteSearchResponse[size];
        }
    };
}



