package com.healthsaverz.healthmobile.medicine.filters.modal;

/**
 * Created by priyankranka on 12/11/14.
 */
public class Filter {

    public int filterId; //unique id
    public String attributeValue; // Display name
    public String attributeDisplayType;// How it will be displayed
    public boolean isChecked;
}
