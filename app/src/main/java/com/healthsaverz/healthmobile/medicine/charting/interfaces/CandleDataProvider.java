package com.healthsaverz.healthmobile.medicine.charting.interfaces;

import com.healthsaverz.healthmobile.medicine.charting.data.CandleData;

public interface CandleDataProvider extends BarLineScatterCandleDataProvider {

    public CandleData getCandleData();
}
