package com.healthsaverz.healthmobile.medicine.ehr.controller;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoader;
import com.healthsaverz.healthmobile.medicine.asyncloader.ParserListener;
import com.healthsaverz.healthmobile.medicine.ehr.model.EmrParcelable;
import com.healthsaverz.healthmobile.medicine.ehr.model.GridDetailFragment;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.Parser;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;

import java.util.ArrayList;

public class EhrActivity extends FragmentActivity implements ViewPager.OnPageChangeListener, ParserListener {

    private static final String TAG = "EmrActivity";
    private int startIndex;
    private ArrayList<EmrParcelable> emrParcelables = new ArrayList<>();
    private Parser parser;
    private ViewPager viewPager;
    private SetTitleListener setTitleListener;
    private ProgressBar progressBar;

    public interface SetTitleListener {
        public void didReceivedTitle(String title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emr);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        //this.productDetailParcelables = getArguments().getParcelableArrayList(Constants.CATEGORY_PRODUCT_LIST);
        viewPager = (ViewPager) findViewById(R.id.center_detail__picture_container);
            progressBar = (ProgressBar)findViewById(R.id.progressbar);
        viewPager.setOnPageChangeListener(this);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_emr, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        super.onOptionsItemSelected(item);
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_settings:
                return true;
            case R.id.callUsButton:
                String uri = "tel:" + AppConstants.call_us.trim() ;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(uri));
                startActivity(intent);
                return true;
        }

        return false;
    }

    //ViewPager.OnPageChangeListener Implementation
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        if (this.setTitleListener != null) {
            this.setTitleListener.didReceivedTitle((emrParcelables.get(position)).created_By);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onStart() {
        super.onStart();

        if(parser != null){
            parser.cancel(true);
            parser = null;
        }

        String sessionID = PreferenceManager.getStringForKey(this, PreferenceManager.SESSION_ID, "test");

        String membarID = PreferenceManager.getStringForKey(this, PreferenceManager.ID, "-1");
        if (!membarID.equals("-1")){
            parser = new Parser(this);
            parser.setListener(this);
            parser.getEmrs(sessionID, membarID);
            //parser.executeRequest(this.imageURL);
        }

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        if(parser != null)
        {
            if(!parser.isCancelled())
            {
                parser.cancel(true);
            }
        }
    }


    @Override
    public void didReceivedError(AsyncLoader.Status errorCode) {
        if (parser != null){
            parser.cancel(true);
            parser = null;
        }
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void didReceivedData(Object object) {
        if (object != null){
            emrParcelables = (ArrayList<EmrParcelable>) object;
            viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager()));
            viewPager.setCurrentItem(startIndex);
        }
        else {
            Toast.makeText(this, "No result found", Toast.LENGTH_SHORT).show();
        }
        progressBar.setVisibility(View.GONE);
        if (parser != null){
            parser.cancel(true);
            parser = null;
        }
    }

    @Override
    public void didReceivedProgress(double progress) {

    }

    class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return GridDetailFragment.newInstance((emrParcelables.get(position)).prescriptionImageUrl, (emrParcelables.get(position)).created_On);
        }

        @Override
        public int getCount() {
            return emrParcelables.size();
        }
    }
}
