package com.healthsaverz.healthmobile.medicine.calendar;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;

import com.healthsaverz.healthmobile.medicine.R;

public class CalendarActivity extends FragmentActivity {

    private ViewPager viewPager;

    public boolean medicationDeleted = false;
    public static boolean addButtonClicked;

    CalendarViewPagerAdapter calendarViewPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.d("CalendarActivity","onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        //getActionBar().setDisplayHomeAsUpEnabled(true);


        medicationDeleted   =   false;

        viewPager = (ViewPager)findViewById(R.id.calendar_view);
        calendarViewPagerAdapter    =   new CalendarViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(calendarViewPagerAdapter);
        viewPager.setCurrentItem(31);


    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("CalendarActivity","onStart");
        if(addButtonClicked){
            addButtonClicked = false;
            refreshCalendar();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            openOptionsMenu();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //getActionBar().show();
        if(addButtonClicked){
            addButtonClicked = false;
            refreshCalendar();
        }
        Log.d("CalendarActivity","onResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("CalendarActivity","onDestroy");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("CalendarActivity","onDestroy");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_calendar, menu);
        return super.onCreateOptionsMenu(menu);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
////        switch (item.getItemId()){
////            case R.id.action_add:
////                startActivity(new Intent(this, ReminderActivity.class));
////                addButtonClicked    =   true;
////                break;
////            case android.R.id.home:
////                finish();
////                break;
////        }
//        return super.onOptionsItemSelected(item);
//    }

    //local methods
    public void refreshCalendar(){

        int currentIndex = viewPager.getCurrentItem();

        viewPager.removeAllViews();
        calendarViewPagerAdapter    =   null;

        calendarViewPagerAdapter    =   new CalendarViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(calendarViewPagerAdapter);
        viewPager.setCurrentItem(currentIndex);

    }

    public void navigatePage(int index){
        viewPager.setCurrentItem(index);
    }

    public class CalendarViewPagerAdapter extends FragmentPagerAdapter {

        public CalendarViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return CalenderFragment.newInstance(i);
        }

        @Override
        public int getCount() {
            return 62;
        }
    }
}
