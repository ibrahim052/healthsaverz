package com.healthsaverz.healthmobile.medicine.medfriend.controller;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.global.DialogProgressFragment;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
import com.healthsaverz.healthmobile.medicine.medfriend.modal.MedFriend;

/**
 * Created by ABCD on 2/6/2015.
 */
public class UpdateMedFriendDialog extends DialogFragment {

    public static final String TAG = "AddMedFriendDialog";
    private Context context;

    UpdateFriendListener updateFriendListener;
    private EditText firstName;
    private EditText email;
    private EditText phoneNumber;
    private DialogProgressFragment progressFragment;
    private MedFriend medFriend;
    private TextView titleText;

    public interface UpdateFriendListener{
        void onUpdateMedFriend(MedFriend medFriend);
    }

    public TextView getTitleText() {
        if (titleText == null) {
            titleText = (TextView) getView().findViewById(R.id.titleText);
        }
        return titleText;
    }

    public EditText getFirstName() {
        if (firstName == null) {
            firstName = (EditText) getView().findViewById(R.id.firstName);
        }
        return firstName;
    }
    public EditText getEmail() {
        if (email == null) {
            email = (EditText) getView().findViewById(R.id.email);
        }
        return email;
    }
    public EditText getPhoneNumber() {
        if (phoneNumber == null) {
            phoneNumber = (EditText) getView().findViewById(R.id.phoneNumber);
        }
        return phoneNumber;
    }

    private Button setButton;
    public Button getVerifyButton() {
        if (setButton == null) {
            setButton = (Button) getView().findViewById(R.id.setButton);
            setButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                        ViewHelper.fadeOut(view, null);
                        return true;
                    } else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                        final int viewID = view.getId();
                        ViewHelper.fadeIn(view, null);
                        if (validateForm()){
                            String cellNumber = getPhoneNumber().getText().toString().trim();
                            if (cellNumber.length() == 10){
                                cellNumber = "91"+cellNumber;
                            }
                            medFriend._friendName = getFirstName().getText().toString().trim();
                            medFriend._friendEmail = getEmail().getText().toString().trim();
                            medFriend._friendMobile = cellNumber;
                            if (updateFriendListener != null){
                                updateFriendListener.onUpdateMedFriend(medFriend);
                            }
                            dismiss();
                        }

                    }

                    return true;


                }
            });
        }
        return setButton;
    }

    private Button cancelButton;
    public Button getCancelButton() {
        if (cancelButton == null) {
            cancelButton = (Button) getView().findViewById(R.id.cancelButton);
            cancelButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (updateFriendListener != null){
                        updateFriendListener.onUpdateMedFriend(null);
                    }
                    dismiss();
                    return true;
                }
            });
        }
        return cancelButton;
    }

    public static UpdateMedFriendDialog newInstance(UpdateFriendListener updateFriendListener, MedFriend medFriend){
        UpdateMedFriendDialog updateMedFriendDialog = new UpdateMedFriendDialog();
        updateMedFriendDialog.updateFriendListener = updateFriendListener;
        updateMedFriendDialog.medFriend = medFriend;
        return updateMedFriendDialog;
    }

    public UpdateMedFriendDialog() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dialog_add_med_friend, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getFirstName().requestFocus();
        getFirstName().setText("");
        getFirstName().append(medFriend._friendName);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        getVerifyButton();
        getCancelButton();
        getEmail().setText("");
        getEmail().append(medFriend._friendEmail);
        getPhoneNumber().setText("");
        getPhoneNumber().append(medFriend._friendMobile);
        getTitleText().setText("Update Med Buddy");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return dialog;
    }

    public  boolean validateForm(){

        String errorMessage = null;
        boolean isErrorOccured = false;

        /*
        if(email.getText().toString().trim().equals("")){

            errorMessage = "Email id can not be empty.";
            isErrorOccured = true;
        }
        else
         */
        if (!email.getText().toString().trim().equals("") &&!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString().trim()).matches())
        {
            errorMessage = "Invalid email id.";
            isErrorOccured = true;
        }
        else if(phoneNumber.getText().toString().trim().equals("")){

            errorMessage = "Cell phone can not be empty.";
            isErrorOccured = true;
        }
        else if (!Patterns.PHONE.matcher(phoneNumber.getText().toString().trim()).matches())
        {
            errorMessage = "Invalid Cell Number.";
            isErrorOccured = true;
        }
        else if (phoneNumber.getText().toString().trim().length() <= 9)
        {
            errorMessage = "Cell Number should have country code followed by 10 digit number.";
            isErrorOccured = true;
        }
        else if (!firstName.getText().toString().equals("")&& firstName.getText().toString().trim().length() < 3)
        {
            errorMessage = "Enter a valid Name.";
            isErrorOccured = true;
        }

        if(isErrorOccured){

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            // Setting Dialog Title
            alertDialog.setTitle("Validation Error");
            // Setting Dialog Message
            alertDialog.setMessage(errorMessage);

            // Setting Positive "OK" Btn
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            dialog.cancel();
                        }
                    });
            // Showing Alert Dialog
            alertDialog.show();

            return false;
        }
        else {

            return true;
        }


    }

}