package com.healthsaverz.healthmobile.medicine.register.controller;



import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;
import com.healthsaverz.healthmobile.medicine.global.UseCases;
import com.healthsaverz.healthmobile.medicine.login.controller.LoginActivity;

/**
 * A simple {@link android.app.Fragment} subclass.
 *
 */
public class DialogVerificationCodeFragment extends DialogFragment {


    public EditText verificationCodeEditText = null;

    public interface DialogVerificationCodeFragmentListener{

        void isVerified(boolean code);//
    }

    private  DialogVerificationCodeFragmentListener verificationListener;

    public DialogVerificationCodeFragmentListener getVerificationListener() {
        return verificationListener;
    }

    public void setVerificationListener(DialogVerificationCodeFragmentListener verificationListener) {
        this.verificationListener = verificationListener;
    }

    public DialogVerificationCodeFragment() {
        // Required empty public constructor
    }

    public static DialogVerificationCodeFragment newInstance(DialogVerificationCodeFragmentListener listener){

        DialogVerificationCodeFragment verificationDialogFragment = new DialogVerificationCodeFragment();
        verificationDialogFragment.setVerificationListener(listener);
        return verificationDialogFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dialog_verification_code, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if( keyCode == KeyEvent.KEYCODE_BACK ) {
                    // leave this blank in order to disable the back press
                    return true;
                } else {
                    return false;
                }
            }
        });

        verificationCodeEditText = (EditText)view.findViewById(R.id.verificationCode);
        TextView verifyTextView = (TextView) view.findViewById(R.id.action_verify);
        TextView cancelTextView = (TextView) view.findViewById(R.id.action_cancel);

//        String verifiedCodeReceived = PreferenceManager.getStringForKey(DialogVerificationCodeFragment.this.getActivity(),PreferenceManager.VERIFICATION_CODE,"-1");
//        verificationCodeEditText1.setText("");
//        verificationCodeEditText1.append(verifiedCodeReceived);

        verifyTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 String verifiedCodeReceived = PreferenceManager.getStringForKey(getActivity(),PreferenceManager.VERIFICATION_CODE,"-1");

                if(verifiedCodeReceived.equals(verificationCodeEditText.getText().toString().trim())){
                    //verifiedCodeReceived.equals(verificationCodeEditText1.getText().toString().trim())

                    if(verificationListener != null){
                        verificationListener.isVerified(true);
                    }
                    else {
                        dismiss();
                    }
                    //
                }
                else {
                    if(verificationListener != null){
                        verificationListener.isVerified(false);
                    }
                    else {
                        dismiss();
                    }
                }
            }
        });
        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                UseCases.startActivityByClearingStack(getActivity(), LoginActivity.class);
            }
        });
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog =  super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        return dialog;
    }


    
}
