package com.healthsaverz.healthmobile.medicine.asyncloader;

/**
 * Created by montydudani on 06/05/14.
 */
public interface ParserListener {
    public void didReceivedError(AsyncLoader.Status errorCode);

    public void didReceivedData(Object result);

    public void didReceivedProgress(double progress);
}
