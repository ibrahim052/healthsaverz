package com.healthsaverz.healthmobile.medicine.medicineDetail.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.medicineSearch.modal.SubstituteSearchResponse;

import java.util.ArrayList;

/**
 * Created by Ibrahim on 04-11-2014.
 */
public class SubstituteAdapter extends BaseAdapter {
    private ArrayList<SubstituteSearchResponse> substituteSearchResponses;
    private Context context;
    LayoutInflater layoutInflater;

    public SubstituteAdapter(ArrayList<SubstituteSearchResponse> substituteSearchResponses, Context context) {
        this.substituteSearchResponses = substituteSearchResponses;
        this.context = context;
        this.layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return substituteSearchResponses.size();
    }

    @Override
    public Object getItem(int i) {
        return substituteSearchResponses.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View productView, ViewGroup viewGroup) {

        if (productView == null){

            productView = layoutInflater.inflate(R.layout.cell_substitute_pattern, null);
            ViewHolder holder = new ViewHolder();
            holder.ManfactureName= (TextView)productView.findViewById(R.id.subst_title);
            holder.GenericName = (TextView)productView.findViewById(R.id.descriptionSubst);
            holder.tabletQuantity = (TextView)productView.findViewById(R.id.tabletofSubst);
            holder.savePercentage = (TextView)productView.findViewById(R.id.savePercentage);
            holder.logo = (ImageView) productView.findViewById(R.id.subst_icon);
            holder.partition = (ImageView) productView.findViewById(R.id.dummyView);
            productView.setTag(holder);
        }
//brand name, strength name, strenght unit, strngth unit name, packaging size name, packaging unit name, packaging name
        ViewHolder holder = (ViewHolder)productView.getTag();
        SubstituteSearchResponse searchResponse = substituteSearchResponses.get(i);
        holder.ManfactureName.setText(searchResponse.getBrandsName() + " "
                + ((searchResponse.getStrengthName().equals("0"))?"":searchResponse.getStrengthName()+" ")
                + ((searchResponse.getStrengthUnit() == 0)?"":searchResponse.getStrengthUnit()+" ")
                + ((searchResponse.getStrengthUnitName().equals("0"))?"":searchResponse.getStrengthUnitName()+" ")
                + ((searchResponse.getPackagingSizeName().equals("0"))?"":searchResponse.getPackagingSizeName()+" ")
                + ((searchResponse.getPackagingUnitName().equals("0"))?"":searchResponse.getPackagingUnitName()+" ")
                + ((searchResponse.getProductPackagingName().equals("0"))?"":searchResponse.getProductPackagingName()));
        holder.GenericName.setText(searchResponse.getManfacuterName());
        if (searchResponse.getPackagingSizeName().equals("0")){
            holder.tabletQuantity.setVisibility(View.GONE);
            holder.partition.setVisibility(View.GONE);
        }
        else {
            holder.tabletQuantity.setVisibility(View.VISIBLE);
            holder.partition.setVisibility(View.VISIBLE);
            holder.tabletQuantity.setText("("+ searchResponse.getPackagingSizeName() +" Tablets in a Strip )");
        }
        holder.savePercentage.setText("Save "+searchResponse.getDiscountPricePercentage()*100+"%");

        switch (searchResponse.iconId){
            case 0:
                holder.logo.setImageDrawable(null);
                break;
            case 1:
                holder.logo.setImageResource(R.drawable.bottle);
                break;
            case 2:
                holder.logo.setImageResource(R.drawable.eyedropper);
                break;
            case 3:
                holder.logo.setImageResource(R.drawable.injection);
                break;
            case 4:
                holder.logo.setImageResource(R.drawable.tablet);
                break;
            case 5:
                holder.logo.setImageResource(R.drawable.tablet);
                break;
        }

        return productView;
    }

    private class ViewHolder {
        public TextView ManfactureName;
        public TextView GenericName;
        public TextView tabletQuantity;
        public TextView savePercentage;
        public ImageView logo;
        public ImageView partition;

    }
}
