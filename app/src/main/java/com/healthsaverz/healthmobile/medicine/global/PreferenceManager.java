package com.healthsaverz.healthmobile.medicine.global;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by montydudani on 05/08/14.
 */
public class PreferenceManager {

    private static final String SHARED_PREFERENCES_FILE = "HealthSaverzAppPref";

    public static final String SESSION_ID = "sessionID";
    public static final String CLIENT_ID = "clientID";
    public static final String MESSAGE = "message";
    public static final String GENDER = "gender";
    public static final String DEVICE_UDID = "deviceUDID";
    public static final String DEVICE_TOKEN = "deviceToken";
    public static final String VERIFICATION_CODE = "verificationCode";
    public static final String OS_TYPE = "osType";
    public static final String VERIFICATION_FLAG = "verificationFlag";
    public static final String FILE_DETAIL_LIST = "fileDetailList";
    public static final String STATE = "state";
    public static final String STATUS = "status";
    public static final String COUNTRY = "country";
    public static final String ID = "id";
    public static final String USERNAME = "userName";
    public static final String PASSWORD = "password";
    public static final String ADDRESS1 = "address1";
    public static final String ADDRESS2 = "address2";
    public static final String POSTAL_CODE = "postalCode";
    public static final String EMAIL_ADDRESS = "emailAddress";
    public static final String SUB_USER_ID = "subUserId";
    public static final String USER_GROUP_ID = "userGroupId";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String CREATED_BY = "created_By";
    public static final String CELL_NUMBER = "cellNumber";
    public static final String CITY = "city";

    //UsesCases
    public static final String IS_REGISTRATION_STATUS = "REG_STATUS";
    public static final String IS_VERIFICATION_STATUS = "VER_STATUS";
    public static final String USER_ID = "USER_ID";
    public static final String COUNTRY_CODE = "CountryCode";
    public static final String USER_NAME = "USER_NAME";
    public static final String EMAIL = "EMAIL";
    public static final String PLAN_TYPE = "PlanType";
    public static final String IS_CONTACTS_SYNCED = "CONTACTS_SYNCED";
    public static final String IS_FIRST_TIME = "isFirstTime";
    public static final String MIDNIGHT_RCREATER = "midNightReCreater";
    public static final String DEVICE_NAME = "deviceName";
    public static final String OS_VERSION = "osVersion";
    public static final String DOCTORID = "doctorId";
    public static final String DOCTORNAME = "doctorName";
    public static final String DOCTOR_SIGNATURE = "doctorSignature";

    //Integer operations
    public static void saveIntForKey(Context context, String key, int data) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, data);
        editor.commit();
    }

    public static int getIntForKey(Context context, String key, int defaultData) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(key, defaultData);
    }

    public static void removeIntForKey(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.commit();
    }

    //Float operations
    public static void saveFloatForKey(Context context, String key, int data) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, data);
        editor.commit();
    }

    public static int getFloatForKey(Context context, String key, int defaultData) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(key, defaultData);
    }

    public static void removeFloatForKey(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.commit();
    }

    //String operations
    public static void saveStringForKey(Context context, String key, String data) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, data);
        editor.commit();
    }

    public static String getStringForKey(Context context, String key, String defaultData) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, defaultData);
    }

    public static void removeStringForKey(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.commit();
    }

    //boolean operations
    public static void saveBooleanForKey(Context context, String key, boolean data) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, data);
        editor.commit();
    }
    public static boolean getBooleanForKey(Context context, String key, boolean defaultData) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key, defaultData);
    }

    public static void removeBooleanForKey(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.commit();
    }

    public static void remove(Context context, String key){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.commit();
    }

}
