package com.healthsaverz.healthmobile.medicine.asyncloader;

/**
 * Created by montydudani on 23/02/14.
 */
public class ParserException extends Exception {
    private String detailMessage;

    public ParserException(String detailMessage) {
        super(detailMessage);
        this.detailMessage = detailMessage;
    }

    @Override
    public String toString() {
        return detailMessage;
    }

    @Override
    public String getMessage() {
        return detailMessage;
    }

}

