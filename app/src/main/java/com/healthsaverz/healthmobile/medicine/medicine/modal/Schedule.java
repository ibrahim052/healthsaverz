package com.healthsaverz.healthmobile.medicine.medicine.modal;

/**
 * Created by Ibrahim on 09-10-2014.
 */
public class Schedule {

    private int _scheduleID;
    private int _medID;
    private String _action;
    private int _type;

    public Schedule(int _medID, String _action, int _type) {
        this._medID = _medID;
        this._action = _action;
        this._type = _type;
    }

    public Schedule(int _scheduleID, int _medID, String _action, int _type) {
        this._scheduleID = _scheduleID;
        this._medID = _medID;
        this._action = _action;
        this._type = _type;
    }

    public Schedule() {

    }

    public int get_scheduleID() {
        return _scheduleID;
    }

    public void set_scheduleID(int _scheduleID) {
        this._scheduleID = _scheduleID;
    }

    public int get_medID() {
        return _medID;
    }

    public void set_medID(int _medID) {
        this._medID = _medID;
    }

    public String get_action() {
        return _action;
    }

    public void set_action(String _action) {
        this._action = _action;
    }

    public int get_type() {
        return _type;
    }

    public void set_type(int _type) {
        this._type = _type;
    }
}
