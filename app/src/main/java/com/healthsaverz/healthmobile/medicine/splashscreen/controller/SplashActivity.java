package com.healthsaverz.healthmobile.medicine.splashscreen.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;


import com.facebook.appevents.AppEventsLogger;
import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.DialogProgressFragment;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;
import com.healthsaverz.healthmobile.medicine.global.UseCases;
import com.healthsaverz.healthmobile.medicine.login.controller.LoginActivity;
import com.healthsaverz.healthmobile.medicine.login.modal.LoginUserParser;
import com.healthsaverz.healthmobile.medicine.mainscreen.controller.MainScreenActivity;
import com.healthsaverz.healthmobile.medicine.register.controller.RegisterActvity;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends Activity implements LoginUserParser.LoginUserParserListener {

    private Timer timer;
    DialogProgressFragment progressFragment;

    public static final String TAG = SplashActivity.class.getSimpleName();

    public Timer getTimer() {
        if (timer == null) {
            timer = new Timer();
        }
        return timer;
    }

    //Activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getActionBar().setTitle("");
        getActionBar().setIcon(R.drawable.myhealth);
        getActionBar().hide();

//        UseCases.startActivityByClearingStack(SplashActivity.this, MainScreenActivity.class);
//        finish();
        getTimer().schedule(new TimerTask() {
            @Override
            public void run() {

                String loginStatus =  PreferenceManager.getStringForKey(SplashActivity.this, PreferenceManager.STATUS, "-1");
                String ID           =   PreferenceManager.getStringForKey(SplashActivity.this,PreferenceManager.ID,"-1");
                Log.d("loginStatus", "loginStatus " + loginStatus + " ID " + ID);

                if(ID == null || ID.equals("-1") || ID.equals(""))
                {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
                else if(loginStatus == null || loginStatus.equals("-1") || loginStatus.equals(""))
                {
                    startActivity(new Intent(SplashActivity.this, RegisterActvity.class));
                    finish();
                }
                else if(loginStatus.equals("I") && (ID != null) )
                {
                    /*
                    We need to show directly Verification dialog as user has already regsitered but have not verified.
                     */
                    Intent intent = new Intent(SplashActivity.this, RegisterActvity.class);
                    intent.putExtra("showVerificationDialog",1);
                    startActivity(intent);
                    finish();
                }
                else {

//                    progressFragment =  new DialogProgressFragment();
//                    progressFragment.show(getFragmentManager(), TAG);
//                    progressFragment.setCancelable(false);
//
//                    LoginUserParser loginUserParser = new LoginUserParser(SplashActivity.this);
//                    loginUserParser.loginUser(PreferenceManager.getStringForKey(SplashActivity.this, PreferenceManager.USERNAME, "-1"),PreferenceManager.getStringForKey(SplashActivity.this,PreferenceManager.PASSWORD,"-1"));
//                    loginUserParser.setLoginUserListener(SplashActivity.this);
                    UseCases.startActivityByClearingStack(SplashActivity.this, MainScreenActivity.class);
                    finish();
                }

            }
        }, 2000);

    }


    //LoginUserParser.LoginUserParserListener callbacks
    @Override
    public void loginUserParserDidUserLoggedIn(String message) {

        if(message.equals("100")){
            progressFragment.dismiss();
            progressFragment = null;

            UseCases.startActivityByClearingStack(this, MainScreenActivity.class);
            Toast.makeText(this, "User Logged In", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void loginUserParserDidReceivedConnectionError(AsyncLoaderNew.Status status) {

    }

    @Override
    public void loginUserParserDidReceivedProcessingError(String message) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }
}
