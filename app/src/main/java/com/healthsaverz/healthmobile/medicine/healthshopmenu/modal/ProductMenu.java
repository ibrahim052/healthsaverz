package com.healthsaverz.healthmobile.medicine.healthshopmenu.modal;

/**
 * Created by priyankranka on 14/11/14.
 */
public class ProductMenu {

    public String name;
    public int type;
    public int productCategoryId;
    public int masterCategory;

    public boolean show;//to indicate whether to row in listview cell or not
    public boolean isExpanded;//Indicate whether it has been extended or not.
    public boolean isExpandable;// To indicate whether it is expandable or not.

    public int level;//To indicate how it will appear in list


}
