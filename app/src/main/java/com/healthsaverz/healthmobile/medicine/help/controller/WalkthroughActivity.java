package com.healthsaverz.healthmobile.medicine.help.controller;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
public class WalkthroughActivity extends Activity implements View.OnTouchListener, ViewPager.OnPageChangeListener {
    public static final String TRUE = "true";
    public static final String TAG = "HelpWalkthroughDialog";

    private ViewPager helpViewer;
    private Button skipButton;
    private int[] mCards = {

            R.drawable.screen1, //page_1_6
            R.drawable.screen2, //screen_temp
            R.drawable.screen3, //page_34
            R.drawable.screen4, //page_35
            R.drawable.screen5,
            R.drawable.screen6,
            R.drawable.screen7
    };
    public ViewPager getHelpViewer() {
        if (helpViewer == null){
            helpViewer = (ViewPager) findViewById(R.id.helpViewer);
        }
        return helpViewer;
    }

    public Button getSkipButton() {
        if (skipButton == null){
            skipButton = (Button) findViewById(R.id.skipButton);
        }
        return skipButton;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_walkthrough);
        getSkipButton().setOnTouchListener(this);
        HelpViewPagerAdapter helpViewPagerAdapter = new HelpViewPagerAdapter();
        getHelpViewer().setAdapter(helpViewPagerAdapter);
        getHelpViewer().setOnPageChangeListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_walkthrough, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN)
        {
            ViewHelper.fadeOut(view, null);
            return true;
        }
        else if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP)
        {
            ViewHelper.fadeIn(view, null);
            finish();
            return true;
        }
        return false;
    }
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        if (position == mCards.length-1) {
            getSkipButton().setText("Get Started");
        }
        else {
            getSkipButton().setText("Skip");
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public class HelpViewPagerAdapter extends PagerAdapter {


        @Override
        public int getCount() {
            return mCards.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView imageView = new ImageView(WalkthroughActivity.this);
//            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(convertToDPUnit(getActivity(), 24), convertToDPUnit(getActivity(), 24));
//            imageView.setLayoutParams(layoutParams);
//            int padding = context.getResources().getDimensionPixelSize(R.dimen.standard_margin);
//            imageView.setPadding(padding, padding, padding, padding);
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imageView.setImageResource(mCards[position]);
            container.addView(imageView, 0);
            return imageView;
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((ImageView) object);
        }
    }
}
