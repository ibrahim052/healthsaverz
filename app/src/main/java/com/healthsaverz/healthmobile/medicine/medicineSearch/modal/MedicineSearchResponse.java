package com.healthsaverz.healthmobile.medicine.medicineSearch.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DG on 10/11/2014.
 */
public class MedicineSearchResponse implements Parcelable {

    public static final String SessionID = "sessionID";
    public static final String ClientID = "clientID";
    public static final String Message = "message";
    public static final String MoleculeTypeName = "moleculeTypeName";
    public static final String ProductCategoryName = "productCategoryName";
    public static final String WarehouseName = "warehouseName";
    public static final String GroupName = "groupName";
    public static final String SubGroupName = "subGroupName";
    public static final String DiseaseTypeName = "diseaseTypeName";
    public static final String ProductTypeName        = "productTypeName";
    public static final String Description = "description";
    public static final String Icon = "icon";
    public static final String Status = "status";
    public static final String ItemType = "itemType";
    public static final String ManfacuterName = "manfacuterName";
    public static final String ImageURL   = "imageURL";
    public static final String Created_By  = "created_By" ;
    public static final String BrandsId  = "brandsId" ;
    public static final String ProductPackagingId = "productPackagingId";
    public static final String ManufacturerTagName        = "manufacturerTagName";
    public static final String MoleculeTypeId = "moleculeTypeId";
    public static final String ManufacturerTag        = "manufacturerTag";
    public static final String ManufacturingDate        = "manufacturingDate";
    public static final String PackagingSizeName        = "packagingSizeName";
    public static final String StrengthUnitName        = "strengthUnitName";
    public static final String ProductPackagingName = "productPackagingName";
    public static final String PackagingUnitName        = "packagingUnitName";
    public static final String ThumbnailImageURL = "thumbnailImageURL";
    public static final String MetaDescription = "metaDescription";
    public static final String ProductCategoryId = "productCategoryId";
    public static final String SellingPricePercentage = "sellingPricePercentage";
    public static final String DiscountPricePercentage = "discountPricePercentage";
    public static final String Strength = "strength";
    public static final String StrengthUnit = "strengthUnit";
    public static final String ProductId = "productId";
    public static final String TotalQuantity = "totalQuantity";
    public static final String Mrp = "mrp";
    public static final String NelmtagName = "nelmtagName";
    public static final String India_Or_MNC = "india_Or_MNC";
    public static final String Nelmtag = "nelmtag";
    public static final String India_MNC = "india_MNC";
    public static final String ExpiryDate = "expiryDate";
    public static final String BatchNumber = "batchNumber";
    public static final String PackagingSize	= "packagingSize";
    public static final String PackagingUnit = "packagingUnit";
    public static final String ManfactureId = "manfactureId";
    public static final String GroupId = "groupId";
    public static final String SubGroupId = "subGroupId";
    public static final String DiseaseTypeId = "diseaseTypeId";
    public static final String BrandsName = "brandsName";
    public static final String StrengthName        = "strengthName";
    public static final String Video   = "video";
    public static final String MetaKeywords    = "metaKeywords";
    public static final String BarcodeId = "barcodeId" ;
    public static final String ProductTypeId = "productTypeId";
    public static final String ProductSizeId = "productSizeId" ;
    public static final String WarehouseId  = "warehouseId";
    public static final String Keywords      = "keywords";
    public static final String SellingPrice  ="sellingPrice";
    public static final String DiscountPrice = "discountPrice" ;
    public static final String SalesTax  = "salesTax" ;
    public static final String PurchaseTax        = "purchaseTax";
    public static final String GenericName        = "genericName";
    public static final String Created_On        = "created_On";
    public static final String IconUrl        = "iconUrl";
    public static final String IconId        = "iconId";

//    public static final String ProductSize = "productSize";
//    public static final String Dimension = "dimension";
//    public static final String Title = "title";
//    public static final String ProductFeatureId = "productFeatureId";
//    public static final String DisplayQuantity = "displayQuantity";
//    public static final String OutOfStockFlag = "outOfStockFlag";
//    public static final String BackOrderMaxQty = "backOrderMaxQty";
//    public static final String PreOrderMaxQty = "preOrderMaxQty";
//    public static final String BackOrderDescription = "backOrderDescription";
//    public static final String PreOrderDescription = "preOrderDescription";
//    public static final String NotifyforQuantityBelow = "notifyforQuantityBelow";
//    public static final String Kind_Of_Medicine = "kind_Of_Medicine";
//    public static final String ProductColorId = "productColorId";
//   // public static final String DisplayQuantity ="displayQuantity"
//    public static final String Weight = "weight";
//    public static final String WeightType    = "weightType";
//    public static final String MinimumQty  = "minimumQty";
//    public static final String BackOrderFlag       = "backOrderFlag";
//    public static final String PreOrderFlag       = "preOrderFlag";
    public static final String Extra_1         = "extra_1";
    public static final String Extra_2         = "extra_2";
    public static final String Extra_3         = "extra_3";
    public static final String Extra_4         = "extra_4";
    public static final String Extra_5         = "extra_5";

    public String sessionID = "";
    public String clientID = "";
    public String message = "";
    public String moleculeTypeName	       = "";
    public String productCategoryName	       = "";
    public String warehouseName	       = "";
    public String groupName	       = "";
    public String subGroupName	       = "";
    public String diseaseTypeName	       = "";
    public String productTypeName	       = "";
    public String description	       = "";
    public String icon	       = "";
    public String status = "";
    public String itemType  = "";
    public String manfacuterName	       = "";
    public String imageURL = "";
    public int created_By   ;
    public int brandsId	    ;
    public int productPackagingId	    ;
    public String manufacturerTagName	       = "";
    public int moleculeTypeId	       ;
    public int logo;
    public int manufacturerTag;
    public String manufacturingDate = "";
    public String packagingSizeName = "";
    public String strengthUnitName  = "";
    public String productPackagingName   = "";
    public String packagingUnitName    = "";
    public String thumbnailImageURL   = "";
    public String metaDescription   = "";
    public int productCategoryId   ;
    public int sellingPricePercentage  ;
    public double discountPricePercentage  ;
    public int strength    ;
    public int strengthUnit    ;
    public int productId  ;
    public int totalQuantity   ;
    public double mrp  ;
    public String nelmtagName     = "";
    public String india_Or_MNC    = "";
    public int nelmtag     ;
    public int india_MNC     ;
    public String expiryDate   = "";
    public String batchNumber   = "";
    public int packagingSize  ;
    public int packagingUnit  ;
    public int manfactureId  ;
    public int groupId  ;
    public int subGroupId  ;
    public int diseaseTypeId  ;
    public String brandsName      = "";
    public String strengthName       = "";
    public String video  = "" ;
    public String metaKeywords  = "" ;
    public int barcodeId  ;
    public int productTypeId  ;
    public int warehouseId  ;
    public String keywords  = "" ;
    public double sellingPrice   ;
    public int discountPrice  ;
    public int salesTax   ;
    public int purchaseTax   ;
    public String genericName        = "";
    public String created_On        = "";
    public String iconUrl = "";
    public int iconId ;
    public String extra_1         = "";
    public String extra_2         = "";
    public String extra_3         = "";
    public String extra_4         = "";
    public String extra_5         = "";

    private MedicineSearchResponse(Parcel in){
        sessionID = in.readString();
        clientID = in.readString();
        message = in.readString();
        moleculeTypeName = in.readString();
        productCategoryName = in.readString();
        warehouseName = in.readString();
        groupName = in.readString();
        subGroupName = in.readString();
        diseaseTypeName = in.readString();
        productTypeName = in.readString();
        description = in.readString();
        icon = in.readString();
        status = in.readString();
        itemType = in.readString();
        manfacuterName = in.readString();
        imageURL = in.readString();
        created_By = in.readInt();
        brandsId = in.readInt();
        productPackagingId = in.readInt();
        manufacturerTagName = in.readString();
        moleculeTypeId = in.readInt();
        logo = in.readInt();
        manufacturerTag = in.readInt();
        manufacturingDate = in.readString();
        packagingSizeName = in.readString();
        strengthUnitName = in.readString();
        productPackagingName = in.readString();
        packagingUnitName = in.readString();
        thumbnailImageURL = in.readString();
        metaDescription = in.readString();
        productCategoryId = in.readInt();
        sellingPricePercentage = in.readInt();
        discountPricePercentage = in.readDouble();
        strength = in.readInt();
        strengthUnit = in.readInt();
        productId = in.readInt();
        totalQuantity = in.readInt();
        mrp = in.readDouble();
        nelmtagName = in.readString();
        india_Or_MNC = in.readString();
        nelmtag = in.readInt();
        india_MNC = in.readInt();
        expiryDate = in.readString();
        batchNumber = in.readString();
        packagingSize = in.readInt();
        packagingUnit = in.readInt();
        manfactureId = in.readInt();
        groupId = in.readInt();
        subGroupId = in.readInt();
        diseaseTypeId = in.readInt();
        brandsName = in.readString();
        strengthName = in.readString();
        video = in.readString();
        metaKeywords = in.readString();
        barcodeId = in.readInt();
        productTypeId = in.readInt();
        warehouseId = in.readInt();
        keywords = in.readString();
        sellingPrice = in.readDouble();
        discountPrice = in.readInt();
        salesTax = in.readInt();
        purchaseTax = in.readInt();
        genericName = in.readString();
        created_On = in.readString();
        iconUrl = in.readString();
        iconId = in.readInt();
        extra_1 = in.readString();
        extra_2 = in.readString();
        extra_3 = in.readString();
        extra_4 = in.readString();
        extra_5 = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(sessionID);
        out.writeString(clientID);
        out.writeString(message);
        out.writeString(moleculeTypeName);
        out.writeString(productCategoryName);
        out.writeString(warehouseName);
        out.writeString(groupName);
        out.writeString(subGroupName);
        out.writeString(diseaseTypeName);
        out.writeString(productTypeName);
        out.writeString(description);
        out.writeString(icon);
        out.writeString(status);
        out.writeString(itemType);
        out.writeString(manfacuterName);
        out.writeString(imageURL);
        out.writeInt(created_By);
        out.writeInt(brandsId);
        out.writeInt(productPackagingId);
        out.writeString(manufacturerTagName);
        out.writeInt(moleculeTypeId);
        out.writeInt(logo);
        out.writeInt(manufacturerTag);
        out.writeString(manufacturingDate);
        out.writeString(packagingSizeName);
        out.writeString(strengthUnitName);
        out.writeString(productPackagingName);
        out.writeString(packagingUnitName);
        out.writeString(thumbnailImageURL);
        out.writeString(metaDescription);
        out.writeInt(productCategoryId);
        out.writeInt(sellingPricePercentage);
        out.writeDouble(discountPricePercentage);
        out.writeInt(strength);
        out.writeInt(strengthUnit);
        out.writeInt(productId);
        out.writeInt(totalQuantity);
        out.writeDouble(mrp);
        out.writeString(nelmtagName);
        out.writeString(india_Or_MNC);
        out.writeInt(nelmtag);
        out.writeInt(india_MNC);
        out.writeString(expiryDate);
        out.writeString(batchNumber);
        out.writeInt(packagingSize);
        out.writeInt(packagingUnit);
        out.writeInt(manfactureId);
        out.writeInt(groupId);
        out.writeInt(subGroupId);
        out.writeInt(diseaseTypeId);
        out.writeString(brandsName);
        out.writeString(strengthName);
        out.writeString(video);
        out.writeString(metaKeywords);
        out.writeInt(barcodeId);
        out.writeInt(productTypeId);
        out.writeInt(warehouseId);
        out.writeString(keywords);
        out.writeDouble(sellingPrice);
        out.writeInt(discountPrice);
        out.writeInt(salesTax);
        out.writeInt(purchaseTax);
        out.writeString(genericName);
        out.writeString(created_On);
        out.writeString(iconUrl);
        out.writeInt(iconId);
        out.writeString(extra_1);
        out.writeString(extra_2);
        out.writeString(extra_3);
        out.writeString(extra_4);
        out.writeString(extra_5);

    }


    public static final Creator<MedicineSearchResponse> CREATOR = new Creator<MedicineSearchResponse>() {
        public MedicineSearchResponse createFromParcel(Parcel in) {
            return new MedicineSearchResponse(in);
        }

        public MedicineSearchResponse[] newArray(int size) {
            return new MedicineSearchResponse[size];
        }
    };
    public MedicineSearchResponse() {

    }



}



