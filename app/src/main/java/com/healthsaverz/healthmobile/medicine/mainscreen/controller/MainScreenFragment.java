package com.healthsaverz.healthmobile.medicine.mainscreen.controller;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.calendar.CalendarActivity;
import com.healthsaverz.healthmobile.medicine.ehr.controller.EhrActivity;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;
import com.healthsaverz.healthmobile.medicine.global.UseCases;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
import com.healthsaverz.healthmobile.medicine.help.controller.HelpActivity;
import com.healthsaverz.healthmobile.medicine.help.controller.WalkthroughActivity;
import com.healthsaverz.healthmobile.medicine.help.modal.HelpWalkthroughDialog;
import com.healthsaverz.healthmobile.medicine.login.controller.LoginActivity;
import com.healthsaverz.healthmobile.medicine.medicineSearch.controller.MedicineSearchActivity;
import com.healthsaverz.healthmobile.medicine.pieChart.controller.PieChartActivity;
import com.healthsaverz.healthmobile.medicine.referral.model.ReferalDialog;
import com.healthsaverz.healthmobile.medicine.uploadPrescription.controller.UploadPrescriptionActivity;

import java.util.Calendar;

/**
 * A simple {@link android.app.Fragment} subclass.
 *
 */
public class MainScreenFragment extends Fragment implements View.OnTouchListener {

    public static final String TAG = MainScreenFragment.class.getSimpleName();

    public static MainScreenFragment newInstance() {
        return new MainScreenFragment();
    }

    public MainScreenFragment() {
    }

    public static final int PLAN_TYPE_FIRST = 0;
    public static final int PLAN_TYPE_SECOND = 1;
    public static final int PLAN_TYPE_THIRD = 2;
    public static final int PLAN_TYPE_FOURTH = 3;
    public static final int PLAN_TYPE_FIFTH = 4;
    public static final int PLAN_TYPE_SIXTH = 5;
    public static final int PLAN_TYPE_SEVENTH = 6;
    public static final int PLAN_TYPE_EIGHT = 7;
    public static final int PLAN_TYPE_NINTH = 8;
//    public static final int PLAN_TYPE_BUTTON_1 = 9;
//    public static final int PLAN_TYPE_BUTTON_2 = 10;
//    public static final int PLAN_TYPE_BUTTON_3 = 11;

    private ImageView first;

    public ImageView getFirst() {
        if (first == null) {
            first = (ImageView) getView().findViewById(R.id.first);
            first.setOnTouchListener(this);
            first.setTag(PLAN_TYPE_FIRST);
        }
        return first;
    }

    private ImageView second;

    public ImageView getSecond() {
        if (second == null) {
            second = (ImageView) getView().findViewById(R.id.second);
            second.setOnTouchListener(this);
            second.setTag(PLAN_TYPE_SECOND);
        }
        return second;
    }

    private ImageView third;

    public ImageView getThird() {
        if (third == null) {
            third = (ImageView) getView().findViewById(R.id.third);
            third.setOnTouchListener(this);
            third.setTag(PLAN_TYPE_THIRD);
        }
        return third;
    }

    private ImageView fourth;

    public ImageView getFourth() {
        if (fourth == null) {
            fourth = (ImageView) getView().findViewById(R.id.fourth);
            fourth.setOnTouchListener(this);
            fourth.setTag(PLAN_TYPE_FOURTH);
        }
        return fourth;
    }

    private ImageView fifth;

    public ImageView getFifth() {
        if (fifth == null) {
            fifth = (ImageView) getView().findViewById(R.id.fifth);
            fifth.setOnTouchListener(this);
            fifth.setTag(PLAN_TYPE_FIFTH);
        }
        return fifth;
    }

    private ImageView sixth;

    public ImageView getSixth() {
        if (sixth == null) {
            sixth = (ImageView) getView().findViewById(R.id.sixth);
            sixth.setOnTouchListener(this);
            sixth.setTag(PLAN_TYPE_SIXTH);
        }
        return sixth;
    }

    private ImageView seventh;

    public ImageView getSeventh() {
        if (seventh == null) {
            seventh = (ImageView) getView().findViewById(R.id.seventh);
            seventh.setOnTouchListener(this);
            seventh.setTag(PLAN_TYPE_SEVENTH);
        }
        return seventh;
    }

    private ImageView eight;

    public ImageView getEight() {
        if (eight == null) {
            eight = (ImageView) getView().findViewById(R.id.eight);
            eight.setOnTouchListener(this);
            eight.setTag(PLAN_TYPE_EIGHT);
        }
        return eight;
    }

    private ImageView ninth;

    public ImageView getNinth() {
        if (ninth == null) {
            ninth = (ImageView) getView().findViewById(R.id.ninth);
            ninth.setOnTouchListener(this);
            ninth.setTag(PLAN_TYPE_NINTH);
        }
        return ninth;
    }
    public Button button1;
    public Button getButton1(){
        if (button1 == null){
            button1 = (Button)getView().findViewById(R.id.button1);
            button1.setOnTouchListener(this);
        }
        return button1;
    }
    public Button btnDrName;
    public Button getDrName(){
        if (btnDrName == null){
            btnDrName = (Button)getView().findViewById(R.id.btnDrName);
            btnDrName.setOnTouchListener(this);
        }
        return btnDrName;
    }

    public ImageView btnReferal;
    public ImageView getReference(){
        if (btnReferal == null){
            btnReferal = (ImageView)getView().findViewById(R.id.btnRefer);
            btnReferal.setOnTouchListener(this);
        }
        return btnReferal;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main_screen, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        first = getFirst();
        second = getSecond();
        third = getThird();
        fourth = getFourth();
        fifth = getFifth();
        sixth = getSixth();
        seventh = getSeventh();
        eight = getEight();
        ninth = getNinth();
        button1 = getButton1();
//        button2 = getButton2();
        //startActivity(new Intent(getActivity(), PieChartActivity.class));
//        String s="Debug-infos:";
//        s += "\n OS Version: " + System.getProperty("os.version") + "(" + android.os.Build.VERSION.INCREMENTAL + ")";
//        s += "\n OS API Level: "+android.os.Build.VERSION.RELEASE + "("+android.os.Build.VERSION.SDK_INT+")";
//        s += "\n Device: " + android.os.Build.DEVICE;
//        s += "\n Model (and Product): " + android.os.Build.MODEL + " ("+ Build.MANUFACTURER+ ")";
//        Log.d("DEVICEID", s);

        btnDrName = getDrName();
        btnReferal = getReference();

        String txtdoctorName = PreferenceManager.getStringForKey(getActivity(),PreferenceManager.DOCTORNAME,"");
        if(txtdoctorName!=null && txtdoctorName.length()>0) {
            Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "OpenSans-Bold.ttf");//OpenSans-Bold.ttf  OpenSans-Regular.ttf
            btnDrName.setText("Dr. "+txtdoctorName);
            btnDrName.setTypeface(tf);
            btnReferal.setVisibility(View.VISIBLE);
            btnDrName.setVisibility(View.VISIBLE);
            button1.setVisibility(View.GONE);

        }else{
            btnDrName.setVisibility(View.GONE);
            btnReferal.setVisibility(View.GONE);
            button1.setVisibility(View.VISIBLE);
        }
        if (PreferenceManager.getIntForKey(getActivity(), PreferenceManager.IS_FIRST_TIME, 0) == 0){
            startActivity(new Intent(getActivity(), WalkthroughActivity.class));
            PreferenceManager.saveIntForKey(getActivity(), PreferenceManager.IS_FIRST_TIME, 1);
        }
        /*
        we need to add a scheduler which recreates all the future reminders at midnight
         */
        if (PreferenceManager.getIntForKey(getActivity(), PreferenceManager.MIDNIGHT_RCREATER, 0)== 0) {
            Calendar midNightCal = AppConstants.getCurrentCalendar();
            midNightCal.set(Calendar.HOUR_OF_DAY, 23);
            midNightCal.set(Calendar.MINUTE, 59);
            AppConstants.setScheduleMidNightNotification(getActivity().getApplicationContext(), midNightCal);
            PreferenceManager.getIntForKey(getActivity(), PreferenceManager.MIDNIGHT_RCREATER, 1);
        }
        //button3 = getButton3();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main_screen, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()){
            case R.id.action_logOut:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                // Setting Dialog Title
                alertDialog.setTitle("Confirm Logout");
                // Setting Dialog Message
                alertDialog.setMessage("Are you sure!");
                alertDialog.setCancelable(false);
                // Setting Positive "OK" Btn
                alertDialog.setNegativeButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to execute after dialog
                                PreferenceManager.remove(getActivity(), PreferenceManager.ID);
                                PreferenceManager.remove(getActivity(), PreferenceManager.SESSION_ID);
                                PreferenceManager.remove(getActivity(), PreferenceManager.ADDRESS1);
                                PreferenceManager.remove(getActivity(), PreferenceManager.ADDRESS2);
                                PreferenceManager.remove(getActivity(), PreferenceManager.CELL_NUMBER);
                                PreferenceManager.remove(getActivity(), PreferenceManager.CITY);
                                PreferenceManager.remove(getActivity(), PreferenceManager.CLIENT_ID);
                                PreferenceManager.remove(getActivity(), PreferenceManager.COUNTRY);
                                PreferenceManager.remove(getActivity(), PreferenceManager.CREATED_BY);
                                PreferenceManager.remove(getActivity(), PreferenceManager.EMAIL);
                                PreferenceManager.remove(getActivity(), PreferenceManager.VERIFICATION_FLAG);
                                PreferenceManager.remove(getActivity(), PreferenceManager.VERIFICATION_CODE);
                                PreferenceManager.remove(getActivity(), PreferenceManager.USER_NAME);
                                PreferenceManager.remove(getActivity(), PreferenceManager.EMAIL_ADDRESS);
                                PreferenceManager.remove(getActivity(), PreferenceManager.GENDER);
                                UseCases.startActivityByClearingStack(getActivity(), LoginActivity.class);
                                dialog.cancel();
                            }
                        });
                // Setting Positive "OK" Btn
                alertDialog.setPositiveButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to execute after dialog
                                dialog.cancel();
                            }
                        });
                // Showing Alert Dialog
                alertDialog.show();
                return true;
        }
        return false;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(v, null);
            return true;
        }
        if (event.getActionMasked() == MotionEvent.ACTION_UP) {
            ViewHelper.fadeIn(v, null);
            switch (v.getId()){
                case R.id.first:
                    startActivity(new Intent(getActivity(), MedicineSearchActivity.class));
                    break;
                case R.id.second:
                    startActivity(new Intent(getActivity(), CalendarActivity.class));
                    break;
                case R.id.third:
                    startActivity(new Intent(getActivity(), UploadPrescriptionActivity.class));
                    break;
                case R.id.fifth:
                    startActivity(new Intent(getActivity(), EhrActivity.class));
                    break;
                case R.id.seventh:
                    //startActivity(new Intent(getActivity(), PieChartActivity.class));
                    break;
                case R.id.ninth:
                    break;
                case R.id.fourth:
                    startActivity(new Intent(getActivity(), PieChartActivity.class));
                    break;
                case R.id.sixth:
                    startActivity(new Intent(getActivity(), HelpActivity.class));
                    break;
                case R.id.button1:
                    startActivity(new Intent(getActivity(), AboutUsActivity.class));
                    break;
                case R.id.btnRefer:
                    ReferalDialog.newInstance("Friend Details", "").show(getFragmentManager(), TAG);
                    break;
            }
            return true;
        }
        return false;
    }
}
