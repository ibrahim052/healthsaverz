package com.healthsaverz.healthmobile.medicine.medfriend.modal;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.healthsaverz.healthmobile.medicine.asyncloader.AsyncLoaderNew;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.PreferenceManager;

/**
 * Created by priyankranka on 08/11/14.
 */
public class UpdateMedFriendParser extends AsyncTask <String,Void,Object> implements AsyncLoaderNew.Listener {
     /*
    Start from loginUser method
    Takes String as input and out is based on VerifyUserParserListener
     */

    private Context context;

    public UpdateMedFriendParser(Context context){

        this.context = context;
    }

    public interface UpdateFriendParserListener{
        void updateFriendParserDidReceiveData(Object message); //message as 100 for success
    }

    private UpdateFriendParserListener updateFriendParserListener;

    public void setMedFriendParserListener(UpdateFriendParserListener updateFriendParserListener) {
        this.updateFriendParserListener = updateFriendParserListener;
    }

    private static final String METHOD_ADD_MED_FRIEND = AppConstants.WEB_DOMAIN + "/sHealthSaverz/jaxrs/MedFriendServices/updateMedFriend";

    //local method
    public void updateMedFriend(MedFriend medFriend){
        AsyncLoaderNew asyncLoaderNew = new AsyncLoaderNew(AsyncLoaderNew.RequestMethod.POST_HEADER);
        String clientID = PreferenceManager.getStringForKey(context, PreferenceManager.CLIENT_ID, "healthsaverz");
        String sessionID = PreferenceManager.getStringForKey(context, PreferenceManager.SESSION_ID, "-1");
        String userID = PreferenceManager.getStringForKey(context, PreferenceManager.ID, "-1");
        String requestXML = "<medFriend>" +
                "<clientID>"+ clientID +"</clientID>" +
                "<sessionID>"+ sessionID +"</sessionID>" +
                "<med_friend_id>"+medFriend._medFriendID+"</med_friend_id>"+
                "<user_id>"+ userID+"</user_id>" +
                "<med_friend_name>"+ medFriend._friendName+"</med_friend_name>" +
                "<med_friend_email>"+ medFriend._friendEmail+"</med_friend_email>" +
                "<med_friend_mobile>"+medFriend._friendMobile +"</med_friend_mobile>" +
                "<med_miss_count>"+medFriend._missCount+"</med_miss_count>" +
                "<med_skip_count>"+medFriend._skipCount+"</med_skip_count>"+
                "<extra_1/>" +
                "<extra_2/>" +
                "<extra_3/>" +
                "<extra_4/>" +
                "<extra_5/>"+
                "</medFriend>";

        Log.d("requestXML ","requestXML "+requestXML);

        asyncLoaderNew.requestMessage   =   requestXML;
        asyncLoaderNew.setListener(this);
        asyncLoaderNew.executeRequest(METHOD_ADD_MED_FRIEND);
    }


    @Override
    protected Object doInBackground(String... params) {
//        if(!isCancelled())
//        {
//            try {
//            /*
//            {"sessionID":"","clientID":"","message":"100","gender":"Male",
//            "deviceUdid":"353743054004708","deviceToken":"DEVICE_TOKEN","osType":"A",
//            "verificationCode":"7031","verificationFlag":"","fileDetailList":null,"state":"",
//            "country":"","id":361,"password":"","userName":"priyank@nimapinfotech.com","status":"I",
//            "firstName":"","lastName":"","address1":"","address2":"","postalCode":"","emailAddress":"priyank@nimapinfotech.com",
//            "subUserId":0,"userGroupId":3,"phoneNumber":"","cellNumber":"+919869357889","created_By":0,"city":""}
//             */
//                JSONObject jsonObject = new JSONObject(params[0]);
//                String k = jsonObject.getString("user_id");
//                MedFriend medFriend;
//                if(jsonObject.getString("message").equals("100") || jsonObject.getString("message").equals("you are activated valid user")){
//                    // indicates success in registering the user. We need to add to persistent storage so that it can be used later on when ever needed.
//                    medFriend = new MedFriend();
//                    medFriend.sessionID = jsonObject.getString("sessionID");
//                    medFriend.clientID = jsonObject.getString("clientID");
//                    medFriend._medFriendID = jsonObject.getInt("med_friend_id");
//                    medFriend._userID = jsonObject.getInt("user_id");
//                    medFriend._friendName = jsonObject.getString("med_friend_name");
//                    medFriend._friendEmail = jsonObject.getString("med_friend_email");
//                    medFriend._friendMobile = jsonObject.getString("med_friend_mobile");
//                    medFriend._missCount = jsonObject.getInt("med_miss_count");
//                    medFriend._skipCount = jsonObject.getInt("med_skip_count");
//                    medFriend.extra_1 = jsonObject.getString("extra_1");
//                    medFriend.extra_2 = jsonObject.getString("extra_2");
//                    medFriend.extra_3 = jsonObject.getString("extra_3");
//                    medFriend.extra_4 = jsonObject.getString("extra_4");
//                    medFriend.extra_5 = jsonObject.getString("extra_5");
//
//                    return medFriend;
//                }
//                else
//                {
//                    return null;
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//        else {
//            cancel(true);
//        }
        return null;
    }

    @Override
    protected void onPostExecute(Object s) {
        super.onPostExecute(s);

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();

        if (this.updateFriendParserListener != null) {
            this.updateFriendParserListener.updateFriendParserDidReceiveData("Process Abrupt");
        }
    }


    //AsyncLoaderNew.Listener method implementation
    @Override
    public void didReceivedError(AsyncLoaderNew.Status status) {
        if(updateFriendParserListener != null){
            updateFriendParserListener.updateFriendParserDidReceiveData(status);
        }
    }

    @Override
    public void didReceivedData(byte[] data) {
        String response = new String(data);
        Log.d("UpdateFriendParser", "didReceivedData is " + response);
        if(this.updateFriendParserListener != null){
            switch (response) {
                case "100":
                    updateFriendParserListener.updateFriendParserDidReceiveData(response);
                    break;
                case "101":
                    updateFriendParserListener.updateFriendParserDidReceiveData("Error occurred while updating MedFriend");
                    break;
                default:
                    updateFriendParserListener.updateFriendParserDidReceiveData("Connection Error");
                    break;
            }
        }
        //execute(responseString);
    }

    @Override
    public void didReceivedProgress(Double progress) {

    }
}
