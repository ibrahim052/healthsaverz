package com.healthsaverz.healthmobile.medicine.reminder.modal;

import java.util.Calendar;

/**
 * Created by Ibrahim on 16-10-2014.
 */
public class ReminderTime implements Comparable<ReminderTime> {

    private Calendar calendar;
    private int hours;
    private int minutes;
    private String quantity;

    public ReminderTime(Calendar calendar, String quantity, Integer currentHour, Integer currentMinute) {
        this.calendar = calendar;
        this.quantity = quantity;
        this.hours = currentHour;
        this.minutes = currentMinute;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public String getQuantity() {
        return quantity;
    }

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

    @Override
    public int compareTo(ReminderTime reminderTime) {
        return (this.hours *60) + this.minutes <
                (reminderTime.getHours()*60)+reminderTime.getMinutes() ? -1 : 1;
    }
}

