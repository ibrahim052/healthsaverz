package com.healthsaverz.healthmobile.medicine.reminder.controller;

import android.app.Activity;
import android.os.Bundle;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.global.App;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class ReminderActivity extends Activity {

   public static boolean inEditMode;
    int med_id = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);
        getActionBar().setDisplayHomeAsUpEnabled(true);
//        String medicineName = getIntent().getStringExtra("brand");
//        String dose = getIntent().getStringExtra("strength");
//        if (medicineName != null && dose != null){
//            getFragmentManager().beginTransaction()
//                    .add(R.id.containerReminder, ReminderFragment.newInstance(medicineName, dose), ReminderFragment.TAG).commit();
//        }
//        else {
//            getFragmentManager().beginTransaction()
//                    .add(R.id.containerReminder, ReminderFragment.newInstance(), ReminderFragment.TAG).commit();
//        }
        if (inEditMode){
            int med_id = getIntent().getIntExtra("id", -1);
            if (med_id != -1){
                getFragmentManager().beginTransaction()
                        .add(R.id.containerReminder, ReminderFragment.newInstance(med_id), ReminderFragment.TAG).commit();
                inEditMode = false;
            }
        }
        else {
            Long lo = getIntent().getLongExtra("CurrCal", 0);
            Calendar calendar = new GregorianCalendar() ;
            calendar.setTimeInMillis(lo);
            getFragmentManager().beginTransaction()
                    .add(R.id.containerReminder, ReminderFragment.newInstance(calendar), ReminderFragment.TAG).commit();
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        App app = (App) getApplicationContext();
        app.isMed = false;
    }
}

