package com.healthsaverz.healthmobile.medicine.global;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.crittercism.app.Crittercism;

/**
 * Created by montydudani on 01/09/14.
 */
public class App extends Application implements Application.ActivityLifecycleCallbacks {

    public boolean isMed;
    public boolean isFirstTime = true;
    public String medName;
    public String strengthName;
    public String mCurrentPhotoPath = "";

    @Override
    public void onCreate() {
        super.onCreate();
        Crittercism.initialize(getApplicationContext(), AppConstants.CrittercismAppID);
    }

    //ActivityLifecycleCallbacks
    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }


    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

}
