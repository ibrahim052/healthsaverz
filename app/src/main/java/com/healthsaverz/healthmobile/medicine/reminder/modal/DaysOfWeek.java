package com.healthsaverz.healthmobile.medicine.reminder.modal;

/**
 * Created by Ibrahim on 17-10-2014.
 */
public class DaysOfWeek {
    public String day;
    public boolean ischecked;

    public DaysOfWeek(String day, boolean ischecked) {
        this.day = day;
        this.ischecked = ischecked;
    }

    public String getDay() {
        return day;
    }

    public boolean isIschecked() {
        return ischecked;
    }
}
