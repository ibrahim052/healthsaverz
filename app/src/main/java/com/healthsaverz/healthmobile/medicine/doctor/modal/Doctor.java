package com.healthsaverz.healthmobile.medicine.doctor.modal;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by healthsaverz5 on 3/20/2015.
 */
public class Doctor {

    public static final String SessionID = "sessionID";
    public static final String ClientID = "clientID";
    public static final String Message = "message";
    public String extra_3 = "";
    public String extra_4 = "";
    public String extra_5 = "";
    public int created_By  = 0;
    public int id ;
    public int userGroupId =0;
    public static final String UserName = "userName";
    public static final String  Password = "password";
    public static final String  FirstName ="firstName";
    public static final String LastName = "lastName";
    public static final String Address1 =  "address1";
    public static final String Address2 = "address2";
    public static final String City =   "city";
    public static final String State = "state";
    public static final String PostalCode = "postalCode";
    public static final String Country =  "country";
    public static final String PhoneNumber =   "phoneNumber";
    public static final String EmailAddress =   "emailAddress";
    public static  String CellNumber =  "cellNumber";
    public static final String  Status =  "status";
    public static final String Age =  "age";
    public static final String GroupName =  "groupName";
    public static final String FullAddress = "fullAddress";
    public static final String UniqueNumber = "uniqueNumber";
    public static final String VoucherFlag = "vochuerFlag";
    public static final String  DeviceUDID =  "deviceUDID";
    public static final String OsVersion = "osVersion";
    public static final String DeviceName =  "deviceName";
    public static  String DoctorId = "doctorId";
    public static  String DoctorName = "doctorName";
    public static final String ActualFullName = "actualFullName";
    public static final String SubUserId = "subUserId";
    public static final String Gender = "gender";
    public static final String DeviceUdid  =  "deviceUdid";
    public static final String DeviceToken =  "deviceToken";
    public static final String OsType = "osType";
    public static final String Verification =   "verificationCode";
    public static final String VerificationFlag =   "verificationFlag";
    public static final List<String> fileDetailList = new ArrayList<String>();

    public String doctorId,doctorName,cellNumber,message,fullAddress ;
    public Doctor(Context context, String doctorId, String doctorName, String cellNumber,String fullAddress,String message) {

        this.doctorId = doctorId;
        this.doctorName = doctorName;
        this.cellNumber = cellNumber;
        this.message =  message;
        this.fullAddress = fullAddress;
    }

    public Doctor() {

    }


}
