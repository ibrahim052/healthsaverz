package com.healthsaverz.healthmobile.medicine.medfriend.modal;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;

import java.util.List;

/**
 * Created by Ibrahim on 25-11-2014.
 */
public class FriendListAdapter extends BaseAdapter {
    Context context;
    private List<MedFriend> cells;
    LayoutInflater layoutInflater;
    View.OnClickListener selectFriendClick;
    public FriendListAdapter(Context context, List<MedFriend> objects, View.OnClickListener selectFriendClick) {
        this.context = context;
        this.cells = objects;
        this.selectFriendClick = selectFriendClick;
        this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return cells.size();
    }

    @Override
    public MedFriend getItem(int index) {
        return cells.get(index);
    }

    @Override
    public long getItemId(int index) {
        return index;
    }

    @Override
    public View getView(int i, View convertview, ViewGroup viewGroup) {
        if (convertview == null){

            convertview = layoutInflater.inflate(R.layout.cell_med_friend, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.friendCheck = (ImageView)convertview.findViewById(R.id.friendCheck);
            viewHolder.friendCheck.setOnClickListener(selectFriendClick);
            viewHolder.friendUnCheck = (ImageView)convertview.findViewById(R.id.friendUnCheck);
            viewHolder.friendUnCheck.setOnClickListener(selectFriendClick);
            viewHolder.nameOfFriend = (TextView)convertview.findViewById(R.id.nameOfFriend);
            viewHolder.email = (TextView)convertview.findViewById(R.id.email);
            viewHolder.phoneNumber = (TextView)convertview.findViewById(R.id.phoneNumber);
            convertview.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder)convertview.getTag();
        final MedFriend cell = cells.get(i);


        holder.nameOfFriend.setText(cell._friendName);
        if (!cell._friendEmail.equals("")){
            holder.email.setText(cell._friendEmail);
            holder.email.setVisibility(View.VISIBLE);
        }
        else {
            holder.email.setVisibility(View.GONE);
        }
        holder.phoneNumber.setText(cell._friendMobile);
        if (cell.ischecked){
            holder.friendCheck.setVisibility(View.VISIBLE);
            holder.friendUnCheck.setVisibility(View.INVISIBLE);
        }
        else {
            holder.friendCheck.setVisibility(View.INVISIBLE);
            holder.friendUnCheck.setVisibility(View.VISIBLE);
        }
        return convertview;
    }
    public class ViewHolder {
        public ImageView friendCheck;
        public ImageView friendUnCheck;
        public TextView nameOfFriend;
        public TextView email;
        public TextView phoneNumber;
    }

}
