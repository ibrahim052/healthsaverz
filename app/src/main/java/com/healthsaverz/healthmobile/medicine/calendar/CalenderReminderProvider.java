package com.healthsaverz.healthmobile.medicine.calendar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.medicine.controller.MedicineDBHandler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

/**
 * Created by priyankranka on 23/10/14.
 */
public class CalenderReminderProvider {


    private Context context;
    private static final int START_INDEX = 31;

    private int[] mCards = {
            0,
            0,
            R.drawable.badge1,
            R.drawable.badge2,
            R.drawable.badge3,
            R.drawable.capsule,
            R.drawable.badge5,
            R.drawable.badge6,
            R.drawable.badge7,
            R.drawable.badge8,
            R.drawable.badge9,
            R.drawable.badge10,
            R.drawable.badge11,
            R.drawable.badge12,
            R.drawable.badge14,
            R.drawable.badge15,
            R.drawable.badge16,
            R.drawable.badge17,
            R.drawable.badge18,
            R.drawable.badge19,
            R.drawable.badge20,
            R.drawable.badge21,
            R.drawable.badge22,
            R.drawable.badge23,
            R.drawable.badge24,
            R.drawable.badge25,
            R.drawable.badge26,
            R.drawable.badge27,
            R.drawable.badge28,
            0,
            0};

    private int[] mColors = {
            0,
            0,
            Color.parseColor("#3cbaff"),
            Color.parseColor("#2797d0"),
            Color.parseColor("#005c94"),
            Color.parseColor("#d4ff5f"),
            Color.parseColor("#a9e327"),
            Color.parseColor("#27a227"),
            Color.parseColor("#ffff5c"),
            Color.parseColor("#ff9900"),
            Color.parseColor("#ff7d27"),
            Color.parseColor("#efd597"),
            Color.parseColor("#dba747"),
            Color.parseColor("#936827"),
            Color.parseColor("#ff4141"),
            Color.parseColor("#e12727"),
            Color.parseColor("#c02727"),
            Color.parseColor("#ffae8e"),
            Color.parseColor("#ffa2b2"),
            Color.parseColor("#e9967a"),
            Color.parseColor("#fb9388"),
            Color.parseColor("#f3d2ff"),
            Color.parseColor("#dd82ff"),
            Color.parseColor("#c527ff"),
            Color.parseColor("#2d2d2d"),
            Color.parseColor("#7d7d7d"),
            Color.parseColor("#a9a9a9"),
            Color.parseColor("#d4d4d4"),
            Color.parseColor("#ffffff"),
            Color.parseColor("#000000"),
            0,0};

    public CalenderReminderProvider(Context context) {

        this.context = context;

    }

    //This method will query the DB and sort the medicines and create the single array with the header model inserted.
    ArrayList<CalenderReminder> getReminders(int dayIndex) {

        /*
        Actual Algorithm for extracting the data from the DB and inflating the array

        1. Input is dayIndex which is the viewPager current Index.
        2. With this index we will come to exact day of the calender the way we get the Textview over the listview
        3. From the date we get. We will find the list of medicine ids from DB where the given date is between the startdate and the enddate
        4. We will also extract name,dose,instruction, shape,color1,color2 from the medicine table.
        5. We will put these all data in the CalenderReminder model object
        6. With medicine id we found we will get the time and quantity from the reminder table only those medicines which apply to date
           we got in step 2 and in sorted order of time
        7. We put the object into temperary Array
        8. Now we need to process this temp array into final array where all the objects needs to be sorted as per the timing of intake
           Morning (6:01 AM to 12:00), Afternoon (12:01 to 16:00), Evening (16:01 to 20:00) and night (20:01 to 24:00)
        9. There will be outer for loop which will run only 4 times First object to be inserted will be header and depending upon
              the index we will put Morning, Evening etc.
        10. After placing the header object in the final array we will process the temp array and extract the objects which are in the
              time range of morning for i=0, afternoon for i=1 and so on. After object has been extracted it is put into the final array
        11. After end of for loop we need to send the final array back to the fragment to display data.
        */

        ArrayList<CalenderReminder> tempcalenderReminders = new ArrayList<>();

        MedicineDBHandler dbHandler = new MedicineDBHandler(context);

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        calendar.add(Calendar.DAY_OF_YEAR, dayIndex-START_INDEX);


        tempcalenderReminders.addAll(dbHandler.findMedicinesWithDate(dayIndex));//ArrayList<Medicine> medicines = dbHandler.findMedicinesWithDate(calendar.getTimeInMillis()+"");


        CalenderReminder calenderReminder;

        ArrayList<CalenderReminder> calenderReminders = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            //First add the header in the list, So automatically sections will be added for you.
            if (i == 0) {

                calenderReminder = new CalenderReminder();
                calenderReminder.headerTitle = "MORNING";
                calenderReminder.headerIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.morning);
                calenderReminder.isHeader = true;

                calenderReminders.add(calenderReminder);

            } else if (i == 1) {

                calenderReminder = new CalenderReminder();
                calenderReminder.headerTitle = "AFTERNOON";
                calenderReminder.headerIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.afternoon);
                calenderReminder.isHeader = true;

                calenderReminders.add(calenderReminder);

            } else if (i == 2) {

                calenderReminder = new CalenderReminder();
                calenderReminder.headerTitle = "EVENING";
                calenderReminder.headerIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.evening);
                calenderReminder.isHeader = true;

                calenderReminders.add(calenderReminder);

            } else if (i == 3) {

                calenderReminder = new CalenderReminder();
                calenderReminder.headerTitle = "NIGHT";
                calenderReminder.headerIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.night);
                calenderReminder.isHeader = true;

                calenderReminders.add(calenderReminder);

            }

            //First you need to extract all the data which will fall in the respective sections. Once
            // you get the subset we need to sort the subset and finally put the sorted array into the final array

            ArrayList<CalenderReminder> subSetArrayList = new ArrayList<>();

            String pattern = "HH:mm";
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);

            for (int j = 0; j < tempcalenderReminders.size(); j++) {

                calenderReminder = tempcalenderReminders.get(j);


                if (i == 0) { //We need to get the only those reminders which fall under the morning slots

                    try {
                        Date startTime = sdf.parse("05:01");
                        Date endTime = sdf.parse("12:00");
                        Date reminderTime = sdf.parse(calenderReminder.medicineTime);

                        if ((reminderTime.compareTo(startTime) == 1 || reminderTime.compareTo(startTime) == 0) &&
                                (reminderTime.compareTo(endTime) == -1) || reminderTime.compareTo(endTime) == 0) {

                            if(calenderReminder.medicineShape == 5)//capsule with  two layers
                            {
                                calenderReminder.headerIcon = mergeBitmap(calenderReminder.medicineColor1,calenderReminder.medicineColor2);
                            }
                            else if(calenderReminder.medicineShape == -1)//default shape
                            {
                                calenderReminder.headerIcon = BitmapFactory.decodeResource(context.getResources(), mCards[2]);
                            }
                            else//other shapes
                            {
                                calenderReminder.headerIcon = BitmapFactory.decodeResource(context.getResources(), mCards[calenderReminder.medicineShape]);
                            }
                            subSetArrayList.add(calenderReminder);
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (i == 1) { //We need to get the only those reminders which fall under the morning slots

                    try {
                        Date startTime = sdf.parse("12:01");
                        Date endTime = sdf.parse("16:00");
                        Date reminderTime = sdf.parse(calenderReminder.medicineTime);

                        if ((reminderTime.compareTo(startTime) == 1 || reminderTime.compareTo(startTime) == 0) &&
                                (reminderTime.compareTo(endTime) == -1) || reminderTime.compareTo(endTime) == 0) {

                            if(calenderReminder.medicineShape == 5)//capsule with  two layers
                            {
                                calenderReminder.headerIcon = mergeBitmap(calenderReminder.medicineColor1,calenderReminder.medicineColor2);
                            }
                            else if(calenderReminder.medicineShape == -1)//default shape
                            {
                                calenderReminder.headerIcon = BitmapFactory.decodeResource(context.getResources(), mCards[2]);
                            }
                            else//other shapes
                            {
                                calenderReminder.headerIcon = BitmapFactory.decodeResource(context.getResources(), mCards[calenderReminder.medicineShape]);
                            }
                            subSetArrayList.add(calenderReminder);
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (i == 2) { //We need to get the only those reminders which fall under the morning slots

                    try {
                        Date startTime = sdf.parse("16:01");
                        Date endTime = sdf.parse("20:00");
                        Date reminderTime = sdf.parse(calenderReminder.medicineTime);

                        if ((reminderTime.compareTo(startTime) == 1 || reminderTime.compareTo(startTime) == 0) &&
                                (reminderTime.compareTo(endTime) == -1) || reminderTime.compareTo(endTime) == 0) {
                            if(calenderReminder.medicineShape == 5)//capsule with  two layers
                            {
                                calenderReminder.headerIcon = mergeBitmap(calenderReminder.medicineColor1,calenderReminder.medicineColor2);
                            }
                            else if(calenderReminder.medicineShape == -1)//default shape
                            {
                                calenderReminder.headerIcon = BitmapFactory.decodeResource(context.getResources(), mCards[2]);
                            }
                            else//other shapes
                            {
                                calenderReminder.headerIcon = BitmapFactory.decodeResource(context.getResources(), mCards[calenderReminder.medicineShape]);
                            }

                            subSetArrayList.add(calenderReminder);
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (i == 3) { //We need to get the only those reminders which fall under the morning slots

                    try {
                        Date startTime = sdf.parse("20:01");
                        Date endTime = sdf.parse("24:00");
                        Date reminderTime = sdf.parse(calenderReminder.medicineTime);

                        if ((reminderTime.compareTo(startTime) == 1 || reminderTime.compareTo(startTime) == 0) &&
                                (reminderTime.compareTo(endTime) == -1) || reminderTime.compareTo(endTime) == 0) {

                            if(calenderReminder.medicineShape == 5)//capsule with  two layers
                            {
                                calenderReminder.headerIcon = mergeBitmap(calenderReminder.medicineColor1,calenderReminder.medicineColor2);
                            }
                            else if(calenderReminder.medicineShape == -1)//default shape
                            {
                                calenderReminder.headerIcon = BitmapFactory.decodeResource(context.getResources(),mCards[2]);
                            }
                            else//other shapes
                            {
                                calenderReminder.headerIcon = BitmapFactory.decodeResource(context.getResources(), mCards[calenderReminder.medicineShape]);
                            }
                            subSetArrayList.add(calenderReminder);
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }


            }

            //Once you get the subset lets sort the array using bubble sort

            int length = subSetArrayList.size();

            if (length == 0) {// if there is no medicine schedule we need to add one with no medicine schedule. else we will sort and add to final array
                calenderReminder = new CalenderReminder();
                calenderReminder.isHeader = false;
                calenderReminder.medicineID = -1;
                calenderReminder.medicineName = context.getResources().getString(R.string.no_medicine_text);
                calenderReminder.medicineDose = "0";
                calenderReminder.medicineShape = -1;
                calenderReminder.medicineColor1 = -1;
                calenderReminder.medicineColor2 = -1;
                calenderReminder.medicineScheduleDose = "0";
                calenderReminder.medicineTime = "0";
                calenderReminder.medicineStatus = 0;

                subSetArrayList.add(calenderReminder);
            } else {
                int m;
                for (int k = length; k > 0; k--) {

                    for (int l = 0; l < length - 1; l++) {

                        m = l + 1;

                        CalenderReminder calenderReminder1 = subSetArrayList.get(l);
                        CalenderReminder calenderReminder2 = subSetArrayList.get(m);

                        try {
                            Date startTime = sdf.parse(calenderReminder1.medicineTime);
                            Date endTime = sdf.parse(calenderReminder2.medicineTime);

                            if (endTime.compareTo(startTime) == -1) {

                                subSetArrayList.set(l, calenderReminder2);
                                subSetArrayList.set(m, calenderReminder1);
                            }

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }

                }


            }


            //add the sorted array into the final array

            for (int ii = 0; ii < subSetArrayList.size(); ii++) {

                CalenderReminder calenderReminder1 = subSetArrayList.get(ii);
                calenderReminders.add(calenderReminder1);
            }
        }


        return calenderReminders;

    }

    ArrayList<CalenderReminder> getHeaders(ArrayList<CalenderReminder> medicines) {

        ArrayList<CalenderReminder> headers = new ArrayList<>();

        for (int i = 0; i < medicines.size(); i++) {
            CalenderReminder calenderReminder = medicines.get(i);

            if (calenderReminder.isHeader) {
                headers.add(calenderReminder);
            }
        }

        return headers;
    }

    HashMap<String, ArrayList<CalenderReminder>> getMedicines(ArrayList<CalenderReminder> fullMedicines) {

        HashMap<String, ArrayList<CalenderReminder>> medicines = new HashMap<>();

        ArrayList<CalenderReminder> list = null;
        int headerIndex = 0;

        for (int i = 0; i < fullMedicines.size(); i++) {

            CalenderReminder calenderReminder = fullMedicines.get(i);
            if (calenderReminder.isHeader) {

                if (headerIndex == 0) {

                    list = new ArrayList<>();
                } else if (headerIndex == 1) {

                    medicines.put("morning", list);
                    list = new ArrayList<>();
                } else if (headerIndex == 2) {

                    medicines.put("afternoon", list);
                    list = new ArrayList<>();
                } else if (headerIndex == 3) {

                    medicines.put("evening", list);
                    list = new ArrayList<>();
                }

                headerIndex++;

            } else {

                assert list != null;
                list.add(calenderReminder);
            }
        }

        medicines.put("night", list);

        return medicines;
    }

    public Bitmap mergeBitmap(int colo1,int colo2)
    {

        Bitmap firstBitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.badge3left);
        Bitmap secondBitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.badge3right);

        Bitmap comboBitmap;

        float width, height;

        width = firstBitmap.getWidth() + secondBitmap.getWidth();
        height = firstBitmap.getHeight();

        comboBitmap = Bitmap.createBitmap((int)(width/2), (int)height, Bitmap.Config.ARGB_4444);

        Paint firstPaint = new Paint();
        firstPaint.setColorFilter(new PorterDuffColorFilter(mColors[colo1], PorterDuff.Mode.MULTIPLY));

        Paint secondPaint = new Paint();
        secondPaint.setColorFilter(new PorterDuffColorFilter(mColors[colo2], PorterDuff.Mode.MULTIPLY));

        Canvas comboImage = new Canvas(comboBitmap);

        comboImage.drawBitmap(firstBitmap, 0f, 0f, firstPaint);
        comboImage.drawBitmap(secondBitmap,0f,0f, secondPaint);

        return comboBitmap;

    }
}
