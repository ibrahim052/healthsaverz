package com.healthsaverz.healthmobile.medicine.mainscreen.controller;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthsaverz.healthmobile.medicine.R;
import com.healthsaverz.healthmobile.medicine.calendar.CalendarActivity;
import com.healthsaverz.healthmobile.medicine.ehr.controller.EhrActivity;
import com.healthsaverz.healthmobile.medicine.global.AppConstants;
import com.healthsaverz.healthmobile.medicine.global.ViewHelper;
import com.healthsaverz.healthmobile.medicine.help.controller.HelpActivity;
import com.healthsaverz.healthmobile.medicine.medicineSearch.controller.MedicineSearchActivity;
import com.healthsaverz.healthmobile.medicine.pieChart.controller.PieChartActivity;
import com.healthsaverz.healthmobile.medicine.uploadPrescription.controller.UploadPrescriptionActivity;

public class AboutUsActivity extends Activity implements View.OnTouchListener {

    public static final String TAG = "AboutUs";
    private ImageView aboutUsFB;
    private ImageView aboutUsWeb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        ImageView aboutUsFB = (ImageView) findViewById(R.id.aboutUsFB);
        ImageView aboutUsWeb = (ImageView) findViewById(R.id.aboutUsWeb);
        TextView aboutUsWebText = (TextView) findViewById(R.id.aboutUsWebText);
        TextView aboutUsFBText = (TextView) findViewById(R.id.aboutUsFBText);
        aboutUsWebText.setOnTouchListener(this);
        aboutUsFBText.setOnTouchListener(this);
        aboutUsFB.setOnTouchListener(this);
        aboutUsWeb.setOnTouchListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_about_us, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            ViewHelper.fadeOut(v, null);
            return true;
        }
        if (event.getActionMasked() == MotionEvent.ACTION_UP) {
            ViewHelper.fadeIn(v, null);
            Intent fbIntent;
            Intent browserIntent;
            switch (v.getId()){
                case R.id.aboutUsFB:
                    fbIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.facebook));
                    startActivity(fbIntent);
                    break;
                case R.id.aboutUsWeb:
                    browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.website));
                    startActivity(browserIntent);
                    break;
                case R.id.aboutUsFBText:
                    fbIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.facebook));
                    startActivity(fbIntent);
                    break;
                case R.id.aboutUsWebText:
                    browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.website));
                    startActivity(browserIntent);
                    break;
            }
            return true;
        }
        return false;
    }
}
